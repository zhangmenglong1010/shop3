const {
    reponseWrapper
} = require('./utils')
// 云函数入口文件
const cloud = require('wx-server-sdk')
const TcbRouter = require('tcb-router')
cloud.init({
    env: cloud.DYNAMIC_CURRENT_ENV // 给定 DYNAMIC_CURRENT_ENV 常量：接下来的 API 调用都将请求到与该云函数当前所在环境相同的环境
})
const paramsValidatorMiddleware = require('./validator')
const MAX_LIMIT = 1000
// 云函数入口函数
exports.main = async (event) => {
    const app = new TcbRouter({
        event
    })
    const wxContext = cloud.getWXContext()
    const openid = event["morphy_userinfo"] && event["morphy_userinfo"].openid ? event["morphy_userinfo"]["openid"] : wxContext.OPENID
    const db = cloud.database()
    const _ = db.command
    const $ = db.command.aggregate
    /**
     * 创建表单
     */
    app.router('add', paramsValidatorMiddleware(event, {
        name: paramsValidatorMiddleware.isRequire().string(),
        tableName: paramsValidatorMiddleware.isRequire().string(),
        desc: paramsValidatorMiddleware.string(),
        formSetting: paramsValidatorMiddleware.isRequire().array()
    }), async (ctx, next) => {
        let {
            name,
            desc = "",
            tableName,
            formSetting
        } = event
        try {
            tableName = tableName.replace(/\s*/g, "");
            let map = {
                tableName,
                hidden: _.or(_.eq(false), _.exists(false))
            }
            const getResult = await db.collection('morphy_formSetting').where(map).get()

            if (getResult.data.length > 0) {
                ctx.body = reponseWrapper(-102, '已存在同名表单，请勿重复创建')
            } else {
                const data = {
                    name,
                    tableName,
                    desc,
                    formSetting,
                    createAt: Date.now(),
                    creator: openid,
                    hidden: false
                }
                const addResult = await db.collection('morphy_formSetting').add({
                    data
                })
                data._id = addResult._id
                if (addResult._id) {
                    ctx.body = reponseWrapper(0, '创建成功', data)
                } else {
                    ctx.body = reponseWrapper(-1101)
                }
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     * 删除表单
     */
    app.router('delete', paramsValidatorMiddleware(event, {
        _id: paramsValidatorMiddleware.isRequire().string(),
        force: paramsValidatorMiddleware.boolean() //是否强制删除
    }), async (ctx, next) => {
        const {
            _id,
            force = false
        } = event
        try {
            if (!force) {
                //假删除
                const deleteUpdateResult = await db.collection('morphy_formSetting').doc(_id).update({
                    data: {
                        hidden: true, //假删除状态
                        hiddenSys: {
                            morphy: {
                                time: Date.now(),
                                by: openid
                            }
                        } //删除时间
                    }
                })
                if (deleteUpdateResult.stats.updated === 1) {
                    ctx.body = reponseWrapper(0, '删除成功')
                } else {
                    ctx.body = reponseWrapper(-1101)
                }
            } else {
                // 真删除
                const deleteResult = await db.collection('morphy_formSetting').doc(_id).remove()
                if (deleteResult.stats.removed === 1) {
                    ctx.body = reponseWrapper(0, '删除成功')
                } else {
                    ctx.body = reponseWrapper(-1101)
                }
            }

        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     * 更新表单信息
     */
    app.router('update', paramsValidatorMiddleware(event, {
        _id: paramsValidatorMiddleware.isRequire().string(),
        name: paramsValidatorMiddleware.string(),
        desc: paramsValidatorMiddleware.string(),
        formSetting: paramsValidatorMiddleware.isRequire().array(),
        isShow: paramsValidatorMiddleware.boolean()
    }), async (ctx, next) => {
        const {
            _id,
            name,
            desc,
            formSetting,
        } = event
        let isShow = event.isShow ? event.isShow : false;
        try {
            const updateResult = await db.collection('morphy_formSetting').doc(_id).update({
                data: {
                    name,
                    desc,
                    formSetting,
                    isShow,
                    updateSys: {
                        morphy: {
                            time: Date.now(),
                            by: openid
                        }
                    }
                }
            })
            if (updateResult.stats.updated === 1) {
                ctx.body = reponseWrapper(0, '更新成功')
            } else {
                ctx.body = reponseWrapper(-1101)
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     * 获取表单列表
     */
    app.router('get', paramsValidatorMiddleware(event, {
        page: paramsValidatorMiddleware.number(),
        size: paramsValidatorMiddleware.number(),
        isShow: paramsValidatorMiddleware.boolean()
    }), async (ctx, next) => {
        const {
            page = 1, size = 10, isShow
        } = event
        const map = {}
        if (isShow) {
            map.isShow = true
        }
        map.hidden = _.or(_.eq(false), _.exists(false))
        try {
            const getResult = await db.collection('morphy_formSetting').where(map).orderBy('createAt', 'desc').skip((page - 1) * size).limit(size).get()
            const totalResult = await db.collection('morphy_formSetting').where(map).count()

            ctx.body = reponseWrapper(0, '获取成功', {
                list: getResult.data,
                total: totalResult.total
            })
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     * 获取单个表单详细信息
     */
    app.router('getDetail', paramsValidatorMiddleware(event, {
        tableName: paramsValidatorMiddleware.isRequire().string()
    }), async (ctx, next) => {
        const {
            tableName
        } = event
        try {
            const getResult = await db.collection('morphy_formSetting').where({
                tableName
            }).get()
            if (getResult.data !== undefined && getResult.data.length > 0) {
                ctx.body = reponseWrapper(0, '获取成功', getResult.data[0])
            } else {
                ctx.body = reponseWrapper(-1101, '不存在对应表单')
            }

        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     * 新增某个表单的数据
     */
    app.router('addFormData', paramsValidatorMiddleware(event, {
        formData: paramsValidatorMiddleware.isRequire().object(),
        tableName: paramsValidatorMiddleware.isRequire().string(),
    }), async (ctx, next) => {
        // 检查formData是否是合法的
        const {
            formData,
            tableName
        } = event
        const data = {
            formData,
            createAt: Date.now(),
            openid,
            hidden: false //是否隐藏
        }
        try {
            //查询tableName对应的表格
            const getFormInfo = await db.collection('morphy_formSetting').where({
                tableName
            }).get()
            console.log(getFormInfo, "getFormInfo")
            if (getFormInfo.data !== undefined && getFormInfo.data.length > 0) {
                data.formSettingId = getFormInfo.data[0]._id
                const addResult = await db.collection('morphy_formData').add({
                    data
                })
                // 检查填写字段
                if (addResult._id) { // 添加成功
                    data._id = addResult._id
                    ctx.body = reponseWrapper(0, '添加成功', data)
                } else {
                    ctx.body = reponseWrapper(-1101)
                }
            } else {
                ctx.body = reponseWrapper(-1102, "没有找到对应的表单信息，记录创建失败")
            }


        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }

    })
    /**
     * 获取某个表单每页数据
     */
    app.router('getFormData', paramsValidatorMiddleware(event, {
        tableName: paramsValidatorMiddleware.isRequire().string(),
        page: paramsValidatorMiddleware.number(),
        size: paramsValidatorMiddleware.number(),
        order: paramsValidatorMiddleware.enums('desc', 'asc')
    }), async (ctx, next) => {
        const {
            tableName, //表单英文名
            page = 1, //当前页
            size = 10, //每页长度
            search = {}, //搜索条件
            order = "desc" //顺序
        } = event
        try {
            const getFormInfoResult = await db.collection('morphy_formSetting').where({
                tableName
            }).get()
            if (getFormInfoResult.data !== undefined && getFormInfoResult.data.length > 0) {
                const formSettingId = getFormInfoResult.data[0]._id
                let map = {
                    formSettingId,
                    hidden: false
                }
                if (search.duplicateColumn !== undefined) {
                    const field = `formData.${search.duplicateColumn}`
                    const result = await db.collection('morphy_formData').aggregate().match(map).group({
                        _id: `\$${field}`,
                        num: $.sum(1)
                    }).match({
                        num: $.gt(1)
                    }).end()

                    if (result.list.length > 0) {
                        const repeatValue = result.list.map(item => item._id)
                        map[`${field}`] = _.in(repeatValue)
                        const countResult = await db.collection('morphy_formData').where(map).count()
                        let getResultData = []
                        if (countResult.total > MAX_LIMIT) {
                            const batchTimes = Math.ceil(countResult.total / MAX_LIMIT)
                            const tasks = []
                            for (let i = 0; i < batchTimes; i++) {
                                const promise = db.collection('morphy_formData').where(map).skip(i * MAX_LIMIT).limit(MAX_LIMIT).get()
                                tasks.push(promise)
                            }
                            (await Promise.all(tasks)).forEach(item => {
                                getResultData = getResultData.concat(item.data)
                            })
                        } else {
                            const getResult = await db.collection('morphy_formData').where(map).get()
                            getResultData = getResult.data
                        }
                        const duplicateColumnMap = {}
                        let i = 0
                        const len = getResultData.length
                        for (; i < len; i++) {
                            if (!duplicateColumnMap[getResultData[i]['formData'][`${search.duplicateColumn}`]]) {
                                duplicateColumnMap[getResultData[i]['formData'][`${search.duplicateColumn}`]] = []
                            }
                            duplicateColumnMap[getResultData[i]['formData'][`${search.duplicateColumn}`]].push(getResultData[i])
                        }
                        let newDataList = []
                        for (let key in duplicateColumnMap) {
                            newDataList = newDataList.concat(duplicateColumnMap[key])
                        }
                        ctx.body = reponseWrapper(0, '获取成功', {
                            list: newDataList,
                            total: getResultData.length
                        })
                    } else {
                        ctx.body = reponseWrapper(0, '获取成功', {
                            list: [],
                            total: 0
                        })
                    }
                } else {
                    if (Object.keys(search).length !== 0) {
                        map["formData"] = {};
                        if (search.column && search.keyword) {
                            map["formData"][`${search.column}`] = new db.RegExp({
                                regexp: search.keyword,
                                options: 'i'
                            })
                        }
                        if (search.statusType) {
                            if (search.statusTypeValue === undefined) {
                                map["formData"][`${search.statusType}`] = _.or(_.eq(0), _.eq(-1), _.eq(true), _.eq(false), _.eq(1), _.exists(false), _.eq(null))
                            } else {
                                if (search.statusTypeValue === 0) {
                                    //查找=0或不存在这个字段
                                    console.log("等于0或者不存在")
                                    map["formData"][`${search.statusType}`] = _.or(_.eq(0), _.exists(false))
                                } else if (search.statusTypeValue === false) {
                                    //查找是false或者不存在这个字段的
                                    console.log("等于false或者不存在")
                                    map["formData"][`${search.statusType}`] = _.not(_.eq(true))
                                } else {
                                    map["formData"][`${search.statusType}`] = search.statusTypeValue
                                }
                            }
                        }
                        if (search.optionType) {
                            if (search.optionTypeValue !== undefined) {
                                let {
                                    value = "0", type = "normal"
                                } = search.optionTypeValue;
                                //查询选项数组中，value值，这里需要考虑value是数字还是字符串，兼容弱类型（避免前端传错了等历史问题）
                                map["formData"][`${search.optionType}`] = _.elemMatch({
                                    value: _.in([parseInt(value), value + ""])
                                })
                            }
                        }
                    }
                    const getResult = await db.collection('morphy_formData').where(map).orderBy('createAt', order).skip((page - 1) * size).limit(size).get()
                    const totalResult = await db.collection('morphy_formData').where(map).count()

                    ctx.body = reponseWrapper(0, '获取成功', {
                        list: getResult.data,
                        total: totalResult.total
                    })
                }

            } else {
                ctx.body = reponseWrapper(-1102, "找不到表单信息，请重试")
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     * 获取某个表单的所有数据(敏感，导出用)
     */
    app.router('getAllFormData', paramsValidatorMiddleware(event, {
        tableName: paramsValidatorMiddleware.isRequire().string()
    }), async (ctx, next) => {
        const {
            tableName, //表单英文名
        } = event
        try {
            const getFormInfoResult = await db.collection('morphy_formSetting').where({
                tableName
            }).get()
            console.log(getFormInfoResult.data[0]._id, "getFormInfoResult")
            if (getFormInfoResult.data !== undefined && getFormInfoResult.data.length > 0) {
                const map = {}
                map.formSettingId = getFormInfoResult.data[0]._id
                map.hidden = false; //非"删除"状态
                console.log(map, "map")
                const countResult = await db.collection('morphy_formData').where(map).count()
                const total = countResult.total;
                const batchTimes = Math.ceil(total / MAX_LIMIT)
                // 承载所有读操作的 promise 的数组
                const tasks = []
                for (let i = 0; i < batchTimes; i++) {
                    const promise = db.collection('morphy_formData').where(map).skip(i * MAX_LIMIT).limit(MAX_LIMIT).get()
                    tasks.push(promise)
                }
                // 等待所有
                let allFormData = (await Promise.all(tasks)).reduce((acc, cur) => {
                    return {
                        data: acc.data.concat(cur.data),
                        errMsg: acc.errMsg,
                    }
                })
                ctx.body = reponseWrapper(0, "获取成功", allFormData)
            } else {
                ctx.body = reponseWrapper(-1102, "找不到表单信息，请重试")
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     * 更新某个表单单条数据
     */
    app.router('updateFormData', paramsValidatorMiddleware(event, {
        _id: paramsValidatorMiddleware.isRequire().string(),
        formData: paramsValidatorMiddleware.isRequire().object()
    }), async (ctx, next) => {
        const {
            _id,
            formData,
        } = event

        try {
            //获取待更新内容
            const rowData = await db.collection('morphy_formData').doc(_id).get();
            if (rowData.data && rowData.data.formSettingId) {
                const formSettingData = await db.collection('morphy_formSetting').doc(rowData.data.formSettingId).get();
                console.log(formSettingData, "formSettingData")
                if (formSettingData.data && formSettingData.data.formSetting) {
                    let formSettings = formSettingData.data.formSetting;
                    for (var i in formData) {
                        let type = formSettings.find(item => item.column === i)
                        if (type && (type.type === 'statusBoolean' || type.type === 'statusSwitch')) {
                            //操作类型，增加操作时间和操作人的记录
                            formData[`${i}Sys`] = {
                                morphy: {
                                    time: Date.now(), //操作时间
                                    by: openid //操作人
                                }
                            }
                        }
                    }
                }
                const data = {
                    formData: formData
                }
                const updateResult = await db.collection('morphy_formData').doc(_id).update({
                    data: {
                        ...data,
                        updateSys: {
                            morphy: {
                                time: Date.now(),
                                by: openid
                            }
                        }
                    }
                })
                if (updateResult.stats.updated === 1) {
                    const getResult = await db.collection('morphy_formData').doc(_id).get()
                    ctx.body = reponseWrapper(0, '更新成功', getResult.data)
                } else {
                    ctx.body = reponseWrapper(-1101)
                }
            } else {
                ctx.body = reponseWrapper(-1101, "找不到更新记录")
            }

        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     * 删除某个表单下的单条数据
     */
    app.router('deleteFormData', paramsValidatorMiddleware(event, {
        _id: paramsValidatorMiddleware.isRequire().string()
    }), async (ctx, next) => {
        const {
            _id,
            hidden = true
        } = event;
        try {
            //默认假删除
            let deleteResult;
            if (hidden) {
                deleteResult = await db.collection('morphy_formData').doc(_id).update({
                    data: {
                        hidden: true,
                        hiddenSys: {
                            morphy: {
                                time: Date.now(),
                                by: openid
                            }
                        }
                    }
                });
                if (deleteResult.stats.updated === 1) {
                    ctx.body = reponseWrapper(0, '删除成功')
                } else {
                    ctx.body = reponseWrapper(-1101)
                }
                //真删除
            } else {
                deleteResult = await db.collection('morphy_formData').doc(_id).remove();
                if (deleteResult.stats.removed === 1) {
                    ctx.body = reponseWrapper(0, '删除成功')
                } else {
                    ctx.body = reponseWrapper(-1101)
                }
            }


        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    //同步创建表单到另外一个环境
    app.router('copyFormTo', paramsValidatorMiddleware(event, {
        _id: paramsValidatorMiddleware.isRequire().string(),
        targetEnv: paramsValidatorMiddleware.isRequire().string(),
        force: paramsValidatorMiddleware.boolean()
    }), async (ctx, next) => {
        const {
            _id,
            targetEnv,
            force = false
        } = event
        try {
            const targetDB = cloud.database({
                env: targetEnv
            })
            const getLocalResult = await db.collection('morphy_formSetting').where({
                _id
            }).get()
            if (getLocalResult.data.length > 0) {
                const formInfo = getLocalResult.data[0];
                const {
                    tableName,
                    name,
                    desc,
                    formSetting,
                    isShow
                } = formInfo;
                if (!tableName) {
                    ctx.body = reponseWrapper(-102, '原表单缺少tableName');
                    return
                }
                //查询目标数据库是否存在
                const getTargetResult = await targetDB.collection('morphy_formSetting').where({
                    tableName
                }).get()
                console.log(getTargetResult, "getTargetResult")
                //目标数据库已有相同表，采用update
                if (getTargetResult.data && getTargetResult.data.length > 0) {
                    //已存在,是否强制更新
                    if (force) {
                        const updateResult = await targetDB.collection('morphy_formSetting').doc(getTargetResult.data[0]._id).set({
                            data: {
                                tableName,
                                name,
                                desc,
                                formSetting,
                                isShow,
                                updateSys: {
                                    morphy: {
                                        time: Date.now(),
                                        by: openid
                                    }
                                }
                            }
                        })
                        if (updateResult.stats.updated === 1) {
                            ctx.body = reponseWrapper(0, '同步更新成功')
                        } else {
                            ctx.body = reponseWrapper(-1101)
                        }
                    } else {
                        ctx.body = reponseWrapper(-102, '目标数据库已存在表单，无法创建')
                    }
                } else {
                    //目标数据库没有相同表，采用insert
                    const addData = {
                        tableName,
                        name,
                        desc,
                        formSetting,
                        isShow,
                        createAt: Date.now(),
                        creator: openid
                    }
                    const addResult = await targetDB.collection('morphy_formSetting').add({
                        data: addData
                    })
                    if (addResult._id) {
                        ctx.body = reponseWrapper(0, '同步创建成功')
                    } else {
                        ctx.body = reponseWrapper(-1101)
                    }
                }
            } else {
                ctx.body = reponseWrapper(-102, '当前数据库找不到表单，请重试')
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     * batchImport 批量导入
     */
    app.router('batchImport', paramsValidatorMiddleware(event, {
        tableName: paramsValidatorMiddleware.isRequire().string(),
        excelData: paramsValidatorMiddleware.isRequire().array(),
        upsertBy: paramsValidatorMiddleware.array(), //基于什么字段更新? <array>
        upsert: paramsValidatorMiddleware.boolean(), //是否是插入&更新模式
    }), async (ctx, next) => {
        const {
            excelData,
            tableName,
            upsert = false,
            upsertBy
        } = event;
        console.log(upsert === true && upsertBy === undefined, "upsert===true&&upsertBy===undefined")
        if (upsert === true && upsertBy === undefined) {
            ctx.body = reponseWrapper(-1102, "暂未选择导入更新基准字段，导入操作失败");
            return;
        }
        try {
            //查询tableName对应的表格
            const getFormInfo = await db.collection('morphy_formSetting').where({
                tableName
            }).get()
            console.log(getFormInfo, "getFormInfo")
            let formInfo = getFormInfo.data;
            if (formInfo !== undefined && formInfo.length > 0) {
                let updateCount = 0,
                    addCount = 0,
                    defaultFormItem = {},
                    targetFormSetting = formInfo[0].formSetting
                if (targetFormSetting) {
                    for (var k in targetFormSetting) {
                        let tType = targetFormSetting[k].type,
                            tColumn = targetFormSetting[k].column;
                        if (tType === 'statusBoolean') {
                            //默认否
                            defaultFormItem[tColumn] = false
                        } else if (tType === 'statusSwitch') {
                            //默认未处理
                            defaultFormItem[tColumn] = 0
                        } else {
                            defaultFormItem[tColumn] = ''
                        }
                    }
                }
                for (var i = 0; i < excelData.length; i++) {
                    let formItem = excelData[i];
                    //待导入的的数据格式
                    let insertData = {
                        formSettingId: getFormInfo.data[0]._id,
                        formData: Object.assign(defaultFormItem, formItem), //新增数据情况,如果excel数据不全,自动补齐其他属性,避免属性undefined造成客户端报错
                        createAt: Date.now(),
                        source: "import",
                        openid,
                        hidden: false //是否隐藏
                    }
                    // 如果是upsert模式
                    if (upsert) {
                        console.log(upsertBy, "upsertBy")
                        let upsertWhere = {
                            formData: {},
                            formSettingId: getFormInfo.data[0]._id,
                            hidden: false
                        }
                        upsertBy.forEach(item => {
                            upsertWhere.formData[item] = _.eq(formItem[item])
                        })
                        let dataExist = await db.collection('morphy_formData').where(upsertWhere).limit(1).get();
                        console.log(dataExist, "dataExist")
                        //如果已存在记录则更新
                        if (dataExist.data && dataExist.data.length > 0) {
                            console.log("upsert更新记录")
                            let doc = dataExist.data[0];
                            //更新已存在案例
                            const updateExist = await db.collection('morphy_formData').doc(doc._id).update({
                                data: {
                                    formData: formItem,
                                    updateSys: {
                                        morphy: {
                                            time: Date.now(),
                                            source: "import",
                                            by: openid
                                        }
                                    }
                                }
                            })
                            updateCount++;
                            //如果没有记录则新增
                        } else {
                            console.log("upsert新增记录")
                            const addResult = await db.collection('morphy_formData').add({
                                data: insertData
                            })
                            addCount++;
                        }
                    } else {
                        const addResult = await db.collection('morphy_formData').add({
                            data: insertData
                        })
                        addCount++;
                    }
                }
                ctx.body = reponseWrapper(0, '批量导入成功', {
                    addCount,
                    updateCount
                })
            } else {
                ctx.body = reponseWrapper(-1102, "没有找到对应的表单信息，记录创建失败")
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }

    })

    /**
     * batchUpsertEffected 批量upsert影响量查询
     */
    app.router('batchUpsertEffected', paramsValidatorMiddleware(event, {
        tableName: paramsValidatorMiddleware.isRequire().string(),
        excelData: paramsValidatorMiddleware.isRequire().array(),
        upsertBy: paramsValidatorMiddleware.array(), //基于什么字段更新? <array>
    }), async (ctx, next) => {
        const {
            excelData,
            tableName,
            upsertBy
        } = event;
        try {
            //查询tableName对应的表格
            const getFormInfo = await db.collection('morphy_formSetting').where({
                tableName
            }).get()
            if (getFormInfo.data !== undefined && getFormInfo.data.length > 0) {
                let updateCount = 0,
                    updateList = [], //受影响的内容
                    addCount = 0;
                let upsertWhere = {
                        formData: {},
                        hidden: false,
                        formSettingId: getFormInfo.data[0]._id
                    },
                    formDataObj = {}
                for (var i = 0; i < excelData.length; i++) {
                    let formItem = excelData[i];
                    upsertBy.forEach(item => {
                        if (!Array.isArray(formDataObj[item])) {
                            formDataObj[item] = []
                        }
                        formDataObj[item].push(formItem[item])
                    })
                }
                for (var k in formDataObj) {
                    upsertWhere.formData[k] = _.in(formDataObj[k])
                }
                let dataExist = await db.collection('morphy_formData').where(upsertWhere).count();
                //如果已存在记录则更新
                if (dataExist && dataExist.total !== undefined) {
                    updateCount = dataExist.total;
                    addCount = excelData.length - updateCount
                } else {
                    addCount = excelData.length
                }
                ctx.body = reponseWrapper(0, '查询成功', {
                    addCount,
                    updateCount
                })
            } else {
                ctx.body = reponseWrapper(-1102, "没有找到对应的表单信息")
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }

    })

    // 获取表单记录
    app.router('client/getMyFormData', paramsValidatorMiddleware(event, {
        order: paramsValidatorMiddleware.enums('desc', 'asc'),
        tableName: paramsValidatorMiddleware.isRequire().string(),
        page: paramsValidatorMiddleware.number(),
        size: paramsValidatorMiddleware.number(),
    }), async (ctx, next) => {
        const {
            tableName,
            page = 1,
            size = 10,
        } = event
        try {
            const map = {}
            const getFormInfoResult = await db.collection('morphy_formSetting').where({
                tableName
            }).get()
            console.log(getFormInfoResult.data[0]._id, "getFormInfoResult")
            if (getFormInfoResult.data !== undefined && getFormInfoResult.data.length > 0) {
                map.formSettingId = getFormInfoResult.data[0]._id
                map.hidden = false; //非"删除"状态map.hidden = false; //非"删除"状态
                map.openid = openid;
                console.log(map, "map")
                const getResult = await db.collection('morphy_formData').where(map).skip((page - 1) * size).limit(size).get()
                const totalResult = await db.collection('morphy_formData').where(map).count()
                ctx.body = reponseWrapper(0, '获取成功', {
                    list: getResult.data,
                    total: totalResult.total
                })
            } else {
                ctx.body = reponseWrapper(-1102, "找不到表单信息，请重试")
            }

        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     * 获取用户详情信息
     */
    app.router('client/getFromDetail', paramsValidatorMiddleware(event, {
        tableName: paramsValidatorMiddleware.isRequire().string()
    }), async (ctx, next) => {
        const {
            tableName
        } = event
        try {
            const getResult = await db.collection('morphy_formSetting').where({
                tableName
            }).get()
            if (getResult.data !== undefined && getResult.data.length > 0) {
                ctx.body = reponseWrapper(0, '获取成功', getResult.data[0])
            } else {
                ctx.body = reponseWrapper(-1102, "找不到表单信息，请重试")
            }

        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     * 添加form 信息
     */
    app.router('client/addFormData', paramsValidatorMiddleware(event, {
        formData: paramsValidatorMiddleware.isRequire().object(),
        tableName: paramsValidatorMiddleware.isRequire().string(),
    }), async (ctx, next) => {
        // 检查formData是否是合法的
        const {
            formData,
            tableName
        } = event
        const data = {
            formData,
            createAt: Date.now(),
            openid,
            hidden: false, //是否隐藏
            source: "client" //来源morphy
        }
        try {
            //查询tableName对应的表格
            const getFormInfo = await db.collection('morphy_formSetting').where({
                tableName
            }).get()
            console.log(getFormInfo, "getFormInfo")
            if (getFormInfo.data !== undefined && getFormInfo.data.length > 0) {
                data.formSettingId = getFormInfo.data[0]._id
                const addResult = await db.collection('morphy_formData').add({
                    data
                })
                // 检查填写字段
                if (addResult._id) { // 添加成功
                    data._id = addResult._id
                    ctx.body = reponseWrapper(0, '添加成功', data)
                } else {
                    ctx.body = reponseWrapper(-1101)
                }
            } else {
                ctx.body = reponseWrapper(-1102, "没有找到对应的表单信息，记录创建失败")
            }


        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }

    })
    /**
     * 获取用户详情信息
     */
    app.router('client/getFromDetail', paramsValidatorMiddleware(event, {
        tableName: paramsValidatorMiddleware.isRequire().string()
    }), async (ctx, next) => {
        const {
            tableName
        } = event
        try {
            const getResult = await db.collection('morphy_formSetting').where({
                tableName
            }).get()
            if (getResult.data !== undefined && getResult.data.length > 0) {
                ctx.body = reponseWrapper(0, '获取成功', getResult.data[0])
            } else {
                ctx.body = reponseWrapper(-1102, "找不到表单信息，请重试")
            }

        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     * 小程序获取查询表单记录
     * search必选参数，且只能精准匹配字符串，不支持非字符串类型
     * 暂不支持大于100条记录的搜索返回
     */
    app.router('client/searchFormData', paramsValidatorMiddleware(event, {
        tableName: paramsValidatorMiddleware.isRequire().string(),
        search: paramsValidatorMiddleware.isRequire().object(),
    }), async (ctx, next) => {
        const {
            tableName, //表单英文名
            search = {}, //搜索条件
        } = event
        try {
            // 找到对应tableName的表单信息
            const targetFormInfo = await db.collection('morphy_formSetting').where({
                tableName
            }).get()
            console.log(targetFormInfo.data[0]._id, "找到对应tableName的表单信息")
            if (targetFormInfo.data !== undefined && targetFormInfo.data.length > 0) {
                let map = {
                    formSettingId: targetFormInfo.data[0]._id,
                    hidden: false //非"删除"状态
                }
                //如果搜索对象有相应属性
                if (Object.keys(search).length !== 0) {
                    map["formData"] = {};
                    for (var k in search) {
                        map["formData"][k] = search[k];
                    }
                    //查询满足条件的表单内容
                    const getResult = await db.collection('morphy_formData').where(map).get()
                    const getResultCount = await db.collection('morphy_formData').where(map).count()
                    ctx.body = reponseWrapper(0, '获取成功', {
                        list: getResult.data,
                        total: getResultCount.total
                    })
                    console.log(getResult, "getResult")
                } else {
                    console.log("请指定搜索内容")
                    ctx.body = reponseWrapper(-1102, "请指定搜索内容")
                }
                //
                // const totalResult = await db.collection('morphy_formData').where(map).count()


            } else {
                ctx.body = reponseWrapper(-1102, "找不到表单信息，请重试")
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     * 小程序更新表单记录
     */
    app.router('client/updateFormData', paramsValidatorMiddleware(event, {
        _id: paramsValidatorMiddleware.isRequire().string(),
        formData: paramsValidatorMiddleware.isRequire().object()
    }), async (ctx, next) => {
        const {
            _id,
            formData,
        } = event

        try {
            //查询待更新id是否存在并找到对应的表单详情
            const rowData = await db.collection('morphy_formData').doc(_id).get();
            if (rowData.data && rowData.data.formSettingId) {
                //获取记录对应的表单详情
                const formSettingData = await db.collection('morphy_formSetting').doc(rowData.data.formSettingId).get();
                console.log(formSettingData, "formSettingData")
                if (formSettingData.data && formSettingData.data.formSetting) {
                    let formSettings = formSettingData.data.formSetting;
                    // 找到操作类属性，自动更新操作人和时间
                    for (var i in formData) {
                        let type = formSettings.find(item => item.column === i)
                        if (type && (type.type === 'statusBoolean' || type.type === 'statusSwitch')) {
                            console.log("包含操作类型，自动更新时间和操作人")
                            //操作类型，增加操作时间和操作人的记录
                            formData[`${i}Sys`] = {
                                //Sys中区分是小程序还是中台的操作记录
                                client: {
                                    time: Date.now(), //操作时间
                                    by: openid //小程序上下文的openid
                                }
                            }
                        }
                    }
                }
                const data = {
                    formData: formData,
                    openid: openid
                }
                const updateResult = await db.collection('morphy_formData').doc(_id).update({
                    data: {
                        ...data,
                        updateAt: Date.now()
                    }
                })
                if (updateResult.stats.updated === 1) {
                    const getResult = await db.collection('morphy_formData').doc(_id).get()
                    ctx.body = reponseWrapper(0, '更新成功', getResult.data)
                } else {
                    ctx.body = reponseWrapper(-1101)
                }
            } else {
                ctx.body = reponseWrapper(-1101, "找不到更新记录")
            }

        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     * 客户端获取指定表单所有数据，支持分页
     */
    app.router('client/getAllFormData', paramsValidatorMiddleware(event, {
        order: paramsValidatorMiddleware.enums('desc', 'asc'),
        tableName: paramsValidatorMiddleware.isRequire().string(),
        page: paramsValidatorMiddleware.number(),
        size: paramsValidatorMiddleware.number(),
        orderBy:paramsValidatorMiddleware.string(),
        filter:paramsValidatorMiddleware.object()
    }), async (ctx, next) => {
        const {
            tableName,
            page = 1,
            size = 10,
            order="desc",orderBy="createAt",
            filter={}
        } = event
        try {
            let map = {}
            if(Object.keys(filter).length !== 0){
                map = Object.assign(map,filter)
            }
            const getFormInfoResult = await db.collection('morphy_formSetting').where({
                tableName
            }).get()
            console.log(getFormInfoResult.data[0]._id, "getFormInfoResult")
            if (getFormInfoResult.data !== undefined && getFormInfoResult.data.length > 0) {
                map.formSettingId = getFormInfoResult.data[0]._id
                map.hidden = false; //非"删除"状态map.hidden = false; //非"删除"状态
                const getResult = await db.collection('morphy_formData').where(map).orderBy(orderBy, order).skip((page - 1) * size).limit(size).get()
                const totalResult = await db.collection('morphy_formData').where(map).count()
                ctx.body = reponseWrapper(0, '获取成功', {
                    list: getResult.data,
                    total: totalResult.total
                })
            } else {
                ctx.body = reponseWrapper(-1102, "找不到表单信息，请重试")
            }

        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    return app.serve()
}
