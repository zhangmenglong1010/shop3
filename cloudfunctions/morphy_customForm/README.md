## 自定义表单

模块名：自定义表单

版本：`V1.0`

维护人：nornorchen

云函数：`morphy_customForm`

特性：支持定义表单（报名，问卷，满意度调查）。创建表单可以定义是单行，多行，单选，多选等问题类型。同时也可以创建操作类型的按钮用于管理表单，如布尔型操作（是否类），状态型操作（通过，拒绝，未处理）


### 小程序前端接口

>开发约定：由于该模块使用云函数，因此可直接按照微信调用云函数方法的格式调用，包括promise链式调用以及complete等回调调用

#### 获取特定tableName的表单配置

一般用于前端渲染，比如有什么字段，字段是否是多选等

**调用示例**

```javascript
// 小程序端调用
wx.cloud.callFunction({
  // 需调用的云函数名
  name: 'morphy_customForm',
  // 传给云函数的参数
  data: {
      '$url':'client/getFromDetail',
      'tableName':'定义的表单英文名，可在中台获取'
  },
  // 成功回调
  complete: console.log
})
```

**请求参数**

|  参数   | 必填  | 类型  | 备注  |
|:------|:-------:|:--------:|:--------|
| $url  | 是 |String|请求路由client/getFromDetail|
| tableName  | 是 | String|表单英文名。可在中台表单管理中获取|

**返回格式**

```json
{
    code: 0
    data:
        {
            createAt: 1590565317220
            desc: "报名表单"
            formSetting: (2) [{…}, {…}]
            isShow: true
            name: "峰会报名表"
            tableName: "202007signup"
            updateAt: 1592979490962
            _id: "989f4e215ece19c50050ca4661006ce2"
        }
    errMsg: "获取成功"
}
```



***



#### 创建表单记录

给指定的tableName表单写入数据

**调用示例**

```javascript
// 小程序端调用
wx.cloud.callFunction({
  // 需调用的云函数名
  name: 'morphy_customForm',
  // 传给云函数的参数
  data: {
      '$url':'client/addFormData',
    	'tableName':'定义的表单英文名，可在中台获取',
    	'formData':{
        ...提交表单的内容
      }
  },
  // 成功回调
  complete: console.log
})
```

**请求参数**

| 参数| 必填 | 类型   | 备注|
|:------|:-------:|:--------:|:--------|
| $url| 是| String | 请求路由client/addFormData|
| tableName | 是   | String | 表单英文名。可在中台表单管理中获取|
| formData  | 是   | Object | 请根据getFromDetail接口返回的formSetting中的字段配置和格式进行提交 |

**返回格式**

```json
{
    code: 0
    errMsg: "添加成功"
}
```



***



#### 更新单条表单记录

给指定的表单id更新数据

**调用示例**

```javascript
// 小程序端调用
wx.cloud.callFunction({
  // 需调用的云函数名
  name: 'morphy_customForm',
  // 传给云函数的参数
  data: {
      '$url':'client/updateFormData',
    	'_id':'formData表中记录的id',
    	'formData':{
        ...待更新的表单内容
      }
  },
  // 成功回调
  complete: console.log
})
```

**请求参数**

| 参数     | 必填 | 类型   | 备注|
|:------|:-------:|:--------:|:--------|
| $url     | 是   | String | 请求路由client/updateFormData                                |
| _id      | 是   | String | formData表中记录的id                                         |
| formData | 是   | Object | 请根据getFromDetail接口返回的formSetting中的字段配置和格式进行提交 |

**返回格式**

```json
{
    code: 0
    errMsg: "更新成功"
   	data:{} //更新后的记录信息
}
```



------

#### 搜索表单记录（根据特定条件）

根据特定formData字段搜索表单内容

只能精准匹配字符串，不支持非字符串类型

**调用示例**

```javascript
// 小程序端调用
wx.cloud.callFunction({
  // 需调用的云函数名
  name: 'morphy_customForm',
  // 传给云函数的参数
  data: {
      '$url':'client/searchFormData',
    	'tableName':'定义的表单英文名，可在中台获取',
    	'search':{
        //...待搜索的表单内容，如name:'张三',mobile:'13322221111'
      }
  },
  // 成功回调
  complete: console.log
})
```

**请求参数**

| 参数      | 必填 | 类型   | 备注|
|:------|:-------:|:--------:|:--------|
| $url      | 是   | String | 请求路由client/updateFormData                                |
| tableName | 是   | String | 表单英文名。可在中台表单管理中获取                           |
| search    | 是   | Object | 1. 必填参数，且只能精准匹配字符串，不支持非字符串类型<br />2. 暂不支持大于100条记录的搜索返回<br />3. 多个字段，逻辑为与判断，同时满足才返回 |

**返回格式**

```json
{
    code: 0
    errMsg: "获取成功"
   	data:{} //更新后的记录信息
}
```

------

#### 获取我的表单记录

根据请求用户的上下文openid查询表单中的记录

**调用示例**

```javascript
// 小程序端调用
wx.cloud.callFunction({
  // 需调用的云函数名
  name: 'morphy_customForm',
  // 传给云函数的参数
  data: {
      '$url':'client/getMyFormData',
    	'tableName':'定义的表单英文名，可在中台获取，不填则取全部表单openid对应的记录'
  },
  // 成功回调
  complete: console.log
})
```

**请求参数**

| 参数      | 必填 | 类型   | 备注|
|:------|:-------:|:--------:|:--------|
| $url      | 是   | String | 请求路由client/getMyFormData                                |
| tableName | 否   | String | 表单英文名。可在中台表单管理中获取。不指定则查询所有表单                           |
| size    | 否   | Number | 默认值10，每次最多返回10条 |
| page    | 否   | Number | 默认值1，用户数据多的情况下翻页查询 |

**返回格式**

```json
{
    code: 0
    errMsg: "获取成功"
   	data:{
   		list:[], //返回记录<array>
   		total:0  //返回记录数<number>
   	}
}
```
