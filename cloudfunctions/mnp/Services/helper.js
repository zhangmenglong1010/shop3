/**
 *
 */
const {
	db
} = require('./cloud');
const http = require('http');
const url = require('url');
// const urlExists = require('url-exists');

function getDateString(timestamp, splitString = '-') {
	let date = new Date(timestamp);
	let year = date.getFullYear();
	let month = date.getMonth() + 1;
	let day = date.getDate();
	month = month < 10 ? `0${month}` : `${month}`;
	day = day < 10 ? `0${day}` : `${day}`;

	return `${year}${splitString}${month}${splitString}${day}`;
}

function getTimeText(startTime, endTime) {
	if (!startTime || !endTime) {
		throw '函数getTimeText错误，参数错误';
	}
	// let {startHour, start}
	// let hourOffset = new Date().getTimezoneOffset() / 60;
	let startDate = new Date(startTime);
	let endDate = new Date(endTime);
	let startHour = startDate.getHours();
	// let startHour = startDate.getHours() + 8 + hourOffset;
	let startMinute = startDate.getMinutes();
	let endHour = endDate.getHours();
	// let endHour = endDate.getHours() + 8 + hourOffset;
	let endMinute = endDate.getMinutes();
	if (startHour < 10) {
		startHour = `0${startHour}`;
	}
	if (startMinute < 10) {
		startMinute = `0${startMinute}`;
	}
	if (endHour < 10) {
		endHour = `0${endHour}`;
	}
	if (endMinute < 10) {
		endMinute = `0${endMinute}`;
	}
	return `${startHour}:${startMinute}-${endHour}:${endMinute}`;
}

module.exports = {
	/**
	 * @description 获取自增数
	 * @param {*} sequenceName 自增字段名
	 */
	async getNextSequenceValue(sequenceName, isRandom = false) {
		const _ = db.command;
		let originData = (
			await db
			.collection('vs_counters')
			.where({
				_id: sequenceName,
			})
			.get()
		).data;
		if (!originData.length) {
			throw `非法的自增字段${sequenceName}`;
		}
		// 开始事务
		// 递增为1或递增为一个 5～14 的随机数
		let incCounter = isRandom ? Math.floor(Math.random() * 10) + 5 : 1;
		const result = await db.runTransaction(async (transaction) => {
			let sequenceDocument = await transaction
				.collection('vs_counters')
				.doc(sequenceName)
				.update({
					data: {
						sequence_value: _.inc(incCounter),
					},
				});
			let currentData = (await transaction.collection('vs_counters').doc(sequenceName).get()).data;

			// 会作为 runTransaction resolve 的结果返回
			return currentData.sequence_value;

			// 会作为 runTransaction reject 的结果出去
			//  await transaction.rollback(-100)
		});
		return result;
	},

	arrayUnique(array) {
		return Array.from(new Set(array));
	},

	/**
	 *
	 * @param {*} timestamp 时间戳
	 * @param {*} splitString 日期间隔符，默认为-
	 */
	getDateString,

	getTimeText,

	/**
	 * @description 判断是否为当天
	 * @param {*} timestamp 
	 */
	isCurrentDay(timestamp) {
		let currentDate = new Date();
		let dateString = getDateString(currentDate);
		let startTime = new Date(`${dateString} 00:00:00`).getTime();
		let endTime = new Date(`${dateString} 23:59:00`).getTime();
		return timestamp > startTime && timestamp < endTime;
	},

	/**
	 * @description 查看视频url是否存在，用于判断cos vod 服务是否自动转为mp4成功
	 * 
	 */
	async getIsUrlExist(urlPath) {
		urlPath = `http://${urlPath}`;
		return new Promise((resolve, reject) => {
			var options = {
				method: 'HEAD',
				host: url.parse(urlPath).host,
				port: 80,
				path: url.parse(urlPath).pathname
			};
			let req = http.request(options, function (r) {
				resolve(r.statusCode == 200);
			});
			req.end();
		});

		// await axios.get(urlPath).then(res => {
		// 	console.log(res);
		// }).catch (err => {
		// 	console.log(err)
		// });
		// return new Promise((resolve, rejects) => {
		// 	urlExists(urlPath, (err, exists) => {
		// 		if (err) {
		// 			rejects(err);
		// 		}
		// 		console.log(exists);
		// 		resolve(exists)
		// 	})
		// })
		// return false;

	},

	async addErrorLog(data) {
		if (typeof data !== 'object') {
			throw "函数[addErrorLog]执行失败，参数错误";
		}
		await db.collection('vs_error_logs').add({
			data: data
		});
	}
};