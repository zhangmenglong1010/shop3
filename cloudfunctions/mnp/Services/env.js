const cloud = require('wx-server-sdk');

module.exports = {
	// appConfig
	appConfig: {
		// env_id: 'test-eiozq',
		// env_id: 'release-rcd69',
		// env_id: 'hangzhou-8gxsziyy0b51946e',
		env_id: cloud.DYNAMIC_CURRENT_ENV
	},
  defaultAvatar: "https://game.gtimg.cn/images/game/act/a20201001vstation/my/default_avatar.png",

	adminOpenidList: [
		'oK05P5X6Ctv7I6OEOhENhQ-sxbzo', // randle
		'oK05P5XVkhjtja27_CKxe0PC5aEs', // seventh
		// '' // kimi
	],

	// 预约配置
	reserveConfig: {
		ticket: ['通票', '工作日票'], // 票种
		actType: ['全日', '工作日', '王者荣耀周年庆', '主题活动日', '主题活动日'], // 活动类型
		maxCount: 1, // 每个时间段最大人数
		calendar: [
			// 常规工作日/节假日
			{
				// data: 
				// actType: 0 / 1
				timeline: [
					{
						isCustom: true,
						startTime: '10:30',
						endTime: '13:00',
						resCount: 375,
					},
					{
						isCustom: true,
						startTime: '13:00',
						endTime: '16:00',
						resCount: 450,
					},
					{
						isCustom: true,
						startTime: '16:00',
						endTime: '19:00',
						resCount: 450,
					},
					{
						isCustom: true,
						startTime: '19:00',
						endTime: '21:00',
						resCount: 375,
					},
				],
			},

			// 王者荣耀周年庆 10-28
			{
				date: ['2020-10-28'],
				actType: 2,
				// text: '王者荣耀周年庆',
				timeline: [
					{
						isCustom: true,
						startTime: '10:30',
						endTime: '13:00',
						resCount: 375,
					},
					{
						isCustom: true,
						startTime: '13:00',
						endTime: '15:00',
						resCount: 300,
					},
					{
						isCustom: true,
						startTime: '15:00',
						endTime: '17:00',
						resCount: 425,
					},
					{
						isCustom: true,
						startTime: '17:00',
						endTime: '18:00',
						resCount: 0,
					},
					{
						isCustom: true,
						startTime: '18:00',
						endTime: '21:00',
						resCount: 0,
					},
				],
			},

			// 主题活动日（10-11、11-07、11-22）
			{
				date: ['2020-10-11', '2020-11-07', '2020-11-22'],
				actType: 3,
				// text: '主题活动日',
				timeline: [
					{
						isCustom: true,
						startTime: '10:30',
						endTime: '12:30',
						resCount: 300,
					},
					{
						isCustom: true,
						startTime: '12:30',
						endTime: '14:30',
						resCount: 350,
					},
					{
						isCustom: true,
						startTime: '14:30',
						endTime: '15:00',
						resCount: 0,
					},
					{
						isCustom: true,
						startTime: '15:00',
						endTime: '17:00',
						resCount: 0,
					},
					{
						isCustom: true,
						startTime: '17:00',
						endTime: '19:30',
						resCount: 375,
					},
					{
						isCustom: true,
						startTime: '19:30',
						endTime: '21:00',
						resCount: 300,
					},
				],
			},
			// 主题活动日（11-01 11-08 11-14）
			{
				date: ['2020-11-01', '2020-11-08', '2020-11-14'],
				actType: 4,
				// text: '主题活动日',
				timeline: [
					{
						isCustom: true,
						startTime: '10:30',
						endTime: '12:00',
						resCount: 225,
					},
					{
						isCustom: true,
						startTime: '12:00',
						endTime: '13:30',
						resCount: 276,
					},
					{
						isCustom: true,
						startTime: '13:30',
						endTime: '14:00',
						resCount: 0,
					},
					{
						isCustom: true,
						startTime: '14:00',
						endTime: '16:00',
						resCount: 0,
					},
					{
						isCustom: true,
						startTime: '16:00',
						endTime: '19:00',
						resCount: 450,
					},
					{
						isCustom: true,
						startTime: '19:00',
						endTime: '21:00',
						resCount: 375,
					},
				],
			},
		],
		// timeline: [
		// 	{
		// 		start: 150 * 60000, // 如果是10：30，填10:30 - 8:00 = 150 * 60 * 1000
		// 		end: 270 * 60000, // 如果是12：30，填12:30 - 8:00 = 270 * 60 * 1000
		// 	},
		// 	{
		// 		start: 270 * 60000, // 如果是10：30，填10:30 - 8:00 = 150 * 60 * 1000
		// 		end: 390 * 60000, // 如果是12：30，填12:30 - 8:00 = 270 * 60 * 1000
		// 	},
		// 	{
		// 		start: 390 * 60000, // 如果是10：30，填10:30 - 8:00 = 150 * 60 * 1000
		// 		end: 510 * 60000, // 如果是12：30，填12:30 - 8:00 = 270 * 60 * 1000
		// 	},
		// 	{
		// 		start: 510 * 60000, // 如果是10：30，填10:30 - 8:00 = 150 * 60 * 1000
		// 		end: 630 * 60000, // 如果是12：30，填12:30 - 8:00 = 270 * 60 * 1000
		// 	},
		// 	{
		// 		start: 630 * 60000, // 如果是10：30，填10:30 - 8:00 = 150 * 60 * 1000
		// 		end: 750 * 60000, // 如果是12：30，填12:30 - 8:00 = 270 * 60 * 1000
		// 	},
		// ],
		/*{ // 每个时间段的显示文字
      0: "10:30-12:30",
      1: "12:30-14:30",
      2: "14:30-16:30",
      3: "16:30-18:30",
      4: "18:30-20:30"
    }*/
		// [9:00-9:30, 930-1000]
	},

	// morphy 中台配置
	morphyConfig: {
		app_id: '5f39f134a14f0', // morphy app id
		dbIdentity: 'morphyAuth',
		domain: 'https://morphy.qq.com/H7ChfbNFyvFQd2dYTq2naw5KWbj8PvMZ/api',
		clientid: '5f447fe4f3810',
		clientSecret: 'VBykoZp1yJl6i5hdOoyXhhXDGldk3qj33vW1dMbe',
	},
};
