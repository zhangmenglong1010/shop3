const {
	db,
	cloud
} = require('./cloud');
const {
	getDateString,
	isCurrentDay,
	getTimeText,
	getNextSequenceValue

} = require('./helper');
const {
	apiResponse
} = require('./http');
const {
	defaultAvatar
} = require('./env');

const userUtil = {
	/**
	 * @description 用户抽奖行为，随机获取到一个奖品id
	 *
	 */
	async getRandomLottery(lotteryList) {
		const _ = db.command;
		// 实体奖品列表

		let totalWeight = 0;
		lotteryList.forEach((item) => {
			totalWeight += item.weight;
		});
		let rand = Math.floor(Math.random() * totalWeight);
		let weightCount = -1;
		let lotteryIndex = 0; // 谢谢参与
		for (let i = 0; i < lotteryList.length; i++) {
			let item = lotteryList[i];
			if (typeof item.weight !== 'number') {
				continue;
			}
			weightCount += item.weight;
			if (rand < weightCount) {
				lotteryIndex = i;
				break;
			}
		}
		return lotteryList[lotteryIndex];
	},
};
/**
 * @description UserServices - 用户通用功能封装
 */
module.exports = {

	/**
	 * find或者创建用户
	 */
	async findOrCreateUser(OPENID, UNIONID) {
		if (!OPENID) {
			throw "缺少OPENID";
		}
		// 获取openid对应的用户数组，判断长度是否为1
		const result = await db.runTransaction(async (transaction) => {
			let user = null;
			let usersData = await transaction
				.collection('vs_users')
				.where({
					openid: OPENID,
				})
				.get();

			// 多于1个用户，抛出异常
			if (usersData.data.length > 1) {
				user = usersData.data[0];
				// ctx.body = apiResponse(20003, null, '数据异常，出现两个或以上该openid对应的用户');
				// return;

				// 未找到该用户
			} else if (usersData.data.length === 0) {
				// ctx.body = apiResponse(10001, null, '未找到该openid对应的用户');
				// return;
				// 添加到数据库中
				let userSequenceValue = await getNextSequenceValue('user_id'); // 获取自增数
				let userUnrealSequenceValue = await getNextSequenceValue('unreal_user_id', true); // 获取自增数，并自增一个随机数
				let timestamp = new Date().getTime();
				const insertor = await transaction.collection('vs_users').add({
					data: {
						increment: userSequenceValue, // 自增id
						incCount: userUnrealSequenceValue, // 虚拟自增id
						avatar: defaultAvatar, // 先提供默认头像
						name: `用户${userUnrealSequenceValue}`, // 和默认昵称
						// tempName: '', // 用户待审核的昵称
						// tempAvatar: '', // 用户待审核的头像
						// isNameCheckup: 1, // 昵称是否已审核，当tempName修改时，修改为0，审核通过改为1，审核不通过改为2
						// isAvatarCheckup: 1, // 头像是否已审核，当tempAvatar修改时，修改为0，审核通过改为1，审核不通过改为2
						openid: OPENID,
						unionid: UNIONID || '',
						tutorId: -1, // 无导师
						score: -1, // 无分数
						timestamp: timestamp, // 生成用户的时间戳
						scoreTimestamp: 0, // 分数生成时的时间戳
						reportVideoCosKey: '', // 合成视频下载地址
						testScoreList: {}, // 6个测试成绩细节
					},
				});
				if (!insertor._id) {
					ctx.body = apiResponse(20401, {}, '录入用户失败，语句返回结果中缺少_id');
					return;
				}
				// 更新头像/昵称
				const selector = await transaction.collection('vs_users').doc(insertor._id).get();
				user = selector.data;

			} else {
				user = usersData.data[0];
			}
			return user;
		});

		return result;
	},

	/**
	 * @description 根据当前的审核情况，返回用户的最新头像和昵称
	 * @param {*} user
	 */
	async getUserInfo(user) {
		if (!user || !user.openid) {
			throw '函数[getUserInfo]错误，缺少参数';
		}
		const _ = db.command;

		// 获取是否有选手表信息
		let contestantTableSelector = await db
			.collection('morphy_formSetting')
			.where({
				tableName: 'contestant',
			})
			.get();
		if (contestantTableSelector.data.length) {
			let contestantFormId = contestantTableSelector.data[0]._id;
			let contestantSelector = await db
				.collection('morphy_formData')
				.where(
					_.and([{
							formSettingId: contestantFormId, // 用户头像审核信息
						},
						{
							openid: user.openid,
						},
						{
							hidden: false
						},
						{
							'formData.isCheck': _.neq(-1), // 不等于-1，即审核中的和审核通过的都可以
						},
					])
				)
				.orderBy('createAt', 'desc')
				.get();

			// 如果有选手信息，直接返回
			if (contestantSelector.data.length && contestantSelector.data[0].formData) {
				return contestantSelector.data[0].formData;
			}
		}

		// 获取用户表信息
		let userTableSelector = await db
			.collection('morphy_formSetting')
			.where({
				tableName: 'userInfo',
			})
			.get();
		if (!userTableSelector.data.length) {
			throw apiResponse(10005, null, '未找到自定义的userInfo表字段');
		}
		let userInfoFormId = userTableSelector.data[0]._id;
		// 查看过审和不过审的各有多少
		let userSelector = await db
			.collection('morphy_formData')
			.where(
				_.and([{
						formSettingId: userInfoFormId, // 用户头像审核信息
					},
					{
						openid: user.openid,
					},
					{
						hidden: false, //非"删除"状态
					},
				])
			)
			.orderBy('createAt', 'desc')
			.get();

		// 区分过审和不过审的各有多少
		let checkList = userSelector.data.filter((item) => item.formData.isCheck !== -1);
		// 如果为空，即用户的所有头像，昵称都不过审核，返回默认头像和默认昵称
		if (!checkList.length || !checkList[0].formData) {
			return {
				name: user.name || '用户',
				avatar: user.avatar || defaultAvatar,
				isRequireWxInfo: userSelector.data.length > 0 ? false : true,
				// isRequireWxInfo: user.avatar ? false : true,
			};
		} else {
			return {
				name: checkList[0].formData.name,
				avatar: checkList[0].formData.avatar,
				isRequireWxInfo: false,
			};
		}
	},

	/**
	 * @description 更新用户头像或昵称，同时提交审核，后审机制：先生效，驳回后再处理
	 * @param {*} params
	 */
	async updateNameOrAvatar(params) {
		const {
			name,
			avatar,
			openid
		} = params;
		if (!name || !avatar || !openid) {
			throw '函数updateNameOrAvatar错误，缺少参数';
		}
		let morphyRequest = await cloud.callFunction({
			name: 'morphy_customForm',
			data: {
				$url: 'client/addFormData',
				tableName: 'userInfo',
				morphy_userinfo: {
					openid: openid,
				},
				formData: {
					name: name,
					avatar: avatar,
				},
			},
		});

		if (morphyRequest.result.code !== 0) {
			throw apiResponse(20503, morphyRequest.result, '更新用户信息失败，数据库操作失败');
		} else {
			return morphyRequest.result;
		}
	},

	/**
	 * @description 根据openid列表，返回对应的所有用户最新头像
	 * @param {*} openidList
	 */
	async getUserListInfo(openidList) {
		const _ = db.command;
		let userListJson = {};

		// 获取选手表信息
		let contestantTableSelector = await db
			.collection('morphy_formSetting')
			.where({
				tableName: 'contestant',
			})
			.get();

		if (contestantTableSelector.data.length) {
			let contestantFormId = contestantTableSelector.data[0]._id;
			let contestantList = (await db
				.collection('morphy_formData')
				.where(
					_.and([{
							formSettingId: contestantFormId, // 用户头像审核信息
						},
						{
							openid: _.in(openidList),
						},
						{
							hidden: false
						},
						{
							'formData.isCheck': _.neq(-1), // 不等于-1，即审核中的和审核通过的都可以
						},
					])
				)
				.orderBy('createAt', 'desc')
				.get()).data;
			contestantList.forEach(item => {
				if (!userListJson[item.openid]) {
					userListJson[item.openid] = item.formData;
				}
			});
		}

		let userDefaultInfoList = (
			await db
			.collection('vs_users')
			.where({
				openid: _.in(openidList),
			})
			.get()
		).data;

		let userTableSelector = await db
			.collection('morphy_formSetting')
			.where({
				tableName: 'userInfo',
			})
			.get();
		if (!userTableSelector.data.length) {
			throw apiResponse(10005, null, '未找到自定义的support表字段');
		}
		let userInfoFormId = userTableSelector.data[0]._id;
		let userCheckInfoList = (
			await db
			.collection('morphy_formData')
			.where(
				_.and([{
						formSettingId: userInfoFormId, // 用户头像审核信息
					},
					{
						openid: _.in(openidList),
					},
					{
						hidden: false, //非"删除"状态
					},
					{
						'formData.isCheck': _.neq(-1), // 不等于-1，即审核中的和审核通过的都可以
					},
				])
			)
			.orderBy('createAt', 'desc')
			.get()
		).data;

		let count = 0;
		userCheckInfoList.forEach((item) => {
			if (!userListJson[item.openid]) {
				count++;
				userListJson[item.openid] = {
					name: item.formData.name,
					avatar: item.formData.avatar,
				};
			}
		});
		userDefaultInfoList.forEach((item) => {
			if (!userListJson[item.openid]) {
				count++;
				userListJson[item.openid] = {
					name: item.name,
					avatar: item.avatar,
				};
			}
		});
		if (count < openidList.length) {
			openidList.forEach((item) => {
				if (!userListJson[item]) {
					userListJson[item] = {
						name: '用户',
						avatar: defaultAvatar,
					};
				}
			});
		}
		return userListJson;
	},

	/**
	 * @description 获取用户预约列表
	 * @param {*} uid
	 */
	async getUserReserveList(uid) {
		let selector = await db
			.collection('vs_user_rel_reserve')
			.aggregate()
			.lookup({
				from: 'vs_reserves',
				localField: 'reserve_id',
				foreignField: '_id',
				as: 'reserveList',
			})
			.sort({
				created_at: -1,
			})
			.match({
				uid: uid,
			})
			.end();
		let time = Date.now();
		// 处理预约信息
		selector.list.forEach((item) => {
			if (item.reserveList.length) {
				item.startTime = item.reserveList[0].startTime;
				item.endTime = item.reserveList[0].endTime;
				item.dateString = getDateString(item.startTime);
				item.text = getTimeText(item.reserveList[0].startTime, item.reserveList[0].endTime);
				item.isLocked = item.reserveList[0].isLocked;
				item.isExpired = item.reserveList[0].endTime < time ? true : false;
				delete item.reserveList;
			}
		});
		return selector.list;
	},

	/**
	 * @description 能否激活抽奖
	 * @param {*} uid 
	 */
	async checkCanActiveLottery(user) {
		if (!user || !user._id) {
			throw "函数执行失败，参数错误 checkCanActiveLottery";
		}
		let uid =  user._id;
		// let scoreData = (await db.collection('vs_users').doc(uid).get()).data;
		let lotteryRecord = await db
			.collection('vs_user_rel_lottery')
			.where({
				uid: uid,
			})
			.get();
		if (lotteryRecord.data.filter((item) => isCurrentDay(item.created_at)).length) {
			// 今天已抽过奖
			throw apiResponse(10004, null, '抽奖逻辑执行失败，今天已抽过奖');
		}
		let hasScoreInCurrentDay = false;
		if (isCurrentDay(user.scoreTimestamp) || isCurrentDay(user.tutorChangedAt) || isCurrentDay(user.photoAddTimestamp)) {
			hasScoreInCurrentDay = true;
		}
		// for (let key in scoreData.testScoreList) {
		// 	let item = scoreData.testScoreList[key];
		// 	if (isCurrentDay(item.created_at)) {
		// 		hasScoreInCurrentDay = true;
		// 	}
		// }

		if (!hasScoreInCurrentDay) {
			throw apiResponse(10004, null, '抽奖逻辑执行失败，今天未参与过互动测试');
		}
	},

	/**
	 * @description 执行发放奖品逻辑
	 */
	async dealLottery(uid, removeActiveLottery = false) {
		let timestamp = Date.now();
		// 随机获取一个奖品id

		// 开始事务
		const result = await db.runTransaction(async (transaction) => {
			const _ = db.command;
			// 实体
			let lotteryList = (
				await transaction
				.collection('vs_lottery_types')
				.where(_.and([{
					count: _.gt(0),
				}, {
					weight: _.gt(0),
				}]))
				.get()
			).data;
			// 虚拟
			let virtualTypeList = (
				await transaction
				.collection('vs_lottery_keys')
				.aggregate()
				.match({
					isValid: true,
				})
				.group({
					_id: '$lotteryType',
				})
				.end()
			).list.map((item) => item._id);
			lotteryList = lotteryList.filter((item) => {
				return item.isNeedVirtual === false || virtualTypeList.indexOf(item._id) >= 0;
			});
			let collect = {};
			let randomLottery = await userUtil.getRandomLottery(lotteryList);
			collect.lotteryType = randomLottery;

			// 获得数据
			let sqlData = {
				uid: uid,
				lotteryType: randomLottery._id, // 获得的奖品类型
				created_at: timestamp, // 抽奖时间
				isExchange: false, // 是否已兑换
				exchanged_at: 0, // 兑换时间
			};
			let updator;
			console.log(randomLottery.lotStatus)
			if (randomLottery.lotStatus !== 2) { // 非实体奖品，都设置为trye
				// console.log(randomLottery.lotStatus)
				sqlData.isExchange = true; // 虚拟奖品，直接将兑换状态设置为已兑换
			}
			if (randomLottery.isNeedVirtual) {
				// 如果是虚拟奖品，从cdKey池中拿取一个，记录到抽奖记录sqlData中
				let selector = await transaction
					.collection('vs_lottery_keys')
					.where(
						_.and([{
								lotteryType: randomLottery._id,
							},
							{
								isValid: true, // 有效的cdKey
							},
						])
					)
					.get();
				if (!selector.data.length) {
					throw apiResponse(20005, null, '奖品已发完');
				}
				let virtualItem = selector.data[0]; // 第一个
				collect.virtualInfo = virtualItem;
				// 把该cdKey从数据表中移除，标记为无效
				updator = await transaction
					.collection('vs_lottery_keys')
					.doc(virtualItem._id)
					.update({
						data: {
							isValid: false,
						},
					});
				if (!updator.stats || updator.stats.updated !== 1) {
					throw apiResponse(20005, updator, '奖品数量核算更新失败');
				}
				sqlData.virtual_id = virtualItem._id;

			} else {
				// 实体奖品，直接数据表-1
				updator = await transaction
					.collection('vs_lottery_types')
					.doc(randomLottery._id)
					.update({
						data: {
							count: _.inc(-1),
						},
					});
				if (!updator.stats || updator.stats.updated !== 1) {
					throw apiResponse(20005, updator, '奖品数量核算更新失败');
				}
			}

			// 将用户的 isActiveLottery 设置为false
			if (removeActiveLottery) {
				console.log('testss')
				let userUpdator = await transaction.collection('vs_operator_rel_lottery')
					.where({
						uid: uid,
						isActiveLottery: true
					})
					.update({
						data: {
							updated_at: timestamp,
							isActiveLottery: false
						}
					});
				if (!userUpdator.stats || userUpdator.stats.updated !== 1) {
					throw apiResponse(20005, updator, '用户抽奖失活无效，userUpdator执行失败');
				}
			}

			// 添加中奖记录
			let insertor = await transaction.collection('vs_user_rel_lottery').add({
				data: sqlData,
			});
			if (!insertor._id) {
				throw apiResponse(20005, updator, '抽奖记录添加失败');
			}
			collect._id = insertor._id;
			return collect;
		});
		return result;
	},
};