/**
 *
 */
const { db } = require('./cloud');
module.exports = {
	/**
	 * @description 获取已过审的应援列表
	 * @param {*} params
	 */
	async getSupportList(params) {
		let { page, size, order = 'desc', timestamp } = params;
		// 找到对应tableName的表单信息
		const targetFormInfo = await db
			.collection('morphy_formSetting')
			.where({
				tableName: 'support',
			})
			.get();
		if (!targetFormInfo.data || !targetFormInfo.data.length) {
			throw '未找到support表单信息，请重试';
		}
		const _ = db.command;
		let map = {
			formSettingId: targetFormInfo.data[0]._id,
			hidden: false, //非"删除"状态
			createAt: _.lte(params.timestamp),
			formData: {
				isCheck: 1,
			},
		};

		//查询满足条件的表单内容
		const getResult = await db
			.collection('morphy_formData')
			.where(map)
			.orderBy('createAt', order)
			.skip((page - 1) * size)
			.limit(size)
			.get();
		const getResultCount = await db.collection('morphy_formData').where(map).count();
		return {
			list: getResult.data,
			total: getResultCount.total,
		};
	},
};
