const {reserveConfig} = require('./env');
// const {db} = require('./cloud');
const { isWorkday } = require('./holiday');

module.exports = {
  /**
   * 添加单天的时间段
   * @param {*} dateString 
   */
  getDayReserve(dateString) {
    let calenderIndex = 0;
    for (let i = 0; i < reserveConfig.calendar.length; i++) {
      let item = reserveConfig.calendar[i]
      if (item.date && item.date.indexOf(dateString) >= 0) {
        calenderIndex = i;
        break;
      }
    }

    const configItem = reserveConfig.calendar[calenderIndex]
    if (!configItem.timeline) {
      throw "reserveConfig配置错误";
    }

    let dataArray = [];
    let timestamp = Date.now();
    let dayType = isWorkday(dateString) ? 1 : 0;
    configItem.timeline.forEach(item => {
      dataArray.push({
        actType: configItem.actType ? [configItem.actType] : [dayType], // 0-周末，1-工作日，2-活动3，3-活动4
        created_at: timestamp,
        currentCount: 0,
        startTime: new Date(`${dateString} ${item.startTime}`).getTime(),
        endTime: new Date(`${dateString} ${item.endTime}`).getTime(),
        supportTicket: isWorkday(dateString) ? [0, 1] : [0], // 支持的票种
        maxCount: item.resCount
      })
    });
    return dataArray;
  }
}