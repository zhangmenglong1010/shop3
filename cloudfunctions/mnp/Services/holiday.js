/**
 * 节假日检查
 */
const { getDateString } = require('./helper');
const holidayConfig = {
	// 特殊节假日
	holiday: [
		// 元旦
		// 春节

		// 清明

		// 五一

		// 端午
		// 中秋
		// 国庆

		// 2020中秋，国庆
		'20201001',
		'20201002',
		'20201003',
		'20201004',
		'20201005',
		'20201006',
		'20201007',
		'20201008',

		// 2021元旦
		'20210101',
	],
	// 调休的工作日
	workday: ['20200927', '20201010'],
};

module.exports = {
	isWorkday(timestamp) {
		if (timestamp == 'undefine' || timestamp == '') {
			throw '参数错误，isWorkday';
		}

		let isWeek = new Date(timestamp).getDay(); // 0 周日  6周六

		let ymd = getDateString(timestamp, '');

		// 判断是否为调休日 必定是工作日
		if (holidayConfig.workday.indexOf(ymd) > -1) {
			return true;
		}
		// 判断是否为假期 必定休息
		if (holidayConfig.holiday.indexOf(ymd) > -1) {
			return false;
		}
		// 判断是否为周六周天
		if (isWeek === 0 || isWeek === 6) {
      return false;
      
		} else {
      return true;
    }
	},
};
