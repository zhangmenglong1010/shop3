// 在cloud.init后再调用cloud
const cloud = require('wx-server-sdk');
const { appConfig } = require('./env')

cloud.init({
  // env: cloud.DYNAMIC_CURRENT_ENV
  env: appConfig.env_id
});
// console.log('cloud function init');

const db = cloud.database();

module.exports = {
  cloud,
  db
}