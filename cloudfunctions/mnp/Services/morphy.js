/**
 * morphy线下中台对接
 */
const axios = require('axios');
const { morphyConfig, appConfig } = require('./env');
const { db } = require('./cloud');

const COSUtil = {
	/**
	 * 获取请求用的令牌
	 */
	async getOAuth() {
		// 读缓存，无缓存或缓存过期时，重新请求
		let selector = await db
			.collection('miniprogram_admin')
			.where({
				identity: morphyConfig.dbIdentity,
			})
			.get();
		let timestamp = new Date().getTime();
		let sessionItem = {};
		// console.log(selector)
		// 不存在时创建
		if (!selector.data.length) {
			let insertor = await db.collection('miniprogram_admin').add({
				data: {
					identity: morphyConfig.dbIdentity,
					OAuth: '',
					expired: 0,
				},
			});
			console.log(insertor._id);
			sessionItem = {
				_id: insertor._id,
			};
		} else {
			sessionItem = selector.data[0];
		}
		if (sessionItem.OAuth && sessionItem.expired > timestamp) {
			return sessionItem.OAuth;

			// 重新请求
		} else {
			let result = (
				await axios.post(`${morphyConfig.domain}/ext/oauth/token`, {
					grant_type: 'client_credentials',
					client_id: morphyConfig.clientid,
					client_secret: morphyConfig.clientSecret,
				})
			).data;
			// console.log(result)
			if (!result || result.code !== 0) {
				throw result;
			}
			// 将result更新到数据表
			let updator = await db
				.collection('miniprogram_admin')
				.doc(sessionItem._id)
				.update({
					data: {
						OAuth: result.data.access_token,
						expired: timestamp + result.data.expires_in * 1000, // 毫秒
					},
				});
			return result.data.access_token;
		}
	},

	/**
	 * 获取 cos 信息
	 */
	async getCosToken(ext, module = 'report') {
		if (!ext) {
			throw '函数错误getCosToken: 缺失参数ext';
		}
		let OAuth = await COSUtil.getOAuth();
		let result = (
			await axios.get(`${morphyConfig.domain}/ext/cos/temp_key`, {
				params: {
					app_id: morphyConfig.app_id,
					env_id: appConfig.env_id,
					module: module,
					object_ext: ext,
				},
				headers: {
					Authorization: 'Bearer ' + OAuth,
				},
				// object_path: ''
			})
		).data;
		//
		if (result.code !== 0) {
			throw result;
		} else {
			return result.data;
		}
	},
};

module.exports = {
	getCosToken: COSUtil.getCosToken,
};
