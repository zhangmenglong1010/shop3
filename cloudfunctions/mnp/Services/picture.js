const { db } = require('./cloud');
const { getUserListInfo } = require('./user')

module.exports = {
	async getPictureList(params, openid) {
		const {
			size = 10, // 每页长度
			order = 'desc', // 顺序
			checkStatus = 1, // 审核状态
			isRecommend, // 是否推荐
			lastRequestTime = new Date().getTime(),
			isSticky, // 是否置顶
		} = params;
    const map = {};
    const _ = db.command;

		if (isRecommend) {
			map['recommend'] = {
				status: isRecommend,
			};
		}
		if (isSticky) {
			map['sticky'] = {
				status: isSticky,
			};
		}
		if (lastRequestTime) {
			if (order === 'desc') {
				map['createTime'] = _.lt(lastRequestTime);
			} else {
				map['createTime'] = _.gt(lastRequestTime);
			}
		}
    //
    let condition = _.and([
      map,
      _.or([
        {
          creator: openid,
        },
        {
          checkStatus: checkStatus,
        },
      ]),
    ])
		const {
			data,
			data: { len },
		} = await db
			.collection('morphy_userAlbum')
			.where(
				condition
			)
			.orderBy('createTime', order)
			.limit(size)
      .get();
    const { total } = await db.collection('morphy_userAlbum').where(condition).count();
    const openidList = data.map(item => item.creator);
    const userListInfo = await getUserListInfo(openidList);

    let list = data.map(item => {
      return {
        _id: item._id,
        userInfo: Object.assign(userListInfo[item.creator], {
          openid: item.creator
        }),
        content: item.content, // 文本内容
        createTime: item.createTime, // 更新时间
        mediaInfo: item.mediaInfo,
        isLiked: item.like.indexOf(openid) !== -1, // 自己是否点赞
        likeCount: item.like.length // 总点赞数
      }
      // item.isLiked = item.like.indexOf(openid) !== -1; // 自己是否点赞
      // item.likeCount = item.like.length; // 总点赞数
    })
		return {
			list: list,
			total,
		};
	},
};
