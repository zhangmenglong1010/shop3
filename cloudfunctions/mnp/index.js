

// 云函数入口文件
const koa = require('wx-koa').WxKoa;
const app = new koa();
const router = require('./Routers');
const {cloud} = require('./Services/cloud');

app.use(router.routes()).use(router.allowedMethods());

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext();
  return app.service(event, wxContext);

  // return {
  //   event,
  //   openid: wxContext.OPENID,
  //   appid: wxContext.APPID,
  //   unionid: wxContext.UNIONID,
  // }
}