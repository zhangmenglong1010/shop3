/**
 * 接口路由列表
 */
const Router = require('koa-router');
const router = new Router();
const adminMiddleware = require('../Middleware/admin');
const operatorMiddleware = require('../Middleware/operator');
const whitelistMiddleware = require('../Middleware/whitelist');
const adminRouter = require('./admin');
const operatorRouter = require('./operator');
const whitelistRouter = require('./whitelist');
const controllers = require('../Controllers');

// admin
router.use('/admin', adminMiddleware, adminRouter.routes());

// 运营人
router.use('/operator', operatorMiddleware, operatorRouter.routes());

// 白名单
router.use('/whitelist', whitelistMiddleware, whitelistRouter.routes());

// user
router.post('/user/update', controllers.user.update);
router.get('/user/detail', controllers.user.detail);
router.get('/user/support/list', controllers.user.supportList);
router.get('/user/photo/list', controllers.user.photoList);
router.post('/user/delete', controllers.user.delete);
router.post('/user/reserve/add', controllers.user.addReserve);
router.post('/user/reserve/remove', controllers.user.removeReserve);
router.get('/user/reserve/list', controllers.user.reserveList);
router.get('/user/phone', controllers.user.getPhoneNumber);
// 用户抽奖接口
router.post('/user/lottery', controllers.user.lottery);
router.get('/user/lottery/list', controllers.user.lotteryList);
router.post('/user/lottery/exchange', controllers.user.lotteryExchange);

// 系统白名单运营数据接口
router.get('/user/operator/data', controllers.user.getOperatorData);

// // tutor
// router.get('/tutor/list', controllers.tutor.list);
// router.get('/tutor/detail', controllers.tutor.detail);

// support
router.get('/support/list', controllers.support.list);
router.post('/support/add', controllers.support.add);

// rank
router.get('/rank/list', controllers.rank.list);

// picture
router.get('/picture/list', controllers.picture.list);
router.post('/picture/add', controllers.picture.add);
router.post('/picture/thumb', controllers.picture.thumb);

// reserve
router.get('/reserve/list', controllers.reserve.list);
router.get('/reserve/detail', controllers.reserve.detail);
router.get('/reserve/config/get', controllers.reserve.getConfig);

// file
router.get('/file/getWXACode', controllers.file.getWXACode);
router.get('/file/cos/token', controllers.file.getCosToken);

// // system info
router.get('/system/mnp/info', controllers.system.getProgramAdminInfo);

module.exports = router;
