/**
 * 接口路由列表
 * 用于服务端调用云函数
 */
const Router = require('koa-router');
const router = new Router();
const controllers = require('../Controllers');

// admin
router.get('/operator/data', controllers.whitelist.getOperatorData);
router.post('/reserve/maxCount/update', controllers.whitelist.reserveMaxCountUpdate);


module.exports = router;
