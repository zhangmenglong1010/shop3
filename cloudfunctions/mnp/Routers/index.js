/**
 * 路由入口
 */
const Router = require('wx-koa').WxRoute;
const router = new Router();
const openidMiddleware = require('../Middleware/openid');
const user = require('../Middleware/user');
const tcb = require('./tcb');
const api = require('./api');

// 云函数对服务器版本，提供服务器修改数据的接口
router.use('/tcb', openidMiddleware, tcb.routes(), api.allowedMethods());

router.use('/api', user, api.routes(), api.allowedMethods());

router.all('*', (ctx, next) => {
	ctx.body = {
		code: 10002,
		msg: '未知 [url] 或未知的 [method]',
	};
});
// router.get('/user', (ctx)=> {
//   ctx.body = {
//     code: 0,
//     "test": 123
//   }
// })
// router.all('/*', (ctx, next) => {
//   ctx.body = {
//     code: 0
//   }
// })

module.exports = router;
