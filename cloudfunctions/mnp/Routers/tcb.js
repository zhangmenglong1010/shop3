/**
 * 接口路由列表
 * 用于服务端调用云函数
 */
const Router = require('koa-router');
const router = new Router();
const controllers = require('../Controllers');

// user
router.post('/user/photo/add', controllers.user.photoAdd);
router.post('/user/video/report/add', controllers.user.videoReportAdd);

module.exports = router;
