/**
 * 接口路由列表
 * 用于服务端调用云函数
 */
const Router = require('koa-router');
const router = new Router();
const controllers = require('../Controllers');

router.post('/lottery/sign', controllers.operator.lotterySign); // 分配一次抽奖名额

module.exports = router;
