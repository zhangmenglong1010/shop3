/**
 * 接口路由列表
 * 用于服务端调用云函数
 */
const Router = require('koa-router');
const router = new Router();
const controllers = require('../Controllers');

// admin
router.get('/checkup/list', controllers.admin.checkupList);
router.post('/checkup/name', controllers.admin.checkupName);
router.post('/checkup/avatar', controllers.admin.checkupAvatar);
router.post('/reserve/add', controllers.admin.addReserve);
router.post('/reserve/remove', controllers.admin.removeReserve);
router.get('/reserve/list', controllers.admin.reserveList);
router.get('/reserve/timeline/list', controllers.admin.reserveTimelineList);
router.post('/test/remove', controllers.admin.remove);
router.post('/tutor/add', controllers.tutor.add);
router.get('/userAvgScore', controllers.admin.userAvgScore);
router.get('/lottery/list', controllers.admin.lotteryList);
router.get('/lottery/clear', controllers.admin.lotteryClear);

module.exports = router;
