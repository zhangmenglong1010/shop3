/**
 * 中间件 - 超级管理员身份验证
 */
const {
  apiResponse
} = require('../Services/http');
const {
  db
} = require('../Services/cloud');

module.exports = async (ctx, next) => {
  const user = ctx.request.user;
  let selector = await db.collection('miniprogram_admin')
    .get();

  if (!selector.data.length || !selector.data[0].whitelist) {
    throw apiResponse(10004, null, "无白名单");
  }
  let whitelist = selector.data[0].whitelist;
  if (whitelist.indexOf(user.openid) < 0) {
    throw apiResponse(10004, null, "无权限");

  } else {
    await next();
  }
};