/**
 * 中间件 - 用户信息
 */
const {
	apiResponse
} = require('../Services/http');
const {
	db
} = require('../Services/cloud');
const {
	addErrorLog
} = require('../Services/helper');
const {
	getUserInfo,
	findOrCreateUser
} = require('../Services/user');
const {
	defaultAvatar
} = require('../Services/env');

module.exports = async (ctx, next) => {
	const {
		OPENID,
		UNIONID
	} = ctx;
	if (!OPENID) {
		ctx.body = apiResponse(20003, null, '数据异常，openid为空');
		return;
	}

	let user = await findOrCreateUser(OPENID);

	if (UNIONID && !user.unionid) {
		// 更新用户的unionid
		let updator = await db
			.collection('vs_users')
			.doc(user._id)
			.update({
				data: {
					unionid: UNIONID,
				},
			});
		user.unionid = UNIONID;
	}
	let {
		name,
		avatar,
		isRequireWxInfo
	} = await getUserInfo(user);
	user.name = name;
	user.avatar = avatar;
	user.isRequireWxInfo = isRequireWxInfo;
	ctx.request.user = user;
	try {
		await next();
	} catch (err) {
		let body = null;
		if (err.code && err.msg) {
			body = err;
		} else {
			body = apiResponse(20001, err, '后台错误');
		}
		// 后台错误，记录到数据库
		if (typeof body.code === 'number' && body.code > 10000) {
			await addErrorLog({
				uid: user._id,
				created_at: Date.now(),
				api: ctx.url,
				body: ctx.request.body,
				msg: body
			});
		}
		ctx.body = body;
	}
};