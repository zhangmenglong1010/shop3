/**
 * 中间件 - 系统工作人员身份验证
 * 发放抽奖资格
 */
const { apiResponse } = require('../Services/http');
const { db } = require('../Services/cloud');

module.exports = async (ctx, next) => {
	const user = ctx.request.user;
	const _ = db.command;
  let whitelistTableSelector = await db
			.collection('morphy_formSetting')
			.where({
				tableName: 'whitelist',
			})
			.get();
		if (!whitelistTableSelector.data.length) {
			throw apiResponse(10005, null, '未找到自定义的whitelist表字段');
		}
		let whitelistFormId = whitelistTableSelector.data[0]._id;
		let userCheckInfoList = (
			await db
			.collection('morphy_formData')
			.where(
				_.and([{
						formSettingId: whitelistFormId, // 白名单审核信息
					},
					{
						openid: user.openid,
					},
					{
						hidden: false, //非"删除"状态
					},
					{
						'formData.ischeck': 1, // 审核通过
					},
				])
			)
			.orderBy('createAt', 'desc')
			.get()
		).data;
  if (!userCheckInfoList.length) {
    ctx.body = apiResponse(10004, null, '权限不足，无管理权限');
    return;
    
  } else {
    await next();
  }
};
