/**
 * 中间件 - 超级管理员身份验证
 */
const { apiResponse } = require('../Services/http');
const { db } = require('../Services/cloud');

module.exports = async (ctx, next) => {
  const user = ctx.request.user;
  if (!user.role || user.role !== 1) {
    ctx.body = apiResponse(10004, null, '权限不足，无管理权限');
    return;
    
  } else {
    await next();
  }
};
