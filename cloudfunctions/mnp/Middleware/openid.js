/**
 * 中间件 - 用户信息
 * 自定义openid请求参数，用于服务器请求云函数，
 * 服务器端使用accessToken调用云函数，没有登录态
 */
const { apiResponse } = require('../Services/http');
const { db } = require('../Services/cloud');

module.exports = async (ctx, next) => {
	let OPENID = ctx.request.body.openid;
	if (!OPENID || typeof OPENID !== 'string') {
		ctx.body = apiResponse(20003, null, '[openid]数据异常');
		return;
	}
	// 获取openid对应的用户数组，判断长度是否为1
	let usersData = await db
		.collection('vs_users')
		.where({
			openid: OPENID,
		})
		.get();
	let user = null;
	// 多于1个用户，抛出异常
	if (usersData.data.length > 1) {
		user = usersData.data[0];
    // ctx.body = apiResponse(20003, null, '数据异常，出现两个或以上该openid对应的用户');
		// return;
		
  // 未找到该用户
	} else if (usersData.data.length === 0) {
		ctx.body = apiResponse(10001, null, '未找到该openid对应的用户');
		return;
		
	} else {
		user = usersData.data[0];
	}
	ctx.request.user = user;
	try {
		await next();
	} catch (err) {
    if (err.code && err.msg) {
      ctx.body = err;
    } else {
      ctx.body = apiResponse(20001, err, '后台错误');
    }
	}

	// const { OPENID, APPID, UNIONID } = cloud.getWXContext();
};
