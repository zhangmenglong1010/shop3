const {
  db
} = require('../Services/cloud');
const {
  apiResponse,
  requestValidation
} = require('../Services/http');
const {
  checkCanActiveLottery
} = require('../Services/user');

module.exports = {
  /**
   * @description 为用户分配一次抽奖名额
   * @param {*} ctx 
   */
  async lotterySign(ctx) {
    const user = ctx.request.user;
    const params = await requestValidation(ctx.request.body, {
      openid: 'required|string|min:1|max:255|exists:vs_users,openid', // 当前量
    });

    let targetUser = (await db.collection('vs_users')
      .where({
        "openid": params.openid
      })
      .get()).data[0];

    // 先检查能否激活抽奖
    await checkCanActiveLottery(targetUser);

    let timestamp = Date.now();
    let selector = await db.collection('vs_operator_rel_lottery')
      .where({
        uid: targetUser._id,
        isActiveLottery: true
      })
      .get();
    // 如果有，直接返回
    if (selector.data.length) {
      ctx.body = apiResponse(0, null, '成功激活一次抽奖机会');
      return;
    }

    let insertor = await db.collection('vs_operator_rel_lottery')
      .add({
        data: {
          created_at: timestamp,
          uid: targetUser._id, // 分配给的用户id
          operator_id: user._id, // 运营人员id
          isActiveLottery: true, // 是否激活
        }
      });

    // 本身即为true，或updator执行成功
    if (insertor._id) {
      ctx.body = apiResponse(0, null, '成功激活一次抽奖机会');

    } else {
      throw apiResponse(20005, null, '激活失败，updator执行失败');
    }
  }
}