const {
	apiResponse,
	requestValidation
} = require('../Services/http');
const {
	db,
	cloud
} = require('../Services/cloud');
const {
	adminOpenidList,
	reserveConfig
} = require('../Services/env');
const {
	getIsUrlExist,
	getDateString
} = require('../Services/helper');
const {
	updateNameOrAvatar,
	getUserReserveList,
	checkCanActiveLottery,
	dealLottery
} = require('../Services/user');

module.exports = {

	async getOperatorData(ctx) {
		let user = ctx.request.user;
		let selector = await db.collection('miniprogram_admin')
			.get();

		if (!selector.data.length || !selector.data[0].whitelist) {
			throw apiResponse(10004, null, "无白名单");
		}
		let whitelist = selector.data[0].whitelist;
		if (whitelist.indexOf(user.openid) < 0) {
			throw apiResponse(10004, null, "无权限");
		}
		// 每日到场互动人数，预约/未预约
		const _ = db.command;
		let timestamp = Date.now();
		// let timestamp = new Date("2020-10-01").getTime();
		let dateString = getDateString(timestamp);
		// 每天开始时间，结束时间
		let startTime = new Date(`${dateString} 00:00:01`).getTime();
		let endTime = new Date(`${dateString} 23:59:00`).getTime();

		// 互动人数
		let activeCount = await db.collection('vs_users')
			.where({
				activityTimestamp: _.gte(0)
			}).count();
		// 每日互动人数
		let dayActiveCount = await db.collection('vs_users')
			.where(
				_.and([{
						activityTimestamp: _.gte(startTime)
					},
					{
						activityTimestamp: _.lte(endTime)
					}
				])).count();
		// 每日预约人列表
		let reserveList = await db.collection('vs_reserves')
				.aggregate()
				.match(_.and([
					{
						startTime: _.gte(startTime),
						endTime: _.lte(endTime)
					}
				]))
				.lookup({
					from: 'vs_user_rel_reserve',
					localField: '_id',
					foreignField: 'reserve_id',
					as: 'users',
				})
				.end();
			let userIdList = [];
			reserveList.list.forEach(item => {
				userIdList = userIdList.concat(item.users.map(cell => cell.uid))
			});
		let reserveTotal = userIdList.length;
		// 其中当天玩了交互的人数
		let dayReserveActiveCount = await db.collection('vs_users')
			.where(
				_.and([
					{
						activityTimestamp: _.gte(startTime)
					},
					{
						activityTimestamp: _.lte(endTime)
					},
					{
						_id: _.in(userIdList)
					}
				]))
				.count();

		// 每日完成了6项互动完成人数，总人数
		let totalScoreCount = await db.collection('vs_users')
			.where({
				score: _.gte(0)
			}).count();
		let dayTotalScoreCount = await db.collection('vs_users')
			.where(
				_.and([{
						scoreTimestamp: _.gte(startTime)
					},
					{
						scoreTimestamp: _.lte(endTime)
					},
					{
						score: _.gte(0)
					}
				])).count();

		// 抽奖人数，当天/总量，兑换人数
		let lotteryCount = await db.collection('vs_user_rel_lottery')
			.where({
				created_at: _.gte(0)
			}).count();

		let dayLotteryCount = await db.collection('vs_user_rel_lottery')
			.where(
				_.and([{
					created_at: _.gte(startTime)
					},
					{
						created_at: _.lte(endTime)
					}
				])).count();

		// 当日实体奖人数
		let matIdList = (await db.collection('vs_lottery_types')
				.where({
					lotStatus: 2
				})
				.get()).data.map(item => item._id);

		let dayLotteryMatCount = await db.collection('vs_user_rel_lottery')
			.where(
				_.and([
					{
						created_at: _.gte(startTime)
					},
					{
						created_at: _.lte(endTime)
					},
					{
						lotteryType: _.in(matIdList)
					}
				]))
				.count();

		// 每日兑换人数
		let dayExchangeLotteryCount = await db.collection('vs_user_rel_lottery')
			.where(
				_.and([
					{
						created_at: _.gte(startTime)
					},
					{
						created_at: _.lte(endTime)
					},
					{
						isExchange: true
					}
				])).count();

		ctx.body = apiResponse(0, {
			activeCount: activeCount.total, // 互动总人数
			dayActiveCount: dayActiveCount.total, // 今天互动总人数
			totalScoreCount: totalScoreCount.total, // 6个测试总人数
			dayTotalScoreCount: dayTotalScoreCount.total, // 今天6个测试总人数
			lotteryCount: lotteryCount.total, // 抽奖总人数
			dayLotteryCount: dayLotteryCount.total, // 今天抽奖人数
			dayLotteryMatCount: dayLotteryMatCount.total, // 今天的实体奖数量
			dayExchangeLotteryCount: dayExchangeLotteryCount.total, // 今天已兑换人数
			reserveCount: reserveTotal, // 预约总人数
			// 预约后到场
			dayReserveActiveCount: dayReserveActiveCount.total,
			// 放飞机
			dayReserveUnactiveCount: reserveTotal - dayReserveActiveCount.total,
			// 未预约后到场
			dayUnreserveActiveCount: dayActiveCount.total - dayReserveActiveCount.total
		});
	},

	/**
	 * @description 获取用户手机号码
	 * @method GET
	 * @param {*} ctx
	 */
	async getPhoneNumber(ctx) {
		const params = await requestValidation(ctx.request.body, {
			cloudID: 'string',
		});
		let phone = await cloud.getOpenData({
			list: [params.cloudID],
		});
		ctx.body = apiResponse(0, phone);
	},
	/**
	 * @description 更新用户信息
	 * @method POST
	 * @param {*} ctx
	 */
	async update(ctx) {
		const user = ctx.request.user;
		const params = await requestValidation(ctx.request.body, {
			name: 'string|max:255',
			avatar: 'string|max:255',
			gender: 'integer',
			language: 'string|max:255',
			city: 'string|max:255',
			country: 'string|max:255',
			province: 'string|max:255',
		});
		if (!user.isRequireWxInfo) {
			// console.log('duplicate');
			if (params.name === user.name) {
				params.name = undefined;
				// console.log('duplicate name');
			}
			if (params.avatar === user.avatar) {
				params.avatar = undefined;
				// console.log('duplicate avatar');
			}
		}

		let keyList = ['gender', 'language', 'city', 'country', 'province'];

		for (let key in params) {
			if (params[key] !== undefined && params[key] === user[key] && keyList.indexOf(key) >= 0) {
				params[key] = undefined;
			}
		}

		let json = {};
		let count = 0;
		keyList.forEach((item) => {
			if (params[item] !== undefined) {
				count++;
				json[item] = params[item];
			}
		});
		if (count === 0 && !params.name && !params.avatar && !user.isRequireWxInfo) {
			ctx.body = apiResponse(0, null, '更新成功');
			return;
		}

		if (params.name || params.avatar) {
			// console.log('update to morphy');
			// 更新用户头像和昵称，并默认提交审核
			await updateNameOrAvatar({
				name: params.name || user.name,
				avatar: params.avatar || user.avatar,
				openid: user.openid,
			});
		}

		if (count > 0) {
			let updateMsg = await db.collection('vs_users').doc(user._id).update({
				data: json,
			});
			if (updateMsg.stats.updated !== 1) {
				ctx.body = apiResponse(20503, null, `更新用户信息未成功，更新条数为${updateMsg.stats.updated}`);
				return;
			}
		}

		let selector = await db.collection('vs_users').doc(user._id).get();
		selector.data.name = params.name || user.name;
		selector.data.avatar = params.avatar || user.avatar;
		ctx.body = apiResponse(0, selector.data);
	},

	/**
	 * @description 获取用户信息
	 * @method GET
	 * @param {*} ctx
	 */
	async detail(ctx) {
		const user = ctx.request.user;

		let score = user.score >= 0 ? user.score : 0;
		const _ = db.command;
		// 计算全服排名
		// 获取该测试中高于该分数的人数
		let rankCount = await db
			.collection('vs_users')
			.where({
				score: _.gt(score),
			})
			.count();

		if (user.testScoreList) {
			for (let key in user.testScoreList) {
				let item = user.testScoreList[key];
				if (item.param) {
					item.param = 'hidden';
				}
			}
		}

		// 获取合影字段
		user.reportVideoUrl = '';
		let videoReport = await db
			.collection('vs_reports')
			.where({
				'uid': user._id
			})
			.orderBy('created_at', 'desc')
			.get();
		if (videoReport.data.length) {
			user.reportVideoUrl = videoReport.data[0].videoUrl;
		}

		// 抽奖预案 - 获取抽奖是否激活
		user.canLottery = false;
		let lotteryCount = await db.collection('vs_operator_rel_lottery')
			.where({
				uid: user._id,
				isActiveLottery: true
			})
			.get();
		if (lotteryCount.data.length > 0) {
			user.canLottery = true;
		}

		ctx.body = apiResponse(0, Object.assign(user, {
			currentRank: rankCount.total,
		}));
	},

	/**
	 * @description 获取用户的预约记录列表
	 * @param {*} ctx
	 */
	async reserveList(ctx) {
		const user = ctx.request.user;

		let reserveList = await getUserReserveList(user._id);

		ctx.body = apiResponse(0, reserveList, '获取用户的预约记录列表成功');
	},

	/**
	 * @description 获取用户应援列表
	 * @method GET
	 * @param {*} ctx
	 */
	async supportList(ctx) {
		// const user = ctx.request.user;
		// const params = await requestValidation(ctx.request.body, {
		// 	page: 'integer|min:1',
		// 	pageSize: 'integer|max:20|min:1',
		// });
		// // let timestamp = params.timestamp || new Date().getTime() - 3600 * 1000 * 24 * 30;
		// if (params.page === undefined) {
		// 	params.page = 1;
		// }
		// if (params.pageSize === undefined) {
		// 	params.pageSize = 10;
		// }
		// let selector = await db
		// 	.collection('vs_supports')
		// 	.where({
		// 		uid: user._id,
		// 	})
		// 	.orderBy('timestamp', 'desc')
		// 	.skip((params.page - 1) * params.pageSize)
		// 	.limit(params.pageSize)
		// 	// .limit(quantity)
		// 	.get();
		// const countor = await db
		// 	.collection('vs_supports')
		// 	.where({
		// 		uid: user._id,
		// 	})
		// 	.count();
		// ctx.body = apiResponse(
		// 	0,
		// 	{
		// 		page: params.page,
		// 		pageSize: params.pageSize,
		// 		total: countor.total,
		// 		pageTotal: Math.ceil(countor.total / params.pageSize),
		// 		list: selector.data,
		// 	},
		// 	'获取用户应援列表成功'
		// );
	},

	/**
	 * @description 获取合影列表
	 * @method GET
	 * @param {*} ctx
	 */
	async photoList(ctx) {
		const user = ctx.request.user;
		// let timestamp = params.timestamp || new Date().getTime() - 3600 * 1000 * 24 * 30;
		const params = await requestValidation(ctx.request.body, {
			page: 'integer|min:1',
			pageSize: 'integer|max:20|min:1',
		});
		// let timestamp = params.timestamp || new Date().getTime() - 3600 * 1000 * 24 * 30;
		if (params.page === undefined) {
			params.page = 1;
		}
		if (params.pageSize === undefined) {
			params.pageSize = 10;
		}

		let selector = await db
			.collection('vs_photos')
			.where({
				uid: user._id,
			})
			.orderBy('created_at', 'desc')
			.skip((params.page - 1) * params.pageSize)
			.limit(params.pageSize)
			.get();

		const countor = await db
			.collection('vs_photos')
			.where({
				uid: user._id,
			})
			.count();

		for (let i = 0; i < selector.data.length; i++) {
			let item = selector.data[i];
			if (item.video && item.video.path && !item.video.mp4) {
				let videoMp4 = item.video.path.replace(/\.webm$/, '.mp4');
				let isMp4Exist = await getIsUrlExist(videoMp4);
				if (isMp4Exist) {
					item.video.mp4 = videoMp4;
					// 更新mp4到数据库中
					let updator = await db.collection('vs_photos')
						.doc(item._id)
						.update({
							data: {
								"video.mp4": videoMp4
							}
						});
					if (!updator.stats || updator.stats.updated !== 1) {
						console.log("updator执行错误")
					}
				}
			}
		}

		ctx.body = apiResponse(
			0, {
				page: params.page,
				pageSize: params.pageSize,
				total: countor.total,
				pageTotal: Math.ceil(countor.total / params.pageSize),
				list: selector.data,
			},
			'获取用户合影列表成功'
		);
	},

	/**
	 *
	 * @param {*} ctx
	 */
	async photoAdd(ctx) {
		const user = ctx.request.user;
		const params = await requestValidation(ctx.request.body, {
			teamId: 'required|string|min:1|max:255',
			photos: 'required|array', // 合影照片信息
			video: 'required|object', // 合影视频信息
			'photos.*.size': 'required|integer',
			'photos.*.path': 'required|string|min:1|max:512',
			'video.size': 'required|integer',
			'video.path': 'required|string|min:1|max:512',
		});
		let insertor = await db.collection('vs_photos').add({
			data: {
				uid: user._id,
				created_at: new Date().getTime(),
				photos: params.photos,
				video: params.video,
				teamId: params.teamId, // 现场选择合影的电竞团队ID（用做统计用）
			},
		});

		if (!insertor._id) {
			ctx.body = apiResponse(20401, {}, '添加合影失败，insertor执行失败');
			return;
		}

		let updateMsg = await db.collection('vs_users').doc(user._id).update({
			data: {
				photoAddTimestamp: Date.now(),
				activityTimestamp: Date.now()
			}
		});
		if (updateMsg.stats.updated !== 1) {
			ctx.body = apiResponse(20503, null, `更新用户信息未成功，更新条数为${updateMsg.stats.updated}`);
			return;
		}

		ctx.body = apiResponse(0, params, '添加合影成功');
	},

	/**
	 * @description 添加合影视频成功
	 * @param {*} ctx
	 */
	async videoReportAdd(ctx) {
		const user = ctx.request.user;
		const params = await requestValidation(ctx.request.body, {
			videoUrl: 'required|string', // 视频报告地址
			tutorId: 'required|integer|min:0|max:12',
			level: 'required|integer', // 分数等级
			totalScore: 'required',
			totalScoreDetail: "required"
		});
		let insertor = await db.collection('vs_reports').add({
			data: Object.assign(params, {
				uid: user._id,
				created_at: Date.now(),
			})
		});

		if (!insertor._id) {
			ctx.body = apiResponse(20401, {}, '添加视频报告失败，insertor执行失败');
			return;
		}

		// let selector = await db.collection('vs_photos').doc(insertor._id).get();
		ctx.body = apiResponse(0, params, '添加视频报告成功');
	},

	/**
	 * @description 删除用户数据
	 * @method POST
	 * @param {*} ctx
	 */
	async delete(ctx) {
		// const user = ctx.request.user;

		// let deletor = await db
		// 	.collection('vs_users')
		// 	.where({
		// 		_id: user._id,
		// 	})
		// 	.remove();

		// if (deletor.stats && deletor.stats.removed === 1) {
		// 	ctx.body = apiResponse(0, null, '删除用户成功');
		// } else {
		// 	ctx.body = apiResponse(20003, null, '删除用户成失败，未知的数据库操作结果');
		// }
	},

	/**
	 * @description 用户预约某个时间段
	 * @param {*} ctx
	 */
	async addReserve(ctx) {
		const user = ctx.request.user;
		const params = await requestValidation(ctx.request.body, {
			reserve_id: 'required|string|exists:vs_reserves', // 预约时间段的id
			idcard: 'required|idcard|min:15|max:18', // 身份证
			name: 'required|string|min:1|max:50', // 姓名
			ticketTypeId: 'required|integer', // 票种id
			phoneNumber: 'string|min:5|max:20', // 用户的手机号，一个用户只有一个预约，除非预约已过期
			member: 'array', // 随同成员，可为空
			'member.*.idcard': 'required|idcard|distinct', // 成员身份证id
			'member.*.name': 'required|string|min:1|max:50', // 成员名称
		});

		// 查询该票号是否已用过，已存在于数据表vs_user_rel_reserve中
		// 只校验该用户下的ticket，因为ticket有可能被其他人误用过，这种情况应该继续允许该用户预约
		// let ticketSelector = await db
		// 	.collection('vs_user_rel_reserve')
		// 	.where({
		// 		uid: user._id,
		// 		ticket: params.ticket,
		// 	})
		// 	.get();
		// if (ticketSelector.data.length) {
		// 	// 该票号已被其他 reserve_id 用过
		// 	let ticketItem = ticketSelector.data[0];
		// 	let tempReserveItem = (await db.collection('vs_reserves').doc(ticketItem.reserve_id).get()).data;
		// 	if (tempReserveItem) {
		// 		throw apiResponse(10004, null, `预约失败，该票号已被用于[${getDateString(tempReserveItem.startTime)} ${tempReserveItem.text}]的预约，请先取消`);

		// 	} else { // 无效的reserve_id，脏数据，默认删掉
		// 		await db
		// 		.collection('vs_user_rel_reserve')
		// 		.where({
		// 			_id: ticketItem._id,
		// 		})
		// 		.remove();
		// 	}
		// }

		let timestamp = new Date().getTime();
		let reserveItem = (await db.collection('vs_reserves').doc(params.reserve_id).get()).data;

		// 小于当前时间
		if (reserveItem.endTime < timestamp) {
			throw apiResponse(10004, null, `预约失败，无法预约过去的时间段`);
		}

		// 查询该用户的预约记录，检查是否已预约过该时间段

		let recordSelector = await getUserReserveList(user._id);
		if (recordSelector.length) {
			for (let i = 0; i < recordSelector.length; i++) {
				let item = recordSelector[i];
				if (item.endTime > timestamp) {
					throw apiResponse(10006, null, '预约失败，您已有待参加的预约，请先取消');
					break;
				}
			}
		}

		// 检查该时间段是否满员，当剩下1个名额时，允许预约，即使有随同成员
		if (reserveItem.currentCount >= reserveItem.maxCount) {
			// 预约已满
			throw apiResponse(10004, null, '预约失败，该时间段已满员');
		}
		//  else if (reserveItem.actType.indexOf(reserveConfig.actType.indexOf('特殊活动')) >= 0) {
		// 	// 活动日
		// 	throw apiResponse(10004, null, '预约失败，活动时间段无法预约');
		// }
		else if (reserveItem.isLocked) {
			throw apiResponse(10004, null, '预约失败，该时间段已锁定，无法预约');
			//
		} else if (
			reserveItem.actType.indexOf(reserveConfig.actType.indexOf('家庭日')) === -1 &&
			params.member &&
			params.member.length
		) {
			// 非家庭日
			throw apiResponse(10004, null, '预约失败，非家庭日无法添加随同成员');
		} else if (reserveItem.supportTicket.indexOf(params.ticketTypeId) === -1) {
			throw apiResponse(10004, null, '预约失败，不支持的票种');
		}

		// 开始添加预约逻辑，分别是 vs_reserves表的预约数+1 和 用户的reserve关系表 加一条记录
		// 在事务中完成
		const result = await db.runTransaction(async (transaction) => {
			// vs_reserves 预约人数+1+member人数
			let incCount = 1 + (params.member ? params.member.length : 0);

			const _ = db.command;
			let updator = await transaction
				.collection('vs_reserves')
				.doc(params.reserve_id)
				.update({
					data: {
						// 对timeline的index下标进行自增+1+N个同行者
						currentCount: _.inc(incCount),
					},
				});
			if (!updator.stats || updator.stats.updated !== 1) {
				throw apiResponse(20004, null, '预约失败，请重新预约，updator操作失败');
			}

			// 在 用户-预约关系表 中，添加预约记录
			let insertor = await transaction.collection('vs_user_rel_reserve').add({
				data: {
					uid: user._id,
					reserve_id: params.reserve_id,
					phoneNumber: params.phoneNumber,
					member: params.member || [],
					idcard: params.idcard,
					name: params.name,
					created_at: timestamp,
				},
			});
			if (!insertor._id) {
				throw apiResponse(20004, null, '预约失败，请重新预约，insertor操作失败');
			}
		});

		ctx.body = apiResponse(0, null, '用户预约成功');
	},

	/**
	 * @description 用户取消某个预约
	 * @param {*} ctx
	 */
	async removeReserve(ctx) {
		const user = ctx.request.user;
		const params = await requestValidation(ctx.request.body, {
			reserveRecord_id: 'required|string|exists:vs_user_rel_reserve', // 预约记录的id
		});

		let recordSelector = (await db.collection('vs_user_rel_reserve').doc(params.reserveRecord_id).get()).data;
		let reserveSelector = (
			await db
			.collection('vs_reserves')
			.where({
				_id: recordSelector.reserve_id,
			})
			.get()
		).data;
		if (!reserveSelector.length) {
			// 未找到对应的预约id，无效的预约记录，直接删除
			await db
				.collection('vs_user_rel_reserve')
				.where({
					_id: params.reserveRecord_id,
				})
				.remove();
			ctx.body = apiResponse(0, null, '用户取消预约成功');
			return;
		}

		let timestamp = new Date().getTime();

		if (recordSelector.uid !== user._id) {
			throw apiResponse(10004, null, '取消预约失败，用户未预约过该记录');
		} else if (reserveSelector.length && reserveSelector[0].endTime < timestamp) {
			throw apiResponse(10004, null, '该预约已失效，无需删除');
		}

		// 开始添加取消预约逻辑，分别是 vs_reserves表的预约数-1 和 用户的reserve关系表 删除一条记录
		// 在事务中完成
		const result = await db.runTransaction(async (transaction) => {
			let incCount = -1 - (recordSelector.member ? recordSelector.member.length : 0);
			// vs_reserves 预约人数-1以及同行成员人数
			let timestamp = new Date().getTime();
			const _ = db.command;
			await transaction
				.collection('vs_reserves')
				.doc(recordSelector.reserve_id)
				.update({
					data: {
						// 对timeline的index下标进行自增-1-N，
						currentCount: _.inc(incCount),
					},
				});

			// 在用户预约关系表中，删除预约记录
			await transaction
				.collection('vs_user_rel_reserve')
				.where({
					_id: params.reserveRecord_id,
				})
				.remove();
		});

		ctx.body = apiResponse(0, null, '用户取消预约成功');
	},

	/**
	 * @description 用户抽奖，每天仅有一次机会，前提是用户在当天玩了测试游戏
	 * @param {object} ctx
	 */
	async lottery(ctx) {
		const user = ctx.request.user;

		let checkActive = false; // 是否开启预案
		// throw apiResponse(10004, null, '抽奖失败，未开启抽奖，敬请期待');

		// 先判断能否激活抽奖
		await checkCanActiveLottery(user);

		// 再判断是否已激活抽奖，未激活则找工作人员
		if (checkActive) {
			let lotteryCount = await db.collection('vs_operator_rel_lottery')
				.where({
					uid: user._id,
					isActiveLottery: true
				})
				.get();
			if (!lotteryCount.data.length) {
				throw apiResponse(10003, null, '抽奖失败，请联系工作人员激活名额');
			}
		}

		// 执行发放奖品逻辑
		let result = await dealLottery(user._id, checkActive);
		delete result.lotteryType.weight;
		delete result.lotteryType.count;

		ctx.body = apiResponse(
			0,
			result,
			'抽奖成功'
		);
	},

	/**
	 * @description 获取用户中奖记录列表
	 * @param {*} ctx
	 */
	async lotteryList(ctx) {
		const user = ctx.request.user;
		let selector = await db
			.collection('vs_user_rel_lottery')
			.aggregate()
			.lookup({
				from: 'vs_lottery_types',
				localField: 'lotteryType',
				foreignField: '_id',
				as: 'lotteryTypeList',
			})
			.lookup({
				from: 'vs_lottery_keys',
				localField: 'virtual_id',
				foreignField: '_id',
				as: 'virtualInfoList',
			})
			.sort({
				created_at: -1,
			})
			.match({
				uid: user._id,
			})
			.end();
		selector.list.forEach((item) => {
			if (item.lotteryTypeList.length) {
				delete item.lotteryTypeList[0].count;
				delete item.lotteryTypeList[0].weight;
				item.lotteryType = item.lotteryTypeList[0];
			}
			delete item.lotteryTypeList;
			if (item.virtualInfoList.length) {
				item.virtualInfo = {
					content: item.virtualInfoList[0].content,
					_id: item.virtualInfoList[0]._id
				};
			}
			delete item.virtualInfoList;
		});
		ctx.body = apiResponse(0, selector.list, '获取用户中奖记录列表成功');
	},

	/**
	 * @description 用户抽奖后的兑换，核销接口
	 * @param {object} ctx
	 */
	async lotteryExchange(ctx) {
		const user = ctx.request.user;
		const params = await requestValidation(ctx.request.body, {
			record_id: 'required|string|exists:vs_user_rel_lottery', // 抽奖记录的id
		});

		let lotteryRecord = (await db.collection('vs_user_rel_lottery').doc(params.record_id).get()).data;
		if (lotteryRecord.uid !== user._id) {
			throw apiResponse(10004, null, '核销失败，该奖不属于本用户');
		} else if (lotteryRecord.isExchange !== false) {
			throw apiResponse(10004, null, '核销失败，该奖已被兑换');
		}

		let timestamp = Date.now();
		let updator = await db
			.collection('vs_user_rel_lottery')
			.doc(params.record_id)
			.update({
				data: {
					isExchange: true, // 是否已兑换
					exchanged_at: timestamp, // 兑换时间
				},
			});
		if (!updator.stats || updator.stats.updated !== 1) {
			throw apiResponse(20005, updator, '奖品核销失败，数据表未更新');
		}
		ctx.body = apiResponse(0, null, '奖品兑换核销成功');
	},

	/**
	 *
	 */
	async adminMigrate(ctx) {
		// const _ = db.command;
		// let updator = await db
		// 	.collection('users')
		// 	.where({
		// 		openid: _.in(adminOpenidList),
		// 	})
		// 	.update({
		// 		data: {
		// 			role: 1,
		// 		},
		// 	});
		// for (let i = 0; i < openidList.length; i++) {
		// 	let openid = openidList[i];
		// }
	},
};