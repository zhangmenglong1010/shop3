const {
  apiResponse,
  requestValidation
} = require('../Services/http');

const {
  getDateString
} = require('../Services/helper');

const {
  db,
} = require('../Services/cloud');

module.exports = {
  async getOperatorData(ctx) {
    // 每日到场互动人数，预约/未预约
    const _ = db.command;
    let timestamp = Date.now();
    // let timestamp = new Date("2020-10-01").getTime();
    let dateString = getDateString(timestamp);
    // 每天开始时间，结束时间
    let startTime = new Date(`${dateString} 00:00:01`).getTime();
    let endTime = new Date(`${dateString} 23:59:00`).getTime();

    // 互动人数
    let activeCount = await db.collection('vs_users')
      .where({
        activityTimestamp: _.gte(0)
      }).count();
    // 每日互动人数
    let dayActiveCount = await db.collection('vs_users')
      .where(
        _.and([{
            activityTimestamp: _.gte(startTime)
          },
          {
            activityTimestamp: _.lte(endTime)
          }
        ])).count();
    // 每日预约人列表
    let reserveList = await db.collection('vs_reserves')
      .aggregate()
      .match(_.and([{
        startTime: _.gte(startTime),
        endTime: _.lte(endTime)
      }]))
      .lookup({
        from: 'vs_user_rel_reserve',
        localField: '_id',
        foreignField: 'reserve_id',
        as: 'users',
      })
      .end();
    let userIdList = [];
    reserveList.list.forEach(item => {
      userIdList = userIdList.concat(item.users.map(cell => cell.uid))
    });
    let reserveTotal = userIdList.length;
    // 其中当天玩了交互的人数
    let dayReserveActiveCount = await db.collection('vs_users')
      .where(
        _.and([{
            activityTimestamp: _.gte(startTime)
          },
          {
            activityTimestamp: _.lte(endTime)
          },
          {
            _id: _.in(userIdList)
          }
        ]))
      .count();

    // 每日完成了6项互动完成人数，总人数
    let totalScoreCount = await db.collection('vs_users')
      .where({
        score: _.gte(0)
      }).count();
    let dayTotalScoreCount = await db.collection('vs_users')
      .where(
        _.and([{
            scoreTimestamp: _.gte(startTime)
          },
          {
            scoreTimestamp: _.lte(endTime)
          },
          {
            score: _.gte(0)
          }
        ])).count();

    // 抽奖人数，当天/总量，兑换人数
    let lotteryCount = await db.collection('vs_user_rel_lottery')
      .where({
        created_at: _.gte(0)
      }).count();

    let dayLotteryCount = await db.collection('vs_user_rel_lottery')
      .where(
        _.and([{
            created_at: _.gte(startTime)
          },
          {
            created_at: _.lte(endTime)
          }
        ])).count();

    // 当日实体奖人数
    let matIdList = (await db.collection('vs_lottery_types')
      .where({
        lotStatus: 2
      })
      .get()).data.map(item => item._id);

    let dayLotteryMatCount = await db.collection('vs_user_rel_lottery')
      .where(
        _.and([{
            created_at: _.gte(startTime)
          },
          {
            created_at: _.lte(endTime)
          },
          {
            lotteryType: _.in(matIdList)
          }
        ]))
      .count();

    // 每日兑换人数
    let dayExchangeLotteryCount = await db.collection('vs_user_rel_lottery')
      .where(
        _.and([{
            created_at: _.gte(startTime)
          },
          {
            created_at: _.lte(endTime)
          },
          {
            isExchange: true
          }
        ])).count();

    ctx.body = apiResponse(0, {
      activeCount: activeCount.total, // 互动总人数
      dayActiveCount: dayActiveCount.total, // 今天互动总人数
      totalScoreCount: totalScoreCount.total, // 6个测试总人数
      dayTotalScoreCount: dayTotalScoreCount.total, // 今天6个测试总人数
      lotteryCount: lotteryCount.total, // 抽奖总人数
      dayLotteryCount: dayLotteryCount.total, // 今天抽奖人数
      dayLotteryMatCount: dayLotteryMatCount.total, // 今天的实体奖数量
      dayExchangeLotteryCount: dayExchangeLotteryCount.total, // 今天已兑换人数
      reserveCount: reserveTotal, // 预约总人数
      // 预约后到场
      dayReserveActiveCount: dayReserveActiveCount.total,
      // 放飞机
      dayReserveUnactiveCount: reserveTotal - dayReserveActiveCount.total,
      // 未预约后到场
      dayUnreserveActiveCount: dayActiveCount.total - dayReserveActiveCount.total
    });
  },

  // 修改预约最大人数
  async reserveMaxCountUpdate(ctx) {
    // 
    const params = await requestValidation(ctx.request.body, {
      reserve_id: 'required|string|min:1|exists:vs_reserves',
      maxCount: 'required|integer|min:0|max:500',
    });
    let reserveItem = await db.collection('vs_reserves')
      .doc(params.reserve_id).get();
    if (params.maxCount < reserveItem.data.currentCount) {
      throw apiResponse(10003, "参数错误，[maxCount]不能小于当前预约数");

    }
    let updator = await db.collection('vs_reserves')
      .doc(params.reserve_id)
      .update({
        data: {
          maxCount: params.maxCount
        }
      });

    if (!updator.stats || updator.stats.updated !== 1) {
      throw apiResponse(20103, "更新失败，updator执行错误");
    }
    ctx.body = apiResponse(0, "修改预约人数成功");
  }
}