const { apiResponse, requestValidation, Rule } = require('../Services/http');
const { db, cloud } = require('../Services/cloud');
const { getUserListInfo } = require('../Services/user');
const { getSupportList } = require('../Services/support')

module.exports = {
	/**
	 * @description 获取列表
	 */
	async list(ctx) {
		const user = ctx.request.user;
		const params = await requestValidation(ctx.request.body, {
			page: ['integer', 'min:1', Rule.default(1)],
			size: ['integer', 'min:1', 'max:50', Rule.default(10)],
			order: ['string', Rule.in('desc', 'asc'), Rule.default('desc')],
			timestamp: ['integer', Rule.default(new Date().getTime() - 24 * 3600 * 1000)]
		});

		// 获取应援列表
		let supportData = await getSupportList(params);
		// (
		// 	await cloud.callFunction({
		// 		name: 'morphy_customForm',
		// 		data: {
		// 			$url: 'client/searchFormData',
		// 			tableName: 'support',
		// 			page: params.page, // 页数，可不填，默认为1
		// 			size: params.size, // 页码，可不填，默认为10，每次最多返回10条，看10条是否满足前端需求
		// 			order: params.order, // 排序 desc 或者 asc，可不填，默认为desc
		// 			search: {
		// 				isCheck: 1
		// 			}
		// 		},
		// 	})
		// ).result;
		// if (cloudRequest.code !== 0) {
		// 	throw cloudRequest;
		// }
		// 读取涉及的用户列表信息
		let userListJson = await getUserListInfo(supportData.list.map(item => item.openid))
		// console.log(test);

		// 添加用户头像/昵称
		const result = [];
		supportData.list.forEach((item) => {
			if (item.formData) {
				result.push({
					_id: item._id,
					text: item.formData.text,
					name: userListJson[item.openid].name,
					avatar: userListJson[item.openid].avatar,
					createAt: item.createAt
				});
			}
		});

		ctx.body = apiResponse(0, {
			list: result,
			total: supportData.total
		});
	},

	/**
	 * @description 新增应援
	 * @param {*} ctx
	 */
	async add(ctx) {
		// const user = ctx.request.user;
		// const params = await requestValidation(ctx.request.body, {
		// 	text: 'required|string|min:1|max:255',
		// });
		// // if (!params.text || typeof params.text !== 'string' || params.text.length > 255) {
		// // 	ctx.body = apiResponse(10003, '参数[text]错误');
		// // }
		// const timestamp = new Date().getTime();
		// // const
		// let queryResult = await db.collection('vs_supports').add({
		// 	data: {
		// 		timestamp,
		// 		created_at: timestamp,
		// 		text: params.text,
		//     uid: user._id,
		// 	},
		// });
		// if (!queryResult._id) {
		// 	ctx.body = apiResponse(20401, {}, '添加应援失败，语句返回结果中缺少_id');
		// 	return;
		// }
		// const selector = await db.collection('vs_supports').doc(queryResult._id).get();
		// ctx.body = apiResponse(0, selector.data, '添加应援成功');
	},
};
