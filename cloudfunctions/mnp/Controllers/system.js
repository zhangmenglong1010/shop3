const { apiResponse, requestValidation } = require('../Services/http');
const { db } = require('../Services/cloud');

module.exports = {

	async getProgramAdminInfo(ctx){
		const user = ctx.request.user;
		let info = await db.collection('miniprogram_admin').get();
		if(info.data[0].whitelist.indexOf(user.openid) >= 0){
			info.data[0].isAdmin = true;
		}
		ctx.body = apiResponse(0,info.data[0]);
	},

}