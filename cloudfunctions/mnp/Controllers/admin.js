const {
	apiResponse,
	requestValidation
} = require('../Services/http');
const {
	db
} = require('../Services/cloud');
const {
	reserveConfig
} = require('../Services/env');
const {
	arrayUnique,
	getDateString,
	getTimeText
} = require('../Services/helper');
const {
	getDayReserve
} = require('../Services/reserve');


module.exports = {

	/**
	 * @description 返回头像/昵称待审核的用户列表
	 * @method GET
	 * @param {*} ctx
	 */
	async checkupList(ctx) {
		// const params = await requestValidation(ctx.request.body, {
		// 	page: 'integer|min:1',
		// 	pageSize: 'integer|max:20|min:1',
		// 	checkupStatus: 'integer|min:0|max:2',
		// });
		// // let timestamp = params.timestamp || new Date().getTime() - 3600 * 1000 * 24 * 30;
		// if (params.page === undefined) {
		// 	params.page = 1;
		// }
		// if (params.pageSize === undefined) {
		// 	params.pageSize = 10;
		// }
		// const _ = db.command;
		// let condition = {
		// 	isNameCheckup: _.gte(0),
		// };
		// if (params.checkupStatus !== undefined) {
		// 	condition = _.or([
		// 		{
		// 			isNameCheckup: params.checkupStatus,
		// 		},
		// 		{
		// 			isAvatarCheckup: params.checkupStatus,
		// 		},
		// 	]);
		// }
		// let selector = await db
		// 	.collection('vs_users')
		// 	.where(condition)
		// 	.orderBy('timestamp', 'desc')
		// 	.skip((params.page - 1) * params.pageSize)
		// 	.limit(params.pageSize)
		// 	// .limit(quantity)
		// 	.get();
		// const countor = await db.collection('vs_supports').where(condition).count();
		// ctx.body = apiResponse(0, {
		// 	page: params.page,
		// 	pageSize: params.pageSize,
		// 	total: countor.total,
		// 	pageTotal: Math.ceil(countor.total / params.pageSize),
		// 	list: selector.data,
		// });
	},

	/**
	 * @description 对某个用户的头像/昵称进行审核
	 * @method GET
	 * @param {*} ctx
	 */
	async checkupName(ctx) {
		// const params = await requestValidation(ctx.request.body, {
		// 	uid: 'required|string|exists:vs_users',
		// });
		// let currentUser = (await db.collection('vs_users').doc(params.uid).get()).data;
		// if (currentUser.isNameCheckup === 1) {
		// 	ctx.body = apiResponse(10006, null, '已审批过的昵称，请勿重复审批');
		// 	return;
		// }
		// let updateMsg = await db
		// 	.collection('vs_users')
		// 	.doc(params.uid)
		// 	.update({
		// 		data: {
		// 			name: currentUser.tempName,
		// 			isNameCheckup: 1,
		// 		},
		// 	});
		// if (updateMsg.stats.updated !== 1) {
		// 	ctx.body = apiResponse(20503, null, '更新用户信息失败，数据库操作失败');
		// 	return;
		// }
		// let selector = await db.collection('vs_users').doc(params.uid).get();
		// ctx.body = apiResponse(0, selector.data);
	},

	/**
	 * @description 对某个用户的头像/昵称进行审核
	 * @method GET
	 * @param {*} ctx
	 */
	async checkupAvatar(ctx) {
		// const params = await requestValidation(ctx.request.body, {
		// 	uid: 'required|string|exists:vs_users',
		// });
		// let currentUser = (await db.collection('vs_users').doc(params.uid).get()).data;
		// if (currentUser.isAvatarCheckup === 1) {
		// 	ctx.body = apiResponse(10006, null, '已审批过的昵称，请勿重复审批');
		// 	return;
		// }
		// let updateMsg = await db
		// 	.collection('vs_users')
		// 	.doc(params.uid)
		// 	.update({
		// 		data: {
		// 			avatar: currentUser.tempAvatar,
		// 			isAvatarCheckup: 1,
		// 		},
		// 	});
		// if (updateMsg.stats.updated !== 1) {
		// 	ctx.body = apiResponse(20503, null, '更新用户信息失败，数据库操作失败');
		// 	return;
		// }
		// let selector = await db.collection('vs_users').doc(params.uid).get();
		// ctx.body = apiResponse(0, selector.data);
	},

	/**
	 * @description 添加活动预约，周期
	 * @param {*} ctx
	 */
	async addReserve(ctx) {
		const params = await requestValidation(ctx.request.body, {
			startTime: 'required|integer|min:0|max:1900000000000',
			endTime: 'required|integer|min:0|max:1900000000000',
			// actType: 'array', // 活动类型id
			// supportTicket: 'array', // 支持的票种
			// isLocked: 'boolean', // 是否锁定预约
		});
		if (params.endTime - params.startTime > 3600000 * 24 * 120) {
			// 不能超过120天
			throw apiResponse(10003, null, 'endTime不能比startTime大于120天');
		}
		// if (!params.actType || params.actType.length === 0) {
		// 	params.actType = [0];
		// }
		// 验证活动类型是否在配置列表中
		// params.actType.forEach((item) => {
		// 	// console.log(item >= reserveConfig.actType.length)
		// 	if (typeof item !== 'number' || item < 0 || item >= reserveConfig.actType.length) {
		// 		throw apiResponse(10003, null, '[actType]格式有误');
		// 	}
		// });

		// 验证票种是否在配置列表中，且不能是日常票，活动票，因为这两种票由系统自动分配
		// if (!params.supportTicket || params.supportTicket.length === 0) {
		// 	params.supportTicket = [0];
		// }
		// params.supportTicket.forEach((item) => {
		// 	if (typeof item !== 'number' || item < 0 || item >= reserveConfig.ticket.length) {
		// 		throw apiResponse(10003, null, '[supportTicket]格式有误');
		// 	}
		// });

		let dayMs = 86400000; // 一天的毫秒数：86400000 = 3600 * 1000 * 24
		let startDateTime = new Date(getDateString(params.startTime)).getTime(); // 2020-08-24 08:00:00
		let endDateTime = new Date(getDateString(params.endTime)).getTime(); // 2021-01-01

		let timestamp = Date.now();
		let sqlArray = [];
		for (let i = startDateTime; i <= endDateTime; i += dayMs) {
			// 累加一天
			// let timeItem = reserveConfig.timeline[j];
			let dateString = getDateString(i);

			let dateReserveList = getDayReserve(dateString);
			sqlArray = sqlArray.concat(dateReserveList);
			// let isSpecialDay = false;

			// let startTime = i + timeItem.start;
			// let endTime = i + timeItem.end;
			// // console.log(endTime, )
			// if (endTime <= params.startTime || startTime >= params.endTime) {
			// 	continue;
			// }
			// let text = timeItem.text;
			// let selector = await db
			// 	.collection('vs_reserves')
			// 	.where({
			// 		startTime,
			// 		endTime,
			// 	})
			// 	.get();
			// if (selector.data.length) {
			// 	// 该日期已存在，进行增量处理
			// 	let currentItem = selector.data[0];
			// 	let updateJson = {
			// 		text,
			// 		actType: arrayUnique(currentItem.actType.concat(params.actType)),
			// 		supportTicket: arrayUnique(currentItem.supportTicket.concat(params.supportTicket)),
			// 	};
			// 	if (params.isLocked) {
			// 		updateJson.isLocked = params.isLocked;
			// 	}

			// 	let updator = await db.collection('vs_reserves').doc(currentItem._id).update({
			// 		data: updateJson,
			// 	});
			// } else {
			// 	// insert
			// 	let supportTicket = [];
			// 	if (isWorkday(i)) {
			// 		supportTicket = params.supportTicket.concat([1]);
			// 	} else {
			// 		supportTicket = params.supportTicket.concat([]); // 拷贝数组
			// 	}
			// 	let insertor = await db.collection('vs_reserves').add({
			// 		data: {
			// 			actType: params.actType, // 活动类型 - 对应可使用的票，以及可添加的同伴
			// 			supportTicket: supportTicket, // 支持的票种
			// 			currentCount: 0, // 当前预约人数
			// 			startTime,
			// 			endTime,
			// 			text,
			// 			isLocked: params.isLocked || false, // 该时间段是否上锁，上锁将无法预约，用于活动时间段，以及前2个小时的清场
			// 			created_at: timestamp,
			// 		},
			// 	});
			// }
		}
		let insertor = await db.collection('vs_reserves').add({
			data: sqlArray,
		});
		ctx.body = apiResponse(0, null, '录入活动日历成功');
	},

	/**
	 * @description 删除startTime - endTime之间的活动预约信息
	 * @param {*} ctx
	 */
	async removeReserve(ctx) {
		const params = await requestValidation(ctx.request.body, {
			startTime: 'required|integer|min:0|max:1900000000000', // 时间字符串，开始时间
			endTime: 'required|integer|min:0|max:1900000000000', // 结束时间
			// isDelUserReserve: ''
		});
		// let startTime = new Date(params.startDateString).getTime() - 8 * 3600000;
		// let endTime = new Date(params.endDateString).getTime() + 16 * 3600000;
		const _ = db.command;
		// 删除用户关系表
		// await db
		// 	.collection('vs_user_rel_reserve')
		// 	.where({
		// 		created_at: _.lte(0),
		// 	})
		// 	.remove();

		// 删除活动预约信息
		await db
			.collection('vs_reserves')
			.where(
				_.and([{
						startTime: _.gte(params.startTime),
						// _id: _.neq(0)
					},
					{
						endTime: _.lte(params.endTime),
					},
				])
			)
			.remove();

		await db
			.collection('vs_user_rel_reserve')
			.where({
				created_at: _.gte(0),
			})
			.remove();
		ctx.body = apiResponse(0, null, '删除活动日历成功');
	},

	async reserveList(ctx) {
		const params = await requestValidation(ctx.request.body, {
			startDateString: 'required|dateString', // 日期字符串 2018-02-20
			endDateString: 'required|dateString', // 日期字符串 2018-02-20
		});
		let startTime = new Date(params.startDateString).getTime() - 8 * 3600000; // 需要减8小时
		let endTime = new Date(params.endDateString).getTime() + 16 * 3600000; // endTime是当天8点，需要再加16小时
		if (startTime > endTime) {
			throw apiResponse(10003, null, '参数有误，结束日期不应小于开始日期');
		} else if (endTime - startTime > 3600000 * 24 * 60) {
			throw apiResponse(10003, null, '参数有误，结束日期不应大于开始日期60天');
		}
		const _ = db.command;
		const selector = await db
			.collection('vs_reserves')
			.where(
				_.and([{
						startTime: _.gte(startTime),
					},
					{
						endTime: _.lte(endTime),
					},
				])
			)
			.orderBy('startTime', 'asc')
			.limit(60 * 6)
			.get();

		let resultList = {};
		let totalCount = 0;
		selector.data.forEach((item) => {
			// 结束时间小于当前时间将无法预约 且 当天所有时间段都满员的无法预约
			// 设置canReserve
			// 检查人数是否已满

			let dateString = getDateString(item.startTime);
			item.text = getTimeText(item.startTime, item.endTime);
			if (!resultList[dateString]) {
				resultList[dateString] = {
					currentCount: 0,
				};
			}
			totalCount += item.currentCount;
			resultList[dateString].currentCount += item.currentCount;
			// 能否预约，取决于当天有没有可预约的字段，只要存在一个时间段可预约，则整天视为可预约
		});
		ctx.body = apiResponse(0, {
			totalCount,
			list: resultList,
		});
	},

	/**
	 * 
	 * @param {*} ctx 
	 */
	async reserveTimelineList(ctx) {
		const params = await requestValidation(ctx.request.body, {
			startDateString: 'required|dateString', // 日期字符串 2018-02-20
			endDateString: 'required|dateString', // 日期字符串 2018-02-20
		});
		let startTime = new Date(params.startDateString).getTime() - 6 * 3600000; // 需要减8小时
		let endTime = new Date(params.endDateString).getTime() + 16 * 3600000; // endTime是当天8点，需要再加16小时
		if (startTime > endTime) {
			throw apiResponse(10003, null, '参数有误，结束日期不应小于开始日期');
		} else if (endTime - startTime > 3600000 * 24 * 60) {
			throw apiResponse(10003, null, '参数有误，结束日期不应大于开始日期60天');
		}
		const _ = db.command;
		const selector = await db
			.collection('vs_reserves')
			.where(
				_.and([{
						startTime: _.gte(startTime),
					},
					{
						endTime: _.lte(endTime),
					},
				])
			)
			.orderBy('startTime', 'asc')
			.limit(60 * 6)
			.get();

		let list = selector.data.map((item) => {
			// 结束时间小于当前时间将无法预约 且 当天所有时间段都满员的无法预约
			// 设置canReserve
			// 检查人数是否已满
			return {
				dateString: getDateString(item.startTime),
				timeString: getTimeText(item.startTime, item.endTime),
				currentCount: item.currentCount,
			}

			// 能否预约，取决于当天有没有可预约的字段，只要存在一个时间段可预约，则整天视为可预约
		});
		ctx.body = apiResponse(0, list);
	},

	/**
	 * @description 删除startTime - endTime之间的活动预约信息
	 * @param {*} ctx
	 */
	async setReserveLock(ctx) {
		const params = await requestValidation(ctx.request.body, {
			reserve_id: 'required|string|exists:vs_reserves', // 预约时间段的id
			isLocked: 'boolean', // 是否锁定预约
		});

		let updator = await db
			.collection('vs_reserves')
			.doc(params.reserve_id)
			.update({
				data: {
					isLocked: params.isLocked,
				},
			});

		ctx.body = apiResponse(0, null, '修改时间段的锁定状态成功');
	},

	async remove(ctx) {
		// const _ = db.command;
		// let updator = await db
		// 	.collection('vs_lottery_types')
		// 	.where({
		// 		lotStatus: 2,
		// 	})
		// 	.remove();

		// ctx.body = apiResponse(0, null, '清除成功');
	},

	/**
	 * @description 批量导入抽奖礼物
	 * @param {*} ctx
	 */
	async migrateLottery(ctx) {
		const params = await requestValidation(ctx.request.body, {
			count: 'required|integer|min:0', // 当前量
			isNeedVirtual: 'required|boolean', // 是否需要虚拟奖品
			lotStatus: 'required|integer|min:0|max:3', // 奖品状态 0-谢谢参与，1-cdKey，2-实体奖品，3-饮料券
			text: 'required|string|min:1|max:255', // 奖品文本
			weight: 'required|float|min:0' // 抽奖权重
		});

		let insertor = await db.collection('vs_lottery_types').add({
			data: Object.assign(params, {
				created_at: Date.now()
			}),
		});

		ctx.body = apiResponse(0, null, '导入成功');
	},

	/**
	 * @description 用户平均分
	 * @params
	 */
	async userAvgScore(ctx) {
		const _ = db.command;
		const $ = db.command.aggregate;
		
		let selector = await db.collection('vs_users')
			.where({
				score: _.gte(0)
			})
			.limit(10000)
			.get();
			let total = 0;
			selector.data.forEach(item => {
				total += item.score;
			});


			ctx.body = apiResponse(0, {
				avg: total / selector.data.length
			});
	},

	/**
	 * @description 抽奖列表
	 */
	async lotteryList(ctx) {
		let lotteryList = await db.collection('vs_user_rel_lottery')
			.where({
				lotteryType: "1b64dd7b5f793aaf0104665c057ca596"
			})
			// .aggregate()
			// .lookup({
			// 	from: 'vs_lottery_types',
			// 	localField: 'lotteryType',
			// 	foreignField: '_id',
			// 	as: 'lottery',
			// })
			// .limit(500)
			.get();
		// let list = [];
		// lotteryList.list.forEach(item => {
		// 	if (item.isExchange === false) {
		// 		list.push(item.lottery[0]);
		// 	}
		// });

		ctx.body = apiResponse(0, lotteryList)
	},

	/**
	 * @description 饮料券奖
	 */
	async lotteryClear(ctx) {
		// let lotteryList = await db.collection('vs_lottery_keys')
		// 	.where({
		// 		lotteryType: '8e5be7055f6df9e500938c012dd9f793',
		// 		isValid: true
		// 	})
		// 	.get();
		let insertor = await db.collection('vs_lottery_types')
			// .where({

			// })
			.add({
				data: {
					count: 2900,
					created_at: Date.now(),
					isNeedVirtual: true,
					lotStatus: 3,
					text: "Tims 咖啡9元兑鲜萃奶咖或其他任意饮品立减6元",
					weight: 0
				}
			})
		ctx.body = apiResponse(0, insertor)

	}

};