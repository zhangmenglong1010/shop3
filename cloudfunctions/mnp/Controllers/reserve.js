/**
 * 预约模块
 */
const { apiResponse, requestValidation } = require('../Services/http');
const { db } = require('../Services/cloud');
const { reserveConfig } = require('../Services/env');
const { getDateString, arrayUnique, getTimeText } = require('../Services/helper');

module.exports = {
	/**
	 * @description 获取活动类型列表 和 票种列表
	 * @param {*} ctx
	 */
	async getConfig(ctx) {
		let actTypeList = reserveConfig.actType.map((item, index) => {
			return {
				id: index,
				name: item,
			};
		});
		let ticketTypeList = reserveConfig.ticket.map((item, index) => {
			return {
				id: index,
				name: item,
			};
		});
		ctx.body = apiResponse(0, {
			actTypeList,
			ticketTypeList,
		});
	},

	/**
	 * @description 获取预约列表
	 * @param {*} ctx
	 */
	async list(ctx) {
		const params = await requestValidation(ctx.request.body, {
			startDateString: 'required|dateString', // 日期字符串 2018-02-20
			endDateString: 'required|dateString', // 日期字符串 2018-02-20
		});

		let startTime = new Date(params.startDateString).getTime() - 8 * 3600000; // 需要减8小时
		let endTime = new Date(params.endDateString).getTime() + 16 * 3600000; // endTime是当天8点，需要再加16小时
		if (startTime > endTime) {
			throw apiResponse(10003, null, '参数有误，结束日期不应小于开始日期');
		} else if (endTime - startTime > 3600000 * 24 * 60) {
			throw apiResponse(10003, null, '参数有误，结束日期不应大于开始日期60天');
		}
		const _ = db.command;
		const selector = await db
			.collection('vs_reserves')
			.where(
				_.and([
					{
						startTime: _.gte(startTime),
					},
					{
						endTime: _.lte(endTime),
					},
				])
			)
			.limit(60 * 6)
			.get();

		let currentTime = Date.now();
		let resultList = {};
		selector.data.forEach((item) => {
			// 结束时间小于当前时间将无法预约 且 当天所有时间段都满员的无法预约
			// 设置canReserve
			// 检查人数是否已满

			let dateString = getDateString(item.startTime);
			item.text = getTimeText(item.startTime, item.endTime);
			if (!resultList[dateString]) {
				resultList[dateString] = {
					canReserve: false,
					actType: [],
					supportTicket: [],
				};
			}
			item.canReserve = item.currentCount < item.maxCount && item.endTime >= currentTime;
			resultList[dateString] = {
				// 能否预约，取决于当天有没有可预约的字段，只要存在一个时间段可预约，则整天视为可预约
				canReserve: item.canReserve || resultList[dateString].canReserve,
				// 合并当天所有字段的活动
				actType: arrayUnique(item.actType.concat(resultList[dateString].actType)),
				// 合并当天所有支持的票种
				supportTicket: arrayUnique(item.supportTicket.concat(resultList[dateString].supportTicket)),
			};
		});
		for (let key in resultList) {
			let item = resultList[key];
			// 设置活动内容
			item.actType = item.actType.map((item) => {
				return {
					id: item,
					text: reserveConfig.actType[item],
				};
			});

			// 设置支持的票种内容
			item.supportTicket = item.supportTicket.map((item) => {
				return {
					id: item,
					text: reserveConfig.ticket[item],
				};
			});
		}

		ctx.body = apiResponse(0, resultList);
	},

	/**
	 * @description 获取某一天的详细预约信息
	 * @param {*} ctx
	 */
	async detail(ctx) {
		const params = await requestValidation(ctx.request.body, {
			dateString: 'required|dateString', // 日期字符串 2018-02-20
		});
		let dateTime = new Date(params.dateString).getTime();
		let startTime = dateTime - 8 * 3600000;
		let endTime = dateTime + 16 * 3600000;

		const _ = db.command;
		const selector = await db
			.collection('vs_reserves')
			.where(
				_.and([
					{
						startTime: _.gte(startTime),
					},
					{
						endTime: _.lte(endTime),
					},
				])
			)
			.get();
		if (selector.data.length === 0) {
			throw apiResponse(10005, null, '未找到该日期的相关信息');
		}

		let currentTime = new Date().getTime();
		selector.data.forEach((item) => {
			item.text = getTimeText(item.startTime, item.endTime);

			// 设置活动内容
			item.actType = item.actType.map((item) => {
				return {
					id: item,
					text: reserveConfig.actType[item],
				};
			});

			// 设置支持的票种内容
			item.supportTicket = item.supportTicket.map((item) => {
				return {
					id: item,
					text: reserveConfig.ticket[item],
				};
			});

			// 小于最大人数 且 不能预约已过去的时间段
			item.canReserve = item.currentCount < item.maxCount && item.endTime >= currentTime;
		});

		ctx.body = apiResponse(0, selector.data, '获取日期活动详情成功');
	},
};
