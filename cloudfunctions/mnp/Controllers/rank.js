/**
 * 排行榜
 */
const { apiResponse, requestValidation } = require('../Services/http');
const { db } = require('../Services/cloud');
const { getRankListWithUserInfo } = require('../Services/rank')

module.exports = {
	/**
	 * 获取列表
	 */
	async list(ctx) {
		const params = await requestValidation(ctx.request.body, {
			size: 'integer|max:100',
		});
		if (params.size === undefined) {
			params.size = 20;
		}
		let list = await getRankListWithUserInfo(params.size);

		ctx.body = apiResponse(0, list, '获取天梯榜成功');
	},
};
