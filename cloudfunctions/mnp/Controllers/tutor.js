const { apiResponse, requestValidation } = require('../Services/http');
const { db } = require('../Services/cloud');

module.exports = {
	/**
	 * 获取导师列表
	 */
	async list(ctx) {
		let query = await db
			.collection('vs_tutors')
      .get();

    ctx.body = apiResponse(0, query.data);
	},

	/**
	 *
	 * @param {*} ctx
	 */
	async detail(ctx) {
    const id = ctx.request.body.tutor_id;
    console.log(id)
    let query = await db
      .collection('vs_tutors')
      .doc(id)
      .get();
		ctx.body = apiResponse(0, query.data);
	},

	/**
	 *
	 * @param {*} ctx
	 */
	async add(ctx) {
    const params = await requestValidation(ctx.request.body, {
			'id': 'required|integer',
			'name': 'required|string',
			'team': 'required|string',
			'title': 'required|string'
		})
    let query = await db
      .collection('vs_tutors')
      .add({
				data: {
					id: params.id,
					name: params.name,
					team: params.team,
					title: params.title
				}
			});
		ctx.body = apiResponse(0, null, '添加成功');
	},
};
