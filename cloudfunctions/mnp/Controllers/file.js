const {
  apiResponse,
  requestValidation,
  Rule
} = require('../Services/http');
const {
  db,
  cloud
} = require('../Services/cloud');
const {
  // getOAuth,
  getCosToken
} = require('../Services/morphy');

/**
 * 现场照片
 */

module.exports = {
  /**
   * @description 获取小程序物料的二维码列表
   * @param {*} ctx 
   */
  async getWXACode(ctx) {
    // 此处将获取永久有效的小程序码，并将其保存在云文件存储中，最后返回云文件 ID 给前端使用
    // const user = ctx.request.user;
    const params = await requestValidation(ctx.request.body, {
      path: 'string|max:255',
      channel: 'required|string|max:255',
      size: 'integer',
    });
    let path = params.path ? params.path + '?source=' + params.channel : 'pages/index/index?source' + params.channel;
    const wxacodeResult = await cloud.openapi.wxacode.get({
      path: path,
      width: params.size || 1280,
    })
    console.log(wxacodeResult)

    const fileExtensionMatches = wxacodeResult.contentType.match(/\/([^\/]+)/)
    const fileExtension = (fileExtensionMatches && fileExtensionMatches[1]) || 'jpg'

    const uploadResult = await cloud.uploadFile({
      // 云文件路径，此处为演示采用一个固定名称
      cloudPath: `wxacode/wxacode_${params.channel}_${new Date().getTime()}.${fileExtension}`,
      // 要上传的文件内容可直接传入图片 Buffer
      fileContent: wxacodeResult.buffer,
    })

    if (!uploadResult.fileID) {
      throw new Error(`upload failed with empty fileID and storage server status code ${uploadResult.statusCode}`)
    }

    ctx.body = apiResponse(0, uploadResult.fileID, '获取物料文件成功');

    // return uploadResult.fileID

  },

  async getCosToken(ctx) {
    // await getOAuth();
    let params = await requestValidation(ctx.request.body, {
      ext: 'required|string|min:1|max:20'
    })
    let cosInfo = await getCosToken(params.ext);

    ctx.body = apiResponse(0, cosInfo);
  }
};