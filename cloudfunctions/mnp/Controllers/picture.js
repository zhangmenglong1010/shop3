/**
 * 现场照片
 */
const { apiResponse, requestValidation, Rule } = require('../Services/http');
const { db } = require('../Services/cloud');
const { getPictureList } = require('../Services/picture')

module.exports = {
	/*
	async list(ctx) {
		const user = ctx.request.user;
		const params = await requestValidation(ctx.request.body, {
			page: 'integer|min:1',
			pageSize: 'integer|max:20|min:1',
		});
		// let timestamp = params.timestamp || new Date().getTime() - 3600 * 1000 * 24 * 30;
		if (params.page === undefined) {
			params.page = 1;
		}
		if (params.pageSize === undefined) {
			params.pageSize = 10;
		}

		let selector = await db
			.collection('vs_pictures')
			.aggregate()
			.sort({
				timestamp: -1,
			})
			.skip((params.page - 1) * params.pageSize)
			.limit(params.pageSize)
			.lookup({
				from: 'users',
				localField: 'uid',
				foreignField: '_id',
				as: 'users',
			})
			.end();
		const countor = await db.collection('vs_pictures').count();
		const list = selector.list.map((item) => {
			if (item.users && item.users.length > 0) {
				item.user = item.users[0];
				delete item.users;
			}
			return item;
		});

		// 处理点赞数据
		let picture_id_list = list.map(item => item._id);
		const _ = db.command;
		let hasIThumbedList = await db.collection('vs_picture_rel_thumb').where({
			picture_id: _.in(picture_id_list),
			uid: user._id
		}).get();
		const hasIThumbedPicIdList = hasIThumbedList.data.map(item => item.picture_id);
		// const $ = db.command.aggregate;
		// let thumbGroup = await db.collection('vs_picture_rel_thumb').aggregate().group({
		// 	_id: '$picture_id',
		// 	num_total: $.sum(1)
		// }).end();
		// console.log(thumbGroup)
		for (let i = 0; i < list.length; i++) {
			let item = list[i];
			// 照片点赞数
			let count = await db.collection('vs_picture_rel_thumb').where({
				picture_id: item._id
			}).count();
			item.thumbCount = count.total;

			// 本人是否已点赞
			if (hasIThumbedPicIdList.indexOf(item._id) >= 0) {
				item.hasIThumbed = true;
			} else {
				item.hasIThumbed = false;
			}
		}

		ctx.body = apiResponse(0, {
			page: params.page,
			pageSize: params.pageSize,
			total: countor.total,
			pageTotal: Math.ceil(countor.total / params.pageSize),
			list,
		});
	},
	*/

	/**
	 * 
	 * @param {*} ctx 
	 */
	async list(ctx) {
		const params = await requestValidation(ctx.request.body, {
			size: 'integer|min:1|max:20',
			lastRequestTime: 'integer',
		});
		const openid = ctx.request.user.openid;
		let {list, total} = await getPictureList(params, openid);
		ctx.body = apiResponse(0, {
			list,
			total
		}, '获取现场照片列表成功');
	},

	/**
	 * @description 添加上墙声明
	 * @param {*} ctx
	 */
	async add(ctx) {
		// const user = ctx.request.user;
		// const params = await requestValidation(ctx.request.body, {
		// 	text: 'required|string|max:255',
		// 	src: 'required|string',
		// });

		// const timestamp = new Date().getTime();
		// // const
		// let queryResult = await db.collection('vs_pictures').add({
		// 	data: {
		// 		created_at: timestamp,
		// 		timestamp: timestamp,
		// 		text: params.text,
		// 		src: params.src,
		// 		uid: user._id
		// 	},
		// });
		// if (!queryResult._id) {
		// 	ctx.body = apiResponse(20401, {}, '添加照片上墙失败，语句返回结果中缺少_id');
		// 	return;
		// }
		// const selector = await db.collection('vs_pictures').doc(queryResult._id).get();
		// ctx.body = apiResponse(0, selector.data, '添加照片上墙成功');
	},

	/**
	 * @description 点赞/取消点赞
	 * @param {*} ctx
	 */
	async thumb(ctx) {
		// const user = ctx.request.user;
		// const params = await requestValidation(ctx.request.body, {
		// 	picture_id: 'required|string|exists:vs_pictures',
		// 	isThumbUp: ['required', 'integer', Rule.in([0, 1])],
		// });
		// // let queryJson = {
		// // 	picture_id: params.picture_id,
		// // 	uid: params.uid,
		// // };
		// const selector = await db
		// 	.collection('vs_picture_rel_thumb')
		// 	.where({
		// 		picture_id: params.picture_id,
		// 		uid: user._id,
		// 	})
		// 	.get();
		// if (params.isThumbUp === 1) {
		// 	if (selector.data.length > 0) {
		// 		ctx.body = apiResponse(10004, null, '操作失败，不能重复点赞该图片');
		// 		return;
		// 	}
		// 	// 添加操作
		// 	let timestamp = new Date().getTime();
		// 	let insertor = await db.collection('vs_picture_rel_thumb').add({
		// 		data: {
		// 			picture_id: params.picture_id,
		// 			created_at: timestamp,
		// 			uid: user._id,
		// 		},
		// 	});
		// 	let countor = await db.collection('vs_picture_rel_thumb').where({
		// 		picture_id: params.picture_id,
		// 	}).count();

		// 	if (insertor && insertor._id) {
		// 		ctx.body = apiResponse(0, {
		// 			picture_id: params.picture_id,
		// 			uid: user._id,
		// 			thumbCount: countor.total,
		// 		}, '照片点赞成功');
		// 	}

		// } else {
		// 	if (selector.data.length === 0) {
		// 		ctx.body = apiResponse(10004, null, '操作失败，未点赞过该图片，不能取消点赞');
		// 		return;
		// 	}
		// 	// 删除操作
		// 	let deletor = await db.collection('vs_picture_rel_thumb').where({
		// 		picture_id: params.picture_id,
		// 		uid: user._id,
		// 	}).remove();
		// 	let countor = await db.collection('vs_picture_rel_thumb').where({
		// 		picture_id: params.picture_id,
		// 	}).count();
		// 	ctx.body = apiResponse(0, {
		// 		picture_id: params.picture_id,
		// 		uid: user._id,
		// 		thumbCount: countor.total,
		// 	}, '取消照片点赞成功');
		// }
	},
};
