const {
    reponseWrapper
} = require('./utils')
// 云函数入口文件
const cloud = require('wx-server-sdk')
const TcbRouter = require('tcb-router')
cloud.init({
    env: cloud.DYNAMIC_CURRENT_ENV // 给定 DYNAMIC_CURRENT_ENV 常量：接下来的 API 调用都将请求到与该云函数当前所在环境相同的环境
})
const paramsValidatorMiddleware = require('./validator')
// 云函数入口函数
exports.main = async (event) => {
    const app = new TcbRouter({
        event
    })
    const wxContext = cloud.getWXContext()
    const openid = wxContext.OPENID
    const db = cloud.database()
    const _ = db.command
    /**
     * 添加form 信息
     */
    app.router('morphy/getAllFormDatas', paramsValidatorMiddleware(event, {}), async (ctx, next) => {
        try {
            const validForms = await db.collection('morphy_formSetting').where({
                isShow: true
            }).get();
            const validFormsCount = await db.collection('morphy_formSetting').where({
                isShow: true
            }).count();
            const list = validForms.data || [];
            let dataList = [];
            if (validFormsCount.total > 0) {
                //筛选出表单中的操作字段，因为和数据相关
                const fs = list.map(item => {
                    return {
                        id: item._id,
                        name: item.name,
                        tableName: item.tableName,
                        dataRelated: item.formSetting.filter(it => it.type === "statusBoolean" || it.type === 'statusSwitch')
                    }
                })

                for (var k = 0; k < fs.length; k++) {
                    const map = {
                        formSettingId: fs[k].id,
                        hidden: _.or(_.eq(false), _.exists(false))
                    }
                    const count_Total = await db.collection("morphy_formData").where({
                        ...map
                    }).count()
                    //表单总量
                    fs[k].total = count_Total.total;
                    // console.log("total:",count_Total.total)
                    for (let i = 0, related = fs[k].dataRelated; i < related.length; i++) {
                        related[i].datas = []
                        if (related[i].type == 'statusBoolean') {
                            const count_TURE = await db.collection("morphy_formData").where({
                                ...map,
                                [`formData.${related[i].column}`]: _.eq(true)
                            }).count()
                            related[i].datas.push({
                                subtitle: "是",
                                count: count_TURE.total
                            })
                            const count_FALSE = await db.collection("morphy_formData").where({
                                ...map,
                                [`formData.${related[i].column}`]: _.not(_.eq(true))
                            }).count()
                            related[i].datas.push({
                                subtitle: "否",
                                count: count_FALSE.total
                            })
                            console.log(count_FALSE.total, "count_FALSE.total")
                        }
                        if (related[i].type == 'statusSwitch') {
                            const count_PASS = await db.collection("morphy_formData").where({
                                ...map,
                                [`formData.${related[i].column}`]: 1
                            }).count()
                            related[i].datas.push({
                                subtitle: "通过",
                                count: count_PASS.total
                            })
                            const count_FAIL = await db.collection("morphy_formData").where({
                                ...map,
                                [`formData.${related[i].column}`]: -1
                            }).count()
                            related[i].datas.push({
                                subtitle: "拒绝",
                                count: count_FAIL.total
                            })
                            const count_HOLD = await db.collection("morphy_formData").where({
                                ...map,
                                [`formData.${related[i].column}`]: _.and(_.not(_.eq(1)), _.not(_.eq(-1)))
                            }).count()
                            related[i].datas.push({
                                subtitle: "未处理",
                                count: count_HOLD.total
                            })
                        }
                    }
                }
                dataList = fs

                console.log(fs, "dataList")
            }
            ctx.body = reponseWrapper(0, '获取成功', dataList)
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }

    })
    return app.serve()
}
