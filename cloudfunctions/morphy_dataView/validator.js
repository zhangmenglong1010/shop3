/**
 * code from https://github.com/peakchen90/koa-params-validator/blob/master/index.js
 * modified by luceinyu
 * 2019-07-10
 */
const Validate = require('easy-object-validator/src/Validate')
const objectValidator = require('easy-object-validator')
const { reponseWrapper } = require('./utils')

/**
 * 请求参数校验
 * @param {Object} body 请求的参数对象
 * @param {Object} options 校验规则
 * @param {Object} [invalidMixinContext] 校验失败时，将合并到ctx
 * @returns {Function} Koa中间件
 */
function validator(body, options, invalidMixinContext) {
  return async (ctx, next) => {
    options = options || {}
    
    const target = body
    console.log('target', target)
    const isValid = objectValidator(target, options)
    if (isValid) {
      await next()
    } else {
      if (Validate.type(invalidMixinContext) !== 'object') {
        invalidMixinContext = {}
      }
      if (!ctx.body) {
        ctx.body = {}
      }
      Object.assign(ctx.body, {
        code: -101,
        errMsg: '请求失败，参数缺失或参数格式不正确'
      }, invalidMixinContext)
    }
  }
}


/**
 * 暴露方法
 */

// 继承，用于自定义校验方法
validator.extend = (options) => {
  // 执行 objectValidator 的继承方法
  objectValidator.extend(options)

  Object.keys(options).forEach(name => {
    // 添加引用
    validator[name] = objectValidator[name]
  })
}
// 添加引用
validator.string = objectValidator.string
validator.shape = objectValidator.shape
validator.number = objectValidator.number
validator.object = objectValidator.object
validator.array = objectValidator.array
validator.boolean = objectValidator.boolean
validator.isRequire = objectValidator.isRequire
validator.test = objectValidator.test
validator.is = objectValidator.is
validator.equals = objectValidator.equals
validator.not = objectValidator.not
validator.arrayOf = objectValidator.arrayOf
validator.oneOf = objectValidator.oneOf
validator.enums = objectValidator.enums
validator.reset = objectValidator.reset


module.exports = validator