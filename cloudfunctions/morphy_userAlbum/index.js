const cloud = require('wx-server-sdk')
const TcbRouter = require('tcb-router')
const paramsValidatorMiddleware = require('./validator')

const {
    reponseWrapper,
    html2Escape
} = require('./utils')

cloud.init({
    env: cloud.DYNAMIC_CURRENT_ENV // 给定 DYNAMIC_CURRENT_ENV 常量：接下来的 API 调用都将请求到与该云函数当前所在环境相同的环境
})
const MAX_LIMIT = 100
const COLLECTION = 'morphy_userAlbum'
exports.main = async (event) => {
    // 初始化控制路由
    const app = new TcbRouter({
        event
    })
    const wxContext = cloud.getWXContext()
    const db = cloud.database()
    const _ = db.command
    const openid = wxContext.OPENID
    /**
     *
     * @api {method} add 添加相册
     * @apiName add
     * @apiGroup userAlbum
     * @apiVersion  1.0.0
     *
     *
     * @apiParam  {String} content 添加的内容
     * @apiParam  {String} cateId 图片分类
     * @apiParam  {Array} tag 图片标签
     * @apiParam  {object} mediaInfo 图片信息
     * @apiParam  {boolean} isShow 是否显示在应用内
     *
     * @apiParamExample  {type} Request-Example:
     * {
     *     content : 'test',
     *     cateId : '32134',
     *     mediaInfo : {
     *        fileUrl: 'http://wwww.pic.com/1.png'
     *        width: 100,
     *        height: 100
     *     },
     *     isShow : false
     * }
     *
     *
     * @apiSuccessExample {type} Success-Response:
     * {
     *     ret: 0,
     *     errMsg: "添加成功"
     * }
     *
     *
     */
    app.router('client/add', paramsValidatorMiddleware(event, {
        mediaInfo: paramsValidatorMiddleware.object(),
        content: paramsValidatorMiddleware.string(),
        userInfo: paramsValidatorMiddleware.object()
    }), async (ctx) => {
        let {
            content,
            mediaInfo,
            userInfo
        } = event
        if (!mediaInfo && !content) {
            ctx.body = reponseWrapper(-101, '请求失败，参数缺失或参数格式不正确')
        } else {
            content = html2Escape(content)
            const data = {
                content, // 图片描述
                mediaInfo, // 图片信息
                userInfo, // 用户信息
                like: [], // 图片点赞数
                recommend: {
                    status: false
                },
                sticky: {
                    status: false
                },
                createTime: Date.now(), // 上传时间
                updateTime: Date.now(), // 更新时间
                creator: openid, // 上传人的openid
            }
            try {
                const {
                    _id
                } = await db.collection(COLLECTION).add({
                    data
                })
                if (_id) {
                    data._id = _id
                    ctx.body = reponseWrapper(0, '创建成功', data)
                } else {
                    ctx.body = reponseWrapper(-1101)
                }
            } catch (e) {
                ctx.body = reponseWrapper(-1102)
            }
        }
    })
    /**
     *
     * @api {method} delete 删除相册
     * @apiName delete
     * @apiGroup userAlbum
     * @apiVersion  1.0.0
     *
     *
     * @apiParam  {String} _id 添加的id
     *
     * @apiParamExample  {type} Request-Example:
     * {
     *     _id : 'test-id'
     * }
     *
     *
     * @apiSuccessExample {type} Success-Response:
     * {
     *     ret : 0,
     *     msg : "删除成功"
     * }
     *
     *
     */
    app.router('delete', paramsValidatorMiddleware(event, {
        _id: paramsValidatorMiddleware.isRequire().string()
    }), async (ctx) => {
        const {
            _id
        } = event
        try {
            const {
                stats: {
                    removed
                }
            } = await db.collection(COLLECTION).doc(_id).remove()
            if (removed === 1) {
                ctx.body = reponseWrapper(0, '删除成功')
            } else {
                ctx.body = reponseWrapper(-1101)
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     *
     * @api {method} update 更新相册
     * @apiName update
     * @apiGroup userAlbum
     * @apiVersion  1.0.0
     *
     * @apiParam  {String} _id 更新的id
     * @apiParam  {String} content 更新的内容
     * @apiParam  {String} cateId 图片分类
     * @apiParam  {object} mediaInfo 图片信息
     * @apiParam  {boolean} isShow 是否显示
     *
     * @apiParamExample  {type} Request-Example:
     * {
     *     _id : 'test-id',
     *     content : 'test',
     *     cateId : '32134',
     *     mediaInfo : {
     *        fileUrl: 'http://wwww.pic.com/1.png'
     *        width: 100,
     *        height: 100
     *     },
     *     isShow : false
     * }
     *
     *
     * @apiSuccessExample {type} Success-Response:
     * {
     *     ret : 0,
     *     errMsg : "更新成功"
     * }
     *
     *
     */
    app.router('update', paramsValidatorMiddleware(event, {
        _id: paramsValidatorMiddleware.isRequire().string(),
        content: paramsValidatorMiddleware.string(),
        mediaInfo: paramsValidatorMiddleware.object(),
        isShow: paramsValidatorMiddleware.boolean(),
        isRecommend: paramsValidatorMiddleware.boolean(),
        checkStatus: paramsValidatorMiddleware.number(),
        isSticky: paramsValidatorMiddleware.boolean(),
        checkReason: paramsValidatorMiddleware.string()
    }), async (ctx) => {
        const {
            _id,
            isRecommend, // 是否推荐
            isSticky, //是否置顶
            isShow, //是否发布
        } = event
        const allowUpdateProps = ['content', 'mediaInfo', 'isShow', 'isSticky', 'checkStatus', 'checkReason', 'userInfoCensored']
        const updateObejct = {}
        const currentTime = Date.now() //当前时间
        Object.keys(event).forEach((key) => {
            if (allowUpdateProps.indexOf(key) !== -1) {
                updateObejct[key] = event[key]
            }
        })
        if (isSticky !== undefined) {
            updateObejct["sticky"] = _.set({
                status: isSticky,
                time: currentTime,
                by: "" //待替换
            })
        }
        if (isShow !== undefined) {
            updateObejct["isShow"] = isShow
            if (isShow) {
                updateObejct["releasedInfo"] = {
                    time: currentTime,
                    by: ""
                }
            } //发布时间
            if (!isShow) {
                updateObejct["offShelfInfo"] = {
                    time: currentTime,
                    by: ""
                }
            } //下架时间

        }
        if (isRecommend !== undefined) {
            updateObejct["recommend"] = _.set({
                status: isRecommend,
                time: currentTime,
                by: "" //待替换
            })
        }
        try {
            const {
                stats: {
                    updated
                }
            } = await db.collection(COLLECTION).doc(_id).update({
                data: {
                    ...updateObejct,
                    updateTime: currentTime
                }
            })
            if (updated === 1) {
                const getResult = await db.collection(COLLECTION).doc(_id).get()
                ctx.body = reponseWrapper(0, '更新成功', getResult.data)
            } else {
                ctx.body = reponseWrapper(-1101)
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     * 获取官方相册
     */
    app.router('client/get', paramsValidatorMiddleware(event, {
        size: paramsValidatorMiddleware.number(),
        order: paramsValidatorMiddleware.enums('desc', 'asc'),
        checkStatus: paramsValidatorMiddleware.number(),
        isRecommend: paramsValidatorMiddleware.boolean(),
        isSticky: paramsValidatorMiddleware.boolean(),
        search: paramsValidatorMiddleware.string(),
        lastRequestTime: paramsValidatorMiddleware.number()
    }), async (ctx) => {
        const {
            size = 10, //每页长度
                order = 'desc', //顺序
                search, //搜索关键字
                checkStatus, //审核状态
                isRecommend, //是否推荐
                lastRequestTime,
                isSticky //是否置顶
        } = event
        try {
            const map = {}
            const countMap = {}
            if (checkStatus !== undefined) {
                map['checkStatus'] = checkStatus
                countMap['checkStatus'] = checkStatus
            }
            if (isRecommend) {
                map['recommend'] = {
                    status: isRecommend
                }
                countMap['recommend'] = {
                    status: isRecommend
                }
            }
            if (isSticky) {
                map['sticky'] = {
                    status: isSticky
                }
                countMap['sticky'] = {
                    status: isSticky
                }
            }
            if (search) {
                map['content'] = new db.RegExp({
                    regexp: search,
                    options: 'i'
                })
                countMap['content'] = new db.RegExp({
                    regexp: search,
                    options: 'i'
                })
            }
            if (lastRequestTime) {
                if (order === 'desc') {
                    map['createTime'] = _.lt(lastRequestTime)
                } else {
                    map['createTime'] = _.gt(lastRequestTime)
                }
            }
            const {
                data,
                data: {
                    len
                }
            } = await db.collection(COLLECTION).where(map).orderBy('createTime', order).limit(size).get()
            const {
                total
            } = await db.collection(COLLECTION).where(countMap).count()
            let i = 0
            for (; i < len; i++) {
                data[i].isLiked = (data[i].like.indexOf(openid) !== -1) // 自己是否点赞
                data[i].likeCount = data[i].like.length // 总点赞数
            }
            ctx.body = reponseWrapper(0, '请求成功', {
                list: data,
                total
            })
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })

    /**
     * 获取玩家分享
     */
    app.router('getByPageSize', paramsValidatorMiddleware(event, {
        order: paramsValidatorMiddleware.enums('desc', 'asc'),
        page: paramsValidatorMiddleware.number(),
        size: paramsValidatorMiddleware.number(),
        checkStatus: paramsValidatorMiddleware.number(),
        isRecommend: paramsValidatorMiddleware.number(),
        search: paramsValidatorMiddleware.string()
    }), async (ctx, next) => {
        const {
            page = 1, //当前页
                size = 10, //每页长度
                order = 'desc', //顺序
                search, //搜索关键字
                checkStatus, //审核状态
                isRecommend, //是否推荐
                isSticky, //是否置顶
        } = event
        try {
            const map = {}
            if (checkStatus !== undefined) {
                map['checkStatus'] = checkStatus
            }
            if (isRecommend) {
                map['recommend'] = {
                    status: true
                }
            }
            if (isSticky) {
                map['sticky'] = {
                    status: true
                }
            }
            if (search) {
                map['content'] = new db.RegExp({
                    regexp: search,
                    options: 'i'
                })
            }
            const {
                data
            } = await db.collection(COLLECTION).where(map).orderBy('createTime', order).skip((page - 1) * size).limit(size).get()
            const totalResult = await db.collection(COLLECTION).where(map).count()
            ctx.body = reponseWrapper(0, '请求成功', {
                list: data,
                total: totalResult.total
            })
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     * 设置点赞状态官方相册
     */
    app.router('client/setLike', paramsValidatorMiddleware(event, {
        _id: paramsValidatorMiddleware.isRequire().string(),
        status: paramsValidatorMiddleware.isRequire().enums(-1, 1)
    }), async (ctx) => {
        const {
            _id,
            status
        } = event
        try {
            const {
                data: {
                    like
                }
            } = await db.collection(COLLECTION).doc(_id).get()
            let updateResult = null
            let responseMsg = ''
            const indexOf = like.indexOf(openid)
            console.log('indexOf', indexOf)
            if (status === 1 && indexOf !== -1) {
                ctx.body = reponseWrapper(-1103, '点赞失败，不能重复点赞')
            } else {
                if (status === 1) {

                    updateResult = await db.collection(COLLECTION).doc(_id).update({
                        data: {
                            like: _.push(openid)
                        }
                    })
                    responseMsg = '点赞'
                } else {
                    like.splice(indexOf, 1)
                    updateResult = await db.collection(COLLECTION).doc(_id).update({
                        data: {
                            like
                        }
                    })
                    responseMsg = '取消点赞'
                }
                if (updateResult.stats.updated === 1) {
                    ctx.body = reponseWrapper(0, responseMsg + '成功')
                } else {
                    ctx.body = reponseWrapper(-1101, responseMsg + '失败')
                }
            }

        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    return await app.serve()
}
