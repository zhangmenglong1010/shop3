## 玩家相册

模块名：玩家相册

版本：`V1.0`

维护人：nornorchen

云函数：`morphy_userAlbum`

特性：支持添加玩家自发创建的图文内容，并可以进行点赞。同时，官方可以对玩家发布的内容进行审核和置顶等定制化的功能。

### 小程序前端接口

>开发约定：由于该模块使用云函数，因此可直接按照微信调用云函数方法的格式调用，包括promise链式调用以及complete等回调调用

#### 获取官方相册分类列表

```javascript
// 小程序端调用
wx.cloud.callFunction({
  // 需调用的云函数名
  name: 'morphy_userAlbum',
  // 传给云函数的参数
  data: {
      '$url':'add',
      'mediaInfo': {
          width: 100,
          height: 100,
          fileUrl: 'xxxx'
      },
      content: 'xxxx' // 玩家发表图册时填写的内容
  },
  // 成功回调
  complete: console.log
})
```

#### 获取玩家相册列表

```javascript
// 小程序端调用
wx.cloud.callFunction({
  // 需调用的云函数名
  name: 'morphy_userAlbum',
  // 传给云函数的参数
  data: {
      '$url':'get',
      'size': 10, // 可选参数，一次拉取的数量
      'page': 10, // 可选参数，拉取的页数
      'order': 'desc', // 可选参数，拉取的排序规则(根据创建时间大小排序), 可选参数为 desc|asc
  },
  // 成功回调
  complete: console.log
})
```

#### 设置点赞状态

```javascript
// 小程序端调用
wx.cloud.callFunction({
  // 需调用的云函数名
  name: 'morphy_userAlbum',
  // 传给云函数的参数
  data: {
      '$url':'setLike',
      '_id': 'xxxx',   // 需要设置点赞状态的相册id
      'status': -1   // 设置的点赞状态，-1为取消点赞，1为点赞
  },
  // 成功回调
  complete: console.log
})
```


#### 获取官方相册列表

```javascript
// 小程序端调用
wx.cloud.callFunction({
  // 需调用的云函数名
  name: 'morphy_userAlbum',
  // 传给云函数的参数
  data: {
      '$url':'get',
      'cateId': 'testCateId',  // 指定获取分类的id
      'size': 10,  // 指定每次获取的数量，最大不超过100
      'tags': ['testTagId1', 'testTagId2'],  // 指定获取标签集合
      'page': 10, // 参数可选，指定页数
      'order': 'desc', // 参数可选，指定获取数据的排序规则
      'search': 'xxx', // 参数可选，根据content内容进行搜索
  },
  // 成功回调
  complete: console.log
})
```
