const {
    reponseWrapper
} = require('./utils')
// 云函数入口文件
const cloud = require('wx-server-sdk')
const TcbRouter = require('tcb-router')
cloud.init({
    env: cloud.DYNAMIC_CURRENT_ENV // 给定 DYNAMIC_CURRENT_ENV 常量：接下来的 API 调用都将请求到与该云函数当前所在环境相同的环境
})
const paramsValidatorMiddleware = require('./validator')
// 云函数入口函数
exports.main = async (event) => {
    const app = new TcbRouter({
        event
    })
    const wxContext = cloud.getWXContext()
    const openid = event["morphy_userinfo"] && event["morphy_userinfo"].openid ? event["morphy_userinfo"]["openid"] : wxContext.OPENID
    const db = cloud.database()
    const _ = db.command
    /**
     * 获取活动列表
     */
    app.router('get', paramsValidatorMiddleware(event, {
        page: paramsValidatorMiddleware.number(),
        size: paramsValidatorMiddleware.number(),
    }), async (ctx, next) => {
        const {
            page = 1, size = 10
        } = event
        let map = {
            hidden: false
        }
        try {
            const getResult = await db.collection('morphy_ibeaconManager').where(map).orderBy('createAt', 'desc').skip((page - 1) * size).limit(size).get()
            const totalResult = await db.collection('morphy_ibeaconManager').where(map).count()
            ctx.body = reponseWrapper(0, '获取成功', {
                list: getResult.data,
                total: totalResult.total
            })
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     * add创建活动
     */
    app.router('add', paramsValidatorMiddleware(event, {
        name: paramsValidatorMiddleware.isRequire().string(), //活动名
        actName: paramsValidatorMiddleware.isRequire().string(), //活动代号，不能修改
        uuid: paramsValidatorMiddleware.isRequire().array(), //uuid，数组
        major: paramsValidatorMiddleware.isRequire().string(), //major
        beaconList: paramsValidatorMiddleware.array(), //对象数组，格式{ key: '场馆1',  minors: [59551]}
    }), async (ctx, next) => {
        let {
            name,
            actName,
            desc = "",
            uuid,
            major,
            beaconList
        } = event
        try {
            actName = actName.replace(/\s*/g, "");
            let map = {
                actName,
                hidden: _.or(_.eq(false), _.exists(false))
            }
            //1.首先查询活动代号是否存在，禁止创建相同代号的活动
            const getResult = await db.collection('morphy_ibeaconManager').where(map).get()
            if (getResult.data.length > 0) {
                ctx.body = reponseWrapper(-102, '已存在活动，请勿重复创建')
            } else {
                const data = {
                    name,
                    actName,
                    desc,
                    uuid,
                    major,
                    beaconList,
                    createAt: Date.now(),
                    creator: openid,
                    hidden: false
                }
                const addResult = await db.collection('morphy_ibeaconManager').add({
                    data
                })
                data._id = addResult._id
                if (addResult._id) {
                    ctx.body = reponseWrapper(0, 'iBeacon活动创建成功', data)
                } else {
                    ctx.body = reponseWrapper(-1101)
                }
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    // 删除
    app.router('delete', paramsValidatorMiddleware(event, {
        _id: paramsValidatorMiddleware.isRequire().string(),
        force: paramsValidatorMiddleware.boolean() //是否强制删除
    }), async (ctx, next) => {
        const {
            _id,
            force = false
        } = event
        try {
            if (!force) {
                //假删除
                const deleteUpdateResult = await db.collection('morphy_ibeaconManager').doc(_id).update({
                    data: {
                        hidden: true, //假删除状态
                        hiddenSys: {
                            morphy: {
                                time: Date.now(),
                                by: openid
                            }
                        } //删除时间
                    }
                })
                if (deleteUpdateResult.stats.updated === 1) {
                    ctx.body = reponseWrapper(0, '删除成功')
                } else {
                    ctx.body = reponseWrapper(-1101)
                }
            } else {
                // 真删除
                const deleteResult = await db.collection('morphy_ibeaconManager').doc(_id).remove()
                if (deleteResult.stats.removed === 1) {
                    ctx.body = reponseWrapper(0, '删除成功')
                } else {
                    ctx.body = reponseWrapper(-1101)
                }
            }

        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    //单个表单更新
    app.router('update', paramsValidatorMiddleware(event, {
        _id: paramsValidatorMiddleware.isRequire().string(),
        name: paramsValidatorMiddleware.string(),
        desc: paramsValidatorMiddleware.string(),
        uuid: paramsValidatorMiddleware.isRequire().array(), //uuid，数组
        major: paramsValidatorMiddleware.isRequire().string(), //major
        beaconList: paramsValidatorMiddleware.array(), //对象数组，格式{ key: '场馆1',  minors: [59551]}
    }), async (ctx, next) => {
        const {
            _id,
            name,
            desc,
            uuid,
            major,
            beaconList
        } = event
        try {
            const updateResult = await db.collection('morphy_ibeaconManager').doc(_id).update({
                data: {
                    name,
                    desc,
                    uuid,
                    major,
                    beaconList,
                    updateSys: {
                        morphy: {
                            time: Date.now(),
                            by: openid
                        }
                    }
                }
            })
            if (updateResult.stats.updated === 1) {
                ctx.body = reponseWrapper(0, '更新成功')
            } else {
                ctx.body = reponseWrapper(-1101)
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     * 获取活动详情信息
     */
    app.router('getDetail', paramsValidatorMiddleware(event, {
        actName: paramsValidatorMiddleware.isRequire().string()
    }), async (ctx, next) => {
        const {
            actName
        } = event
        try {
            const getResult = await db.collection('morphy_ibeaconManager').where({
                actName
            }).get()
            if (getResult.data !== undefined && getResult.data.length > 0) {
                ctx.body = reponseWrapper(0, '获取成功', getResult.data[0])
            } else {
                ctx.body = reponseWrapper(-1101, '活动不存在')
            }

        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    //同步创建活动到另外一个环境
    app.router('copyFormTo', paramsValidatorMiddleware(event, {
        _id: paramsValidatorMiddleware.isRequire().string(),
        targetEnv: paramsValidatorMiddleware.isRequire().string(),
        force: paramsValidatorMiddleware.boolean()
    }), async (ctx, next) => {
        const {
            _id,
            targetEnv,
            force = false
        } = event
        try {
            const targetDB = cloud.database({
                env: targetEnv
            })
            const getLocalResult = await db.collection('morphy_ibeaconManager').where({
                _id
            }).get()
            if (getLocalResult.data.length > 0) {
                const formInfo = getLocalResult.data[0];
                const {
                    name,
                    actName,
                    desc,
                    uuid,
                    major,
                    beaconList,
                    hidden = false
                } = formInfo;
                if (!actName) {
                    ctx.body = reponseWrapper(-102, '原表单缺少actName');
                    return
                }
                //查询目标数据库是否存在
                const getTargetResult = await targetDB.collection('morphy_ibeaconManager').where({
                    actName
                }).get()
                console.log(getTargetResult, "getTargetResult")
                //目标数据库已有相同表，采用update
                if (getTargetResult.data && getTargetResult.data.length > 0) {
                    //已存在,是否强制更新
                    if (force) {
                        const updateResult = await targetDB.collection('morphy_ibeaconManager').doc(getTargetResult.data[0]._id).update({
                            data: {
                                name,
                                actName,
                                desc,
                                uuid,
                                major,
                                beaconList,
                                updateSys: {
                                    morphy: {
                                        time: Date.now(),
                                        by: openid
                                    }
                                }
                            }
                        })
                        if (updateResult.stats.updated === 1) {
                            ctx.body = reponseWrapper(0, '同步更新成功')
                        } else {
                            ctx.body = reponseWrapper(-1101)
                        }
                    } else {
                        ctx.body = reponseWrapper(-102, '目标数据库已存在这个活动，无法覆盖')
                    }
                } else {
                    //目标数据库没有相同表，采用insert
                    const addData = {
                        name,
                        actName,
                        desc,
                        uuid,
                        major,
                        beaconList,
                        hidden,
                        createAt: Date.now(),
                        creator: openid
                    }
                    const addResult = await targetDB.collection('morphy_ibeaconManager').add({
                        data: addData
                    })
                    if (addResult._id) {
                        ctx.body = reponseWrapper(0, '同步创建成功')
                    } else {
                        ctx.body = reponseWrapper(-1101)
                    }
                }
            } else {
                ctx.body = reponseWrapper(-102, '当前数据库找不到表单，请重试')
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })

    /**
     * 小程序获取活动详情信息
     */
    app.router('client/getDetail', paramsValidatorMiddleware(event, {
        actName: paramsValidatorMiddleware.isRequire().string()
    }), async (ctx, next) => {
        const {
            actName
        } = event
        try {
            const getResult = await db.collection('morphy_ibeaconManager').where({
                actName
            }).get()

            if (getResult.data !== undefined && getResult.data.length > 0) {
                ctx.body = reponseWrapper(0, '获取成功', getResult.data[0])
            } else {
                ctx.body = reponseWrapper(-1102, '活动不存在')
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     * 小程序获取活动列表
     */
    app.router('get', paramsValidatorMiddleware(event, {
        page: paramsValidatorMiddleware.number(),
        size: paramsValidatorMiddleware.number(),
    }), async (ctx, next) => {
        const {
            page = 1, size = 10
        } = event
        let map = {
            hidden: false
        }
        try {
            const getResult = await db.collection('morphy_ibeaconManager').where(map).orderBy('createAt', 'desc').skip((page - 1) * size).limit(size).get()
            const totalResult = await db.collection('morphy_ibeaconManager').where(map).count()
            ctx.body = reponseWrapper(0, '获取成功', {
                list: getResult.data,
                total: totalResult.total
            })
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    return app.serve()
}