const cloud = require('wx-server-sdk')
const TcbRouter = require('tcb-router')
const paramsValidatorMiddleware = require('./validator')
const {
    reponseWrapper,
    html2Escape
} = require('./utils')

cloud.init({
    env: cloud.DYNAMIC_CURRENT_ENV // 给定 DYNAMIC_CURRENT_ENV 常量：接下来的 API 调用都将请求到与该云函数当前所在环境相同的环境
})
const MAX_LIMIT = 100

const COLLECTION_NAME = 'morphy_officialAlbum'

exports.main = async (event) => {
    // 初始化控制路由
    const app = new TcbRouter({
        event
    })
    const wxContext = cloud.getWXContext()
    const db = cloud.database()
    const _ = db.command
    const openid = event["morphy_userinfo"] && event["morphy_userinfo"].openid ? event["morphy_userinfo"]["openid"] : wxContext.OPENID
    /**
     *
     * @api {method} add 添加相册
     * @apiName add
     * @apiGroup morphy_officialAlbum
     * @apiVersion  1.0.0
     *
     *
     * @apiParam  {String} content 添加的内容
     * @apiParam  {String} cateId 图片分类
     * @apiParam  {Array} tag 图片标签
     * @apiParam  {object} mediaInfo 图片信息
     * @apiParam  {boolean} isShow 是否显示在应用内
     *
     * @apiParamExample  {type} Request-Example:
     * {
     *     content : 'test',
     *     cateId : '32134',
     *     mediaInfo : {
     *        fileUrl: 'http://wwww.pic.com/1.png'
     *        width: 100,
     *        height: 100
     *     },
     *     isShow : false
     * }
     *
     *
     * @apiSuccessExample {type} Success-Response:
     * {
     *     ret: 0,
     *     errMsg: "添加成功"
     * }
     *
     *
     */
    app.router('add', paramsValidatorMiddleware(event, {
        cateId: paramsValidatorMiddleware.isRequire().string(),
        tags: paramsValidatorMiddleware.array(),
        mediaInfo: paramsValidatorMiddleware.isRequire().object(),
        isShow: paramsValidatorMiddleware.boolean(),
        isSticky: paramsValidatorMiddleware.isRequire().boolean(),
        content: paramsValidatorMiddleware.string()
    }), async (ctx) => {

        let {
            content,
            cateId,
            mediaInfo,
            isShow,
            tags,
            isSticky
        } = event
        content = html2Escape(content)
        const currentTime = Date.now()
        const data = {
            cateId, // 图片分类id
            tags,
            content, // 图片描述
            mediaInfo, // 图片信息
            like: [], // 图片点赞数
            createTime: currentTime, // 上传时间
            sortIndex: currentTime, // 排序索引值
            updateTime: currentTime, // 更新时间
            creator: openid, // 上传人的openid
            isShow,
            sticky: {
                status: isSticky
            }
        }
        try {
            const {
                _id
            } = await db.collection(COLLECTION_NAME).add({
                data
            })
            if (_id) {
                data._id = _id
                ctx.body = reponseWrapper(0, '创建成功', data)
            } else {
                ctx.body = reponseWrapper(-1101)
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     *
     * @api {method} delete 删除相册
     * @apiName delete
     * @apiGroup morphy_officialAlbum
     * @apiVersion  1.0.0
     *
     *
     * @apiParam  {String} _id 添加的id
     *
     * @apiParamExample  {type} Request-Example:
     * {
     *     _id : 'test-id'
     * }
     *
     *
     * @apiSuccessExample {type} Success-Response:
     * {
     *     ret : 0,
     *     msg : "删除成功"
     * }
     *
     *
     */
    app.router('delete', paramsValidatorMiddleware(event, {
        _id: paramsValidatorMiddleware.isRequire().string()
    }), async (ctx) => {
        const {
            _id
        } = event
        try {
            const {
                stats: {
                    removed
                }
            } = await db.collection(COLLECTION_NAME).doc(_id).remove()
            if (removed === 1) {
                ctx.body = reponseWrapper(0, '删除成功')
            } else {
                ctx.body = reponseWrapper(-1101)
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    /**
     *
     * @api {method} update 更新相册
     * @apiName update
     * @apiGroup officialAlbum
     * @apiVersion  1.0.0
     *
     * @apiParam  {String} _id 更新的id
     * @apiParam  {String} content 更新的内容
     * @apiParam  {String} cateId 图片分类
     * @apiParam  {object} mediaInfo 图片信息
     * @apiParam  {boolean} isShow 是否显示
     *
     * @apiParamExample  {type} Request-Example:
     * {
     *     _id : 'test-id',
     *     content : 'test',
     *     cateId : '32134',
     *     mediaInfo : {
     *        fileUrl: 'http://wwww.pic.com/1.png'
     *        width: 100,
     *        height: 100
     *     },
     *     isShow : false
     * }
     *
     *
     * @apiSuccessExample {type} Success-Response:
     * {
     *     ret : 0,
     *     errMsg : "更新成功"
     * }
     *
     *
     */
    app.router('update', paramsValidatorMiddleware(event, {
        _id: paramsValidatorMiddleware.isRequire().string(),
        content: paramsValidatorMiddleware.string(),
        cateId: paramsValidatorMiddleware.string(),
        tags: paramsValidatorMiddleware.array(),
        mediaInfo: paramsValidatorMiddleware.object(),
        isShow: paramsValidatorMiddleware.boolean(),
        isSticky: paramsValidatorMiddleware.boolean(),
        sortIndex: paramsValidatorMiddleware.number()
    }), async (ctx) => {
        const {
            _id,
            isSticky, //是否置顶
            isShow, //是否发布
        } = event
        const allowUpdateProps = ['content', 'cateId', 'mediaInfo', 'tags', 'sortIndex']
        const updateObejct = {}
        const currentTime = Date.now(); //当前时间
        Object.keys(event).forEach((key) => {
            if (allowUpdateProps.indexOf(key) !== -1) {
                updateObejct[key] = event[key]
            }
        })
        if (isSticky !== undefined) {
            updateObejct["sticky"] = _.set({
                status: isSticky,
                time: currentTime,
                by: openid //待替换
            })
        }
        if (isShow !== undefined) {
            updateObejct["isShow"] = isShow;
            if (isShow) {
                updateObejct["releasedInfo"] = {
                    time: currentTime,
                    by: openid
                };
            } //发布时间
            if (!isShow) {
                updateObejct["offShelfInfo"] = {
                    time: currentTime,
                    by: openid
                };
            } //下架时间

        }
        try {
            const {
                stats: {
                    updated
                }
            } = await db.collection(COLLECTION_NAME).doc(_id).update({
                data: {
                    ...updateObejct,
                    updateTime: currentTime
                }
            })
            if (updated === 1) {
                const getResult = await db.collection(COLLECTION_NAME).doc(_id).get()
                ctx.body = reponseWrapper(0, '更新成功', getResult.data)
            } else {
                ctx.body = reponseWrapper(-1101)
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    app.router('batchRelease', paramsValidatorMiddleware(event, {
        batchId: paramsValidatorMiddleware.isRequire().array()
    }), async (ctx, next) => {
        try {
            const {
                batchId
            } = event
            const updateResult = await db.collection(COLLECTION_NAME).where({
                _id: _.in(batchId)
            }).update({
                data: {
                    isShow: true,
                    releaseAt: Date.now(),
                    updateAt: Date.now()
                }
            })
            if (updateResult.stats.updated !== 0) {
                ctx.body = reponseWrapper(0, '更新成功')
            } else {
                ctx.body = reponseWrapper(-1101)
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }

    })
    app.router('batchOut', paramsValidatorMiddleware(event, {
        batchId: paramsValidatorMiddleware.isRequire().array()
    }), async (ctx, next) => {
        try {
            const {
                batchId
            } = event
            const updateResult = await db.collection(COLLECTION_NAME).where({
                _id: _.in(batchId)
            }).update({
                data: {
                    isShow: false,
                    updateAt: Date.now()
                }
            })
            if (updateResult.stats.updated !== 0) {
                ctx.body = reponseWrapper(0, '更新成功')
            } else {
                ctx.body = reponseWrapper(-1101)
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }

    })
    app.router('batchDelete', paramsValidatorMiddleware(event, {
        batchId: paramsValidatorMiddleware.isRequire().array()
    }), async (ctx, next) => {
        try {
            const {
                batchId
            } = event
            const removeResult = await db.collection(COLLECTION_NAME).where({
                _id: _.in(batchId)
            }).remove()
            if (removeResult.stats.removed !== 0) {
                ctx.body = reponseWrapper(0, '删除成功')
            } else {
                ctx.body = reponseWrapper(-1101)
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }

    })
    app.router('batchAdd', paramsValidatorMiddleware(event, {
        mediaInfoList: paramsValidatorMiddleware.isRequire().array(),
        selectCateId: paramsValidatorMiddleware.isRequire().string(),
        selectedTags: paramsValidatorMiddleware.array()
    }), async (ctx, next) => {
        try {
            const {
                mediaInfoList,
                selectCateId,
                selectedTags = []
            } = event
            let i = 0
            const len = mediaInfoList.length
            for (; i < len; i++) {
                const currentTime = Date.now()
                const addResult = await db.collection(COLLECTION_NAME).add({
                    data: {
                        cateId: selectCateId, // 图片分类id
                        tags: selectedTags,
                        content: '', // 图片描述
                        mediaInfo: mediaInfoList[i], // 图片信息
                        like: [], // 图片点赞数
                        createTime: currentTime, // 上传时间
                        sortIndex: currentTime, // 排序索引
                        updateTime: currentTime, // 更新时间
                        creator: openid, // 上传人的openid
                        isShow: false,
                        sticky: {
                            status: false
                        }
                    }
                })
            }
            ctx.body = reponseWrapper(0, '批量上传成功')
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }

    })
    /**
     * 获取官方相册
     */
    app.router('get', paramsValidatorMiddleware(event, {
        cateId: paramsValidatorMiddleware.string(),
        size: paramsValidatorMiddleware.number(),
        tags: paramsValidatorMiddleware.array(),
        page: paramsValidatorMiddleware.number(),
        order: paramsValidatorMiddleware.enums('desc', 'asc'),
        search: paramsValidatorMiddleware.string()
    }), async (ctx) => {
        const {
            order = 'desc',
                cateId,
                tags,
                search,
                size = 10,
                page = 1,
                fileType
        } = event
        const map = {}
        const countMap = {}
        if (cateId) {
            map.cateId = cateId
            countMap.cateId = cateId
        }
        // https://developers.weixin.qq.com/minigame/dev/wxcloud/guide/database/query-array-object.html
        if (tags && tags.length > 0) {
            map.tags = _.all(tags)
            countMap.tags = _.all(tags)
        }
        if (search) {
            map.content = new db.RegExp({
                regexp: search,
                options: 'i'
            })
            countMap.content = new db.RegExp({
                regexp: search,
                options: 'i'
            })
        }
        try {
            const {
                data,
                data: {
                    len
                }
            } = await db.collection(COLLECTION_NAME)
                .where(map)
                .orderBy('sortIndex', order)
                .skip((page - 1) * size)
                .limit(size)
                .get()
            const {
                total
            } = await db.collection(COLLECTION_NAME).where(countMap).count()
            let i = 0
            for (; i < len; i++) {
                data[i].isLiked = (data[i].like.indexOf(openid) !== -1) // 自己是否点赞
                data[i].likeCount = data[i].like.length // 总点赞数
            }
            ctx.body = reponseWrapper(0, '获取成功', {
                list: data,
                total
            })
        } catch (e) {
            ctx.body = reponseWrapper(-1102)
        }
    })
    /**
     * 设置点赞状态官方相册
     */
    app.router('setLike', paramsValidatorMiddleware(event, {
        _id: paramsValidatorMiddleware.isRequire().string(),
        status: paramsValidatorMiddleware.isRequire().enums(-1, 1)
    }), async (ctx) => {
        const {
            _id,
            status
        } = event
        try {
            const {
                data: {
                    like
                }
            } = await db.collection(COLLECTION_NAME).doc(_id).get()
            let updateResult = null
            if (status === 1) {
                const indexOf = like.indexOf(openid)
                like.splice(indexOf, 1)
                updateResult = await db.collection(COLLECTION_NAME).doc(_id).update({
                    data: {
                        like
                    }
                })
            } else {
                updateResult = await db.collection(COLLECTION_NAME).doc(_id).update({
                    data: {
                        like: _.push(openid)
                    }
                })
            }
            if (updateResult.stats.updated === 1) {
                ctx.body = reponseWrapper(0, '更新成功')
            } else {
                ctx.body = reponseWrapper(-1101, '更新成功')
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102)
        }
    })

    app.router('addTag', paramsValidatorMiddleware(event, {
        name: paramsValidatorMiddleware.isRequire().string()
    }), async (ctx) => {
        const {
            name
        } = event

        const getResult = await db.collection('morphy_tag').where({
            name
        }).get()
        if (getResult.data.length > 0) {
            ctx.body = reponseWrapper(-102, '创建的标签已存在')
        } else {
            const data = {
                name,
                creator: openid,
                createAt: Date.now(),
                updateAt: Date.now()
            }
            const addResult = await db.collection('morphy_tag').add({
                data
            })
            if (addResult._id) {
                data._id = addResult._id
                ctx.body = reponseWrapper(0, '创建成功', data)
            } else {
                ctx.body = reponseWrapper(-1101)
            }
        }
    })
    app.router('updateTag', paramsValidatorMiddleware(event, {
        _id: paramsValidatorMiddleware.isRequire().string(),
        name: paramsValidatorMiddleware.isRequire().string()
    }), async (ctx) => {
        const {
            _id,
            name
        } = event
        try {
            const {
                stats: {
                    updated
                }
            } = await db.collection('morphy_tag').doc(_id).update({
                data: {
                    name,
                    updateAt: Date.now()
                }
            })
            if (updated === 1) {
                const getResult = await db.collection('morphy_tag').doc(_id).get()
                ctx.body = reponseWrapper(0, '更新成功', getResult.data)
            } else {
                ctx.body = reponseWrapper(-1101)
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }

    })
    app.router('deleteTag', paramsValidatorMiddleware(event, {
        _id: paramsValidatorMiddleware.isRequire().string()
    }), async (ctx, next) => {
        const {
            _id
        } = event
        try {
            const {
                stats: {
                    removed
                }
            } = await db.collection('morphy_tag').doc(_id).remove()
            if (removed === 1) {
                ctx.body = reponseWrapper(0, '删除成功')
            } else {
                ctx.body = reponseWrapper(-1101)
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }
    })
    app.router('getTag', async (ctx, next) => {
        const {
            data
        } = await db.collection('morphy_tag').get()
        ctx.body = reponseWrapper(0, '请求成功', data)
    })

    app.router('addCate', paramsValidatorMiddleware(event, {
        name: paramsValidatorMiddleware.isRequire().string(),
        order: paramsValidatorMiddleware.isRequire().number(),
        isShow: paramsValidatorMiddleware.isRequire().boolean()
    }), async (ctx) => {
        const {
            name,
            order,
            isShow
        } = event
        try {
            const getResult = await db.collection('morphy_cate').where({
                name
            }).get()
            if (getResult.data.length > 0) {
                ctx.body = reponseWrapper(-103, '添加失败，该分类已经存在，请勿重复添加')
            } else {
                const data = {
                    name,
                    order,
                    isShow,
                    createAt: Date.now(),
                    updateAt: Date.now()
                }
                const {
                    _id
                } = await db.collection('morphy_cate').add({
                    data
                })
                if (_id) {
                    data._id = _id
                    ctx.body = reponseWrapper(0, '添加成功', data)
                } else {
                    ctx.body = reponseWrapper(-1101)
                }
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }

    })
    app.router('updateCate', paramsValidatorMiddleware(event, {
        _id: paramsValidatorMiddleware.isRequire().string(),
        name: paramsValidatorMiddleware.string(),
        order: paramsValidatorMiddleware.number(),
        isShow: paramsValidatorMiddleware.boolean()
    }), async (ctx) => {
        const {
            _id
        } = event
        const allowUpdateProps = ['name', 'order', 'isShow']
        const updateData = {}
        Object.keys(event).forEach(key => {
            if (allowUpdateProps.indexOf(key) !== -1) {
                updateData[key] = event[key]
            }
        })
        let data = {
            ...updateData,
            updateAt: Date.now()
        }
        const {
            stats: {
                updated
            }
        } = await db.collection('morphy_cate').doc(_id).update({
            data: {
                ...updateData,
                updateAt: Date.now()
            }
        })
        if (updated === 1) {
            const getResult = await db.collection('morphy_cate').doc(_id).get()
            ctx.body = reponseWrapper(0, '更新成功', getResult.data)
        } else {
            ctx.body = reponseWrapper(-1101)
        }
    })
    app.router('deleteCate', paramsValidatorMiddleware(event, {
        _id: paramsValidatorMiddleware.isRequire().string()
    }), async (ctx) => {
        try {
            const {
                _id
            } = event
            const {
                stats: {
                    removed
                }
            } = await db.collection('morphy_cate').doc(_id).remove()
            if (removed === 1) {
                ctx.body = reponseWrapper(0, '删除成功')
            } else {
                ctx.body = reponseWrapper(-1101)
            }
        } catch (e) {
            ctx.body = reponseWrapper(-1102, e.message)
        }

    })
    app.router('getCate', paramsValidatorMiddleware({
        sort: paramsValidatorMiddleware.enums('desc', 'asc')
    }), async (ctx) => {
        const {
            sort = 'desc'
        } = event
        const {
            data
        } = await db.collection('morphy_cate').orderBy('order', sort).get()
        ctx.body = reponseWrapper(0, '请求成功', data)
    })
     /**
     * 客户端获取官方相册列表
     */
    app.router('client/get', paramsValidatorMiddleware(event, {
        cateId: paramsValidatorMiddleware.string(),
        size: paramsValidatorMiddleware.number(),
        tags: paramsValidatorMiddleware.array(),
        page: paramsValidatorMiddleware.number(),
        order: paramsValidatorMiddleware.enums('desc', 'asc'),
        search: paramsValidatorMiddleware.string(),
        showAll: paramsValidatorMiddleware.boolean() //默认是false，即只显示发布过的内容，除非特殊指定
    }), async (ctx) => {
        const {
            order = 'desc',
                cateId,
                tags,
                search,
                size = 10,
                page = 1,
                fileType,
                showAll=false
        } = event
        const map = {}
        const countMap = {}
        if(!showAll){
            map.isShow=true
            countMap.isShow = true
        }
        if (cateId) {
            map.cateId = cateId
            countMap.cateId = cateId
        }
        // https://developers.weixin.qq.com/minigame/dev/wxcloud/guide/database/query-array-object.html
        if (tags && tags.length > 0) {
            map.tags = _.all(tags)
            countMap.tags = _.all(tags)
        }
        if (search) {
            map.content = new db.RegExp({
                regexp: search,
                options: 'i'
            })
            countMap.content = new db.RegExp({
                regexp: search,
                options: 'i'
            })
        }
        try {
            const {
                data,
                data: {
                    len
                }
            } = await db.collection(COLLECTION_NAME)
                .where(map)
                .orderBy('sortIndex', order)
                .skip((page - 1) * size)
                .limit(size)
                .get()
            const {
                total
            } = await db.collection(COLLECTION_NAME).where(countMap).count()
            let i = 0
            for (; i < len; i++) {
                data[i].isLiked = (data[i].like.indexOf(openid) !== -1) // 自己是否点赞
                data[i].likeCount = data[i].like.length // 总点赞数
            }
            ctx.body = reponseWrapper(0, '获取成功', {
                list: data,
                total
            })
        } catch (e) {
            ctx.body = reponseWrapper(-1102)
        }
    })
    return app.serve()
}
