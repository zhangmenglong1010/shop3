## 官方相册

模块名：自定义表单

版本：`V1.0`

维护人：nornorchen

云函数：`morphy_officialAlbum`

特性：支持定义表单（报名，问卷，满意度调查）。创建表单可以定义是单行，多行，单选，多选等问题类型。同时也可以创建操作类型的按钮用于管理表单，如布尔型操作（是否类），状态型操作（通过，拒绝，未处理）

### 小程序前端接口

>开发约定：由于该模块使用云函数，因此可直接按照微信调用云函数方法的格式调用，包括promise链式调用以及complete等回调调用

#### 获取官方相册分类列表

```javascript
// 小程序端调用
wx.cloud.callFunction({
  // 需调用的云函数名
  name: 'morphy_officialAlbum',
  // 传给云函数的参数
  data: {
      '$url':'getCate',
      'sort': 'desc'   // 可以选择两个值 desc|asc，默认为desc
  },
  // 成功回调
  complete: console.log
})
```

#### 获取官方相册标签列表

```javascript
// 小程序端调用
wx.cloud.callFunction({
  // 需调用的云函数名
  name: 'morphy_officialAlbum',
  // 传给云函数的参数
  data: {
      '$url':'getTag'
  },
  // 成功回调
  complete: console.log
})
```


#### 获取官方相册列表

```javascript
// 小程序端调用
wx.cloud.callFunction({
  // 需调用的云函数名
  name: 'morphy_officialAlbum',
  // 传给云函数的参数
  data: {
      '$url':'get',
      'cateId': 'testCateId',  // 指定获取分类的id
      'size': 10,  // 指定每次获取的数量，最大不超过100
      'tags': ['testTagId1', 'testTagId2'],  // 指定获取标签集合
      'page': 10, // 参数可选，指定页数
      'order': 'desc', // 参数可选，指定获取数据的排序规则
      'search': 'xxx', // 参数可选，根据content内容进行搜索
  },
  // 成功回调
  complete: console.log
})
```
