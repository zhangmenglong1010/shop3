function reponseWrapper(code, errMsg, data) {
    const defaults = {
        '-1101': '数据库操作未成功'
    }
    if (defaults[code]) {
        errMsg = defaults[code]
    }
    return {
        code,
        errMsg,
        data
    }
}

function html2Escape(str) {
    if (str) {
        return str.replace(/[<, >, &, "]/g, (code) => {
            return {
                '<': '&lt;',
                '>': '&gt;',
                '&': '&amp;',
                '"': '&quot;'
            } [code]
        })
    }
    return ''

}
module.exports = {
    reponseWrapper,
    html2Escape
}
