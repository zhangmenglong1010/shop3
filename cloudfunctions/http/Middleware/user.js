/**
 * 中间件 - 用户信息
 */
const {
	apiResponse
} = require('../Services/http');
const {
	db
} = require('../Services/cloud');
const {
	getNextSequenceValue
} = require('../Services/helper');
const {
	getUserInfo,
	findOrCreateUser
} = require('../Services/user');
const {
	defaultAvatar
} = require('../Services/env');

module.exports = async (ctx, next) => {
	let OPENID = ctx.request.body.openid;

	if (!OPENID || typeof OPENID !== 'string') {
		ctx.body = apiResponse(10003, null, '请求参数错误，缺少[openid]');
		return;
	}

	// 获取openid对应的用户数组，判断长度是否为1
	// let usersData = await db
	// 	.collection('vs_users')
	// 	.where({
	// 		openid: OPENID,
	// 	})
	// 	.get();
	// let user = null;
	let user = await findOrCreateUser(OPENID);
	// 多于1个用户，抛出异常
	// if (usersData.data.length > 1) {
	// 	user = usersData.data[0];
	// 	// ctx.body = apiResponse(20003, null, '数据异常，出现两个或以上该openid对应的用户');
	// 	// return;

	// 	// 未找到该用户
	// } else if (usersData.data.length === 0) {
	// 	// ctx.body = apiResponse(10001, null, '未找到该openid对应的用户');
	// 	// return;
	// 	// 添加到数据库中
	// 	let userSequenceValue = await getNextSequenceValue('user_id'); // 获取自增数
	// 	let userUnrealSequenceValue = await getNextSequenceValue('unreal_user_id', true); // 获取自增数，并自增一个随机数
	// 	let timestamp = new Date().getTime();
	// 	const insertor = await db.collection('vs_users').add({
	// 		data: {
	// 			increment: userSequenceValue, // 自增id
	// 			incCount: userUnrealSequenceValue, // 虚拟自增id
	// 			avatar: defaultAvatar, // 先提供默认头像
	// 			name: `用户${userUnrealSequenceValue}`, // 和默认昵称
	// 			// tempName: '', // 用户待审核的昵称
	// 			// tempAvatar: '', // 用户待审核的头像
	// 			// isNameCheckup: 1, // 昵称是否已审核，当tempName修改时，修改为0，审核通过改为1，审核不通过改为2
	// 			// isAvatarCheckup: 1, // 头像是否已审核，当tempAvatar修改时，修改为0，审核通过改为1，审核不通过改为2
	// 			openid: OPENID,
	// 			unionid: '',
	// 			tutorId: -1, // 无导师
	// 			score: -1, // 无分数
	// 			timestamp: timestamp, // 生成用户的时间戳
	// 			scoreTimestamp: 0, // 分数生成时的时间戳
	// 			reportVideoCosKey: '', // 合成视频下载地址
	// 			testScoreList: {}, // 6个测试成绩细节
	// 		},
	// 	});
	// 	if (!insertor._id) {
	// 		ctx.body = apiResponse(20401, {}, '录入用户失败，语句返回结果中缺少_id');
	// 		return;
	// 	}
	// 	// 更新头像/昵称
	// 	const selector = await db.collection('vs_users').doc(insertor._id).get();
	// 	user = selector.data;
	// } else {
	// 	user = usersData.data[0];
	// }
	let {
		name,
		avatar,
		team,
		teamLogo
	} = await getUserInfo(user);
	user.name = name;
	user.avatar = avatar;
	user.teamName = team;
	user.team = teamLogo;
	ctx.request.user = user;
	await next();
};