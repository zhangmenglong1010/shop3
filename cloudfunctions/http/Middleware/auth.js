/**
 * 中间件 - http算法鉴权
 */
const { apiResponse, requestValidation } = require('../Services/http');
const { httpSecretKey } = require('../Services/env');
const { md5 } = require('../Services/string');

module.exports = async (ctx, next) => {

	try {
		let params = await requestValidation(ctx.request.body, {
			time: 'required|integer|min:0|max:2000000000000',
			nonce: 'required|string|min:16|max:16',
			token: 'required|string|min:32|max:32',
		});
		let time = params.time;
		let nonce = params.nonce;
		let token = params.token.toLowerCase();
		let currentTimestamp = new Date().getTime();
		let delta = currentTimestamp - time;
		if (delta > 1000 * 60 * 5 || delta < -1000 * 60 * 5) {
			// 5分钟内有效
			ctx.body = apiResponse(10004, null, '鉴权失败，超时');
			return;
		}

		if (md5(`${httpSecretKey}${time}${nonce}`) !== token) {
			ctx.body = apiResponse(10004, null, '鉴权失败，验证错误');
			return;
		}
	} catch (err) {
		ctx.body = apiResponse(10004, null, '鉴权失败，字段缺失');
		return;
	}

	try {
		await next();
	} catch (err) {
		if (err.code && err.msg) {
			ctx.body = err;
		} else {
			ctx.body = apiResponse(20001, err, '后台错误');
		}
	}
};
