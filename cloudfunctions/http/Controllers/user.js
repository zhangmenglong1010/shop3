const { apiResponse, requestValidation } = require('../Services/http');
const { db, cloud } = require('../Services/cloud');
const { addScoreToRank, getTotalScoreLevel, getCanSignTutor, getCanAddPhoto, getCanplayInfo } = require('../Services/user');
const { checkGameTestParam, calculateField, getCurrentRankLowestScore } = require('../Services/game');
const { requestVideoReport } = require('../Services/helper');
const {unLimitOpenid} = require('../Services/env');

module.exports = {
	/**
	 * @description 获取用户信息
	 * @method GET
	 * @param {*} ctx
	 */
	async detail(ctx) {
		const user = ctx.request.user;
		let canSignTutor,canAddPhoto,canplay;
		// 白名单内用户，直接返回都为true，不添加次数限制，白名单外的无限次数
		if(unLimitOpenid.indexOf(user.openid) < 0){
			canSignTutor = await getCanSignTutor(user._id);
			canAddPhoto = await getCanAddPhoto(user._id);
			canplay = await getCanplayInfo(user._id);
		}else{
			canSignTutor = true;
			canAddPhoto = true;
			canplay = {
				'1': true,
				'2': true,
				'3': true,
				'4': true,
				'5': true,
				'6': true,
			}
		}
		user.canSignTutor = canSignTutor;
		user.canAddPhoto = canAddPhoto;
		user.canplay = canplay;
		ctx.body = apiResponse(0, user);
	},

	/**
	 * @description 选择导师
	 * @param {object} query 请求数据参数
	 */
	async signTutor(ctx) {
		const user = ctx.request.user;
		const params = await requestValidation(ctx.request.body, {
			tutorId: 'required|integer|exists:vs_tutors,id', // 11 个导师
		});
		console.log(user);
		// 已选过导师
		// if (user.tutorId >= 0) {

		// }


		let updateMsg = await db
			.collection('vs_users')
			.doc(user._id)
			.update({
				data: {
					tutorChangedAt: Date.now(),
					tutorId: params.tutorId,
					activityTimestamp: Date.now(),
				},
			});

		// 创建导师选择记录
		let insertTutor = await db.collection('vs_user_tutors').add({
			data : {
				uid : user._id,
				created_at : Date.now(),
				tutorId : params.tutorId
			}
		})

		if(!insertTutor._id){
			console.error('导师记录创建失败，insertor执行失败');
		}

		let selector = await db.collection('vs_users').doc(user._id).get();
		let result = {};
		result._id = selector.data._id;
		result.tutorId = selector.data.tutorId;
		if (updateMsg.stats.updated === 1) {
			ctx.body = apiResponse(0, result, '选择导师成功');
		} else {
			ctx.body = apiResponse(0, result, '更新导师完成，无更新记录');
		}
	},

	/**
	 * @desc 获取时间戳后面的最新用户成绩，如果成绩中已有5个或6个，则更新最新成绩，且是否进入前10名，是则进入审核，审核不通过则必须修改其某项成绩，并申明理由
	 * @param {object} query
	 * @param {string} query.openid openid
	 * @param {number} query.gameTestId 当前完成了第几项测试，参数范围1-6,
	 * @param {number} query.score 当前用户的成绩
	 * @param {object} query.param 本次测试中的详细数据
	 */
	async signGameTest(ctx) {
		const user = ctx.request.user;
		const timestamp = new Date().getTime();
		const params = await requestValidation(ctx.request.body, {
			gameTestId: 'required|integer|min:1|max:6', // 11 个导师
			score: 'required|float|min:0|max:100',
			param: 'jsonString|max:1024',
			basic: 'required|jsonString|max:1024',
			'basic.multithread': 'float|max:100',
			'basic.achieve': 'float|max:100',
			'basic.focus': 'float|max:100',
			'basic.coordinate': 'float|max:100',
			'basic.compressive': 'float|max:100',
			'basic.dispatch': 'float|max:100',
			'basic.instant': 'float|max:100',
			attach: 'jsonString|max:1024',
			'attach.multithread': 'float|max:100',
			'attach.achieve': 'float|max:100',
			'attach.focus': 'float|max:100',
			'attach.coordinate': 'float|max:100',
			'attach.compressive': 'float|max:100',
			'attach.dispatch': 'float|max:100',
			'attach.instant': 'float|max:100',
		});

		// let canplayInfo = await getCanplayInfo();
		// if (canplayInfo[params.gameTestId] === false) {
		// 	throw apiResponse(10004, null, '分数提交失败，今天该测试的体验次数已超上限，请明天再来');
		// }

		// 多线程，成就，专注，协调，抗压，调度，即战
		let keyList = ['multithread', 'achieve', 'focus', 'coordinate', 'compressive', 'dispatch', 'instant'];
		let basic = {},
			attach = {};
		for (let key in params.basic) {
			if (keyList.indexOf(key) >= 0 && params.basic[key] >= 0) {
				basic[key] = params.basic[key];
			}
		}
		if (params.attach) {
			for (let key in params.attach) {
				if (keyList.indexOf(key) >= 0 && params.attach[key] >= 0) {
					attach[key] = params.attach[key];
				}
			}
		}

		if (params.param) {
			try {
				// 检查json字段中是否带字符串“-”
				checkGameTestParam(params.param);
			} catch (err) {
				ctx.body = apiResponse(10003, null, '参数[param]格式错误');
				return;
			}
		}

		// 不存在userInfo.testScoreList时初始化
		let testScoreList = {};
		if (user.testScoreList) {
			testScoreList = user.testScoreList;
		}
		testScoreList[params.gameTestId] = {
			score: params.score,
			param: params.param || {},
			timestamp: timestamp,
			created_at: timestamp,
			basic,
			attach,
		};
		let length = 0;
		for (let key in testScoreList) {
			length++;
		}
		const _ = db.command;
		let updateData = {
			scoreTimestamp: timestamp,
			testScoreList: _.set(testScoreList), // 整个对象进行替换
			activityTimestamp: Date.now(),
		};
		// 有一个测试也计算维度，用于维度展示
		updateData.totalScoreDetail = calculateField(testScoreList);
		if (length >= 6) {
			// 当有第6个测试，且导师id为-1时，随机分配一个导师
			if (user.tutorId === -1) {
				updateData.tutorId = user.tutorId = Math.floor(Math.random() * 11);
			}
			// 已经有6个测试了，计算总分
			let totalScore = 0, count = 0;
			for (let key in updateData.totalScoreDetail) {
				let item = updateData.totalScoreDetail[key];
				if (typeof item === 'number') {
					count += 1;
					totalScore += item;
				}
			}
			// 得出该用户的总分
			updateData.score = Number((totalScore / count).toFixed(1));
			// 计算优秀还是一般
			// 计算百分比，获取是优秀还是一般
			updateData.level = await getTotalScoreLevel(updateData.score);

			// 计算该用户是否进入排行榜
			// 获取当前能进入排行榜的最低分数
			let currentRankLowestScore = await getCurrentRankLowestScore();
			if (updateData.score > currentRankLowestScore) {
				// 进入榜单，添加一条待审核的数据，而且，删除该用户的以往数据
				await addScoreToRank(user, updateData.score);
			}
			// 发送请求，生成报告
			// await requestVideoReport({
			// 	openid: user.openid,
			// 	name: user.name, // 用户昵称
			// 	avatar: user.avatar, // 用户头像
			// 	tutorId: user.tutorId, // 导师id
			// 	totalScoreDetail: updateData.totalScoreDetail,
			// 	totalScore: updateData.score,
			// 	level: updateData.level,
			// });
		}
		let updator = await db.collection('vs_users').doc(user._id).update({
			data: updateData,
		});
		if (updator.stats.updated !== 1) {
			throw apiResponse(20005, null, '分数更新失败，updator执行失败');
		}
		// 将分数记录到单独的分数表中，用于数据统计
		let insertor = await db.collection('vs_scores').add({
			data: {
				uid: user._id,
				score: params.score,
				created_at: timestamp,
				basic,
				attach,
				gameTestId: params.gameTestId
			}
		});
		if (!insertor._id) {
			console.error('分数记录创建失败，insertor执行失败');
			// 上报错误
		}
		// 获取新插入到数据表的用户数据
		let result = await db.collection('vs_users').doc(user._id).get();

		// 计算该用户的当前分占比排名
		// 获取所有参加过该测试的人数
		let totalCount = await db
			.collection('vs_users')
			.where({
				[`testScoreList.${params.gameTestId}.score`]: _.gte(0),
			})
			.count();
		// 获取该测试中低于该分数的人数
		let beatCount = await db
			.collection('vs_users')
			.where({
				[`testScoreList.${params.gameTestId}.score`]: _.lt(params.score),
			})
			.count();
		ctx.body = apiResponse(
			0,
			Object.assign(
				{
					totalCount: totalCount.total,
					beatCount: beatCount.total,
				},
				result.data
			),
			'添加分数成功'
		);
	},
};
