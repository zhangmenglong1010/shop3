const { apiResponse, requestValidation } = require('../Services/http');
const { db } = require('../Services/cloud');
const { getUserListInfo } = require('../Services/user');
const { getSupportList } = require('../Services/support');

module.exports = {
	/**
	 * 获取时间戳到当前时间内的应援数据
	 */
	async latestList(ctx) {
		const params = await requestValidation(ctx.request.body, {
			timestamp: 'integer|min:0|max:5000000000000',
			quantity: 'integer|max:100|min:1', // N条数据
		});
		if (params.quantity === undefined) {
			params.quantity = 10;
		}
		if (params.timestamp === undefined) {
			params.timestamp = new Date().getTime() - 1000 * 3600 * 24;
		}

		let supportData = await getSupportList(params);

		// 读取涉及的用户列表信息
		let userListJson = await getUserListInfo(supportData.map((item) => item.openid));

		// 添加用户头像/昵称
		const result = [];
		supportData.forEach((item) => {
			if (item.formData) {
				result.push({
					_id: item._id,
					text: item.formData.text,
					name: userListJson[item.openid].name,
					avatar: userListJson[item.openid].avatar,
					createAt: item.createAt
				});
			}
		});
		// const _ = db.command;
		// let supportFormSelector = await db
		// 	.collection('morphy_formSetting')
		// 	.where({
		// 		tableName: 'support',
		// 	})
		// 	.get();
		// if (!supportFormSelector.data.length) {
		// 	throw apiResponse(10005, null, '未找到自定义的support表字段');
		// }
		// let supportFormId = supportFormSelector.data[0]._id;
		// let selector = await db
		// 	.collection('morphy_formData')
		// 	.where(
		// 		_.and([
		// 			{
		// 				formSettingId: supportFormId, // 应援类型
		// 			},
		// 			{
		// 				'formData.isCheck': 1, // 已过审
		// 			},
		// 			{
		// 				createAt: _.gte(params.timestamp), // 时间戳
		// 			},
		// 		])
		// 	)
		// 	.orderBy('timestamp', 'desc')
		// 	.limit(params.quantity)
		// 	.get();
		ctx.body = apiResponse(
			0,
			{
				timestamp: new Date().getTime(),
				list: result,
			},
			'获取时间戳到当前时间内的应援数据成功'
		);
	},
};
