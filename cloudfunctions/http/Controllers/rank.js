const {
	apiResponse,
	requestValidation
} = require('../Services/http');
const {
	db,
	cloud
} = require('../Services/cloud');
const {
	getLatestRank,
	getRankListWithUserInfo
} = require('../Services/rank');

module.exports = {
	/**
	 * 获取分数排行榜列表
	 */
	async getScoreRank(ctx) {
		const params = await requestValidation(ctx.request.body, {
			quantity: 'integer|max:100|min:1',
		});

		if (params.quantity === undefined) {
			params.quantity = 20;
		}

		// 前N名
		const result = await getRankListWithUserInfo(params.quantity);

		// let selector = await db.collection('vs_users').orderBy('score', 'desc').limit(params.quantity).get();
		ctx.body = apiResponse(0, result, '获取天梯榜成功');
	},

	/**
	 * 时间戳后面最新用户列表成绩
	 */
	async getLatestUserListScore(ctx) {
		const params = await requestValidation(ctx.request.body, {
			timestamp: 'integer|min:0|max:5000000000000',
			quantity: 'integer|max:100|min:1',
		});
		if (params.quantity === undefined) {
			params.quantity = 10;
		}
		if (params.timestamp === undefined) {
			params.timestamp = new Date().getTime() - 1000 * 3600 * 24;
		}
		let list = await getLatestRank(params.timestamp, params.quantity);
		
		ctx.body = apiResponse(
			0, {
				timestamp: new Date().getTime(),
				list,
			},
			//'获取时间戳后的最新用户成绩成功'
		);
	},
};