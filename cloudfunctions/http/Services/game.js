const { db } = require("./cloud");
const { getRankList } = require('./rank');

module.exports = {
	// 检查字段是否带-
	checkGameTestParam(params) {
		for (let key in params) {
			if (key.indexOf('-') >= 0) {
				throw `param字段有误，${key}`;
			}
		}
	},

	// 计算7个维度的分数
	calculateField(testScoreList) {
		let basicWeights = 0.8;
		let attachWeights = 1 - basicWeights;

		// 多线程，成就，专注，协调，抗压，调度，即战
		let keyList = ['multithread', 'achieve', 'focus', 'coordinate', 'compressive', 'dispatch', 'instant'];
		let typeList = ['basic', 'attach'];
		let total = {};
		keyList.forEach((item) => {
			total[item] = {};
			typeList.forEach((typeName) => {
				total[item][typeName] = {
					total: 0,
					count: 0,
				};
			});
		});

		// 遍历，记录相同维度的总分和次数，用以计算平均数
		for (let key in testScoreList) {
			// 1 2 3 4 5 6
			let scoreItem = testScoreList[key];

			typeList.forEach((typeName) => {
				if (scoreItem[typeName]) {
					for (let key in scoreItem[typeName]) {
						if (keyList.indexOf(key) >= 0) {
							total[key][typeName].total += scoreItem[typeName][key];
							total[key][typeName].count++;
						}
					}
				}
			});
		}

		// 得出结果
		let result = {};
		keyList.forEach((keyName) => {
			// 当一个维度同时有基础采样和隐形采样时，各占 0.8 和 0.2 的比重
			if (total[keyName].basic.count > 0 && total[keyName].attach.count > 0) {
				result[keyName] =
					(total[keyName].basic.total / total[keyName].basic.count) * basicWeights +
					(total[keyName].attach.total / total[keyName].attach.count) * attachWeights;

				// 当只有一种采样时，直接算平均数
			} else if (total[keyName].basic.count > 0) {
				result[keyName] = total[keyName].basic.total / total[keyName].basic.count;
			} else if (total[keyName].attach.count > 0) {
				result[keyName] = total[keyName].attach.total / total[keyName].attach.count;
			} else {
				result[keyName] = 0;
			}
			result[keyName] = Math.round(result[keyName]);
		});

		return result;
		// return 10;
	},

	/**
	 * @description 获取当前能进入排行榜的最低分数
	 * @param {int} rankCount 前N名 默认为前100名
	 * @param 
	 */
	async getCurrentRankLowestScore(quantity = 100) {
		const getResult = await getRankList(quantity);

		let length = getResult.data.length
		if (length < quantity) { // 不足quantity人数
			return 0;
		} else if (length === 0) {
			return 0;
		} else {
			return getResult.data[length - 1].formData.score;
		}
	}
};
