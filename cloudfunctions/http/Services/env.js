const cloud = require('wx-server-sdk');

module.exports = {
  httpSecretKey: '#tGe23$1ku5y*',
  // env: 'test-eiozq',
  // env: 'release-rcd69',
  // env: 'hangzhou-8gxsziyy0b51946e',
  env: cloud.DYNAMIC_CURRENT_ENV,

  defaultAvatar: "https://game.gtimg.cn/images/game/act/a20201001vstation/my/default_avatar.png",
  "videoReportUrl": "http://106.52.92.193:8088/api/video/add",
  maxTestCount: 3,
  unLimitOpenid:[
    "oK05P5fv4LqI0vNG64hED92MQRwM",//现场摄影师
    "oK05P5YglJpUC4vakPn9gRD2Hc7s",//书记
    "oK05P5cVuDx0f-ix49PPIKbjr5wE",
    "oK05P5dg8ERc5a0wQzOd2V-O2IEo",
    "oK05P5Vdi-IFXBSBPbi6T32O8l5k",
    "oK05P5fKYij5YCqaDfA76v6AuJH4",
    "oK05P5dJuUyOB1mBvBkdlkutfnFw",   // 啵啵
    "oK05P5TRP9TLz2OpENpVPgOD9AOs",   // 仲记书，仲师傅，现场硬件设备维修
    "oK05P5VsbGKJ3pqf-YXKf_vkQ3Ag",   // 杨思运营人员 "分寸"
    "oK05P5byy3ODNgE4jzs5CSHlpD-A",   // 杨思运营人员 "刘宝库"
    "oK05P5f2rLQpvWjFN2RSaIr4Pl4o",   // 杨思运营人员 "假装此处有昵称"
    "oK05P5ejc0NP7PMg97liGSZiTb_Y",   // 杨思运营人员 "lvamMMmm"
    "oK05P5SwPaA0V1VB8WQcqPX9ves4",   // 杨思运营人员 "Dici-plusuga"
    "oK05P5aNAsjkxXthf1VqZdrsYlSc",   // 杨思运营人员 "Adam"
    "oK05P5dg8ERc5a0wQzOd2V-O2IEo",   // 杨思运营人员 "饼小艺"
    "oK05P5VkxFIL-VQzS22-TdCyMc7M",   // 杨思运营人员 "JyS"
    "oK05P5WsIMgHm5N_Dq8KzDw4IDww"    // 若风
  ]
}