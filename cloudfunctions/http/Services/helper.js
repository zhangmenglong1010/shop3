/**
 *
 */
const {
	db
} = require('./cloud');
const axios = require('axios');
const {
	videoReportUrl,
	httpSecretKey
} = require('./env');
const { md5, getRandomString } = require('./string');

module.exports = {
	/**
	 * @description 获取自增数
	 * @param {*} sequenceName 自增字段名
	 */
	async getNextSequenceValue(sequenceName, isRandom = false) {
		const _ = db.command;
		let originData = (
			await db
			.collection('vs_counters')
			.where({
				_id: sequenceName,
			})
			.get()
		).data;
		if (!originData.length) {
			throw `非法的自增字段${sequenceName}`;
		}
		// 开始事务
		// 递增为1或递增为一个 5～14 的随机数
		let incCounter = isRandom ? Math.floor(Math.random() * 10) + 5 : 1;
		const result = await db.runTransaction(async (transaction) => {
			let sequenceDocument = await transaction
				.collection('vs_counters')
				.doc(sequenceName)
				.update({
					data: {
						sequence_value: _.inc(incCounter),
					},
				});
			let currentData = (await transaction.collection('vs_counters').doc(sequenceName).get()).data;

			// 会作为 runTransaction resolve 的结果返回
			return currentData.sequence_value;

			// 会作为 runTransaction reject 的结果出去
			//  await transaction.rollback(-100)
		});
		return result;
	},

	arrayUnique(array) {
		return Array.from(new Set(array));
	},

	/**
	 *
	 * @param {*} timestamp 时间戳
	 * @param {*} splitString 日期间隔符，默认为-
	 */
	getDateString(timestamp, splitString = '-') {
		let date = new Date(timestamp);
		let year = date.getFullYear();
		let month = date.getMonth() + 1;
		let day = date.getDate();
		month = month < 10 ? `0${month}` : `${month}`;
		day = day < 10 ? `0${day}` : `${day}`;

		return `${year}${splitString}${month}${splitString}${day}`;
	},

	async requestVideoReport(params) {
		let {
			openid,
			name, // 用户昵称
			avatar, // 用户头像
			tutorId, // 导师id
			totalScoreDetail,
			totalScore,
			level, // 优秀等级，1-优秀，2-一般
		} = params
		if (!openid || !name || !avatar || tutorId === undefined || totalScoreDetail === undefined || totalScore === undefined || level === undefined) {
			throw "requestVideoReport函数执行错误，参数错误";
		}
		let timestamp = Date.now();
		let nonce = getRandomString(16);
		let res = await axios.post(videoReportUrl, {
			time: timestamp,
			nonce: nonce,
			token: md5(httpSecretKey + timestamp + nonce),
			openid,
			name, // 用户昵称
			avatar, // 用户头像
			tutorId, // 导师id
			totalScoreDetail,
			totalScore,
			level, // 优秀等级，1-优秀，2-一般
		});
		if (res.status !== 200 || res.data.code.code !== 0) {
			// throw "生成报告视频失败";
			console.error(res);
			// 上报错误
		}
		return res;
	}
};