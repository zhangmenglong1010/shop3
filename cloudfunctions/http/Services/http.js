/**
 * http通用模块服务封装
 */

const { db } = require('./cloud');
const validateIdCard = require('./idcard');
const { arrayUnique } = require('./helper')

const httpService = {
	/**
	 * @description 接口格式统一返回
	 * @param {int} code 状态码
	 * @param {string} msg 消息提示
	 * @param {object} data 数据对象
	 */
	apiResponse(code = 0, data = {}, msg = 'Success') {
		return {
			code,
			msg,
			data,
		};
	},

	/**
	 * @description 检查整数的字符串格式
	 * @param {*} value
	 */
	checkInteger(value) {
		if (typeof value === 'number' && value % 1 === 0) {
			return true;
		} else if (typeof value === 'string') {
			// 正则表达式，数字0，或者（非0开头，至少一位，然后接上0-9，0位或N位）。
			let pattern = /^[0-9-]+[0-9]*$/;
			if (pattern.test(value)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
  },
  
  /**
	 * @description 检查浮点的字符串格式
	 * @param {*} value
	 */
	checkFloat(value) {
		if (typeof value === 'number') {
			return true;
		} else if (typeof value === 'string') {
			// 正则表达式，整数，或小数点后1-8位
			let pattern = /^[0-9-]+(.[0-9]{1,8})?$/;
			if (pattern.test(value)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	},

	/**
	 * @description 检查日期字符串格式 2018-02-20
	 * @param {*} value 值
	 */
	checkDateString(value) {
		let pattern = /^[1-9]\d{3}-\d{2}-\d{2}$/;
		// 验证格式 2018-02-20 同时 验证日期是否合法，2020-09-55，判断getTime 是否为 NaN
		return pattern.test(value) && !isNaN(new Date(value).getTime());
	},

	/**
	 * @description 检查常规格式
	 * @param {*} typeName 变量类型
	 * @param {*} key 字段名
	 * @param {*} value 字段值
	 */
	checkNormalType(typeName, key, value) {
		// 检查特殊格式dateString
		if (typeName === 'dateString') {
			if (!this.checkDateString(value)) {
				throw this.apiResponse(10003, null, `参数格式有误：[${key}]，必须为日期类型（0000-00-00）`);
			}
			return;
		} else if (typeName === 'array') {
			if (!Array.isArray(value)) {
				throw this.apiResponse(10003, null, `参数格式有误：[${key}]，必须为数组`);
			}
			return;
		} else if (typeName === 'idcard') {
			if (!validateIdCard(value)) {
				throw this.apiResponse(10003, null, `参数格式有误：[${key}]，必须为有效的身份证号码`);
			}
			return;
		}

		if (typeName === 'integer') {
			// 对可能为字符串的整数进行特殊处理，
			if (!this.checkInteger(value)) {
				// 整数校验不通过
				throw this.apiResponse(10003, null, `参数格式有误：[${key}]，必须为整数`);
			}
			// 转为整数
			value = parseInt(value);
			// 格式转为 number
			typeName = 'number';
		} else if (typeName === 'float') {
      // 对可能为字符串的整数进行特殊处理，
			if (!this.checkFloat(value)) {
				// 整数校验不通过
				throw this.apiResponse(10003, null, `参数格式有误：[${key}]，必须为整数或8位内小数`);
			}
			// 转为整数
			value = parseFloat(value);
			// 格式转为 number
			typeName = 'number';

    } else if (typeName === 'jsonString') {
      try {
        value = JSON.parse(value);
      } catch (err) {
        throw this.apiResponse(10003, null, `参数格式有误：[${key}]`);
      }
      
    } else if (typeof value !== typeName) {
		// 统一校验常规格式
			throw this.apiResponse(10003, null, `参数格式有误：[${key}]，必须为${typeName}类型`);
		}
	},

	/**
	 * @description 检查数据库中是否存在
	 * @param {*} typeName exists:picture
	 * @param {*} key 字段名
	 * @param {*} value 字段值
	 */
	async checkExistsType(typeName, key, value) {
		let matchResult = typeName.match(/^exists:([a-zA-Z_]+),*([a-zA-Z_]*)$/);
		if (!matchResult) {
			throw this.apiResponse(10003, null, `后台错误，未知的参数校验规则[${typeName}]`);
		} else {
			let tableName = matchResult[1];
			let keyName = matchResult[2];
			let selector = null;
			if (!keyName) {
				selector = await db
					.collection(tableName)
					.where({
						_id: value,
					})
					.get();
			} else {
				let queryJson = {};
				queryJson[keyName] = value;
				selector = await db.collection(tableName).where(queryJson).get();
			}

			if (!selector || !selector.data || selector.data.length === 0) {
				throw this.apiResponse(10003, null, `参数值有误，无效的[${key}]`);
			}
		}
	},

	/**
	 * @description 检查是否超过最大值
	 * @param {*} typeName exists:picture
	 * @param {*} key 字段名
	 * @param {*} value 字段值
	 */
	checkMaxType(typeName, key, value) {
		let matchResult = typeName.match(/^max:([0-9]+)$/);
		if (!matchResult) {
			throw this.apiResponse(10003, null, `后台错误，未知的参数校验规则[${typeName}]`);
		} else {
			let length = matchResult[1];
			if (typeof value === 'string') {
				if (value.length > length) {
					throw this.apiResponse(10003, null, `参数值有误，[${key}]长度不得大于${length}`);
				}
			} else if (typeof value === 'number') {
				if (value > length) {
					throw this.apiResponse(10003, null, `参数值有误，[${key}]不得大于${length}`);
				}
			}
		}
	},

	/**
	 * @description 检查是否小于最小值
	 * @param {*} typeName exists:picture
	 * @param {*} key 字段名
	 * @param {*} value 字段值
	 */
	checkMinType(typeName, key, value) {
		let matchResult = typeName.match(/^min:([0-9]+)$/);
		if (!matchResult) {
			throw this.apiResponse(10003, null, `后台错误，未知的参数校验规则[${typeName}]`);
		} else {
			let length = matchResult[1];
			if (typeof value === 'string') {
				if (value.length < length) {
					throw this.apiResponse(10003, null, `参数值有误，[${key}]长度不得小于${length}`);
				}
			} else if (typeof value === 'number') {
				if (value < length) {
					throw this.apiResponse(10003, null, `参数值有误，[${key}]不得小于${length}`);
				}
			}
		}
	},

	/**
	 * @description 接口请求参数校验
	 * @param {Array[object]} rules
	 */
	async requestValidation(request, rules) {
		let result = {};
		let objectParamReg = /^(\w+)\.(\w+)$/; // test.aaa
		let arrayParamReg = /^(\w+)\.\*\.(\w+)$/; // test.*.aaa

		if (!request) {
			throw this.apiResponse(20003, null, '参数校验失败，请求参数格式有误');
		}

		for (let key in rules) {
			let rule = rules[key];

			if (objectParamReg.test(key)) {
				// 对象属性检查
				let [tempString, keyPrefix, keySubfix] = key.match(objectParamReg);
				if (rules[keyPrefix].indexOf('required') === -1 && request[keyPrefix] === undefined) {
					continue;
				}
				await this.checkItem(rule, request[keyPrefix], keySubfix);
				// 

			} else if (arrayParamReg.test(key)) {
				// 数组属性检查
				let [tempString, keyPrefix, keySubfix] = key.match(arrayParamReg);
				if (rules[keyPrefix].indexOf('required') === -1 && request[keyPrefix] === undefined) {
					continue;
				}
				if (!Array.isArray(request[keyPrefix])) {
					throw this.apiResponse(10003, null, `参数[${keyPrefix}]有误：必须为数组`);
				}
				for (let i = 0; i < request[keyPrefix].length; i++) {
					await this.checkItem(rule, request[keyPrefix][i], keySubfix, request[keyPrefix]);
				}
			} else {
				await this.checkItem(rule, request, key);
				result[key] = request[key]; // 赋值给result
			}
		}
		return result;
	},

	/**
	 * @description 检查单项
	 * @param {*} rule
	 * @param {*} requestObj 当前检查的对象，传对象，用于字符串转整数时重新赋值
	 * @param {*} key
	 * @param {*} parentObj 可不传，当检查的为数组类型时，必须传该数组的值
	 */
	async checkItem(rule, requestObj, key, parentObj = []) {
		if (typeof rule === 'string') {
			rule = rule.split('|');
		} else if (!Array.isArray(rule)) {
			throw this.apiResponse(20003, null, '参数校验失败，rule格式有误');
		}
		if (rule.indexOf('required') === -1 && requestObj[key] === undefined) {
			return;
		}

		let typeList = ['boolean', 'integer', 'float', 'string', 'jsonString', 'dateString', 'array', 'object', 'idcard'];

		for (let i = 0; i < rule.length; i++) {
			let item = rule[i];

			// integer, string, {type: 'in', values: [0, 1]}, exist:picture
			// 统一校验的常规格式
			if (item === 'required') {
				if (requestObj[key] === undefined) {
					throw this.apiResponse(10003, null, `参数缺失：缺少[${key}]`);
				}
			} else if (item === 'distinct') { // 唯一
        let array = parentObj.map(item => item[key]);
        let distinctArray = arrayUnique(array);
        if (array.length !== distinctArray.length) {
					throw this.apiResponse(10003, null, `参数错误：数组中重复的[${key}]`);
        }

      } else if (typeList.indexOf(item) >= 0) {
				this.checkNormalType(item, key, requestObj[key]);
				// 有可能从字符串改为数字
				if (item === 'integer') {
					requestObj[key] = parseInt(requestObj[key]);
				} else if (item === 'float') {
					requestObj[key] = Number(Number(requestObj[key]).toFixed(3));

        } else if (item === 'jsonString') {
					requestObj[key] = JSON.parse(requestObj[key]);
				}

				// 校验特殊格式，对象类型的规则rule: {type: 'in', values: [0, 1]}
			} else if (typeof item === 'object') {
				if (item.type === 'in' && item.values.indexOf(requestObj[key]) === -1) {
					throw this.apiResponse(10003, null, `参数[${key}]有误：必须为[${item.values.join(', ')}]中的一个`);
				}

				// 处理exists，在数据库中查找
			} else if (typeof item === 'string') {
				if (item.indexOf('exists:') === 0) {
					await this.checkExistsType(item, key, requestObj[key]);
				} else if (item.indexOf('max:') === 0) {
					await this.checkMaxType(item, key, requestObj[key]);
				} else if (item.indexOf('min:') === 0) {
					await this.checkMinType(item, key, requestObj[key]);
				} else {
					throw this.apiResponse(20003, null, `后台错误，未知的参数校验规则：${item}`);
				}
			}
		}
	},
};

module.exports = {
	apiResponse: httpService.apiResponse,

	requestValidation: httpService.requestValidation.bind(httpService),

	Rule: {
		in(array) {
			return {
				type: 'in',
				values: array,
			};
		},
	},
};
