/**
 *
 */
const { db } = require('./cloud');
module.exports = {
	/**
	 * @description 获取已过审的应援列表
	 * @param {*} params
	 */
	async getSupportList(params) {
		const { quantity, timestamp } = params;
		// 找到对应tableName的表单信息
		const targetFormInfo = await db
			.collection('morphy_formSetting')
			.where({
				tableName: 'support',
			})
			.get();
		if (!targetFormInfo.data || !targetFormInfo.data.length) {
			throw '未找到support表单信息，请重试';
		}
		const _ = db.command;

		//查询满足条件的表单内容
		const getResult = await db
			.collection('morphy_formData')
			.where(
				_.and([
					{
						formSettingId: targetFormInfo.data[0]._id, // 应援类型
					},
					{
						hidden: false, //非"删除"状态
					},
					{
						'formData.isCheck': 1, // 已过审
					},
					{
						"updateSys.morphy.time": _.gte(timestamp), // 时间戳
					},
				])
			)
			.orderBy("updateSys.morphy.time", 'desc')
			.limit(quantity)
			.get();

		return getResult.data;
	},
};
