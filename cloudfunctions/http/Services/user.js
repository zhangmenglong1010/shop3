const {
	db
} = require('./cloud');
const {
	apiResponse
} = require('./http');
const {
	defaultAvatar,
	maxTestCount,
	unLimitOpenid
} = require('./env');
const {
	getNextSequenceValue,
	getDateString
} = require('./helper');

/**
 * @description UserServices - 用户通用功能封装
 */
module.exports = {

	/**
	 * find或者创建用户
	 */
	async findOrCreateUser(OPENID) {
		if (!OPENID) {
			throw "缺少OPENID";
		}
		// 获取openid对应的用户数组，判断长度是否为1
		const result = await db.runTransaction(async (transaction) => {
			let user = null;
			let usersData = await transaction
				.collection('vs_users')
				.where({
					openid: OPENID,
				})
				.get();

			// 多于1个用户，抛出异常
			if (usersData.data.length > 1) {
				user = usersData.data[0];
				// ctx.body = apiResponse(20003, null, '数据异常，出现两个或以上该openid对应的用户');
				// return;

				// 未找到该用户
			} else if (usersData.data.length === 0) {
				ctx.body = apiResponse(10001, null, 'openid错误，未找到该openid对应的用户');
				return;

			} else {
				user = usersData.data[0];
			}
			return user;
		});

		return result;
	},

	/**
	 * @description 根据当前的审核情况，返回用户的最新头像和昵称
	 * @param {*} user
	 */
	async getUserInfo(user) {
		if (!user || !user.openid) {
			throw '函数[getUserInfo]错误，缺少参数';
		}
		const _ = db.command;

		// 获取是否有选手表信息
		let contestantTableSelector = await db
			.collection('morphy_formSetting')
			.where({
				tableName: 'contestant',
			})
			.get();
		if (contestantTableSelector.data.length) {
			let contestantFormId = contestantTableSelector.data[0]._id;
			let contestantSelector = await db
				.collection('morphy_formData')
				.where(
					_.and([
						{
							formSettingId: contestantFormId, // 用户头像审核信息
						},
						{
							openid: user.openid,
						},
						{
							hidden: false
						},
						{
							'formData.isCheck': _.neq(-1), // 不等于-1，即审核中的和审核通过的都可以
						},
					])
				)
				.orderBy('createAt', 'desc')
				.get();

			// 如果有选手信息，直接返回
			if (contestantSelector.data.length && contestantSelector.data[0].formData) {
				return contestantSelector.data[0].formData;
			}
		}

		// 获取用户表信息
		let userTableSelector = await db
			.collection('morphy_formSetting')
			.where({
				tableName: 'userInfo',
			})
			.get();
		if (!userTableSelector.data.length) {
			throw apiResponse(10005, null, '未找到自定义的userInfo表字段');
		}
		let userInfoFormId = userTableSelector.data[0]._id;
		let userSelector = await db
			.collection('morphy_formData')
			.where(
				_.and([{
						formSettingId: userInfoFormId, // 用户头像审核信息
					},
					{
						openid: user.openid,
					},
					{
						hidden: false
					},
					{
						'formData.isCheck': _.neq(-1), // 不等于-1，即审核中的和审核通过的都可以
					},
				])
			)
			.orderBy('createAt', 'desc')
			.get();

		// 如果为空，即用户的所有头像，昵称都不过审核，返回默认头像和默认昵称
		if (!userSelector.data.length || !userSelector.data[0].formData) {
			return {
				name: user.name || '用户',
				avatar: user.avatar || defaultAvatar,
			};
		} else {
			return {
				name: userSelector.data[0].formData.name,
				avatar: userSelector.data[0].formData.avatar,
			};
		}
	},

	/**
	 * @description 根据openid列表，返回对应的所有用户最新头像
	 * @param {*} openidList
	 */
	async getUserListInfo(openidList) {
		const _ = db.command;
		let userListJson = {};

		// 获取选手表信息
		let contestantTableSelector = await db
			.collection('morphy_formSetting')
			.where({
				tableName: 'contestant',
			})
			.get();

		if (contestantTableSelector.data.length) {
			let contestantFormId = contestantTableSelector.data[0]._id;
			let contestantList = (await db
				.collection('morphy_formData')
				.where(
					_.and([{
							formSettingId: contestantFormId, // 用户头像审核信息
						},
						{
							openid: _.in(openidList),
						},
						{
							hidden: false
						},
						{
							'formData.isCheck': _.neq(-1), // 不等于-1，即审核中的和审核通过的都可以
						},
					])
				)
				.orderBy('createAt', 'desc')
				.get()).data;
			contestantList.forEach(item => {
				if (!userListJson[item.openid]) {
					userListJson[item.openid] = item.formData;
				}
			});
		}

		let userDefaultInfoList = (
			await db
			.collection('vs_users')
			.where({
				openid: _.in(openidList),
			})
			.get()
		).data;

		let userTableSelector = await db
			.collection('morphy_formSetting')
			.where({
				tableName: 'userInfo',
			})
			.get();
		if (!userTableSelector.data.length) {
			throw apiResponse(10005, null, '未找到自定义的userInfo表字段');
		}
		let userInfoFormId = userTableSelector.data[0]._id;
		let userCheckInfoList = (
			await db
			.collection('morphy_formData')
			.where(
				_.and([{
						formSettingId: userInfoFormId, // 用户头像审核信息
					},
					{
						openid: _.in(openidList),
					},
					{
						hidden: false, //非"删除"状态
					},
					{
						'formData.isCheck': _.neq(-1), // 不等于-1，即审核中的和审核通过的都可以
					},
				])
			)
			.orderBy('createAt', 'desc')
			.get()
		).data;

		let count = 0;
		userCheckInfoList.forEach((item) => {
			if (!userListJson[item.openid]) {
				count++;
				userListJson[item.openid] = {
					name: item.formData.name,
					avatar: item.formData.avatar,
				};
			}
		});
		userDefaultInfoList.forEach((item) => {
			if (!userListJson[item.openid]) {
				count++;
				userListJson[item.openid] = {
					name: item.name,
					avatar: item.avatar,
				};
			}
		});
		if (count < openidList.length) {
			openidList.forEach((openString) => {
				if (!userListJson[openString]) {
					userListJson[openString] = {
						name: '用户',
						avatar: defaultAvatar,
					};
				}
			});
		}
		return userListJson;
	},

	/**
	 * @description 添加分数到排行榜
	 * @param {object} user 用户对象
	 * @param {number} totalScore 总分
	 */
	async addScoreToRank(user, totalScore) {
		if (!user || !user.openid || !totalScore) {
			throw apiResponse(20002, null, '函数参数错误addScoreToRank');
		}
		const map = {};
		const getFormInfoResult = await db
			.collection('morphy_formSetting')
			.where({
				tableName: 'rank',
			})
			.get();
		if (!getFormInfoResult.data || !getFormInfoResult.data.length) {
			throw apiResponse(20202, null, '找不到表单信息，请重试');
		}
		map.formSettingId = getFormInfoResult.data[0]._id;
		map.hidden = false; // 非"删除"状态map.hidden = false;
		map.openid = user.openid;

		// 先查询是否有我的旧数据，有则更新，无则新增
		const record = await db.collection('morphy_formData').where(map).get();

		if (record.data.length) {
			// 已有记录，更新分数，头像昵称
			let updator = await db
				.collection('morphy_formData')
				.doc(record.data[0]._id)
				.update({
					data: {
						formData: {
							score: totalScore,
							name: user.name,
							avatar: user.avatar,
							isCheck: 0, // 切换为未审核阶段
						},
						updateAt: Date.now(),
					},
				});
			if (updator.stats.updated !== 1) {
				throw apiResponse(20202, null, '更新自定义表单分数失败，updator语句失败');
			}
		} else {
			// 未有记录，直接新增
			const getFormInfo = await db
				.collection('morphy_formSetting')
				.where({
					tableName: 'rank',
				})
				.get();
			if (!getFormInfo.data || !getFormInfo.data.length) {
				throw apiResponse(20202, null, '更新自定义表单分数失败，未找到表单rank');
			}
			let insertor = await db.collection('morphy_formData').add({
				data: {
					formData: {
						name: user.name,
						avatar: user.avatar,
						score: totalScore,
						isCheck: 0,
					},
					formSettingId: getFormInfo.data[0]._id,
					createAt: Date.now(),
					openid: user.openid,
					hidden: false, // 是否隐藏
				},
			});
			if (!insertor._id) {
				// 添加失败
				throw apiResponse(20202, null, '更新自定义表单分数失败，insertor语句失败');
			}
		}
	},

	/**
	 * @description 计算百分比，返回该分数是优秀还是一般
	 * @param {*} score
	 */
	async getTotalScoreLevel(score) {
		const _ = db.command;
		// 计算该用户的当前分占比排名
		// 获取所有已生成分数的人数
		let scoreTotalCount = (await db
			.collection('vs_users')
			.where({
				score: _.gte(0),
			})
			.count()).total;

		// 获取高于该分数的人数
		let scoreBeatCount = (await db
			.collection('vs_users')
			.where({
				score: _.gt(score),
			})
			.count()).total;
		if (scoreTotalCount <= 0) {
			scoreTotalCount = 1;
		}
		// 优秀
		let percent = scoreBeatCount / scoreTotalCount
		if (percent < 0.2) { // 前20%
			return 1;

		} else if (percent < 0.4) { // 一般
			return 2;

		} else if (percent < 0.6) { // 一般，前60%
			return 3;

		} else if (percent < 0.8) { // 一般前80%
			return 4;

		} else {
			return 5;

		}
	},

	/**
	 * @description 返回能否选择导师
	 * @param {*} user 
	 */
	async getCanSignTutor(uid) {
		// 导师选择次数小于最大次数可操作
		let timestamp = Date.now();
		const _ = db.command;
		let dateString = getDateString(timestamp);
		let startTime = new Date(`${dateString} 00:00:01`).getTime();
		let endTime = new Date(`${dateString} 23:59:59`).getTime();
		let selector = await db.collection('vs_user_tutors').where(_.and([{
				uid: uid
			},
			{
				created_at: _.gte(startTime)
			},
			{
				created_at: _.lte(endTime)
			}
		])).get();
		console.log(selector.data);
		let status = selector.data.length < maxTestCount;
		return status;
	},

	/**
	 * @description 返回能否添加合影
	 * @param {*} user 
	 */
	async getCanAddPhoto(uid) {
		// 拍照时间戳小于当天时间戳可重复提交
		let timestamp = Date.now();
		const _ = db.command;
		let dateString = getDateString(timestamp);
		let startTime = new Date(`${dateString} 00:00:01`).getTime();
		let endTime = new Date(`${dateString} 23:59:59`).getTime();
		let selector = await db.collection('vs_photos').where(_.and([{
				uid: uid
			},
			{
				created_at: _.gte(startTime)
			},
			{
				created_at: _.lte(endTime)
			}
		])).get();
		let status = selector.data.length < maxTestCount;
		return status;
	},

	/**
	 * 获取6个测试的canplay信息
	 */
	async getCanplayInfo(uid) {
		let timestamp = Date.now();
		const _ = db.command;
		let dateString = getDateString(timestamp);
		let startTime = new Date(`${dateString} 00:00:01`).getTime();
		let endTime = new Date(`${dateString} 23:59:59`).getTime();
		let selector = await db.collection('vs_scores').where(_.and([{
				uid: uid
			},
			{
				created_at: _.gte(startTime)
			},
			{
				created_at: _.lte(endTime)
			}
		])).get();
		let countTest = {
			'1': 0,
			'2': 0,
			'3': 0,
			'4': 0,
			'5': 0,
			'6': 0,
		};
		let canplay = {
			'1': true,
			'2': true,
			'3': true,
			'4': true,
			'5': true,
			'6': true,
		};

		selector.data.forEach(item => {
			let gameTestId = item.gameTestId;
			if (gameTestId && typeof countTest[gameTestId] === 'number') {
				countTest[gameTestId]++;
				if (countTest[gameTestId] >= maxTestCount) {
					canplay[gameTestId] = false;
				}
			}
		});
		return canplay;
	}
};