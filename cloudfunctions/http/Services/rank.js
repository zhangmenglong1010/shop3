const {
	db
} = require('./cloud');
const {
	getUserListInfo
} = require('./user');
const { getLatestUserListScore } = require('../Controllers/rank');
const Util = {
	async getRankList(quantity = 20) {
		// 前N名
		const targetFormInfo = await db
			.collection('morphy_formSetting')
			.where({
				tableName: 'rank',
			})
			.get();
		if (!targetFormInfo.data || !targetFormInfo.data.length) {
			throw '未找到rank表单信息，请重试';
		}
		const _ = db.command;
		let map = {
			formSettingId: targetFormInfo.data[0]._id,
			hidden: false, //非"删除"状态
			formData: {
				// score: _.gte(0),
				isCheck: 1, // 已过审
			},
		};

		//查询满足条件的表单内容
		const getResult = await db
			.collection('morphy_formData')
			.where(map)
			.orderBy('formData.score', 'desc')
			.limit(quantity)
			.get();

		return getResult;
	},
};

module.exports = {
	/**
	 * @description 获取中台过审排行榜的前N名
	 * @param {*} quantity 前N名，默认20
	 */
	getRankList: Util.getRankList,

	/**
	 * @description 获取中台过审排行榜的前N名
	 * @param {*} quantity 前N名，默认20
	 */
	async getRankListWithUserInfo(quantity = 20) {
		let selector = await Util.getRankList(quantity);
		let openidList = selector.data.map((item) => item.openid);
		let userInfoList = await getUserListInfo(openidList);
		let list = selector.data.map(item => {
			return {
				_id: item._id,
				score: item.formData.score,
				name: userInfoList[item.openid].name,
				avatar: userInfoList[item.openid].avatar,
				team: userInfoList[item.openid].teamLogo,
			}
		});
		return list;
	},

	async getLatestRank(timestamp, quantity) {
		const _ = db.command;
		let selector = await db
			.collection('vs_users')
			.where(_.and([{
				scoreTimestamp: _.gte(timestamp),
			}, {
				score: _.gte(0),
			}]))
			.orderBy('scoreTimestamp', 'desc')
			.limit(quantity)
			.get();
			selector.data.forEach(item => {
				delete item.testScoreList
			});
		let openidList = selector.data.map((item) => item.openid);
		let userInfoList = await getUserListInfo(openidList);
		let list = selector.data.map(item => {
			return {
				_id: item._id,
				score: item.score,
				name: userInfoList[item.openid].name,
				avatar: userInfoList[item.openid].avatar,
				team: userInfoList[item.openid].teamLogo,
			}
		});
		return list;
	}
};