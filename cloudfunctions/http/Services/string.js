const crypto = require('crypto');
function md5(data, length) {
	if (!data) {
		console.error('参数错误, md5');
		return '';
	}
	let hash = crypto.createHash('md5');
	let md5String = hash.update(data).digest('hex');
	if (length) {
		let defaultLength = 32;
		let start = (defaultLength - length) / 2;
		md5String = md5String.substr(start, length);
	}
	return md5String;
}
module.exports = {
	md5,
	getRandomString(length = 8) {
		let string = ""
		let fullString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		for (let i = 0; i < length; i++) {
			let index = Math.floor(Math.random() * fullString.length);
			string += fullString[index];
		}
		return string;
	}
}