// 云函数入口文件
const koa = require('wx-koa').WxKoa;
const app = new koa();
const router = require('./Routers');
const { cloud } = require('./Services/cloud');

app.use(router.routes()).use(router.allowedMethods());

// 云函数入口函数
exports.main = async (event, context) => {
	const wxContext = cloud.getWXContext();
	// 兼容http服务
	let params = {};
	// let origin = '';
	let action = '';

	if (event.queryStringParameters) {
		// origin = 'http';
		action = event.queryStringParameters.action;
		for (let key in event.queryStringParameters) {
			let item = event.queryStringParameters[key];
			if (['action', 'method', 'url'].indexOf(key) === -1) {
				params[key] = item;
			}
		}
	} else {
		// origin = 'miniprogram';
		// origin = 'http';
		action = event.action;
		params = event;
	}

	let module = '';

	if (['getUserInfo', 'signTutor', 'signGameTest'].indexOf(action) >= 0) {
		module = `user`;
	} else {
		module = `common`;
	}

	return app.service(
		{
			url: `/${module}/${action}`,
			method: 'get',
			data: params,
		},
		wxContext
	);
};
