/**
 * 接口路由列表
 */
const Router = require('koa-router');
const router = new Router();
const controllers = require('../Controllers');

// 获取排行榜成绩
router.get('/getScoreRank', controllers.rank.getScoreRank);

// 获取时间戳后面的最新用户列表成绩
router.get('/getLatestUserListScore', controllers.rank.getLatestUserListScore);

// 应援信息
router.get('/getSupportList', controllers.support.latestList);

module.exports = router;
