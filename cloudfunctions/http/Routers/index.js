/**
 * 路由入口
 */
const Router = require('wx-koa').WxRoute;
const router = new Router();
const auth = require('../Middleware/auth');
const middlewareUser = require('../Middleware/user');
const user = require('./user');
const common = require('./common');

router.use('/user', auth, middlewareUser, user.routes(), user.allowedMethods());
router.use('/common', auth, common.routes(), common.allowedMethods());

router.all('*', (ctx, next) => {
	ctx.body = {
		code: 10002,
		msg: '未知 [action]',
	};
});
// router.all('*', (ctx, next) => {
//   console.log('all')
//   ctx.body = {
//     code: 10002,
//     msg: ctx
//   }
// })
// router.get('/user', (ctx)=> {
//   ctx.body = {
//     code: 0,
//     "test": 123
//   }
// })
// router.all('/*', (ctx, next) => {
//   ctx.body = {
//     code: 0
//   }
// })

module.exports = router;
