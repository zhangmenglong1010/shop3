/**
 * 接口路由列表
 */
const Router = require('koa-router');
const router = new Router();
const controllers = require('../Controllers');

// user
router.get('/getUserInfo', controllers.user.detail);

// 选择导师
router.get('/signTutor', controllers.user.signTutor);

// 上报测试分数
router.get('/signGameTest', controllers.user.signGameTest);


module.exports = router;
