// components/support/supportDialog.js
let request = require('../../utils/scripts/request.js');
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    showed : false,
    loading : false,
    tips : '',
    text : '',
    textNum : 0,
    maxNum : 40,
    userInfo : {}
  },
  lifetimes : {
    ready(){
      request.requestCloud({
        url : '/api/user/detail',
        method : 'get'
      }).then(res => {
        this.setData({userInfo : res.data});
      })
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    triggerDialog(){
      this.setData({showed : !this.data.showed,loading:false,tips:'',textNum:0,text:''})
    },
    textChange(e) {
      let text = e.detail.value;
      text && this.setData({
        textNum : text.length,
        text
      })
    },
    submit(){
      this.setData({tips:''});
      if(this.data.loading) return;
      let that = this;
      let text = this.data.text.replace(/\n/g,'');
      if(!text){
        this.setData({tips:'请输入应援文字'})
        return;
      }else if(text.length > 40){
        this.setData({tips:'文字超出限制！'})
        return;
      }
      text && this.setData({loading:true});
      text && wx.serviceMarket.invokeService({
        service: 'wxee446d7507c68b11',
        api: 'msgSecCheck',
        data: {
          "Action": "TextApproval",
          "Text": text
        },
      }).then(res => {
        let evils = JSON.parse(JSON.stringify(res.data.Response.EvilTokens));
        if(evils.length == 0 ){
          that.supportAdd(text);
        }else{
          wx.PTTSendClick("supdialog", "checkcut", "应援检测");
          that.setData({tips:"文本检测不通过"});
          throw new Error({message : '文本检测不通过'})
        }
      }).catch(e => {
        this.setData({
          loading : false
        })
      })
    },
    bindgetUserInfo(res){
      let detail = res.detail.userInfo;
      // 同步用户基础数据
      // | name | string | √ | - | 昵称 |
      // | avatar | string | √ | - | 头像 |
      // | gender | int | √ | - | 性别 |
      // | city | string | - | - | 城市 |
      // | country | string | - | - | 国家 |
      // | province | string | - | - | 省份 |
      // | language | string | - | - | 语言 |
      let userInfo = {
        name: detail.nickName,
        avatar: detail.avatarUrl,
        gender: detail.gender,
        city: detail.city,
        country: detail.country,
        province: detail.province,
        language: detail.language,
        cloudID : res.detail.cloudID
      }
      request.requestCloud({
        url: '/api/user/update',
        data: userInfo
      }).then(res => {
        res.data.isRequireWxInfo = false;
        this.setData({
          userInfo : res.data
        })
        this.submit();
      })
    },
    supportAdd(text){
      // request.requestCloud({
      //   url : '/api/support/add',
      //   data : {
      //     text
      //   }
      // }).then(res => {
      //   wx.showToast({
      //     title: '发送成功',
      //     icon: 'none',
      //     duration: 1500
      //   })
      //   this.triggerEvent('add');
      //   this.triggerDialog()
      // })
      // 小程序端调用
      wx.cloud.callFunction({
        // 需调用的云函数名
        name: 'morphy_customForm',
        // 传给云函数的参数
        data: {
        '$url': 'client/addFormData',
        'tableName': 'support',
        'formData': {
            "text": text,
        }
        },
        // 成功回调
        complete: (res) => {
          wx.showToast({
            title: '发送成功',
            icon: 'none',
            duration: 1500
          })
          let item = {
            content : res.result.data.formData.text,
            color : '#f9cf00',
            image : {
              gap : 50,
              head : {
                height : 20,
                width : 20,
                src : this.data.userInfo.avatar
              }
            }
          }
          this.triggerEvent('add',item);
          this.triggerDialog()
        }
      })
    }
  }
})
