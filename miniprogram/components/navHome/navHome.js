// components/navHome.js
var app = getApp();
Component({
  /**
   * 组件的属性列表
   */
  options: {
    addGlobalClass: true,
  },
  properties: {
  },

  /**
   * 组件的初始数据
   */
  data: {
    statusBarHeight : app.globalData.statusBarHeight,
    list: [],
    keywords: {}
  },


  /**
   * 组件的方法列表
   */
  methods: {
    navBack : function(){
      wx.navigateBack({
        delta: 1
      })
    },
    navHome : function(){
      wx.switchTab({
        url:'/pages/index/index'
      })
    },
  }
})