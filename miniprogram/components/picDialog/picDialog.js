let COS = require('../../utils/scripts/cos-wx-sdk-v5.js');
let request = require('../../utils/scripts/request.js');
// components/picDIalog/picDialog.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    showed : false,
    pic : '',
    loading : false,
    tips : '',
    text : '',
    textNum : 0,
    maxNum : 40,
    userInfo : {},
    checked : false,
    cosFile : '',
    picInfo : {

    },
    cosSrc : ''
  },
  lifetimes : {
    ready(){
      request.requestCloud({
        url : '/api/user/detail',
        method : 'get'
      }).then(res => {
        this.setData({userInfo : res.data});
      })
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    triggerDialog(file) {
      let src = file ? file.src : '';
      this.setData({
        showed: !this.data.showed,
        pic : src,
        cosSrc : '',
        text : '',
        loading : false,
        tips : '',
        textNum : 0,
        checked :false
      });
      if(this.data.showed){
        let exts = src.split('.');
        let ext = exts[exts.length - 1];
        this.initCos(ext);
      }
    },
    textChange(e) {
      let text = e.detail.value;
      text && this.setData({
        textNum : text.length,
        text
      })
    },
    initCos(ext){
      let that = this;
      // 初始化实例
      request.requestCloud({
        url : '/api/file/cos/token',
        method : 'get',
        data : {
          ext : ext
        }
      }).then(res => {
        that.setData({cosFile:res.data.object_path});
        that.cos = new COS({
            getAuthorization: function (options, callback) {
              callback({
                  TmpSecretId: res.data.tmp_secret_id,
                  TmpSecretKey: res.data.tmp_secret_key,
                  XCosSecurityToken: res.data.session_token,
                  StartTime: res.data.start_time, // 时间戳，单位秒，如：1580000000，建议返回服务器时间作为签名的开始时间，避免用户浏览器本地时间偏差过大导致签名错误
                  ExpiredTime: res.data.expired_time, // 时间戳，单位秒，如：1580000900
              });
            }
        });
      })

    },
    submit(e){
      this.setData({tips:''});
      if(this.data.loading) return;
      const that = this;
      let text = this.data.text.replace(/\n/g,'');
      var d = Date.now()
      if(!text){
        this.setData({tips:'请输入照片文字'});
        return;
      }else if(text.length > 40){
        this.setData({tips:'文字超出限制！'})
        return;
      }else if(!this.data.checked){
        this.setData({tips:'请勾选确认隐私政策！'})
        return;
      }
      text && this.setData({text,loading:true});
      let Bucket = 'morphy-5f39f13436ab1-1259793757',
          Region = 'ap-guangzhou';
      var filePath = this.data.pic;
      var filename = filePath.substr(filePath.lastIndexOf('/') + 1);
      let uploadFile = new Promise((resolve,reject) => {
        this.cos.postObject({
            Bucket: Bucket,
            Region: Region,
            Key: this.data.cosFile,
            FilePath: filePath,
            onProgress: function (info) {
            },
        }, function (err, data) {
          data && resolve(data);
          err && reject(data);
        })
      });


      uploadFile.then(res => {
        wx.PTTSendClick("picdialog", "upsuccess", "上传图片");
        let fileSrc = 'https://'+res.Location;
        that.setData({cosSrc: fileSrc});
        return new Promise((resolve,reject) => {
          wx.getImageInfo({
            src : fileSrc,
            success(file){
              resolve(file)
            }
          })
        });
      })
      .then(res => {
        this.setData({
          picInfo : {
            width : res.width,
            height : res.height
          }
        })
        let imageCheck = wx.serviceMarket.invokeService({
          service: 'wxee446d7507c68b11',
          api: 'imgSecCheck',
          data: {
            "Action": "ImageModeration",
            "Scenes": ["PORN", "POLITICS", "TERRORISM"],
            "ImageUrl": this.data.cosSrc,
            "ImageBase64": "",
            "Config": "",
            "Extra": ""
          },
        });
        let textCheck = wx.serviceMarket.invokeService({
          service: 'wxee446d7507c68b11',
          api: 'msgSecCheck',
          data: {
            "Action": "TextApproval",
            "Text": that.data.text
          },
        });
        return Promise.all([imageCheck,textCheck])
      })
      .then(res => {
        if(res[0].data.Response.Suggestion === 'PASS' && res[1].data.Response.EvilTokens.length == 0){
          that.picAdd();
        }else{
          wx.PTTSendClick("picdialog", "checkcut", "检测不通过");
          that.setData({tips:'图片/文本检测不通过'});
          throw new Error({message : '图片/文本检测不通过'})
        }
      })
      .catch(err => {
        this.setData({
          loading : false
        })
      })
    },
    handCheck(){
      this.setData({checked:!this.data.checked})
    },
    bindgetUserInfo(res){
      let detail = res.detail.userInfo;
      // 同步用户基础数据
      // | name | string | √ | - | 昵称 |
      // | avatar | string | √ | - | 头像 |
      // | gender | int | √ | - | 性别 |
      // | city | string | - | - | 城市 |
      // | country | string | - | - | 国家 |
      // | province | string | - | - | 省份 |
      // | language | string | - | - | 语言 |
      let userInfo = {
        name: detail.nickName,
        avatar: detail.avatarUrl,
        gender: detail.gender,
        city: detail.city,
        country: detail.country,
        province: detail.province,
        language: detail.language,
        cloudID : res.detail.cloudID
      }
      request.requestCloud({
        url: '/api/user/update',
        data: userInfo
      }).then(res => {
        res.data.isRequireWxInfo = false;
        this.setData({
          userInfo : res.data
        })
        this.submit();
      })
    },
    picAdd(){
      wx.cloud.callFunction({
        // 需调用的云函数名
        name: 'morphy_userAlbum',
        // 传给云函数的参数
        data: {
          '$url':'client/add',
          'mediaInfo': {
                width: this.data.picInfo.width, // 照片宽度
                height: this.data.picInfo.height, // 照片高度
                fileUrl: this.data.cosSrc // 文件url
          },
          content: this.data.text, // 玩家发表图册时填写的内容，照片墙文本
        }
      }).then(res => {
        let picItem = {
          pic : res.result.data.mediaInfo.fileUrl,
          tit : res.result.data.content,
          avatar : this.data.userInfo.avatar,
          name : this.data.userInfo.name,
          like : 0,
          liked : false,
          width : res.result.data.mediaInfo.width,
          height : res.result.data.mediaInfo.height,
          id : res.result.data._id
        }
        this.triggerEvent('add',picItem);
        this.triggerDialog();
      })
    },
    toPolicy(){
      // https://privacy.qq.com/yszc-m.htm
      
      wx.PTTSendClick("btn","policy","上墙声明");
      wx.navigateTo({
        url : '/pages/webview/webview',
        success: function(res) {
          // 通过eventChannel向被打开页面传送数据
          res.eventChannel.emit('acceptDataFromOpenerPage', 'policy')
        }
      })
    }
  }
})
