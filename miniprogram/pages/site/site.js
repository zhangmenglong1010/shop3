// miniprogram/pages/site/site.js
let request = require("../../utils/scripts/request.js");
const { mockData } = require("./util.js");
//轮播组件文档：https://developers.weixin.qq.com/miniprogram/dev/extended/component-plus/barrage.html

Page({
  /**
   * 页面的初始数据
   */
  data: {
    toggle: true,
    barrageInit : false,
    supportList: [],
    supportTimestamp : new Date().getTime(),
    rankList: [],
    // picList: [],
    loading: false,
    tiantiUser: [],
    picList: [],
    newSupport : false,
    userSupport : {},
    supportTimeout : null
  },
  addBarrage(data) {
    const that = this;
    if(!this.data.barrageInit) {
      const barrageComp = this.selectComponent(".barrage");
      this.barrage = barrageComp.getBarrageInstance({
        font: "bold 12px sans-serif",
        duration: 20,
        alpha : 1,
        lineHeight: 2,
        mode: "separate",
        padding: [10, 0, 10, 0],
        safeGap: 150,
      });
      this.barrage.open();
      this.setData({barrageInit : true})
    }
    let supportData = [];
    data && data.forEach(item => {
      supportData.push({
        content : item.text.replace(/\n/g,''),
        color : '#fff',
        image : {
          gap : 10,
          head : {
            height: 20,
            width: 20,
            src : item.avatar
          }
        }
      })
    })
    clearInterval(this.timer);
    this.barrage.addData(supportData);
    this.setData({supportList : supportData});
    this.timer = setInterval(() => {
      this.barrage.addData(this.data.supportList);
    }, 5000);
    // 设置弹幕动画时间一半，保证弹幕有持续进场,每轮弹幕持续重复播放
    let timeout = setTimeout(() => {
      this.getSupportList();
    },60000)
    // 每分钟重新请求新应援
    this.setData({
      supportTimeout : timeout
    })
  },
  
  onShow(options) {
    if (typeof this.getTabBar === "function" && this.getTabBar()) {
      this.getTabBar().setData({
        selected: 2,
      });
    }
    this.init();
  },
  onHide(){
    clearTimeout(this.data.supportTimeout);
  },
  init: function (options) {
    this.setData({ loading: true });
    this.getSupportList();
    this.getRankList();
    this.getPicList();
  },
  showPic: function (e) {
    let picList = this.data.picList;
    let list = [];
    for (let i of picList) {
      list.push(i.pic);
    }
    wx.previewImage({
      urls: [list[e.currentTarget.dataset.index]],
    });
  },
  getSupportList() {
    request
      .requestCloud({
        url: "/api/support/list",
        method: "get",
        data: {
          page : 1,
          size : 50,
          timestamp : this.data.supportTimestamp
        }
      })
      .then((res) => {
        // 根据返回应援数量，数量超过0，则设置时间戳为最后一个应援-1，否则直接重新开始拉去最新应援列表
        let lastTime = res.data.list.length > 0 ? res.data.list[res.data.list.length -1].createAt - 1 : new Date().getTime();
        this.setData({ supportList: res.data.list,supportTimestamp:lastTime});
        if(res.data.list.length){
          this.addBarrage(res.data.list);
        }else{
          this.getSupportList();
        }
      });// 小程序端调用
  },
  getRankList() {
    request.requestCloud({
      url : '/api/rank/list',
      method : 'get',
      data : {
        size : 3
      }
    }).then(res => {
      let rankList = [];
      let list = res.data;
      list.forEach(item => {
        rankList.push(
          {
            name: item.name,
            score: item.score,
            avatar: item.avatar
          }
        )
      });
      this.setData({tiantiUser : rankList});
    })
  },
  getPicList() {
    request.requestCloud({
      url : '/api/picture/list',
      method : 'get',
      data : {
        size : 4,
        lastRequestTime : new Date().getTime()
      }
    }).then(res => {
      let dataLength = res.data.list.length,
          list = res.data.list,
          picList = [];
      dataLength > 0 && this.setData({picTimestamp:res.data.list[dataLength -1].createTime});
      dataLength < 20 && this.setData({more:false});
      list.forEach(item => {
        picList.push({
          pic : item.mediaInfo.fileUrl,
          tit : item.content,
          avatar : item.userInfo.avatar,
          name : item.userInfo.name,
          like : item.likeCount,
          liked : item.isLiked,
          width : item.mediaInfo.width,
          height : item.mediaInfo.height,
          id : item._id
        })
      })
      this.setData({ picList: picList,loading:false});
    })
  },
  openSupport() {
    wx.PTTSendClick("btns", "support", "打开应援");
    const child = this.selectComponent(".support_dialog");
    child.triggerDialog();
  },
  handleLike(e){
    let that = this;
    let index = e.currentTarget.dataset.index;
    if(this.data.picList[index].loading) {
      return
    }

    let data = {
      picture_id: e.currentTarget.dataset.id,
      isThumbUp: e.currentTarget.dataset.liked ? -1 : 1,
      index : index
    };
    console.log(this.data.picList[data.index].loading)
    wx.PTTSendClick("site", "like", "点赞");
    this.setData({ [`picList[${data.index}].loading`] : true});
    // 小程序端调用
    wx.cloud.callFunction({
      // 需调用的云函数名
      name: 'morphy_userAlbum',
      // 传给云函数的参数
      data: {
          '$url':'client/setLike',
          '_id': data.picture_id,   // 需要设置点赞状态的相册id
          'status': data.isThumbUp   // 设置的点赞状态，-1为取消点赞，1为点赞
      },
      // 成功回调
      complete(res){
        let thumbItem = {},
            likeNum = that.data.picList[data.index].like;
        thumbItem = that.data.picList[data.index];
        thumbItem.liked = data.isThumbUp == 1 ? true : false;
        thumbItem.like = data.isThumbUp == 1 ? likeNum + 1 : likeNum - 1;
        thumbItem.loading = false;
        if (thumbItem.id) {
          that.setData({
            [`picList[${data.index}]`]: thumbItem,
          });
        }

      }
    })
  },
  supportAdd(res) {
    let item = res.detail;
    console.log(res.detail);
    !this.data.userSupport.content && this.setData({
      userSupport : item,
      newSupport : true
    });
    setTimeout(() => {
      this.setData({userSupport:{},newSupport : false});
    },8000)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {
    wx.PTTSendClick("site", "share", "现场");
    return {
      title: "Vstation电竞体验馆",
      path: '/pages/index/index?source=share',
      imageUrl: this.data.bgSrc
    }

  },
  toSitePic(){
    wx.PTTSendClick("btns", "piclist", "照片列表");
  },
  toRanking(){
    wx.PTTSendClick("btns", "rankinglist", "排行榜");
  }
});
