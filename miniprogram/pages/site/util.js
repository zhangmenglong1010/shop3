const msgs = [
  "666666",
  "我要上电视！！",
  "老板晚上好",
  "前方高能预警",
  "主播迟到了~~~",
  "干的漂亮",
  "早",
  "广东人民发来贺电",
  "不爱看的走开，别说话wen我",
];

const message = [
  {
    a: "https://img9.doubanio.com/icon/u53465265-65.jpg",
    c: "666666",
  },
  {
    a: "https://img9.doubanio.com/icon/u53465265-65.jpg",
    c: "我要上电视！！",
  },
  {
    a: "https://img9.doubanio.com/icon/u53465265-65.jpg",
    c: "老板晚上好",
  },
  {
    a: "https://img9.doubanio.com/icon/u53465265-65.jpg",
    c: "前方高能预警",
  },
  {
    a: "https://img9.doubanio.com/icon/u53465265-65.jpg",
    c: "主播迟到了~~~~",
  },
  {
    a: "https://img9.doubanio.com/icon/u53465265-65.jpg",
    c: "干的漂亮",
  },
  {
    a: "https://img9.doubanio.com/icon/u53465265-65.jpg",
    c: "早",
  },
  {
    a: "https://img9.doubanio.com/icon/u53465265-65.jpg",
    c: "广东人民发来贺电",
  },
  {
    a: "https://img9.doubanio.com/icon/u53465265-65.jpg",
    c: "不爱看的走开，别说话wen我",
  },
];

const color = ["#EFC91E", "#ffffff"];

const getRandom = (max = 10, min = 0) =>
  Math.floor(Math.random() * (max - min) + min);

const mockData = (num) => {
  const data = [];
  for (let i = 0; i < num; i++) {
    const msgId = getRandom(msgs.length);
    const colorId = getRandom(color.length);
    data.push({
      content: message[msgId].c,
      color: color[colorId],
      image: {
        head: { src: message[msgId].a, width: 20, height: 20 }, // 弹幕头部添加图片
        gap: 10, // 图片与文本间隔
      },
    });
  }
  return data;
};

module.exports = {
  mockData,
};
