// miniprogram/pages/myQrcode/myQrcode.js
import drawQrcode from '../../utils/scripts/weapp.qrcode.esm.js';
let request = require('../../utils/scripts/request.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userName: '',
    avatar: '',
    canUseInfo: false,
    showDetail: false,
    openid: '',
    tutor: 0,
    testList: {
      1: {},
      2: {},
      3: {},
      4: {},
      5: {},
      6: {}
    },
    score: '',
    support: -1,
    supPicture: -1,
    loading: false,
    deleteing: false,
    test_detail: '',
    testNames : ["双线博弈","全局记忆","轨迹追踪","眼疾手快","临机应变","运筹帷幄"],
    showed : false,
    cdk : 'ckdcdkckdkdsdkal',
    qrcodeSrc : '',
    lotteryed : false,
    lotteryStatus : 0,
    thanks : false,
    canLottery : false,
    lotteryText : '',
    exchangeStatus : false,
    lotteryId : '',
    tutorWatch : null,
    tutorList : ["微笑", "Clearlove", "Uzi", "MLXG", "Letme", "SKY", "姿态", "梦泪", "FLY", "老帅", "若风", "PDD"]
  },
  onLoad(query) {
    query.tips == "true" && this.lottery();
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onShow: function (options) {
    if (typeof this.getTabBar === 'function' &&
      this.getTabBar()) {
      this.getTabBar().setData({
        selected: 4
      })
    }
    wx.setScreenBrightness({
      value : 1
    })
    this.init(options);
  },
  // 初始化用户信息
  init: function (options) {
    this.setData({
      showed: false
    })
    request.requestCloud({
      url: '/api/user/detail',
      method: 'get'
    }).then(res => {
      if (!res.data.isRequireWxInfo) {
        this.drawOpenid(res);
      } else {
        wx.canIUse('button.open-type.getUserInfo') && this.setData({
          canUseInfo: true
        });
      }
    });
  },

  // 初始化用户抽奖状态
  initLottery(res) {
    let date = new Date();
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    month = month < 10 ? `0${month}` : `${month}`;
    day = day < 10 ? `0${day}` : `${day}`;
  
    let dateString = `${year}/${month}/${day}`;

		let today = new Date(`${dateString} 00:00:00`).getTime();
    // 根据当天互动时间，互动时间大于当天则可进行抽奖
    //  && res.data.canLottery
    if (res.data.activityTimestamp > today) {
      // 获取用户历史抽奖信息
      request.requestCloud({
        url: '/api/user/lottery/list',
        method: 'get'
      }).then(res => {
        // 为抽过奖，可进入抽奖
        if(res.data.length == 0){
          // 未抽奖，进入可抽奖状态
          this.setData({
            lotteryStatus: 0,
            canLottery: true
          })
        } else {
          // 最后一次抽奖未超过当天，可以进行抽奖
          if(res.data[0].created_at < today){
            this.setData({
              lotteryStatus : 0,
              canLottery : true
            })
          }else{
            // 已抽奖，判断奖品类型
            this.setLotteryStatue(res.data[0]);
          }
        }
        this.setData({
          loading: true
        });
      })
    } else {
      // 无权限抽奖，进入提示界面
      this.setData({
        lotteryStatus: 0,
        canLottery: false,
        loading: true
      });
    }
  },
  setLotteryStatue(res) {
    let lot = res.lotteryType;
    this.setData({lotteryed:true});
    switch (lot.lotStatus) {
      case 0:
        this.setData({
          lotteryStatus: 0,
          thanks: true,
          canLottery: false,
          lotteryText: lot.text
        });
        break;
      case 1 : 
        this.setData({lotteryStatus:1,cdk:res.virtualInfo.content,canLottery:false,lotteryText:lot.text});
        break;
      case 2:
        this.setData({
          lotteryText: lot.text,
          lotteryStatus: 2,
          canLottery: false,
          exchangeStatus: res.isExchange,
          lotteryId: res._id
        });
        break;
      case 3 : 
        this.setData({lotteryText:lot.text,cdk:res.virtualInfo.content,lotteryStatus:3,canLottery:false});
        break;
      default : 
        this.setData({canLottery:false});
        break;
    }
  },

  // 未登陆授权时，调起授权
  bindGetUserInfo(res) {
    wx.PTTSendClick("btn", "userinfo", "授权信息");
    let detail = res.detail.userInfo,
      that = this;
    // 同步用户基础数据
    // | name | string | √ | - | 昵称 |
    // | avatar | string | √ | - | 头像 |
    // | gender | int | √ | - | 性别 |
    // | city | string | - | - | 城市 |
    // | country | string | - | - | 国家 |
    // | province | string | - | - | 省份 |
    // | language | string | - | - | 语言 |
    let userInfo = {
      name: detail.nickName,
      avatar: detail.avatarUrl,
      gender: detail.gender,
      city: detail.city,
      country: detail.country,
      province: detail.province,
      language: detail.language,
      cloudID: res.detail.cloudID,
    }

    request.requestCloud({
      url: '/api/user/update',
      data: userInfo
    }).then(res => {
      this.setData({
        canUseInfo: false
      })
      setTimeout(() => {
        that.drawOpenid(res);
      }, 100)
    })
  },
  watchTutor(){
    let that = this;
    clearTimeout(this.data.tutorWatch);
    let tutorWatch = setTimeout(()=> {
      request.requestCloud({
        url : '/api/user/detail',
        method : 'get'
      }).then(res => {
        console.log(res.data.tutorId);
        let tutorName = res.data.tutorId >= 0 && res.data.tutorId < 12 ? that.data.tutorList[res.data.tutorId] : '';
        if(res.data.tutorId >= 0 && res.data.tutorId < 12){
          that.setData({tutor:res.data.tutorId,tutorName})
        }else{
          that.watchTutor();
        }
      })
    },5000);
    this.setData({
      tutorWatch
    })
  },
  // 绘制用户open-id二维码
  drawOpenid(res) {
    let that = this;
    that.initLottery(res);
    let scoreList = {
        "1": "",
        "2": "",
        "3": "",
        "4": "",
        "5": "",
        "6": "",
      };
    for (var item in scoreList) {
      if(res.data.testScoreList[item]){
        res.data.testScoreList[item].score = String(res.data.testScoreList[item].score).slice(0,4);
        scoreList[item] = res.data.testScoreList[item];
      }
    }
    if(res.data.tutorId < 0 || res.data.tutorId > 12){
      this.watchTutor();
    }
    let tutorName = res.data.tutorId >= 0 && res.data.tutorId < 12 ? this.data.tutorList[res.data.tutorId] : '';
    this.setData({
      score: res.data.score,
      userName: res.data.name,
      avatar: res.data.avatar,
      openid: res.data.openid,
      showDetail: true,
      tutor: res.data.tutorId,
      tutorName: tutorName,
      testList: scoreList || this.data.testList,
      test_detail: JSON.stringify(res.data.testScoreList)
    })
    drawQrcode({
      width: 150,
      height: 150,
      canvasId: 'qrcode',
      text: this.data.openid,
      callback: () => {
        // 将二维码转为图片，解决弹层层级问题
        setTimeout(() => {
          let ctx = wx.createCanvasContext('qrcode');
          wx.canvasToTempFilePath({
            canvasId: "qrcode",
            x: 0,
            y: 0,
            width: 150,
            height: 150,
            destWidth: 150,
            destHeight: 150,
            fileType: 'jpg',
            quality: 1,
            success(res) {
              that.setData({
                qrcodeSrc: res.tempFilePath
              });
            }
          })
        }, 500)

      }
    });
    this.getUserPhoto();
  },
  // 获取用户照片情况
  getUserPhoto() {
    request.requestCloud({
      url: '/api/user/photo/list',
      method: 'get'
    }).then(res => {
      this.setData({
        picture: res.data.total
      })
    })
  },
  onShareAppMessage() {
    wx.PTTSendClick("mycode", "share", "我的页面分享");
    return {
      title: "Vstation电竞体验馆",
      path: '/pages/index/index?source=share',
      imageUrl: 'https://game.gtimg.cn/images/game/act/a20201001vstation/share.png'
    }
  },
  toPageReport() {
    wx.PTTSendClick("mycode", "report", "我的测试报告");
    if(this.data.score > 0 && this.data.tutor >= 0){
      wx.navigateTo({
        url: '/pages/report/report'
      });
    }
  },
  toPagePic() {
    wx.PTTSendClick("mycode", "pic", "我的合影");
    this.data.picture > 0 && wx.navigateTo({
      url: '/pages/my/my'
    });
  },
  // 抽奖逻辑，开始抽奖 || 展示抽奖内容
  lottery() {
    wx.PTTSendClick("mycode", "lottery", "抽奖弹层");
    if (this.data.canLottery) {
      request.requestCloud({
        url: '/api/user/lottery',
        method: 'post'
      }).then(res => {
        this.setLotteryStatue(res.data);
        this.triggerDialog();
      });
    } else {
      this.triggerDialog();
    }
  },
  triggerDialog() {
    this.setData({
      showed: !this.data.showed
    });
  },
  clipCdk() {
    wx.PTTSendClick("mycode", "clipcdk", "复制cdk");
    wx.setClipboardData({
      data: this.data.cdk,
      success(res) {
        console.log('clip success');
      }
    })
  },
  confirmLottery() {
    let that = this;
    if (!this.data.exchangeStatus) {
      wx.showModal({
        title: '提示',
        content: '是否确认已拿到奖品？',
        success(res) {
          if (res.confirm) {
            wx.PTTSendClick("mycode", "objectconfirm", "确认领奖");
            request.requestCloud({
              url: '/api/user/lottery/exchange',
              method: 'post',
              data: {
                record_id: that.data.lotteryId
              }
            }).then(res => {
              that.setData({
                exchangeStatus: true
              })
            })
          } else if (res.cancel) {
            wx.PTTSendClick("mycode", "objectcancle", "暂停领奖");
            that.triggerDialog();
          }
        }
      });
    } else {
      that.triggerDialog();
    }
  },
  onHide(){
    clearTimeout(this.data.tutorWatch);
  }
})