// miniprogram/pages/info/info.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    detail : [],
    url : 'https://game.qq.com/act/a20201001vstation/show_info.html',
  },
  onShareAppMessage(){
    wx.PTTSendClick("infodetail","share","分享");
    return {
      title : "Vstation电竞体验馆",
      path: '/pages/index/index?source=share',
      imageUrl : 'https://game.gtimg.cn/images/game/act/a20201001vstation/share.png'
    }
  },
  onLoad(option){
  },
  onShow(option){
    let that = this;
    if (typeof this.getTabBar === 'function' &&
      this.getTabBar()) {
      this.getTabBar().setData({
        selected: 1
      })
    }
    const eventChannel = this.getOpenerEventChannel()
    // 监听acceptDataFromOpenerPage事件，获取上一页面通过eventChannel传送到当前页面的数据
    eventChannel && eventChannel.on && eventChannel.on('acceptDataFromOpenerPage', function(data) {
      let url = 'https://game.qq.com/act/a20201001vstation/news_v2.html?id='+data.id;
      that.setData({url})
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  }
})