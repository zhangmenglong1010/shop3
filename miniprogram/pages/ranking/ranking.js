// miniprogram/pages/site/site.js
let request = require("../../utils/scripts/request.js");
Page({
  /**
   * 页面的初始数据
   */
  data: {
    loading: false,
    tiantiUser: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onShow(options) {
    request.requestCloud({
      url : '/api/rank/list',
      method : 'get',
      data : {
        size : 100
      }
    }).then(res => {
      let rankList = [];
      let list = res.data;
      list.forEach(item => {
        rankList.push(
          {
            name: item.name,
            score: item.score,
            avatar: item.avatar
          }
        )
      });
      this.setData({tiantiUser : rankList});
    })
  },
  onShareAppMessage() {
    wx.PTTSendClick("ranking", "share", "分享");
    return {
      title: "Vstation电竞体验馆",
      path: '/pages/index/index?source=share',
      imageUrl: 'https://game.gtimg.cn/images/game/act/a20201001vstation/share.png'
    }
  },
});
