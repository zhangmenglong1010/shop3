//index.js
let request = require('../../utils/scripts/request.js');
const app = getApp()
Page({
  data: {
    hostTime: '上海 · 正大广场9F',
    hostDesc: '',
    playing: false,
    videoHeight: '100px',
    bigScreen: false,
    superEarlyOpen: false,
    earlyOpen: false,
    defaultOpen: false,
    ticketLink: '',
    superearlayTickerNumber: '',
    earlyStart: '',
    earlyEnd: '',
    defaultStart: '',
    arrowShow: 1,
    isAdmin: '' // 是否系统管理员，用于显示互动&小程序整体数据
  },
  startPlay() {
    let windowWidth = wx.getSystemInfoSync().windowWidth;
    this.setData({
      videoHeight: windowWidth * (1448 / 750),
      playing: true
    })
  },
  onShow() {
    if (typeof this.getTabBar === 'function' &&
      this.getTabBar()) {
      this.getTabBar().setData({
        selected: 0
      })
    }
    wx.getSystemInfo().then(res => {
      res.windowHeight > 700 && this.setData({
        bigScreen: true
      })
    })
    let that = this;
    request.requestCloud({
      url: '/api/system/mnp/info',
      method: 'get'
    }).then(res => {
      console.log(123, res);
      let hostTime = res.data.host_time,
        host = res.data.host,
        superEarlyOpen = res.data.superearly_ticket_open,
        earlyOpen = res.data.early_ticket_open,
        defaultOpen = res.data.default_ticket_open,
        ticketLink = res.data.ticket_link;
      this.setData({
        // hostTime: `${hostTime} ${host}`,
        hostTime: host,
        hostDesc: res.data.host_desc,
        superEarlyOpen,
        earlyOpen,
        defaultOpen,
        ticketLink,
        superearlayTickerNumber: res.data.superearly_ticket_number || '限量1000张',
        earlyStart: res.data.early_start || '8月24日开售',
        earlyEnd: res.data.early_end || '9月14日截止',
        defaultStart: res.data.default_start || '9月15日开售',
        isAdmin: res.data.isAdmin
      });
      app.globalData.commInfo.host = host;
    })
    // wx.request({
    //   url: 'https://game.gtimg.cn/images/game/act/a20201001vstation/info-list.json',
    //   success(res) {
    //     app.globalData.commInfo = res.data;
    //   }
    // })
  },
  onShareAppMessage() {
    wx.PTTSendClick("index", "share", "分享");
    return {
      title: "Vstation电竞体验馆",
      path: '/pages/index/index?source=share',
      imageUrl: 'https://game.gtimg.cn/images/game/act/a20201001vstation/share.png'
    }
  },
  onLoad: function () {
    // getApp().globalData && this.setData({
    //   hostTime : getApp().globalData.hostTime
    // })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              this.setData({
                avatarUrl: res.userInfo.avatarUrl,
                userInfo: res.userInfo
              })
            }
          })
        }
      }
    })
    this.loadList();
  },
  pageDiscount(e) {
    wx.PTTSendClick("indexbtns", "discount", "裂变购票");
    const path = e.currentTarget.dataset.webpath;
    wx.navigateTo({
      url: '/pages/fission/webView/webView?webPath=' + path,
    })
  },
  pagecalendar() {
    wx.PTTSendClick("indexbtns", "calendar", "进入电竞日历");
  },
  toBuyTicket() {
    wx.PTTSendClick("indexbtns", "buy", "直接购票");
    wx.navigateToMiniProgram({
      appId: 'wx938b41d0d7e8def0',
      path: '/pages/detail/item?projectId=626042467221&utm=NtQRNZ'
    });
  },
  //新闻 S
  loadList() {
    var that = this;
    var idxOrder = 'sIdxTime';
    let id = 580,
      type = 1,
      size = 20,
      page = 1;
    wx.request({
      url: "https://app.ingame.qq.com/gingame/content/content_list_by_channel?game=esports&order=1&ingame_channel_id=" + id + "&type=" + type + "&num=" + size + "&start=" + (page - 1) * size,
      method: 'GET',
      header: {
        'Accept': 'application/json'
      }, // 设置请求的 header
      success: function (res) {
        if (res.data.data.content_list && res.data.data.content_list.length > 0) {
          that.setData({
            list: res.data.data.content_list,
            newsMax: res.data.data.total
          })
        }
        wx.hideToast();
      }
    });
  },
  infoDetail(event) {
    let id = event.currentTarget.dataset.id;
    wx.PTTSendClick("info", "list", "列表项");
    wx.navigateTo({
      url: '/pages/infoDetail/infoDetail?id=' + id,
      success: function (res) {
        // 通过eventChannel向被打开页面传送数据
        res.eventChannel.emit('acceptDataFromOpenerPage', {
          id: id
        })
      }
    })
  },
  onShareAppMessage() {
    wx.PTTSendClick("info", "share", "分享");
    return {
      title: "Vstation电竞体验馆",
      path: '/pages/index/index?source=share',
      imageUrl: 'https://game.gtimg.cn/images/game/act/a20201001vstation/share.png'
    }
  },
  toClub() {
    wx.PTTSendClick("btns", "club", "俱乐部信息");
    wx.navigateTo({
      url: '/pages/club/club',
    })
  },
  hideArrow() {
    this.setData({
      arrowShow: 0
    })
  }
  //新闻 E
})