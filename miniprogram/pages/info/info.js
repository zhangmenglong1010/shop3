// miniprogram/pages/info/info.js

const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list : []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onShow: function (options) {
    if (typeof this.getTabBar === 'function' &&
      this.getTabBar()) {
      this.getTabBar().setData({
        selected: 1
      })
    }
    let that = this;
    // this.setData({
    //   list : app.globalData.commInfo.infoList
    // })
    this.loadList();
  },
  loadList() {
    var that = this;
    var idxOrder = 'sIdxTime';
    let id = 580,
        type = 1,
        size = 10,
        page = 1;
    wx.request({
      url: "https://app.ingame.qq.com/gingame/content/content_list_by_channel?game=esports&order=1&ingame_channel_id=" + id + "&type=" + type + "&num=" + size + "&start=" + (page - 1) * size,
      method: 'GET',
      header: {
        'Accept': 'application/json'
      }, // 设置请求的 header
      success: function (res) {
        if (res.data.data.content_list && res.data.data.content_list.length>0){
          that.setData({
            list : res.data.data.content_list,
            newsMax: res.data.data.total
          })
        }
        wx.hideToast();
      }
    });
  },
  infoDetail(event){
    let id = event.currentTarget.dataset.id;
    wx.PTTSendClick("info","list","列表项");
    wx.navigateTo({
      url : '/pages/infoDetail/infoDetail?id='+id,
      success: function(res) {
        // 通过eventChannel向被打开页面传送数据
        res.eventChannel.emit('acceptDataFromOpenerPage', { id: id })
      }
    })
  },
  onShareAppMessage(){
    wx.PTTSendClick("info","share","分享");
    return {
      title : "Vstation电竞体验馆",
      path: '/pages/index/index?source=share',
      imageUrl : 'https://game.gtimg.cn/images/game/act/a20201001vstation/share.png'
    }
  },
  toClub(){
    wx.PTTSendClick("btns","club","俱乐部信息");
    wx.navigateTo({
      url: '/pages/club/club',
    })
  }
})