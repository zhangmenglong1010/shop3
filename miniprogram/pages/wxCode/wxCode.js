// miniprogram/pages/wxCode/wxCode.js
let request = require('../../utils/scripts/request.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    loading : false,
    wxCodeCloudId : 0,
  },
  formSubmit(e){
    this.setData({loading:true});
    const that = this;
    let channel = e.detail.value.channel, path = e.detail.value.path || 'pages/index/index',size = e.detail.value.size;
    request.requestCloud({
      url : '/api/file/getWXACode',
      method : 'get',
      data : {
        channel,
        path,
        size
      }
    }).then(res => {
      console.log(res);
      this.setData({wxCodeCloudId:res.data,loading:false});
    })
  },
  saveCode(){
    wx.previewImage({
      urls: [this.data.wxCodeCloudId],
    })
  }
})