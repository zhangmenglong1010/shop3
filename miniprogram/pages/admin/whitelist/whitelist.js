// miniprogram/pages/admin/whitelist/whitelist.js
let request = require('../../../utils/scripts/request.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    avatar : '',
    userName : '用户名',
    phoneNumber : '',
    canUseInfo : false,
    ischeck : false,
    needed : false,
    showScan : false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    request.requestCloud({
      url : "/api/user/detail",
      method : "get"
    }).then(res => {
      let needGetInfo = false;
      if(res.data.isRequireWxInfo){
        needGetInfo = true;
      }
      this.setData({
        avatar : res.data.avatar,
        userName : res.data.name,
        getUserInfo : needGetInfo
      })
      this.getAdminInfo();
    });
  },
  getAdminInfo(){
    // 小程序端调用
    wx.cloud.callFunction({
      // 需调用的云函数名
      name: 'morphy_customForm',
      // 传给云函数的参数
      data: {
        '$url':'client/getMyFormData',
        'tableName':'whitelist'
      }
    }).then(res => {
      if(res.result.data.list.length > 0){
        this.setData({
          needed : false,
          ischeck : res.result.data.list[0].formData.ischeck
        })
      }else{
        this.setData({needed:true});
      }
    })
  },
  bindGetUserInfo(res){
    let detail = res.detail.userInfo,
      that = this;
      let userInfo = {
        name: detail.nickName,
        avatar: detail.avatarUrl,
        gender: detail.gender,
        city: detail.city,
        country: detail.country,
        province: detail.province,
        language: detail.language,
        cloudID: res.detail.cloudID,
      }
      request.requestCloud({
        url: '/api/user/update',
        data: userInfo
      }).then(res => {
        this.setData({
          getUserInfo: false,
          avatar : res.data.avatar,
          userName : res.data.name
        })
      })

  },
  getPhoneNumber(e){
    let that = this;
    if(e.detail.errMsg == 'getPhoneNumber:ok'){
      wx.login({
        success(res){
          request.requestCloud({
            url : '/api/user/phone',
            method : 'get',
            data : {
              cloudID : e.detail.cloudID
            }
          }).then(res => {
            that.setData({
              phoneNumber : res.data.list[0].data.phoneNumber
            })
          })
        }
      })
    }
  },
  submit(){
    if(this.data.userName && this.data.avatar && this.data.phoneNumber.length == 11){
      // 小程序端调用
      wx.cloud.callFunction({
        // 需调用的云函数名
        name: 'morphy_customForm',
        // 传给云函数的参数
        data: {
            '$url':'client/addFormData',
              'tableName':'whitelist',
              'formData':{
                userName : this.data.userName,
                avatar : this.data.avatar,
                phone : this.data.phoneNumber,
                ischeck : 0
              }
        }
      }).then(res => {
        this.setData({
          needed : false
        })
      })
    }
  },
  getCode(res){
    if(res.detail.result){
      let scanOpenid = res.detail.result;
      this.setData({showScan:false});
      this.activeLottery(scanOpenid);
    }
  },
  activeLottery(openid){
    let that = this;
    wx.showLoading({
      title: '激活中',
    });
    request.requestCloud({
      url : '/api/operator/lottery/sign',
      method : 'post',
      data : {
        openid : openid
      }
    }).then(res => {
      wx.hideLoading();
      wx.showToast({
        title: '激活成功！',
        duration : 2000,
        icon : 'none'
      })
    }).catch(err => {
      wx.hideLoading();
      wx.showToast({
        title: '该用户今日已激活！',
        duration : 2000,
        icon : 'none'
      })
    })
  },
  openScan(res){
    this.setData({
      showScan : true
    })
  }
})