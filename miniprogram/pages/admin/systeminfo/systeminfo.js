// miniprogram/pages/admin/systeminfo/systeminfo.js
let request = require('../../../utils/scripts/request.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info : ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    request.requestCloud({
      url: '/api/system/mnp/info',
      method: 'get'
    }).then(res => {
      if(!res.data.isAdmin){
        wx.switchTab({
          url: '/pages/index/index',
        })
      }else{
        return request.requestCloud({
          url : '/api/user/operator/data',
          method : 'get'
        })
      }
    }).then(res => {
      console.log('res',res.data)
      this.setData({
        info : res.data
      })
    })

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})