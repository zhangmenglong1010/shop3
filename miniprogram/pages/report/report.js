// miniprogram/pages/report/report.js
let request = require('../../utils/scripts/request.js');
Page({

  /**
   * 页面的初始数据
   */


  data: {
    userInfo: {},
    day: '',
    radarInfo: [
      [213, 64], // 手眼协调
      [278, 146], // 多线加工
      [254, 248], // 成就动机
      [162, 294], // 调度力
      [68, 248], // 即战力
      [45, 146], // 抗压力
      [109, 63] // 专注力
    ],
    userRadar: [],
    centerPoint: [165, 174],
    percent: 0,
    canvasSize: [],
    radarImage: null,
    loaded: false,
    tutorInfo: [],
    scaleData: [],
    speakText: "",
    sealUrl : "https://game.gtimg.cn/images/game/act/a20201001vstation/seal/1-1.png",
    tutorEnames : ["微笑", "Clearlove", "Uzi", "MLXG", "Letme", "SKY", "姿态", "梦泪", "FLY", "老帅", "若风", "PDD"],
    tutorNames : ["高学成", "明凯", "简自豪", "刘世宇", "严君泽", "李晓峰", "刘志豪", "肖闽辉", "彭云飞", "张宇晨", "禹景曦", "刘谋"],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {
    wx.PTTSendClick("report", "share", "测试报告");
    return {
      title: "Vstation电竞体验馆",
      path: '/pages/index/index?source=share',
      imageUrl: this.data.bgSrc
    }

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    request.requestCloud({
      url: '/api/system/mnp/info',
      method: 'get'
    }).then(res => {
      let tutorInfo = res.data.tutorInfo;
      this.setData({
        tutorInfo
      });
      return request.requestCloud({
        url: '/api/user/detail',
        method: 'get'
      })
    }).then(res => {
      let y = new Date().getFullYear(),
        m = new Date().getMonth() + 1,
        d = new Date().getDate();
      let day = `${y}.${m}.${d}`;
      let defaultData = this.data.radarInfo,
        scaleData = [
          res.data.totalScoreDetail.coordinate,
          res.data.totalScoreDetail.multithread,
          res.data.totalScoreDetail.achieve,
          res.data.totalScoreDetail.dispatch,
          res.data.totalScoreDetail.instant,
          res.data.totalScoreDetail.compressive,
          res.data.totalScoreDetail.focus
        ],
        userRadar = [],
        centerPoint = this.data.centerPoint;
      for (var i = 0; i < defaultData.length; i++) {
        let scaleDetail = scaleData[i] / 100 * 0.7 + 0.3;
        userRadar.push([
          centerPoint[0] + ((defaultData[i][0] - centerPoint[0]) * scaleDetail),
          centerPoint[1] + ((defaultData[i][1] - centerPoint[1]) * scaleDetail)
        ])
      }
      let level = res.data.level == 1 ? "good" : "normal",
          Text = this.data.tutorInfo[res.data.tutorId][level],
          speakText = Text[Math.floor(Math.random()*(Text.length))];
      let randomNum = [6,4,7,6,6],
          sealNum = Math.floor(Math.random() * randomNum[res.data.level - 1])+1;
      this.setData({
        userInfo: res.data,
        day,
        userRadar,
        scaleData,
        speakText,
        sealUrl : `https://game.gtimg.cn/images/game/act/a20201001vstation/seal/${res.data.level}-${sealNum}.png`
      });
      this.drawScore();
    });
  },
  savePic() {
    wx.PTTSendClick("report", "savepic", "保存图片");
    this.saveTest();
  },
  saveVideo(){
    wx.PTTSendClick("report", "savevideo", "保存视频");
    if(this.data.userInfo.reportVideoUrl){
      wx.showLoading({
        title: '保存中',
        mask: true
      })
      wx.downloadFile({
        url: 'https://'+this.data.userInfo.reportVideoUrl,
        filePath : wx.env.USER_DATA_PATH + "/" + new Date().getTime().toString() + '.mp4',
        success(res){
          console.log(res);
          wx.saveVideoToPhotosAlbum({
            filePath: res.filePath,
            success(){
              wx.hideLoading();
              wx.showToast({
                title: '已保存，请到手机相册查看！',
                icon: 'none',
                duration: 1500
              })
            },
            fail(err){
              console.log('err',err);
              wx.hideLoading();
              wx.showToast({
                title: '保存失败，请重试！',
                icon: 'none',
                duration: 1500
              })
            }
          })
        }
      })
    }else{
      wx.hideLoading();
      wx.showToast({
        title: '视频生成中，请稍等几分钟重试！',
        icon: 'none',
        duration: 2000
      })
    }
  },
  drawScore() {
    const query = wx.createSelectorQuery()
    query.select('#scoreRadar')
      .fields({
        node: true,
        size: true
      })
      .exec((res) => {
        const canvas = res[0].node
        const ctx = canvas.getContext('2d');
        const dpr = wx.getSystemInfoSync().pixelRatio
        canvas.width = res[0].width * dpr;
        canvas.height = res[0].height * dpr;
        this.setData({
          canvasSize: [canvas.width, canvas.height]
        })
        let scale = res[0].width / 338;
        ctx.scale(dpr * scale, dpr * scale)
        wx.getImageInfo({
          src: 'https://game.gtimg.cn/images/game/act/a20201001vstation/my/radar.png'
        }).then(res => {
          let radarPath = res.path;
          let radarImage = canvas.createImage();
          radarImage.src = radarPath;
          radarImage.onload = () => {
            ctx.fillStyle = '#fff';
            ctx.save();
            // 中心点
            ctx.arc(165, 174, 4, 0, 2 * Math.PI);
            this.setData({
              radarImage,
              loaded: true
            });
            this.drawRadar(ctx);
          }
        })
      })
  },
  drawRadar(ctx) {
    let userRadar = this.data.userRadar,
      radarLength = userRadar.length,
      radarInfo = [],
      centerPoint = this.data.centerPoint,
      percent = this.data.percent,
      radarImage = this.data.radarImage;
    if (percent > 1) return;
    for (var i = 0; i < userRadar.length; i++) {
      radarInfo.push([
        centerPoint[0] + ((userRadar[i][0] - centerPoint[0]) * percent),
        centerPoint[1] + ((userRadar[i][1] - centerPoint[1]) * percent)
      ])
    }
    ctx.clearRect(0, 0, this.data.canvasSize[0], this.data.canvasSize[1]);
    ctx.drawImage(radarImage, 0, 0, 338, 368);
    ctx.restore();
    // 开始画雷达图
    ctx.beginPath();
    ctx.moveTo(radarInfo[radarLength - 1][0], radarInfo[radarLength - 1][1]);
    for (var i = 0; i < radarLength; i++) {
      ctx.lineTo(radarInfo[i][0], radarInfo[i][1])
    }
    ctx.strokeStyle = "#FECE00";
    ctx.shadowOffsetX = 0;
    ctx.shadowOffsetY = 0;
    ctx.shadowBlur = 20;
    ctx.shadowColor = "rgba(255, 167, 0,1)";
    ctx.closePath();
    ctx.stroke();
    const grd = ctx.createRadialGradient(165, 174, 1, 165, 174, 200)
    grd.addColorStop(0, 'rgba(254,256,0,.1)')
    grd.addColorStop(1, 'rgba(254,206,0,.1)')
    ctx.fillStyle = grd;
    ctx.fill();

    // 开始画小点
    ctx.beginPath();
    ctx.shadowOffsetX = 0;
    ctx.shadowOffsetY = 0;
    ctx.shadowBlur = 0;
    ctx.shadowColor = "rgba(0,0, 0, 0)";
    for (var i = 0; i < radarLength; i++) {
      ctx.beginPath();
      ctx.fillStyle = '#FECE00';
      ctx.arc(radarInfo[i][0], radarInfo[i][1], 4, 0, 2 * Math.PI);
      ctx.closePath();
      ctx.fill();
      ctx.beginPath();
      ctx.fillStyle = '#656565';
      ctx.arc(radarInfo[i][0], radarInfo[i][1], 2, 0, 2 * Math.PI);
      ctx.closePath();
      ctx.fill();
    }
    this.setData({
      percent: percent + 0.02
    });
    setTimeout(() => {
      this.drawRadar(ctx);
    }, 10)
  },
  saveTest() {
    wx.showLoading({
      title: '保存中',
      mask: true
    })
    const query = wx.createSelectorQuery()
    query.select('#save')
      .fields({
        node: true,
        size: true
      })
      .exec((res) => {
        const canvas = res[0].node
        const ctx = canvas.getContext('2d');
        const dpr = wx.getSystemInfoSync().pixelRatio
        canvas.width = res[0].width * dpr;
        canvas.height = res[0].height * dpr;
        let scale = res[0].width / 648;
        ctx.scale(dpr * scale, dpr * scale)

        let saveBg = canvas.createImage();
        saveBg.src = 'https://game.gtimg.cn/images/game/act/a20201001vstation/my/saved_bg.jpg';
        saveBg.onload = () => {
          ctx.drawImage(saveBg, 0, 0, 648, 1374);
          this.saveTestInfo(ctx, canvas);
        }
      })
  },
  saveTestInfo(ctx, canvas) {
    let avatar = new Promise((resolve, reject) => {
      let avatarImg = canvas.createImage();
      avatarImg.src = this.data.userInfo.avatar;
      avatarImg.onload = () => {
        resolve(avatarImg);
      }
      avatarImg.onerror = (err) => {
        reject(err);
      }
    });
    let seal = new Promise((resolve, reject) => {
      let sealImg = canvas.createImage();
      sealImg.src = this.data.sealUrl;
      sealImg.onload = () => {
        resolve(sealImg);
      }
      sealImg.onerror = (err) => {
        reject(err);
      }
    });
    let qrcode = new Promise((resolve, reject) => {
      let qrcodeImg = canvas.createImage();
      qrcodeImg.src = 'https://game.gtimg.cn/images/game/act/a20201001vstation/calendar/wxacode_default.jpeg';
      qrcodeImg.onload = () => {
        resolve(qrcodeImg);
      }
      qrcodeImg.onerror = (err) => {
        reject(err);
      }
    });
    let tutor = new Promise((resolve, reject) => {
      let tutorImg = canvas.createImage();
      // tutorImg.src = `https://game.gtimg.cn/images/game/act/a20201001vstation/tutor/full_tutor${this.data.userInfo.tutorId+2}.png`;
      tutorImg.src = `https://game.gtimg.cn/images/game/act/a20201001vstation/tutor/full_tutor${this.data.userInfo.tutorId+1}.png`;
      tutorImg.onload = () => {
        resolve(tutorImg);
      }
      tutorImg.onerror = (err) => {
        reject(err);
      }
    });
    let sign = new Promise((resolve, reject) => {
      let signImg = canvas.createImage();
      signImg.src = `https://game.gtimg.cn/images/game/act/a20201001vstation/my/tutor_sign.png`;
      signImg.onload = () => {
        resolve(signImg);
      }
      signImg.onerror = (err) => {
        reject(err);
      }
    });
    Promise.all([avatar, seal, qrcode, tutor, sign]).then(res => {
      let avatarImg = res[0];
      ctx.save();
      var x = 42,
        y = 34,
        w = 96,
        h = 96;
      ctx.arc(x + w / 2, y + h / 2, w / 2, 0, 2 * Math.PI);
      ctx.clip();
      ctx.drawImage(avatarImg, x, y, w, h);
      ctx.restore();
      ctx.save();
      ctx.strokeStyle = '#F9CF00';
      ctx.drawImage(avatarImg, 382, 486, 56, 56);
      ctx.strokeRect(382, 486, 56, 56);
      ctx.restore();


      let sealImg = res[1];
      ctx.save();
      ctx.drawImage(sealImg, 430, 670, 159, 136);
      ctx.restore();


      let qrcodeImg = res[2];
      ctx.save();
      var x = 110,
        y = 1220,
        w = 140,
        h = 140;
      ctx.arc(x + w / 2, y + h / 2, w / 2, 0, 2 * Math.PI);
      ctx.clip();
      ctx.drawImage(qrcodeImg, x, y, w, h);
      ctx.restore();

      let tutorImg = res[3];
      ctx.drawImage(tutorImg, 202, 170, tutorImg.width * (232 / tutorImg.height), 232);

      let signImg = res[4];
      // ctx.drawImage(signImg, 464, 1086, signImg.width, signImg.height);


      ctx.textAlign = "left";
      ctx.font = 'bolder 36px "tencentW7"';
      ctx.fillStyle = '#FAD500';
      ctx.fillText(this.data.userInfo.name, 154, 70);
      ctx.fillText('的专属报告', 154, 115);
      ctx.font = "bolder 36px 'tencentW7'";
      ctx.fillStyle = "#FFF";
      ctx.fillText(this.data.tutorEnames[this.data.userInfo.tutorId],20,340);
      ctx.font = "bolder 20px 'tencentW7'";
      ctx.fillStyle = "#FFF";
      ctx.fillText(this.data.tutorNames[this.data.userInfo.tutorId],20,370);
      ctx.textAlign = "left";
      ctx.font = 'bolder 20px "tencentW7"';
      ctx.fillStyle = '#FAD500';
      ctx.fillText('天赋测试:', 382, 606);
      ctx.font = '12px "tencentW7"';
      ctx.fillText('综合评分', 382, 626);
      ctx.font = 'bolder 50px "tencentW7"';
      ctx.fillText(this.data.userInfo.score, 480, 626);
      ctx.font = 'bolder 30px "tencentW7"';
      ctx.fillStyle = '#fff';
      ctx.fillText(this.data.userInfo.name, 450, 512);
      ctx.font = '20px "tencentW7"';
      ctx.fillStyle = '#fff';
      ctx.fillText(this.data.day, 450, 542);
      ctx.font = '24px "tencentW7"';
      ctx.fillStyle = '#fff';
      ctx.fillText('HI，亲爱的', 80, 972);
      ctx.font = '24px "tencentW7"';
      ctx.fillStyle = '#FAD500';
      ctx.fillText(this.data.userInfo.name, 205, 972);
      ctx.fillStyle = '#FFF';
      this.canvasTextAutoLine(this.data.speakText, ctx, 520, 40, 80, 1020);
      this.drawSavedRadar(ctx, canvas);
    });
  },
  saveCanvas(canvas) {
    wx.canvasToTempFilePath({
      canvas,
      x: 0,
      y: 0,
      width: 648,
      height: 1374,
      destWidth: 648,
      destHeight: 1374,
      fileType: 'jpg',
      quality: 1,
      success(res) {
        wx.saveImageToPhotosAlbum({
          filePath: res.tempFilePath,
          success(res) {
            wx.hideLoading();
            wx.showToast({
              title: '已保存，请到手机相册查看！',
              icon: 'none',
              duration: 1500
            })
          },
          fail(res) {
            wx.hideLoading();
            wx.showToast({
              title: '保存失败，请重新尝试！',
              icon: 'none',
              duration: 1500
            })
          }
        })
      }
    })

  },
  canvasTextAutoLine(str, ctx, width, lineHeight, x, y) {
    var lineWidth = 0;
    var lastSubStrIndex = 0;
    for (let i = 0; i < str.length; i++) {
      lineWidth += ctx.measureText(str[i]).width;
      if (lineWidth > width) { //减去initX,防止边界出现的问题
        ctx.fillText(str.substring(lastSubStrIndex, i), x, y);
        y += lineHeight;
        lineWidth = 0;
        lastSubStrIndex = i;
      }
      if (i == str.length - 1) {
        ctx.fillText(str.substring(lastSubStrIndex, i + 1), x, y);
      }
    }
  },
  drawSavedRadar(ctx, canvas) {
    let userRadar = this.data.userRadar,
      radarLength = userRadar.length,
      fullInfo = this.data.radarInfo,
      radarInfo = [],
      userScore = this.data.scaleData,
      textInfo = [
        [250, 520],
        [324, 645],
        [295, 757],
        [180, 815],
        [65, 758],
        [40, 644],
        [110, 520]
      ];
    // 雷达坐标转换为保存版本坐标
    for (var i = 0; i < radarLength; i++) {
      radarInfo.push([
        userRadar[i][0] + 18,
        userRadar[i][1] + 489
      ])
    }

    let radarImage = canvas.createImage();
    radarImage.src = 'https://game.gtimg.cn/images/game/act/a20201001vstation/my/radar_save.png';
    radarImage.onload = () => {
      ctx.fillStyle = '#fff';
      ctx.save();
      // 中心点
      ctx.arc(165, 174, 4, 0, 2 * Math.PI);
      ctx.drawImage(radarImage, 18, 489, 338, 368);
      ctx.restore();
      // 开始画雷达图
      ctx.beginPath();
      ctx.moveTo(radarInfo[radarLength - 1][0], radarInfo[radarLength - 1][1]);
      for (var i = 0; i < radarLength; i++) {
        ctx.lineTo(radarInfo[i][0], radarInfo[i][1])
      }
      ctx.strokeStyle = "#FECE00";
      ctx.shadowOffsetX = 0;
      ctx.shadowOffsetY = 0;
      ctx.shadowBlur = 20;
      ctx.shadowColor = "rgba(255, 167, 0,1)";
      ctx.closePath();
      ctx.stroke();
      const grd = ctx.createLinearGradient(162, 64, 162, 294)
      grd.addColorStop(0, 'rgba(254,256,0,.15)')
      grd.addColorStop(1, 'rgba(254,206,0,.15)')
      ctx.fillStyle = grd;
      ctx.fill();

      // 开始画小点
      ctx.beginPath();
      ctx.shadowOffsetX = 0;
      ctx.shadowOffsetY = 0;
      ctx.shadowBlur = 0;
      ctx.shadowColor = "rgba(0,0, 0, 0)";
      for (var i = 0; i < radarLength; i++) {
        ctx.textAlign = "center";
        ctx.font = '28px "tencentW7"';
        ctx.fillStyle = '#fff';
        ctx.fillText(userScore[i], textInfo[i][0], textInfo[i][1]);
        ctx.beginPath();
        ctx.fillStyle = '#FECE00';
        ctx.arc(radarInfo[i][0], radarInfo[i][1], 4, 0, 2 * Math.PI);
        ctx.closePath();
        ctx.fill();
        ctx.beginPath();
        ctx.fillStyle = '#656565';
        ctx.arc(radarInfo[i][0], radarInfo[i][1], 2, 0, 2 * Math.PI);
        ctx.closePath();
        ctx.fill();
      }
      this.saveCanvas(canvas);
    }
  }
})