// miniprogram/pages/my/my.js
let request = require('../../utils/scripts/request.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    picList : [],
    videoList : [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onShow: function (options) {

    this.data.picList == 0  && request.requestCloud({
      url : '/api/user/photo/list',
      method : 'get',
      data : {
        pageSize : 5
      }
    }).then(res => {
      if(res.data.total > 0 ){
        let picList = [],videoList = [];
        res.data.list.forEach(item => {
          item.photos.forEach(photo => {
            if(photo.path.indexOf('cloud://') < 0 ){
              photo.path = 'https://' + photo.path
            }
          })
          if(item.video.mp4 && item.video.mp4.indexOf('cloud://') < 0){
            item.video.mp4 = 'https://' + item.video.mp4;
          }
          item.photos.length && (picList = picList.concat(item.photos));
          item.video.mp4 && item.video.mp4.indexOf('.mp4') > 0 && videoList.push(item.video);
        })
        this.setData({picList,videoList});
      }
    })
  },
  preview(e){
    wx.PTTSendClick("my", "preview", "预览");
    let x = e.currentTarget.dataset.x;
    wx.previewImage({
      urls: [this.data.picList[x].path],
    })
  },
  onShareAppMessage() {
    wx.PTTSendClick("my", "share", "分享");
    return {
      title: "Vstation电竞体验馆",
      path: '/pages/index/index?source=share',
      imageUrl: 'https://game.gtimg.cn/images/game/act/a20201001vstation/share.png'
    }
  },
  downVideo(e){
    let index = e.currentTarget.dataset.index;
    wx.PTTSendClick("myvideo", "savevideo", "保存视频");
    wx.showLoading({
      title: '保存中',
      mask: true
    })
    wx.downloadFile({
      url: this.data.videoList[index].mp4,
      filePath : wx.env.USER_DATA_PATH + "/" + new Date().getTime().toString() + '.mp4',
      success(res){
        console.log(res);
        wx.saveVideoToPhotosAlbum({
          filePath: res.filePath,
          success(){
            wx.hideLoading();
            wx.showToast({
              title: '已保存，请到手机相册查看！',
              icon: 'none',
              duration: 1500
            })
          },
          fail(err){
            console.log('err',err);
            wx.hideLoading();
            wx.showToast({
              title: '保存失败，请重试！',
              icon: 'none',
              duration: 1500
            })
          }
        })
      }
    })
  }
})