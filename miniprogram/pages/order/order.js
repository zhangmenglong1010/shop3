let request = require('../../utils/scripts/request.js');
const app = getApp()
// 引入插件安装器
import plugin from '../../components/v2/plugins/index'
// 禁用/启用可选状态
import selectable from '../../components/v2/plugins/selectable'
// 开始安装，支持链式调用
plugin
  .use(selectable);
// miniprogram/pages/order/order.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    host : '上海 · 正大广场9F',
    last : '10.01-11.26',
    calendarConfig : {
      disableMode: {
        // 禁用某一天之前/之后的所有日期
        type: 'before', 
      },
      highlightToday: false,
    },
    popShow : true,
    popCancle : false,
    type : 1,
    dateInfo : {},
    inited : false,
    ordered : false,
  },
  checkOrder(){
    request.requestCloud({
      url : '/api/user/reserve/list',
      method : 'get'
    }).then(res => {
      if(res.data.some(item => !item.isExpired)){
        if(!this.data.ordered){
          this.setData({ordered:true,popShow:false})
          !app.globalData.commInfo.ordered && wx.navigateTo({
            url: '/pages/order/ordered/ordered',
          })
        }
      }else{
        this.setData({ordered:false,popShow:true})
      }
    });
  },
  onShow: function (query) {
    this.checkOrder();
    if (typeof this.getTabBar === 'function' &&
      this.getTabBar()) {
      this.getTabBar().setData({
        selected: 3
      })
    }
    request.requestCloud({
      url : '/api/system/mnp/info',
      method : 'get'
    }).then(res => {
      let last = res.data.last,
          host = res.data.host;
      this.setData({
        last : last,
        host : host
      });
    })
    this.data.popCancle && this.setData({popShow:true,popCancle:false});
  },
  afterCalendarRender(){
    let calendar = this.calendar;
    let cur = {year : 2020,month:10};
    let curDay = 1;
    let endDay = new Date(new Date('2020/10/01').getTime() + 59*3600*24*1000);
    let familyDate = [];
    calendar.jump(2020,11,1).then()
    request.requestCloud({
      url : '/api/reserve/list',
      method : 'get',
      data : {
        startDateString : `${cur.year}-${cur.month < 10 ? '0'+cur.month : cur.month}-${curDay < 10 ? '0'+curDay : curDay}`,
        endDateString : `${endDay.getFullYear()}-${endDay.getMonth() < 9 ? '0'+(endDay.getMonth()+1) : endDay.getMonth()+1}-${endDay.getDate() < 10 ? '0'+endDay.getDate() : endDay.getDate()}`
      }
    })
    .then(res => {
      this.setData({
        dateInfo : res.data
      })
      !this.data.inited && !this.data.popShow && this.initCalendar();
      // for(var item in res.data){
      //   if(res.data[item].canReserve && res.data[item].actType.some(item => item.id == 2)){
      //     let dateTime = item.split('-');
      //     familyDate.push({
      //       year : dateTime[0],
      //       month : dateTime[1],
      //       day : dateTime[2],
      //       class: 'family-date'
      //     });
      //   }
      // }
      // familyDate.length && calendar.setDateStyle(familyDate);
    })
  },
  /**
   * 选择日期后执行的事件
   * currentSelect 当前点击的日期
   * allSelectedDays 选择的所有日期（当multi为true时，allSelectedDays有值）
   */
  afterTapDay(e) {
    let day = `${e.detail.year}-${e.detail.month < 10 ? '0'+e.detail.month : e.detail.month}-${e.detail.day < 10 ? '0' + e.detail.day : e.detail.day}`;
    if(!this.data.ordered){
      wx.PTTSendClick("btn","orderday","点击预约");
      wx.navigateTo({
        url: `/pages/order/time/time?date=${day}&type=${this.data.type}`,
      })
    } else{
      wx.showToast({
        title: '已预约，请点击我的预约查看！',
        icon: 'none',
        duration: 1500
      })
    }
  },
  myOrder(){
    wx.PTTSendClick('btn','myorder','我的预约');
  },
  handleType(e){
    let type = e.currentTarget.dataset.type;
    this.setData({type});
  },
  handleDialog(e){
    let type = this.data.type;
    wx.PTTSendClick("btn","tickettype"+type,"购票类型"+type);
    this.initCalendar();
    this.setData({
      popShow : false
    })
  },
  initCalendar(){
    let dateList = this.data.dateInfo,
        activeDate = [],
        type = parseInt(this.data.type),
        calendar = this.calendar;
    for(var item in dateList){
      if(dateList[item].canReserve && dateList[item].supportTicket.some(item => item.id == type)){
        activeDate.push(item);
      }
    }
    if(calendar && activeDate.length){
      calendar.enableDays(activeDate);
      this.setData({inited : true});
    }
  },
  onShareAppMessage(){
    wx.PTTSendClick("order","share","预约页面分享");
    return {
      title : "Vstation电竞体验馆",
      path: '/pages/index/index?source=share',
      imageUrl : 'https://game.gtimg.cn/images/game/act/a20201001vstation/share.png'
    }
  },
})