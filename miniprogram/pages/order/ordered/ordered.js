let request = require('../../../utils/scripts/request.js');
// miniprogram/pages/order/ordered/ordered.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    reserveInfo : [],
    time : {
      month : '',
      day : '',
      hour : '',
      min : ''
    },
    last : '',
    host : '上海 · 正大广场9F',
  },
  onShow: function () {
    wx.showLoading({});
    request.requestCloud({
      url : '/api/user/reserve/list',
      method : 'get'
    }).then(res => {
      this.setData({
        reserveInfo : res.data.filter(item => !item.isExpired)
      });
      wx.hideLoading({});
      if(res.data.length == 0){
        wx.switchTab({
          url: '/pages/order/order?cancle=true',
        })
      }
    });
    request.requestCloud({
      url : '/api/system/mnp/info',
      method : 'get'
    }).then(res => {
      let last = res.data.last,
          host = res.data.host;
      this.setData({
        last : last,
        host : host
      });
    })
    this.timeRefresh();
  },
  timeRefresh(){
    let that = this;
    let time = new Date();
    let hour = time.getHours() < 10 ? '0' + time.getHours() : time.getHours(),
        min = time.getMinutes() < 10 ? '0' + time.getMinutes() : time.getMinutes(),
        sec = time.getSeconds() < 10 ? '0' + time.getSeconds() : time.getSeconds();

    this.setData({
      time : {
        hour:hour.toString(),
        min:min.toString(),
        sec:sec.toString()
      }
    });
    setTimeout(() => {
      that.timeRefresh();  
    }, 10);
  },
  cancleReserve(e){
    request.requestCloud({
      url : '/api/user/reserve/remove',
      data : {
        reserveRecord_id : e.currentTarget.dataset.id
      }
    }).then(res => {
      wx.PTTSendClick("ordered","cancle","取消预约");
      wx.navigateTo({
        url: '/pages/order/order',
      })
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage(){
    wx.PTTSendClick("orderd","share","我的预约分享");
    return {
      title : "Vstation电竞体验馆",
      path: '/pages/index/index?source=share',
      imageUrl : 'https://game.gtimg.cn/images/game/act/a20201001vstation/share.png'
    }
  }
})