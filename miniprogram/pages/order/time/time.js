let request = require('../../../utils/scripts/request.js');
const app = getApp()
// miniprogram/pages/order/time/time.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    popShow : false,
    timeInfo : [],
    date : '2020.10.01',
    type : '',
    day : '',
    timeId : '',
    familyDay : false,
    actType : 0,
    phoneNumber : '',
    // 检测身份证格式正确
    idInfo : {
      idSelf : true,
      member1 : true,
      member2 : true,
    },
    nameInfo : {
      nameSelf : true,
      member1 : true,
      member2 : true,
    },
    user : {
      selfName : '',
      selfId : '',
      member1Name : '',
      member1Id : '',
      member2Name : '',
      member2Id : ''
    },
    ordered : false,
    admin : false,
    adminShow : false,
    adminItem : null,
    maxCount : 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (query) {
    let day = query.date,
        type = query.type,
        actType = query.act;
    day && type && this.setData({day,type,actType});
    this.setData({date : day.replace(/-/g,'.')});
    this.initDate();
  },
  initDate(){
    request.requestCloud({
      url : '/api/reserve/detail',
      method : 'get',
      data : {
        dateString : this.data.day
      }
    }).then(res => {
      this.setData({timeInfo : res.data})
    })
  },
  onShow(){
    this.checkOrder();
    this.checkAdmin();
  },
  checkOrder(){
    request.requestCloud({
      url : '/api/user/reserve/list',
      method : 'get'
    }).then(res => {
      if(res.data.some(item => !item.isExpired)){
        this.setData({ordered:true})
      }else{
        this.setData({ordered:false})
      }
    });
  },
  checkAdmin(){
    let uid = '';
    request.requestCloud({
      url : '/api/user/detail',
      method : 'get',
    }).then(res => {
      uid = res.data.openid;
      return  request.requestCloud({
        url : '/api/system/mnp/info',
        method : 'get'
      })
    }).then(res => {
      res.data.whitelist.indexOf(uid) >= 0 && this.setData({admin:true})
    })

  },

  submitForm(e){
    var validate = false;
    // var family = false;
    var validate = this.data.user.selfName && this.data.idInfo.idSelf && this.data.user.selfId;
    // if(!this.data.familyDay){
    //   // 非家庭日，检测本人姓名与身份证
    //   validate = self;
    // }
    // else{
    //   if(this.data.user.member1Name || this.data.user.member1Id){
    //     family = this.data.user.member1Name && this.data.user.member1Id && this.data.idInfo.member1;
    //   }
    //   if(this.data.user.member2Name || this.data.user.member2Id){
    //     family = this.data.user.member2Name && this.data.user.member2Id && this.data.idInfo.member2;
    //   }
    //   validate = self && family;
    // }
    if(!validate){
      this.setData({
        ['nameInfo.nameSelf'] : !!this.data.user.selfName,
        ['idInfo.idSelf'] : this.data.idInfo.idSelf && this.data.user.selfId,
      })
      return
    }
    let reserveData = {
      reserve_id : this.data.timeId,
      ticketTypeId : this.data.type,
      idcard : this.data.user.selfId,
      name : this.data.user.selfName
    };
    // if(family){
    //   reserveData.member = [];
    //   this.data.user.member1Name && reserveData.member.push({idcard:this.data.user.member1Id,name:this.data.user.member1Name});
    //   this.data.user.member2Name && reserveData.member.push({idcard:this.data.user.member2Id,name:this.data.user.member2Name})
    // }
    if(this.data.phoneNumber){
      reserveData.phoneNumber = this.data.phoneNumber;
    }
    wx.PTTSendClick("time","reserve","新建预约");
    request.requestCloud({
      url : '/api/user/reserve/add',
      method : 'post',
      data : reserveData
    }).then(res => {
      this.setData({popShow:false})
      app.globalData.commInfo.ordered = true;
      wx.navigateTo({
        url: '/pages/order/ordered/ordered',
      })
    }).catch(err => {
      this.setData({
        ['idInfo.idSelf'] : false
      })
    })
  },
  handleOrder(e){
    let timeId = e.currentTarget.dataset.time,
        timeIndex = e.currentTarget.dataset.index;
    // let familyDay = this.data.timeInfo[timeIndex].actType.some(item => item.id == 2);
    if(this.data.ordered){
      wx.showToast({
        title: '已预约，请点击我的预约查看！',
        icon: 'none',
        duration: 1500
      })
    }else if(!this.data.timeInfo[timeIndex].canReserve){
      wx.showToast({
        title: '预约已满，请选择其他时间段预约！',
        icon: 'none',
        duration: 1500
      })
    }else {
      this.setData({timeId,popShow:true});
      wx.PTTSendClick("time","openpop","打开弹层");
    }
  },
  handleAdmin(e){
    let timeId = e.currentTarget.dataset.time,
        timeIndex = e.currentTarget.dataset.index;
    if(timeId){
      this.setData({
        adminItem : this.data.timeInfo[timeIndex],
        maxCount : this.data.timeInfo[timeIndex].maxCount
      });
    }
    this.setData({
      adminShow : !this.data.adminShow
    })
  },
  checkMaxCount(e){
    let maxCount = parseInt(e.detail.value);
    this.setData({
      maxCount
    })
  },
  submitAdmin(){
    if(this.data.maxCount != this.data.adminItem.maxCount){
      request.requestCloud({
        url : "/api/whitelist/reserve/maxCount/update",
        method : 'post',
        data : {
          reserve_id : this.data.adminItem._id,
          maxCount : this.data.maxCount
        }
      }).then(res => {
        this.setData({
          adminShow : !this.data.adminShow
        });
        this.initDate();
      })
    }
    
  },
  checkIdcard(e){
    let value = e.detail.value,
        type = e.currentTarget.dataset.type;
    let checked = false;
    if(value.length >= 15 && value.length <= 18){
      checked = true;
    }
    switch(type){
      case 'idcard' : 
        this.setData({
          ['idInfo.idSelf'] : checked,
          ['user.selfId'] : value
        });
        break;
      case 'idcard1' :
        this.setData({
          ['idInfo.member1'] : checked,
          ['user.member1Id'] : value
        });
        break;
      case 'idcard2' : 
        this.setData({
          ['idInfo.member2'] : checked,
          ['user.member2Id'] : value
        }); 
        break;
      default :
        break;
    }
  },
  checkName(e){
    let value = e.detail.value,
        type = e.currentTarget.dataset.type;
    let checked = false;
    if(value.length > 0){
      checked = true;
    }
    switch(type){
      case 'name' : 
        this.setData({
          ['nameInfo.nameSelf'] : checked,
          ['user.selfName'] : value
        });
        break;
      case 'name1' :
        this.setData({
          ['nameInfo.member1'] : checked,
          ['user.member1Name'] : value
        });
        break;
      case 'name2' : 
        this.setData({
          ['nameInfo.member2'] : checked,
          ['user.member2Name'] : value
        }); 
        break;
      default :
        break;
    }
  },
  closeForm(){
    this.setData({popShow:false});
    wx.PTTSendClick("time","closepop","关闭弹层");
  },
  checkPhone(e){
    this.setData({phoneNumber : e.detail.value});
  },
  getPhoneNumber(e){
    let that = this;
    if(e.detail.errMsg == 'getPhoneNumber:ok'){
      wx.PTTSendClick("btn","phone","获取手机");
      wx.login({
        success(res){
          request.requestCloud({
            url : '/api/user/phone',
            method : 'get',
            data : {
              cloudID : e.detail.cloudID
            }
          }).then(res => {
            that.setData({
              phoneNumber : res.data.list[0].data.phoneNumber
            })
          })
        }
      })
    }else{
      wx.PTTSendClick("btn","unphone","不获取手机");
    }
    this.handleOrder(e);
  },
  onShareAppMessage(){
    wx.PTTSendClick("time","share","预约详细页分享");
    return {
      title : "Vstation电竞体验馆",
      path: '/pages/index/index?source=share',
      imageUrl : 'https://game.gtimg.cn/images/game/act/a20201001vstation/share.png'
    }
  },
})