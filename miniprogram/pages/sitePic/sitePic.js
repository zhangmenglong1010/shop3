// miniprogram/pages/site/site.js
let request = require("../../utils/scripts/request.js");

Page({
  /**
   * 页面的初始数据
   */
  data: {
    supportList: [],
    rankList: [],
    loading: false,
    picList: [],
    picTimestamp : new Date().getTime(),
    more : true,
    listHeight : [0,0]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onShow(options) {
    this.init(options);
  },
  getPicList(){
    request.requestCloud({
      url : '/api/picture/list',
      method : 'get',
      data : {
        size : 20,
        lastRequestTime : this.data.picTimestamp
      }
    }).then(res => {
      let dataLength = res.data.list.length,
          list = res.data.list,
          picList = [];
      dataLength > 0 && this.setData({picTimestamp:res.data.list[dataLength -1].createTime});
      dataLength < 20 && this.setData({more:false});
      list.forEach(item => {
        picList.push({
          pic : item.mediaInfo.fileUrl,
          tit : item.content,
          avatar : item.userInfo.avatar,
          name : item.userInfo.name,
          like : item.likeCount,
          liked : item.isLiked,
          width : item.mediaInfo.width,
          height : item.mediaInfo.height,
          id : item._id
        });
      })
      this.setPicList(picList);
    })
  },
  init: function (options) {
    this.setData({loading: true});
    this.getPicList();
  },
  showPic: function (e) {
    let picList = this.data.picList;
    wx.PTTSendClick("sitepic", "preview", "照片预览");
    wx.previewImage({
      urls: [picList[e.currentTarget.dataset.index].pic],
    });
  },
  setPicList: function (data) {
    //向页面写入图片
    if(data.length){
      let listHeight = this.data.listHeight;
      for (let i = 0; i < data.length; i++) {
        let item = data[i];
        let h = (317 / item.width) * item.height;
        item["picH"] = h;
        if (listHeight[0] <= listHeight[1]) {
          item["float"] = "left";
          item["top"] = listHeight[0];
          listHeight[0] += h + 16;
        } else {
          item["float"] = "right";
          item["top"] = listHeight[1];
          listHeight[1] += h + 16;
        }
      }
      this.setData({
        picList: this.data.picList.concat(data),
        listHeight:listHeight
      });
    }
  },
  loadingList: function () {
    this.data.more && this.getPicList();
  },
  handleLike(e){
    let that = this;
    let data = {
      picture_id: e.currentTarget.dataset.id,
      isThumbUp: e.currentTarget.dataset.liked ? -1 : 1,
      index : e.currentTarget.dataset.index
    };
    if(this.data.picList[data.index].loading) {
      return
    }
    wx.PTTSendClick("sitepic", "like", "点赞照片");
    this.setData({ [`picList[${data.index}].loading`] : true});
    // 小程序端调用
    wx.cloud.callFunction({
      // 需调用的云函数名
      name: 'morphy_userAlbum',
      // 传给云函数的参数
      data: {
          '$url':'client/setLike',
          '_id': data.picture_id,   // 需要设置点赞状态的相册id
          'status': data.isThumbUp   // 设置的点赞状态，-1为取消点赞，1为点赞
      },
      // 成功回调
      complete(res){
        let thumbItem = {},
            likeNum = that.data.picList[data.index].like;
        thumbItem = that.data.picList[data.index];
        thumbItem.liked = data.isThumbUp == 1 ? true : false;
        thumbItem.like = data.isThumbUp == 1 ? likeNum + 1 : likeNum - 1;
        thumbItem.loading = false;
        if (thumbItem.id) {
          that.setData({
            [`picList[${data.index}]`]: thumbItem,
          });
        }

      }
    })
  },
  openPic() {
    const child = this.selectComponent(".pic_dialog");
    wx.chooseImage({
      count: 1,
      sizeType: ["original", "compressed"],
      sourceType: ["album", "camera"],
      success(res) {
        // tempFilePath可以作为img标签的src属性显示图片
        wx.PTTSendClick("btns", "picdalog", "照片弹层");
        child.triggerDialog({ src: res.tempFilePaths[0]});
      },
    });
  },
  picAdd(res) {
    this.setPicList([res.detail]);
  },
  onShareAppMessage() {
    wx.PTTSendClick("sitepic", "share", "分享");
    return {
      title: "Vstation电竞体验馆",
      path: '/pages/index/index?source=share',
      imageUrl: 'https://game.gtimg.cn/images/game/act/a20201001vstation/share.png'
    }
  },
});
