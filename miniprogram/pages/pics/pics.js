// miniprogram/pages/pages/pics.js
let request = require('../../utils/scripts/request.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    picList : []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onShow: function (options) {
    request.requestCloud({url:'/api/picture/list',method:'get'})
      .then(res => {
        this.setData({picList : res.data.list})
      })
  },
  picThumb(e){
    let data = {
      picture_id : e.target.dataset.id,
      isThumbUp : e.target.dataset.thumbed ? 0 : 1
    }
    request.requestCloud({
      url : '/api/picture/thumb',
      data
    })
    .then(res => {
      let thumbItem  = {},index = 0;
      this.data.picList.forEach((item,i) => {
        if(item._id == res.data.picture_id){
          thumbItem = item,index = i;
          thumbItem.thumbCount = res.data.thumbCount;
          thumbItem.hasIThumbed = data.isThumbUp ? true : false;
        }
      })
      if(thumbItem._id){
        this.setData({
          [`picList[${index}]`] : thumbItem,
        });
      }
    })
  },
  onShareAppMessage() {
    wx.PTTSendClick("pics", "share", "分享");
    return {
      title: "Vstation电竞体验馆",
      path: '/pages/index/index?source=share',
      imageUrl: 'https://game.gtimg.cn/images/game/act/a20201001vstation/share.png'
    }
  },
})