// miniprogram/pages/rank.js
let request = require('../../utils/scripts/request.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    rankList : []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onShow: function (options) {

    request.requestCloud({url:'/api/rank/list',method:'get'})
      .then(res => {
        this.setData({rankList : res.data})
      })
  },
  onShareAppMessage() {
    wx.PTTSendClick("rank", "share", "分享");
    return {
      title: "Vstation电竞体验馆",
      path: '/pages/index/index?source=share',
      imageUrl: 'https://game.gtimg.cn/images/game/act/a20201001vstation/share.png'
    }
  },
})