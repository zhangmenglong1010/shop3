// miniprogram/pages/calendar/calendar.js
let request = require('../../utils/scripts/request.js');
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bgSrc : '',
    sharePic : true,
    showUserInfo : false,
    userName : '',
    avatar : '',
    date : [],
    increment : 1,
    loading : false,
    hostTime : '',
  },
  onShow(){
    if (typeof this.getTabBar === 'function' &&
      this.getTabBar()) {
      this.getTabBar().setData({
        selected: 2
      })
    }
    let curDay = new Date();
    let monthStr = ['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SET','OCT','NOV','DEC'];
    let bgUrl = 'https://game.gtimg.cn/images/game/act/a20201001vstation/calendar/'+monthStr[curDay.getMonth()] +'_'+curDay.getDate()+'.jpg';
    this.setData({
      bgSrc : bgUrl,
      date : [curDay.getFullYear(),curDay.getMonth()+1,curDay.getDate()]
    });
    let userInfo = request.requestCloud({
          url : '/api/user/detail',
          method : 'get',
        }),
        globalData = request.requestCloud({
          url : '/api/system/mnp/info',
          method : 'get'
        })
    Promise.all([userInfo,globalData]).then(res => {
      let user = res[0].data,adminData = res[1].data;
      if(!user.isRequireWxInfo){
        this.setData({userName:user.name,avatar:user.avatar,increment:user.incCount || user.increment});
      }else{
        wx.canIUse('button.open-type.getUserInfo') && this.setData({showUserInfo : true});
      }
      this.setData({
        hostTime : adminData.host || '上海 · 正大广场9F',
      })
    })
  },
  bindgetUserInfo(res){
    this.setData({userName:res.detail.userInfo.nickName,avatar:res.detail.userInfo.avatarUrl});
    let detail = res.detail.userInfo;
    // 同步用户基础数据
    // | name | string | √ | - | 昵称 |
    // | avatar | string | √ | - | 头像 |
    // | gender | int | √ | - | 性别 |
    // | city | string | - | - | 城市 |
    // | country | string | - | - | 国家 |
    // | province | string | - | - | 省份 |
    // | language | string | - | - | 语言 |
    let userInfo = {
      name: detail.nickName,
      avatar: detail.avatarUrl,
      gender: detail.gender,
      city: detail.city,
      country: detail.country,
      province: detail.province,
      language: detail.language,
      cloudID : res.detail.cloudID
    }
    request.requestCloud({
      url: '/api/user/update',
      data: userInfo
    }).then(res => {
      this.setData({
        showUserInfo: false,
        increment:res.data.incCount || res.data.increment
      })
      this.saveCalendar();
    })
  },
  onShareAppMessage(){
    wx.PTTSendClick("calendarbtns","share","分享");
    return {
      title : "Vstation电竞体验馆",
      path: '/pages/index/index?source=share',
      imageUrl : this.data.bgSrc
    }
  },
  shareBtn(){
  },
  saveCalendar(){
    if(this.data.loading) return;
    this.setData({loading:true});
    wx.showLoading({
      title: '保存中，请稍后',
      mask:true
    })
    wx.PTTSendClick("calendarbtns","save","保存图片");
    let that = this;
    const ctx = wx.createCanvasContext('shareCanvas');
    let calendar = new Promise((resolve,reject) => {
      wx.getImageInfo({
        src : that.data.bgSrc,
        success(res){
          var file = res.path;
          resolve({res:res,file:file})
        }
      })
    });
    let background = new Promise((resolve,reject) => {
      wx.getImageInfo({
        src : 'https://game.gtimg.cn/images/game/act/a20201001vstation/calendar/calendar_save.jpg',
        success(res){
          var file = res.path;
          resolve({res:res,file:file});
        }
      })
    })
    let avatar = new Promise((resolve,reject) => {
      wx.getImageInfo({
        src : this.data.avatar,
        success(res){
          var file = res.path;
          resolve({res : res,file:file});
        }
      })
    })
    let wxcode = new Promise((resolve,reject) => {
      wx.getImageInfo({
        src : 'https://game.gtimg.cn/images/game/act/a20201001vstation/calendar/wxacode_default.jpeg',
        success(res){
          var file = res.path;
          resolve({res:res,file:file});
        }
      })
    })
    Promise.all([calendar,background,avatar,wxcode]).then(res => {
      let coverPath = res[0].file,
          bgPath = res[1].file,
          avatarPath = res[2].file,
          wxcodePath = res[3].file;

      let curDate = this.data.date.join('/');
      ctx.drawImage(bgPath,0,0,750,1334);
      ctx.save();
      ctx.rect(0,0,750,745);      
      ctx.clip();
      ctx.drawImage(coverPath,-1,-7,753,753);
      ctx.restore();
      ctx.save();
      ctx.beginPath();
      var x = 306,y = 876, w = 156, h = 156;
      ctx.arc(x + w /2, y + h/2, w/2, 0, 2 * Math.PI);
      ctx.clip();
      ctx.drawImage(avatarPath,x,y,w,h);
      ctx.restore();
      ctx.save();
      ctx.beginPath();
      x = 590,y = 1150, w = 136, h = 136;
      ctx.arc(x + w /2, y + h/2, w/2, 0, 2 * Math.PI);
      ctx.clip();
      ctx.drawImage(wxcodePath,x,y,w,h);
      ctx.restore();
      ctx.setFillStyle('#FFCE00');
      ctx.setTextAlign('left');
      ctx.setTextBaseline('toop');
      ctx.font = "28px tencentW7";
      ctx.fillText(this.data.hostTime,200,1313);
      ctx.fillText(`第${this.data.increment}人`,24,1250);
      ctx.file
      ctx.font = "16px tencentW7";
      ctx.fillText(curDate,632,800);
      ctx.font = "36px 苹方";
      ctx.setTextAlign('center');
      ctx.setFillStyle('#FFFFFF');
      ctx.fillText(this.data.userName,375,1120);
      ctx.draw(false,function(){
        wx.canvasToTempFilePath({
          canvasId: "shareCanvas",
          x:0,
          y:0,
          width:750,
          height:1334,
          destWidth:750,
          destHeight:1334,
          fileType:'jpg',
          quality:1,
          success(res){
            console.log('tem',res.tempFilePath);
            wx.saveImageToPhotosAlbum({
              filePath : res.tempFilePath,
              success(res) {
                wx.hideLoading();
                wx.showToast({
                  title: '已保存，请到手机相册查看！',
                  icon: 'none',
                  duration: 1500
                })
                setTimeout(function(){
                  that.setData({loading:false});
                },1500);
                wx.PTTSendClick("calendarbtns","savesuccess","保存成功");
              },
              fail(res){
                that.setData({loading:false});
                wx.hideLoading();
                wx.showToast({
                  title: '保存失败，请重新尝试！',
                  icon: 'none',
                  duration: 1500
                })
              }
            })
          }
        })
      })
    }).catch(error => {
      that.setData({loading:false});
      wx.hideLoading();
      wx.showToast({
        title: '信息获取失败，请重新尝试！',
        icon: 'none',
        duration: 1500
      })
    })
  }
})