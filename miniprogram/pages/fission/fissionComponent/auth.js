/** API **/
var GLOBAL = {
  shareData: {
    title: "超火的V-Station电竞身价鉴定，你也来测测呗~",
    path: "/pages/index/index?ADTAG=share.fission&source=share",
    imageUrl: "https://game.gtimg.cn/images/game/act/a20200824fission/share_app.jpg"
  },
  toDamai: function (num) {
    const path = ["/pages/detail/item?projectId=626042467221&utm=NtQRNZ",'/pages/webview/index?isRedirect=true&url=https://m.damai.cn/damai/mine/coupon/index.html'];
    num = num?num:0;
    wx.navigateToMiniProgram({
      appId: 'wx938b41d0d7e8def0',
      path: path[num]
    });
  },
  toH5: function (e) {
    const path = e.currentTarget.dataset.webpath;
    wx.navigateTo({
      url: '/pages/fission/webView/webView?webPath=' + path,
    })
  },
  navigatorTo: function (e) {
    if(e){
      wx.navigateTo({
        url:e.target.dataset.url,
      })
    }else{
      wx.switchTab({
        url:"/pages/index/index"
      })
    }
  },
  bindGetUserInfo: function (e) {
    return new Promise(function (resolve, reject) {
      var userInfo = wx.getStorageSync('userInfo');
      if (!userInfo) {
        if (!e.detail.userInfo) {
          wx.showToast({
            title: '请同意获取信息',
            image:"./../fissionComponent/warn.png"
          })
          return false;
        }
        userInfo = e.detail.userInfo;
        console.log("userInfo:", e);
        wx.cloud.callFunction({
          name: "getunit",
          data: {
            weRunData: wx.cloud.CloudID(e.detail.cloudID)
          },
          success: function (res) {
            userInfo.unionid = res.result.event.weRunData.data.unionId;
            wx.setStorageSync('userInfo', userInfo);
            console.log("userInfo+unionid:::", userInfo)
            resolve(userInfo)
          }, fail: function (res) { reject(res) }
        })
      } else {
        resolve(userInfo);
      }
    })
  },
  //统计裂变深度的参数
  unitshareDepth: function (options) {
    var sharedepth = wx.getStorageSync('sharedepth') || 1;
    var urlDepth = options.sharedepth || 1;
    var value = Math.max(sharedepth, urlDepth);
    wx.setStorageSync('sharedepth', value);
    return "sharedepth=" + (value + 1);
  }
}
//定义axios
function axios(obj) {
  const _config = {
    ...obj,
    url: obj.baseURL + obj.url,
  }
  const { baseURL, ...config } = _config;
  return new Promise(function (resolve, reject) {
    wx.request({
      ...config,
      success(res) {
        resolve(res)
      },
      fail(res) {
        reject(res)
      }
    })

  })
}
var API = {
  //axios
  config: {
    baseURL: 'https://service-7tuvtj46-1300342644.gz.apigw.tencentcs.com',
    header: { 'Authorization': wx.getStorageSync('birdToken') }
  },
  //创建会话
  createSession: function (obj) {
    return new Promise(function (resolve, reject) {
      var _data = {
        nickname: obj.nickName,
        avatar_url: obj.avatarUrl,
        unionid: obj.unionid,
        channel: 'wx'
      }
      var config = {
        method: "post",
        url: "/api/session/create",
        data: _data,
        ...API.config,
      }
      axios(config).then(function (res) {
        console.log("createSession:", res)
        if (res.data.code === 0) {
          wx.setStorageSync('birdToken', res.data.data.token)
          API.config.header = { 'Authorization': res.data.data.token };
          resolve(res)
        }
      }).catch(function (res) {
        reject(res);
      })
    })
  },
  //获取会话
  getSession: function (obj) {
    var token = wx.getStorageSync('birdToken');
    if (token) {
      return new Promise(function (resolve, reject) {
        var config = {
          method: "get",
          url: "/api/session",
          ...API.config,
        }
        axios(config).then(function (res) {
          if (res.data.code === 0) {
            resolve(res)
          } else {
            return new Promise(function () {
              API.createSession(obj).then(function (res) {
                resolve(res);
              }).catch(function (res) {
                reject(res);
              })
            })
          }
        }).catch(function (res) {
          reject(res);
        })
      })
    } else {
      return new Promise(function (resolve, reject) {
        API.createSession(obj).then(function (res) {
          resolve(res);
        }).catch(function (res) {
          reject(res);
        })
      })
    }
  },

  //计算电竞潜质得分
  postScore: function (obj) {
    return new Promise(function (resolve, reject) {
      var _data = {
        ...obj
      }
      var config = {
        method: "post",
        url: "/api/user/esp/calculate",
        data: _data,
        ...API.config,
      }
      axios(config).then(function (res) {
        resolve(res)
      }).catch(function (res) {
        reject(res);
      })
    })
  },

  //获取电竞潜质得分
  getScore: function () {
    return new Promise(function (resolve, reject) {
      var config = {
        method: "get",
        url: "/api/user/esp",
        ...API.config,
      }
      axios(config).then(function (res) {
        resolve(res)
      }).catch(function (res) {
        reject(res);
      })
    })
  },

  //领取游戏代金券,10101：用户未玩游戏;10102：用户已领取游戏代金券
  getGameVoucher: function () {
    return new Promise(function (resolve, reject) {
      var config = {
        method: "post",
        url: "/api/user/esp/exchange",
        ...API.config,
      }
      axios(config).then(function (res) {
        resolve(res)
      }).catch(function (res) {
        reject(res);
      })
    })
  },

  //获取电竞潜质榜单
  getRank: function (obj) {
    return new Promise(function (resolve, reject) {
      var params = [];
      for (key in obj) {
        params.push(key + "=" + encodeURIComponent(obj[key]));
      }
      params = params.length > 0 ? "?" + params.join("&") : "";
      var config = {
        method: "get",
        url: "/api/esp/ranking" + params,
        ...API.config,
      }
      axios(config).then(function (res) {
        resolve(res)
      }).catch(function (res) {
        reject(res);
      })
    })
  },

  //我的代金券,返回代金券的数组;test 玩测试所领取;group 组队成功所得;give 朋友赠送;bonus 用户进入系统静默授予的，不能用于兑换优惠码，只能赠送给好友
  getVouchers: function () {
    return new Promise(function (resolve, reject) {
      var config = {
        method: "get",
        url: "/api/user/voucher/list",
        ...API.config,
      }
      axios(config).then(function (res) {
        resolve(res)
      }).catch(function (res) {
        reject(res);
      })
    })
  },

  //代金券生成大麦网的优惠码
  createCoupon: function () {
    return new Promise(function (resolve, reject) {
      var config = {
        method: "post",
        url: "/api/user/voucher/exchange",
        ...API.config,
      }
      axios(config).then(function (res) {
        resolve(res)
      }).catch(function (res) {
        reject(res);
      })
    })
  },
  //根据大麦网的优惠码获取信息
  getInfoFromCode: function () {
    return new Promise(function (resolve, reject) {
      var config = {
        method: "get",
        url: "/api/user/coupon",
        ...API.config,
      }
      axios(config).then(function (res) {
        resolve(res)
      }).catch(function (res) {
        reject(res);
      })
    })
  },

  //赠送代金券;10105：代金券不属于该用户;10108：赠送券不能赠送;10106：已被生成优惠码；返回的code用以附带在分享链接中
  shareCoupon: function (obj) {
    return new Promise(function (resolve, reject) {
      var _data = {
        voucher_id: obj.voucher_id
      }
      var config = {
        method: "post",
        url: "/api/user/voucher/gift/pick",
        data: _data,
        ...API.config,
      }
      axios(config).then(function (res) {
        resolve(res)
      }).catch(function (res) {
        reject(res);
      })
    })
  },

  //根据赠送码获取赠送券信息
  getCouponInfo: function (obj) {
    return new Promise(function (resolve, reject) {
      var params = obj.code ? '?code=' + obj.code : "";
      var config = {
        method: "get",
        url: "/api/voucher/gift" + params,
        ...API.config,
      }
      axios(config).then(function (res) {
        resolve(res)
      }).catch(function (res) {
        reject(res);
      })
    })
  },

  //领取赠送券;10107：赠送券已被领取；10109：不能领自己赠送券；10110：只能领取一张赠送券
  receiveCoupon: function (obj) {
    return new Promise(function (resolve, reject) {
      var _data = {
        code: obj.code
      }
      var config = {
        method: "post",
        url: "/api/user/voucher/gift/accept",
        data: _data,
        ...API.config,
      }
      axios(config).then(function (res) {
        resolve(res)
      }).catch(function (res) {
        reject(res);
      })
    })
  },

  //获取自己所在队伍
  getMyTeam: function () {
    return new Promise(function (resolve, reject) {
      var config = {
        method: "get",
        url: "/api/user/group",
        ...API.config,
      }
      axios(config).then(function (res) {
        resolve(res)
      }).catch(function (res) {
        reject(res);
      })
    })
  },

  //根据ID获取队伍信息
  getTeam: function (obj) {
    return new Promise(function (resolve, reject) {
      var params = obj.group_id ? '?group_id=' + obj.group_id : "";
      var config = {
        method: "get",
        url: "/api/group" + params,
        ...API.config,
      }
      axios(config).then(function (res) {
        resolve(res)
      }).catch(function (res) {
        reject(res);
      })
    })
  },

  //组建队伍；10111：已创建队伍
  createTeam: function () {
    return new Promise(function (resolve, reject) {
      var config = {
        method: "post",
        url: "/api/group/build",
        ...API.config,
      }
      axios(config).then(function (res) {
        resolve(res)
      }).catch(function (res) {
        reject(res);
      })
    })
  },

  //加入队伍；10112：队伍已满员
  joinTeam: function (obj) {
    return new Promise(function (resolve, reject) {
      var _data = {
        group_id: obj.group_id
      }
      var config = {
        method: "post",
        url: "/api/group/join",
        data: _data,
        ...API.config,
      }
      axios(config).then(function (res) {
        resolve(res)
      }).catch(function (res) {
        reject(res);
      })
    })
  },

}

export { API, GLOBAL };