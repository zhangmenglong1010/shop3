//index.js
let request = require('../../../utils/scripts/request.js');
const app = getApp();
// pages/potential/potential.js
import { API, GLOBAL } from './../fissionComponent/auth';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    playing : false,
    videoHeight : '100px',
    bigScreen : false,
  },
  startPlay(){
    let windowWidth = wx.getSystemInfoSync().windowWidth;
    this.setData({
      videoHeight : windowWidth * (1448/750),
      playing : true
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  onShow(){
    wx.getSystemInfo().then(res => {
      res.windowHeight > 700 && this.setData({bigScreen:true})
    })
    let that = this;
    request.requestCloud({
      url : '/api/system/mnp/info',
      method : 'get'
    }).then(res => {
      let hostTime = res.data.host_time,
          host = res.data.host,
          superEarlyOpen = res.data.superearly_ticket_open,
          earlyOpen = res.data.early_ticket_open,
          defaultOpen = res.data.default_ticket_open,
          ticketLink = res.data.ticket_link;
      this.setData({
        hostTime : `${hostTime} ${host}`,
        superEarlyOpen,
        earlyOpen,
        defaultOpen,
        ticketLink,
        superearlayTickerNumber : res.data.superearly_ticket_number || '限量1000张',
        earlyStart : res.data.early_start || '8月24日开售',
        earlyEnd : res.data.early_end || '9月14日截止',
        defaultStart : res.data.default_start || '9月15日开售',
      });
      app.globalData.commInfo.host = host;
    })
  },

  //到大麦网
  toBuyTicket() {
    GLOBAL.toDamai();
  },
  //到web页面:data-webpath="rules.html"
  toH5(e) {
    GLOBAL.toH5(e);
  }
})