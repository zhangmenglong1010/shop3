// pages/potential/potential.js
import { API, GLOBAL } from './../fissionComponent/auth';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isShowDia6: false,
    authWin: false,
    base64: '',
    img: null,
    userInfo: null,
    espCoupon: null,
    bonusCoupon: null,
    shareCode: null,
    sharedepthStr: null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      sharedepthStr: GLOBAL.unitshareDepth(options)
    })
    const userInfo = wx.getStorageSync('userInfo');
    if (userInfo) {
      //如果授权，则渲染数据
      this.userInit(userInfo);
    } else {
      //如果没有授权，则弹窗授权按钮
      this.setData({
        authWin: true,
      })
    }
  },

  //导航到小程序其他页面:data-url="/pages/fission/tickets/tickets"
  navigatorTo(e) {
    GLOBAL.navigatorTo(e);
  },

   //到web页面:data-webpath="rules.html"
   toH5(e) {
    GLOBAL.toH5(e);
  },

  //分享
  onShareAppMessage: function (res) {
    const that = this;
    console.log(res);
    if (res.from == 'button' && res.target.dataset.realtype === 'coupon') {
      wx.PTTSendClick('btn', 'sendcoupon1', '赠券给好友点击');
      return {
        title: "V-Station电竞体验馆开馆了，鉴定身价即可优惠购票，快来测试~",
        path: that.setShareToOther(),
        imageUrl: "https://game.gtimg.cn/images/game/act/a20200824fission/share_app_coupon.jpg"
      }
    } else {
      return {
        ...GLOBAL.shareData
      }
    }
  },
  onShareTimeline: function () {
    wx.PTTSendClick('share', 'timeline', '朋友圈')
    return {
      title: GLOBAL.shareData.title,
      imageUrl: 'https://game.gtimg.cn/images/game/act/a20200824fission/share.jpg'
    }
  },

  //赠券给他人
  setShareToOther() {
    const vm = this.data;
    let shareLink = "";
    const webUrl = encodeURIComponent("details.html?code_number=" + vm.shareCode.code)
    shareLink = "/pages/fission/webView/webView?ADTAG=share.rewards&" + vm.sharedepthStr + "&webPath=" + webUrl;
    return shareLink;
  },

  //获取wx授权,并获取unionid,回调值userInfo
  getUserInfo(e) {
    const that = this;
    GLOBAL.bindGetUserInfo(e).then(function (res) {
      //关闭授权按钮
      that.setData({
        authWin: false,
      })
      that.userInit(res);
    })
  },
  //鸟哥鉴权
  userInit(e) {
    const that = this;
    GLOBAL.bindGetUserInfo(e).then(function (res) {
      API.getSession(res).then(function (res) {
        console.log("bird接口session:", res)
        that.unitPage(res)
      });
    });
  },
  //鸟哥鉴权后的初始化
  unitPage(res) {
    const that = this;
    var canvasParams = {
      price: 0,
      ranking: 0,
      title: "",
      desc: "",
      sub: "",
      avatar: "",
      name: "",
      order: 0
    }
    var _data = res.data.data;
    that.setData({
      userInfo: _data
    })
    if (_data.esp) {
      canvasParams.order = _data.join_ranking || "-";
      canvasParams.price = _data.esp.score || "-";
      canvasParams.avatar = _data.avatar_url || "";
      canvasParams.name = _data.nickname || "-";
      canvasParams = {
        ...canvasParams,
        ...that.scoreToDesc(canvasParams.price)
      }
      API.getScore().then(function (res) {
        console.log("获取电竞排位:", res);
        if (res.data.data) {
          canvasParams.ranking = res.data.data.national_ranking;
          console.log("canvasParams:", canvasParams);
          that.generatePoster(canvasParams);
        }
      })
    }
    that.getVouchers();
  },

  //获取代金券信息
  getVouchers() {
    const that = this;
    API.getVouchers().then(function (res) {
      var _data = res.data && res.data.data;
      if (_data.vouchers.length > 0) {
        for (var item of _data.vouchers) {
          if (item.origin === "test") {
            that.setData({
              espCoupon: item
            })
          } else if (item.origin === "bonus") {
            that.setData({
              bonusCoupon: item
            })
            //用赠送券到后台兑换赠送码
            API.shareCoupon({ voucher_id: item.id }).then(function (res) {
              console.log("兑换回来的赠送码：", res);
              that.setData({
                shareCode: res.data.data
              })
            });
          }
        }
      }
    })
  },

  //从游戏分数得到评价
  scoreToDesc(num) {
    var index = 0;
    var titleArr = [
      ["无影飞爪","最强王者","神级走位","孤独求败族"],
      ["江湖新贵","热血黄金","究极赢家","话少技术好"],
      ["佛系端水","无畏青铜","别太认真","这是我小号"],
      ["实力弱鸡","万年甩锅","送分童子","跪地逃生党"],
      ["手残联萌","黑铁守门","拉胯大神","最强酸菜鱼"],
    ]
    var title = parseInt(Math.random()*100%4);
    var textArr = [
      { title: "#"+titleArr[0][title]+"#", desc: "生活中人狠话不多，赛场上反手炫技666", sub: "来V-Station！电竞身价天秀就是你", charIndex: 0 },
      { title: "#"+titleArr[1][title]+"#", desc: "简直是给你一点涟漪，世界就全部在晃荡", sub: "精神不可貌相，你在V-Station里最浪", charIndex: 1 },
      { title: "#"+titleArr[2][title]+"#", desc: "闲时呼吸正能量，千万别说我们丧", sub: "要做热血男儿，就不能错过V-Station", charIndex: 2 },
      { title: "#"+titleArr[3][title]+"#", desc: "刷身价是一种态度，情绪总在起起起伏伏", sub: "到V-Station才能领悟巨浪的真谛", charIndex: 3 },
      { title: "#"+titleArr[4][title]+"#", desc: "以云为梦风为马，心里无岸只剩嗨~~~", sub: "快去V-Station偷师吧，再晚我也救不了", charIndex: 4 },
    ]
    index = 90 <= num ? 1 : 70 <= num ? 2 : 50 <= num ? 3 : 30 <= num ? 4 : 5;
    return textArr[index - 1];
  },

  closeDia6: function () {
    this.setData({
      isShowDia6: false,
    })
  },

  savePoster() {
    // console.log(this.data.base64)

    const query = wx.createSelectorQuery()
    query.select('#poster-canvas')
      .fields({ node: true, size: true })
      .exec((res) => {
        const canvas = res[0].node
        wx.canvasToTempFilePath({
          canvas: canvas,
          success(res) {
            wx.saveImageToPhotosAlbum({
              filePath: res.tempFilePath,
              success(res) {
                wx.showToast({
                  title: '保存成功',
                  icon: "success"
                })
              }
            })
          },
          fail(err) { console.log(err) },
          complete(res) { console.log(res) }
        }, this)
      })
  },

  //生成海报的obj只需要：price, ranking, title, desc,sub, avatar, name, order
  generatePoster(obj) {
    this.setData({
      isShowDia6: true,
    });
    var price = obj.price, ranking = obj.ranking, title = obj.title, desc = obj.desc, sub = obj.sub, avatar = obj.avatar, name = obj.name, order = obj.order;
    var charIndex = this.scoreToDesc(price).charIndex;
    let canvas = null
    let ctx = null
    return new Promise(resolve => {
      new Promise(resolve => {
        wx.createSelectorQuery().select('#poster-canvas').node().exec((res) => {
          canvas = res[0].node
          canvas.width = 750
          canvas.height = 1334
          ctx = canvas.getContext('2d')
          wx.downloadFile({
            url: 'https://game.gtimg.cn/images/game/act/a20200824fission/poster' + charIndex + '.jpg',
            success(res) {
              if (res.statusCode === 200) {
                const img = canvas.createImage()
                img.onload = function () {
                  resolve(img)
                }
                img.src = res.tempFilePath
              }
            },
            fail(err) {
              console.log(err)
            }
          })
        })
      }).then((bg) => {
        ctx.drawImage(bg, 0, 0, 750, 1334)
        ctx.fillStyle = '#ffce00'
        ctx.textAlign = 'center'
        ctx.font = 'Bold 50px 微软雅黑'
        ctx.fillText('电竞身价', 236, 220)
        ctx.font = 'Bold 160px 微软雅黑'
        ctx.fillText(price, 236, 370)
        const offset1 = ctx.measureText(price).width / 2
        ctx.font = 'Bold 28px 微软雅黑'
        ctx.textAlign = 'left'
        ctx.fillText('元', 240 + offset1, 365)
        ctx.fillStyle = '#aaaaaa'
        ctx.font = 'Bold 26px 微软雅黑'
        ctx.textAlign = 'center'
        ctx.fillText('在全国排名第' + ranking + '名', 236, 420)
        ctx.fillStyle = '#ffce00'
        ctx.font = 'Bold 43px 微软雅黑'
        ctx.fillText(title, 236, 530)
        ctx.font = 'Bold 24px 微软雅黑'
        ctx.fillText(desc, 236, 590)
        ctx.fillText(sub, 236, 630)
        ctx.textAlign = 'right'
        ctx.font = 'italic Bold 20px 微软雅黑'
        const now = new Date()
        ctx.fillText(`${now.getFullYear()}/${now.getMonth() + 1}/${now.getDate()}`, 740, 800)
        ctx.fillStyle = '#ffffff'
        ctx.font = 'Bold 36px 微软雅黑'
        ctx.textAlign = 'center'
        ctx.fillText(name, 380, 1124)
        ctx.fillStyle = '#ffce00'
        ctx.font = 'italic Bold 64px 微软雅黑'
        ctx.textAlign = 'left'
        ctx.fillText(order, 60, 1270)
        const offset2 = ctx.measureText(order).width + 80
        ctx.font = 'Bold 35px 微软雅黑'
        ctx.fillText('第', 25, 1266)
        ctx.fillText('人', offset2, 1266)
        new Promise(resolve => {
          wx.downloadFile({
            url: avatar,
            success(res) {
              if (res.statusCode === 200) {
                const img = canvas.createImage()
                img.onload = function () {
                  resolve(img)
                }
                img.src = res.tempFilePath
              }
            },
            fail(err) {
              console.log(err)
            }
          })
        }).then(avt => {
          ctx.arc(384, 952, 73, 0, Math.PI * 2)
          ctx.clip()
          ctx.drawImage(avt, 309, 877, 150, 150)
          const url = canvas.toDataURL()

          this.setData({
            base64: url
          })
          resolve(url)
        })
      })
    })
  }
})