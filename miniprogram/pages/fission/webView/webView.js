// miniprogram/pages/webView/webView.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    shareData: {
      title : "Vstation电竞体验馆",
      path: '/pages/index/index?source=share',
      imageUrl : 'https://game.gtimg.cn/images/game/act/a20201001vstation/share.png'
    },
    webBase:"https://game.qq.com/act/a20200824fission/",
    webPath:"tickets.html"
  },
  listenMessage: function (e) {
    console.log("listenMessage:", e);
    this.setData({
      shareData:e.detail.data[e.detail.data.length-1].shareData
    })
  },
  loadEvent:function(e){
    console.log("loadEvent:",e)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    if("webPath" in options)
    this.setData({
      webPath:decodeURIComponent(options.webPath)
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return this.data.shareData
  }
})