// pages/tickets/tickets.js
import { API, GLOBAL } from './../fissionComponent/auth';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isShowDia4: false,
    isShowDia10: false,
    isShowDia12: false,
    authWin: false,
    userInfo: null,
    myVouchers: {
      surplus_total: 0,
      vouchers: [],
      shareCode: null
    },
    exchange: null,
    hasExchanged: false,
    exchangedValue: 0,
    sharedepthStr: null,
  },

  //未获得赠券的时候点击“未获赠券”提示
  showGiveTips(){
    wx.showToast({
      title: '需要好友赠送',
      image:"./../fissionComponent/warn.png"
    })
  },

  //拷贝代金券
  copyCode(e){
    console.log(e)
    const data = e.currentTarget.dataset.code;
    wx.setClipboardData({
      data,
      success:function(){
        wx.PTTSendClick('pop','codecopy','优惠码复制点击')
        wx.showToast({
          title: '复制成功',
          icon:"success"
        })
      }
    })
  },
  //返回
  backTo() {
    let curPages = getCurrentPages();
    if (curPages.length > 1) {
      wx.navigateBack()
    } else {
      GLOBAL.navigatorTo()
    }
  },

  //到大麦网
  toBuyTicket() {
    wx.PTTSendClick('btn','purchase4','前往购票点击')
    GLOBAL.toDamai(1);
  },

  //到web页面:data-webpath="rules.html"
  toH5(e) {
    GLOBAL.toH5(e);
    console.log(e)
    var btn = e.currentTarget.dataset;
    wx.PTTSendClick("btn", btn.btntype, btn.btnname)
  },

  //导航到小程序其他页面:data-url="/pages/fission/tickets/tickets"
  navigatorTo(e) {
    GLOBAL.navigatorTo(e);
    var btn = e.currentTarget.dataset;
    wx.PTTSendClick("btn", btn.btntype, btn.btnname)
  },

  //获取wx授权,并获取unionid,回调值userInfo
  getUserInfo(e) {
    const that = this;
    GLOBAL.bindGetUserInfo(e).then(function (res) {
      //关闭授权按钮
      that.setData({
        authWin: false,
      })
      that.userInit(res);
    })
  },
  //鸟哥鉴权
  userInit(e) {
    const that = this;
    GLOBAL.bindGetUserInfo(e).then(function (res) {
      API.getSession(res).then(function (res) {
        console.log("bird接口session:", res)
        that.unitPage(res)
      });
    });
  },
  //鸟哥鉴权后的初始化
  unitPage(res) {
    wx.showLoading({
      title: '加载中...',
    })
    const that = this;
    var _data = res.data.data;
    that.setData({
      userInfo: _data
    })
    //获取代金券信息
    API.getVouchers().then(function (res) {
      console.log("我的代金券：", res)
      var __data = res.data.data;
      for (var item of __data.vouchers) {
        switch (item.origin) {
          case "test":        //游戏
            var myVouchers = that.data.myVouchers;
            myVouchers.test = item;
            that.setData({
              myVouchers,
            });
            break;
          case "group":       //组队成功
            var myVouchers = that.data.myVouchers;
            myVouchers.group = item;
            that.setData({
              myVouchers,
            });
            break;
          case "give":        //别人赠送
            var myVouchers = that.data.myVouchers;
            myVouchers.give = item;
            that.setData({
              myVouchers,
            });
            break;
          case "bonus":       //可用于赠送
            var myVouchers = that.data.myVouchers;
            myVouchers.bonus = item;
            that.setData({
              myVouchers,
            });
            //用赠送券到后台兑换赠送码
            that.exchangeShareCode({ voucher_id: item.id }).then(function (res) {
              console.log("兑换回来的赠送码：", res);
              var myVouchers = that.data.myVouchers;
              myVouchers.shareCode = res.data.data;
              that.setData({
                myVouchers,
              });
            });
            break;
        }
      }
      var myVouchers = { ...that.data.myVouchers, ...__data };
      that.setData({
        myVouchers,
      });
      //检查是否已经生成过大麦优惠码,递归检查每一种代金券是否被生成过
      for (var item of myVouchers.vouchers) {
        if (item.origin !== "bonus" && item.has_exchanged) {
          var hasExchanged = true;
          var exchangedValue = that.data.exchangedValue + item.face_value;
          that.setData({
            hasExchanged,
            exchangedValue
          });
        }
      }
      wx.hideLoading()
      console.log("unitPage:", that.data)
    })
  },

  //到后台兑换赠送码
  exchangeShareCode(obj) {
    return new Promise(function (resolve, reject) {
      API.shareCoupon(obj).then(function (res) {
        resolve(res)
      }).catch(function (res) { reject(res) })
    })
  },

  //代金券生成优惠码
  getExchangeCode() {
    const vm = this.data;
    const that = this;
    if (vm.myVouchers.surplus_total === 0) {
      wx.showToast({
        title: '没有额度可兑换',
        image:"./../fissionComponent/warn.png"
      })
      return false;
    }
    API.createCoupon().then(function (res) {
      that.setData({
        exchange: res.data.data,
        hasExchanged:true,
        isShowDia4: true,
      })
    }).catch(function (res) {
      wx.showToast({
        title: res.data.message,
        icon:"none"
      })
    })
  },

  //生成优惠码按钮事件
  openExchangeWin() {
    const vm = this.data;
    const that = this;
    if (vm.hasExchanged) {
      wx.PTTSendClick('btn','codeview','查看优惠码点击');
      API.getInfoFromCode().then(function (res) {
        that.setData({
          exchange: res.data.data,
          isShowDia4: true,
        })
      }).catch(function (res) {
        wx.showToast({
          title: res.data.message,
          icon:"none"
        })
      })
    } else {
      wx.PTTSendClick('btn','codegenerate','生成优惠码点击');
      that.setData({
        isShowDia12: true,
      })
    }
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      sharedepthStr: GLOBAL.unitshareDepth(options)
    })
    const userInfo = wx.getStorageSync('userInfo');
    // const userInfo = {
    //   avatarUrl: "https://thirdwx.qlogo.cn/mmopen/vi_32/XYYy2bv5IDkoO3Pqhc7dnbca2mXSQjKdmZUxliaGYvbst8gr6TC54Znve7cR5wQxEHlIibjdhoLcm76T9df7ruFw/96",
    //   nickName: "亚美蝶55",
    //   openid: "oK05P5Z9DXewagchz8kqFNRYBpac55",
    //   unionid: "oNB1Hs1yodTTuI0sdt5a1h89rLhU55",
    //   logintype: "wx",
    // }
    // wx.setStorageSync('userInfo', userInfo);
    if (userInfo) {
      //如果授权，则渲染数据
      this.userInit(userInfo);
    } else {
      //如果没有授权，则弹窗授权按钮
      this.setData({
        authWin: true,
      })
    }
  },

  /**
   * 用户点击右上角分享
   */
  //分享
  onShareAppMessage: function (res) {
    const that = this;
    console.log(res);
    if (res.from == 'button' && res.target.dataset.realtype === 'coupon') {
      wx.PTTSendClick('btn', 'sendcoupon1', '赠券给好友点击');
      let path = that.setShareToOther();
      console.log("path:",path)
      return {
        title: "V-Station电竞体验馆开馆了，鉴定身价即可优惠购票，快来测试~",
        path:path,
        imageUrl: "https://game.gtimg.cn/images/game/act/a20200824fission/share_app_coupon.jpg"
      }
    } else {
      return {
        ...GLOBAL.shareData
      }
    }
  },
  onShareTimeline: function () {
    wx.PTTSendClick('share', 'timeline', '朋友圈')
    return {
      title: GLOBAL.shareData.title,
      imageUrl: 'https://game.gtimg.cn/images/game/act/a20200824fission/share.jpg'
    }
  },

  //赠券给他人
  setShareToOther() {
    const vm = this.data;
    let shareLink = "";
    const webUrl = encodeURIComponent("details.html?ADTAG=share.fission.tickets&code_number=" + vm.myVouchers.shareCode.code)
    shareLink = "/pages/fission/webView/webView?ADTAG=share.rewards&" + vm.sharedepthStr + "&webPath=" + webUrl;
    return shareLink;
  },

  closeDialog: function () {
    this.setData({
      isShowDia4: false,
      isShowDia10: false,
      isShowDia12: false,
    })
  }
})