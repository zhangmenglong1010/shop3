// pages/jointeam/jointeam.js
import { API, GLOBAL } from './../fissionComponent/auth';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    authWin: false,
    isShowDia2: false,
    userInfo: {
      nickname: '',
      openid: '',
      unionid: '',
      headimgurl: ''
    },
    myTeam: {
      id: null,
      has_organized: false,
      members: [],
    },
    myVouchers: {
      surplus_total: 0,
      vouchers: []
    },
    ui: {
      joinSuccess: false,
      teamStatus: 1,           //1：组团进度；2：我的团；3：我加入的团；4：没有团且url带team_id
    },
    options: null,
    shareCode: null,
    picnum: 3, //轮播图片数量，取自云函数
  },
  //分享暂存
  shareData: {

  },

  //关闭弹窗
  closeDialog() {
    this.setData({
      isShowDia2: false
    })
  },

  //到大麦网
  toBuyTicket() {
    GLOBAL.toDamai();
  },

  //到web页面:data-webpath="rules.html"
  toH5(e) {
    GLOBAL.toH5(e);
    var btn = e.currentTarget.dataset;
    wx.PTTSendClick("btn", btn.btntype, btn.btnname)
  },

  //导航到小程序其他页面:data-url="/pages/fission/tickets/tickets"
  navigatorTo(e) {
    GLOBAL.navigatorTo(e);
    var btn = e.currentTarget.dataset;
    wx.PTTSendClick("btn", btn.btntype, btn.btnname)
  },

  //获取wx授权,并获取unionid,回调值userInfo
  getUserInfo(e) {
    const that = this;
    GLOBAL.bindGetUserInfo(e).then(function (res) {
      //关闭授权按钮
      that.setData({
        authWin: false,
      })
      that.userInit(res);
    })
  },
  //鸟哥鉴权
  userInit(e) {
    const that = this;
    GLOBAL.bindGetUserInfo(e).then(function (res) {
      API.getSession(res).then(function (res) {
        console.log("bird接口session:", res)
        var _data = res.data.data;
        if (_data.is_new_user && that.options.ADTAG) wx.PTTSendClick('pv', 'visitor', '客态');
        that.unitPage(res)
      });
    });
  },
  //鸟哥鉴权后的初始化
  unitPage(res) {
    const that = this;
    const vm = that.data;
    var _data = res.data.data;
    that.setData({
      userInfo: _data
    })
    //链接里是否有队伍ID
    if (that.data.options.group_id) {
      wx.showLoading({
        title: '加载中...',
      })
      wx.PTTSendClick('link', 'invited', '收到组团邀请链接点击');
      //获取用户所在队伍信息
      API.getTeam({ group_id: that.data.options.group_id }).then(function (res) {
        console.log("根据group_id获取队伍信息:", res);
        var _data = res.data.data;
        if (_data) {//队伍ID合法，能拉取到数据
          const ui = vm.ui;
          let teamStatus = 4;
          if (_data.member_total > 0) {
            for (var item of _data.members) {
              if (item.id === vm.userInfo.user_id) {
                teamStatus = 2;
              }
            }
          }
          ui.teamStatus = teamStatus;
          that.setData({
            ui,
            isShowDia2: true
          })
          that.showTeamInfo(_data)
        } else {//队伍ID不合法
          wx.showToast({
            title: "无此队伍",
            image: "./../fissionComponent/warn.png"
          })
          that.unitNoParamsPage();
        }
        wx.hideLoading()
      })
    } else {//如果链接没有队伍ID
      that.unitNoParamsPage();
    }

  },

  //获取云函数里的轮播滚动图片数量
  getCloudPicNum() {
    const that = this;
    wx.cloud.callFunction({
      name: "getpic",
      success: function (res) {
        that.setData({ picnum: res.result.picnum })
      }
    })
  },

  //展示信息
  showTeamInfo(data) {
    const that = this;
    const vm = that.data;
    var myTeam = {
      ...vm.myTeam,
      ...data
    }
    that.setData({
      myTeam
    })
    console.log("showTeamInfo:", vm.myTeam);
    that.setShareTeam();
    //获取代金券信息
    API.getVouchers().then(function (res) {
      console.log("我的代金券：", res)
      var _data = res.data.data;
      var myVouchers = {
        ...vm.myVouchers,
        ..._data,
      }
      if (_data.vouchers.length > 0) {
        for (var item of _data.vouchers) {
          if (item.origin === "bonus") {
            //用赠送券到后台兑换赠送码
            API.shareCoupon({ voucher_id: item.id }).then(function (res) {
              console.log("兑换回来的赠送码：", res);
              that.setData({
                shareCode: res.data.data
              })
            });
          }
        }
      }
      that.setData({
        myVouchers
      })
    })
  },

  //点击加入队伍
  joinTeam() {
    const that = this;
    const vm = that.data;
    that.closeDialog();
    //获取用户所在队伍信息
    wx.PTTSendClick('btn', 'jointeam', '加入好友战队点击')
    API.getMyTeam().then(function (res) {
      console.log("获取用户所在队伍信息:", res);
      var _data = res.data.data;
      if (_data) {//如果用户已经有队伍
        if (_data.has_organized) {
          wx.showToast({
            title: "您已经成团",
            image: "./../fissionComponent/warn.png"
          })
          const ui = vm.ui;
          ui.teamStatus = 2;
          that.setData({ ui })
          that.showTeamInfo(_data)
          return false;
        } else if (_data.id === vm.myTeam.id) {
          wx.showToast({
            title: "您已在队伍中",
            //icon:"none"
            image: "./../fissionComponent/warn.png"
          })
          return false;
        }
      }
      //加入队伍或者从别的队伍跳过来
      API.joinTeam({ group_id: vm.myTeam.id }).then(function (res) {
        console.log("点击加入队伍:", res)
        if (res.data.code === 10112) {
          wx.showToast({
            title: "队伍已满员",
            image: "./../fissionComponent/warn.png"
          })
        } else {
          wx.showToast({
            title: "成功加入队伍",
            icon: "success"
          })
          wx.PTTSendClick('link', 'view', '组团成功页曝光')
          const ui = vm.ui;
          var _data = res.data.data;
          var userInfo = { ...that.data.userInfo, group: _data };
          ui.joinSuccess = true;
          ui.teamStatus = 3;
          that.setData({ ui, userInfo })
          that.showTeamInfo(_data)
        }
      })
    })
    if (vm.myTeam.id) {

    }
  },

  //如果链接没有队伍ID
  unitNoParamsPage() {
    const that = this;
    const vm = that.data;
    wx.showLoading({
      title: '加载中...',
    })
    wx.PTTSendClick('btn', 'sefteam', '组建自己战队点击');
    //获取用户所在队伍信息
    API.getMyTeam().then(function (res) {
      var _data = res.data.data;
      //如果用户已经有队伍信息,则显示队伍信息
      if (_data) {
        console.log("如果用户已经有队伍信息,则显示队伍信息:", _data);
        var ui = vm.ui;
        ui.teamStatus = 2;
        that.setData({ ui })
        that.showTeamInfo(_data)
      } else {//如果用户没有队伍信息,则创建队伍
        API.createTeam().then(function (res) {
          var __data = res.data.data;
          console.log("如果用户没有队伍信息,则创建队伍:", __data);
          var ui = vm.ui;
          var userInfo = { ...that.data.userInfo, group: __data };
          ui.teamStatus = 1;
          that.setData({ ui, userInfo })
          that.showTeamInfo(__data)
        })
      }
      wx.hideLoading()
    })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      sharedepthStr: GLOBAL.unitshareDepth(options),
      options,
    })
    this.getCloudPicNum();
    let userInfo = wx.getStorageSync('userInfo');
    // userInfo = {
    //   avatarUrl: "https://thirdwx.qlogo.cn/mmopen/vi_32/XYYy2bv5IDkoO3Pqhc7dnbca2mXSQjKdmZUxliaGYvbst8gr6TC54Znve7cR5wQxEHlIibjdhoLcm76T9df7ruFw/96",
    //   nickName: "亚美蝶1658",
    //   openid: "oK05P5Z9DXewagchz8kqFNRYBpac1658",
    //   unionid: "oNB1Hs1yodTTuI0sdt5a1h89rLhU1658",
    //   logintype: "wx",
    // }
    // wx.setStorageSync('userInfo', userInfo);
    if (userInfo) {
      //如果授权，则渲染数据
      this.userInit(userInfo);
    } else {
      //如果没有授权，则弹窗授权按钮
      this.setData({
        authWin: true,
      })
    }
  },

  //组队设置分享
  setShareTeam() {
    const that = this;
    const vm = that.data;
    var params = vm.myTeam.id ? '&group_id=' + vm.myTeam.id + "&" + vm.sharedepthStr : "&" + vm.sharedepthStr;
    that.shareData.path = '/pages/fission/jointeam/jointeam?ADTAG=share.team' + params;
    that.shareData.title = 'V-Station电竞体验馆开馆了，完成组队即获超大优惠，快来体验~';
    that.shareData.imageUrl = 'https://game.gtimg.cn/images/game/act/a20200824fission/share_app_team.jpg';
    console.log("组队分享链接：", that.shareData)
  },

  //赠券给他人,设置分享
  setShareToOther() {
    const that = this;
    const vm = this.data;
    const webUrl = encodeURIComponent("details.html?code_number=" + vm.shareCode.code);
    that.shareData.path = "/pages/fission/webView/webView?ADTAG=share.team&" + vm.sharedepthStr + "&webPath=" + webUrl;
    that.shareData.title = 'V-Station电竞体验馆开馆了，鉴定身价即可优惠购票，快来测试~';
    that.shareData.imageUrl = 'https://game.gtimg.cn/images/game/act/a20200824fission/share_app_coupon.jpg';
    console.log("赠券分享链接：", that.shareData)
  },

  //设置分享
  onShareAppMessage: function (res) {
    const that = this;
    if (res.from == 'button') {
      var btn = res.target.dataset;
      wx.PTTSendClick("btn", btn.btntype, btn.btnname)
      if (res.target.dataset.realtype === 'coupon') {
        that.setShareToOther();
      } else {
        that.setShareTeam();
      }
      return {
        ...that.shareData
      }
    } else {
      return {
        ...GLOBAL.shareData
      }
    }
  },
  onShareTimeline: function () {
    wx.PTTSendClick('share', 'timeline', '朋友圈')
    return {
      title: GLOBAL.shareData.title,
      imageUrl: 'https://game.gtimg.cn/images/game/act/a20200824fission/share.jpg'
    }
  },
})