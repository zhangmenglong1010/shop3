//index.js
let request = require('../../utils/scripts/request.js');
const app = getApp();
// pages/potential/potential.js
import {
  API,
  GLOBAL
} from '../fission/fissionComponent/auth';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    playing: false,
    videoHeight: '100px',
    bigScreen: false,
    hostTime: '上海 · 正大广场9F'
  },
  startPlay() {
    let windowWidth = wx.getSystemInfoSync().windowWidth;
    this.setData({
      videoHeight: windowWidth * (1448 / 750),
      playing: true
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  onShow() {
    if (typeof this.getTabBar === 'function' &&
      this.getTabBar()) {
      this.getTabBar().setData({
        selected: 0
      })
    }
    wx.getSystemInfo().then(res => {
      res.windowHeight > 700 && this.setData({
        bigScreen: true
      })
    })
    let that = this;
    request.requestCloud({
      url: '/api/system/mnp/info',
      method: 'get'
    }).then(res => {
      let hostTime = res.data.host_time,
        host = res.data.host;
      this.setData({
        hostTime: `${hostTime} ${host}`
      });
      app.globalData.commInfo.host = host;
    })
  },

  //到大麦网
  toBuyTicket() {
    GLOBAL.toDamai();
  },
  //到web页面:data-webpath="rules.html"
  toH5(e) {
    GLOBAL.toH5(e);
  },
  onShareAppMessage() {
    wx.PTTSendClick("index", "share", "分享");
    return {
      title: "Vstation电竞体验馆",
      path: '/pages/index/index?source=share',
      imageUrl: 'https://game.gtimg.cn/images/game/act/a20201001vstation/share.png'
    }
  },
})