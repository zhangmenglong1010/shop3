// miniprogram/pages/calendar/calendar.js
let request = require('../../utils/scripts/request.js');
const app = getApp()

let d = {};
Page({

  /**
   * 页面的初始数据
   */
  data: {
    club: '',
    name: '',
  },
  onLoad: function () {

  },
  setData: function (e) {
    console.log(e);
    let name = e.currentTarget.dataset.name;
    d[name] = e.detail.value;
  },
  showModal: function (c) {
    wx.showModal({
      content: '请填写' + c,
      showCancel: false,
      confirmText: '知道了'
    })
  },
  submitForm: function (e) {
    console.log(d);
    if (!d.club) {
      this.showModal('俱乐部名称')
    } else if (!d.name) {
      this.showModal('姓名')
    } else {
      wx.showLoading({
        title: '提交中',
        mask: true
      })
      wx.cloud.callFunction({
        // 需调用的云函数名
        name: 'morphy_customForm',
        // 传给云函数的参数
        data: {
          '$url': 'client/addFormData',
          'tableName': 'contestant',
          'formData': {
            'name': d.name,
            'team': d.club
          }
        },
        // 成功回调
        complete: () => {
          wx.hideLoading()
          wx.showToast({
            title: '提交成功',
            icon: 'success',
            duration: 2000,
            mask: true
          })
          setInterval(() => {
            wx.switchTab({
              url: '/pages/index/index'
            })
          }, 2000);
        }
      })
    }
  }
})