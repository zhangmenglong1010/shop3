// miniprogram/pages/map/map.js
let ibeacon = require('../../utils/scripts/ibeacon.js');
let request = require('../../utils/scripts/request.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    place : '地图',
    curPlace : -1,
    openPlace : -1,
    hasActs : false,
    placeInfo : [],
    actList : []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onShow: function(){
    let that = this;
    if (typeof this.getTabBar === 'function' &&
      this.getTabBar()) {
      this.getTabBar().setData({
        selected: 1
      })
    }
    // 小程序端调用
    wx.cloud.callFunction({
      // 需调用的云函数名
      name: 'morphy_ibeaconManager',
      // 传给云函数的参数
      data: {
          '$url':'client/getDetail',
          'actName':'vstation'
      },
    }).then(res => {
      let data = res.result.data;
      data.beaconList.forEach(item => {
        let key = item.key.split('_')[0],
            range = item.key.split('_')[1];
        item.key = key;
        item.range = range ? parseInt(range) : 2;
      })
      let beaconData = {
        uuid : data.uuid,
        major : data.major,
        beanconList : data.beaconList
      }
      
      ibeacon.initSetting(beaconData.uuid, beaconData.major, beaconData.beanconList, 3,true)
      ibeacon.openBeaconDiscovery(that.handleBeaconCb)
    })
    this.intiInfo();
  },
  onHide(){
    ibeacon.stopBeaconDiscovery()
  },
  intiInfo(){
    // 地图信息获取
    request.requestCloud({
      url : '/api/system/mnp/info',
      method : 'get'
    }).then(res => {
      this.setData({placeInfo : res.data.mapInfo});
    })


    // 小程序端调用活动信息
    wx.cloud.callFunction({
      // 需调用的云函数名
      name: 'morphy_customForm',
      // 传给云函数的参数
      data: {
        '$url':'client/getAllFormData',
        'tableName':'activity'
      }
    }).then(res => {
      let list = res.result.data.list.sort((a,b) => {new Date(`${a.formData.startTime.split(' ')[0]} ${a.formData.startTime.split(' ')[1].replace(/-g/,':')}`) - new Date(`${b.formData.startTime.split(' ')[0]} ${b.formData.startTime.split(' ')[1].replace(/-g/,':')}`)}),
          actList = {};
      list.forEach(info => {
        let item = info.formData;
        let day = item.startTime.split(' ')[0].replace(/-/g,'.');
        !actList[day] && (actList[day] = {day,acts : []});
        let startTime = item.startTime.split(' ')[1].split('-').slice(0,2).join(':'),
            endTime = item.endTime.split(' ')[1].split('-').slice(0,2).join(':');
        actList[day].acts.push({
          time : startTime + '~'+ endTime,
          content : item.content,
          remarks : item.remarks
        })
      })
      let today = `${new Date().getFullYear()}.${new Date().getMonth() + 1}.${new Date().getDate()}`;
      actList[today] && this.setData({actList:actList[today].acts,hasActs:true});
    })
  },
	handleBeaconCb: function(beacon) {
    if(!beacon) return;
    // do something with beacon
    // console.log('beacon',beacon.key,beacon.detail.minor,beacon.detail.accuracy);
    switch(beacon.key) {
      case '信念之路' : 
        this.setData({
          curPlace : 0
        })
        break;
      case '天赋空间' :
        this.setData({
          curPlace : 1,
        })
        break;
      case '热血赛场' : 
        this.setData({
          curPlace : 2
        })
        break;
      case '荣誉之巅' : 
        this.setData({
          curPlace : 3
        })
        break;
      case '再见主场' : 
        this.setData({
          curPlace : 4
        })
        break;
      default : 
        this.setData({
          curPlace : 0
        })
        break;
    }
    this.setData({
      place : beacon.key
    })
  },
  triggerLocation(e){
    let index = e.currentTarget.dataset.index;
    wx.PTTSendClick("btns", "site"+index, "现场信息"+index);
    let cur = index == this.data.openPlace ? -1 : index;
    this.setData({openPlace:cur});
  } ,

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {
    wx.PTTSendClick("map", "share", "现场地图");
    return {
      title: "Vstation电竞体验馆",
      path: '/pages/index/index?source=share',
      imageUrl: this.data.bgSrc
    }

  },
})