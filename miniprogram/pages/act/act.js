// miniprogram/pages/act/act.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    swiperList: [],
    actList: {}
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (typeof this.getTabBar === 'function' &&
      this.getTabBar()) {
      this.getTabBar().setData({
        selected: 3
      })
    }
  },
  onLoad() {
    this.initAct();
    this.initNewAct();
    this.initSwiper();
  },
  initNewAct() {
    // 小程序端调用
    wx.cloud.callFunction({
      // 需调用的云函数名
      name: 'morphy_customForm',
      // 传给云函数的参数
      data: {
        '$url': 'client/getAllFormData',
        'tableName': 'newactive',
        'order': 'asc',
        'size': 50
      }
    }).then(res => {
      let data = res.result.data.list;

      function compare(pro) {
        return function (obj1, obj2) {
          var val1 = obj1.formData[pro];
          var val2 = obj2.formData[pro];
          if (val1 > val2) { //正序
            return 1;
          } else if (val1 < val2) {
            return -1;
          } else {
            return 0;
          }
        }
      }
      data.sort(compare('sort'));

      let newActList10 = [];
      let newActList11 = [];
      for (let i of res.result.data.list) {
        if (i.formData.date.indexOf('10月') != -1) {
          newActList10.push({
            date: i.formData.date,
            time: i.formData.time,
            title: i.formData.title,
            desc: i.formData.desc,
            money: i.formData.money || false,
          })
        } else if (i.formData.date.indexOf('11月') != -1) {
          newActList11.push({
            date: i.formData.date,
            time: i.formData.time,
            title: i.formData.title,
            desc: i.formData.desc,
            money: i.formData.money || false,
          })
        }
      }
      this.setData({
        newActList10: newActList10,
        newActList11: newActList11
      })
      // let list = res.result.data.list.sort((a, b) => {
      //     new Date(`${a.formData.startTime.split(' ')[0]} ${a.formData.startTime.split(' ')[1].replace(/-g/,':')}`) - new Date(`${b.formData.startTime.split(' ')[0]} ${b.formData.startTime.split(' ')[1].replace(/-g/,':')}`)
      //   }),
      //   actList = {};
      // list.forEach(info => {
      //   let item = info.formData;
      //   let day = item.startTime.split(' ')[0].replace(/-/g, '.');
      //   let monthsString = ['JANUARY', 'FEBURARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER', ],
      //     montString = monthsString[day.split('.')[1]];
      //   !actList[day] && (actList[day] = {
      //     day: day,
      //     dateString: `${montString}.${day.split('.')[2]}.${day.split('.')[0]}`,
      //     acts: []
      //   });
      //   let startTime = item.startTime.split(' ')[1].split('-').slice(0, 2).join(':'),
      //     endTime = item.endTime.split(' ')[1].split('-').slice(0, 2).join(':');
      //   actList[day].acts.push({
      //     time: startTime + '~' + endTime,
      //     content: item.content,
      //     remarks: item.remarks
      //   })
      // })
      // this.setData({
      //   actList
      // });
    })
  },
  initAct() {
    // 小程序端调用
    wx.cloud.callFunction({
      // 需调用的云函数名
      name: 'morphy_customForm',
      // 传给云函数的参数
      data: {
        '$url': 'client/getAllFormData',
        'tableName': 'activity'
      }
    }).then(res => {
      let list = res.result.data.list.sort((a, b) => {
          new Date(`${a.formData.startTime.split(' ')[0]} ${a.formData.startTime.split(' ')[1].replace(/-g/,':')}`) - new Date(`${b.formData.startTime.split(' ')[0]} ${b.formData.startTime.split(' ')[1].replace(/-g/,':')}`)
        }),
        actList = {};
      list.forEach(info => {
        let item = info.formData;
        let day = item.startTime.split(' ')[0].replace(/-/g, '.');
        let monthsString = ['JANUARY', 'FEBURARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER', ],
          montString = monthsString[day.split('.')[1] - 1];
        !actList[day] && (actList[day] = {
          day: day,
          dateString: `${montString}.${day.split('.')[2]}.${day.split('.')[0]}`,
          acts: []
        });
        let startTime = item.startTime.split(' ')[1].split('-').slice(0, 2).join(':'),
          endTime = item.endTime.split(' ')[1].split('-').slice(0, 2).join(':');
        actList[day].acts.push({
          time: startTime + '~' + endTime,
          content: item.content,
          remarks: item.remarks
        })
      })
      this.setData({
        actList
      });
    })
  },
  initSwiper() {
    // 小程序端调用
    wx.cloud.callFunction({
      // 需调用的云函数名
      name: 'morphy_officialAlbum',
      // 传给云函数的参数
      data: {
        '$url': 'getCate',
        'sort': 'desc' // 可以选择两个值 desc|asc，默认为desc
      },
    }).then(res => {
      let cateId = '';
      res.result.data.forEach(item => {
        item.name == "轮播图" && (cateId = item._id);
      });
      return wx.cloud.callFunction({
        // 需调用的云函数名
        name: 'morphy_officialAlbum',
        // 传给云函数的参数
        data: {
          '$url': 'get',
          'cateId': cateId, // 指定获取分类的id
          'size': 10, // 指定每次获取的数量，最大不超过100
          'page': 1, // 参数可选，指定页数
          'order': 'desc', // 参数可选，指定获取数据的排序规则
        },
      })
    }).then(res => {
      this.setData({
        swiperList: res.result.data.list
      })
    })

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {
    wx.PTTSendClick("acts", "share", "分享");
    return {
      title: "Vstation电竞体验馆",
      path: '/pages/index/index?source=share',
      imageUrl: 'https://game.gtimg.cn/images/game/act/a20201001vstation/share.png'
    }
  },
})