//app.js
let ptt = require('./utils/scripts/ptt_for_miniapp.js');
let ibeacon = require('./utils/scripts/ibeacon.js');
let request = require('./utils/scripts/request.js');
App({
  onLaunch(params){
    let map = params.query.map,tips = params.query.tips;
    if(params.query.source){
      let source = params.query.source;
      let depth = wx.getStorageSync('sharedepth');
      source && wx.PTTSendClick("launch",source,"扫码进入"+source);
      depth && wx.PTTSendClick("launch",'sharedepth'+depth,"裂变深度"+depth);
    }
    // 现场用，取消ibeacon初始化，不自动跳转现场地图页
    this.globalData = {beanconInit: map == "false" || tips == "true" ? true : false};
    this.globalData.commInfo = {};
  },
  onShow: function () {
    
    this.globalData.statusBarHeight = wx.getSystemInfoSync()['statusBarHeight']
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        // env 参数说明：
        //   env 参数决定接下来小程序发起的云开发调用（wx.cloud.xxx）会默认请求到哪个云环境的资源
        //   此处请填入环境 ID, 环境 ID 可打开云控制台查看
        //   如不填则使用默认环境（第一个创建的环境）
        env: 'release-rcd69',
        // env: 'test-eiozq',
        traceUser: true,
      })
    }
    if(!this.globalData.beanconInit){
      this.initBeacon();
    }
  },
  watch: function(func) {
    let obj = this.globalData;
    if(this.globalData.commInfo){func(this.globalData.commInfo)}
    Object.defineProperty(obj, "commInfo", {
      configurable: true,
      enumerable: true,
      set: function(val) {
        this._commInfo = val;
        func(val);
      },
      get: function() {
        return this._commInfo;
      }
    })
  },

	handleBeaconCb: function(beacon) {
    if(!beacon) return;
    console.log('init',beacon,this.globalData);
    ibeacon.stopBeaconDiscovery();
    // do something with beacon
    wx.switchTab({
      url: '/pages/map/map'
    })
  },
  initBeacon(){
    let that = this;
    this.globalData.beanconInit = true;

    // 小程序端调用
    wx.cloud.callFunction({
      // 需调用的云函数名
      name: 'morphy_ibeaconManager',
      // 传给云函数的参数
      data: {
          '$url':'client/getDetail',
          'actName':'vstation'
      },
    }).then(res => {
      let data = res.result.data;
      data.beaconList.forEach(item => {
        let key = item.key.split('_')[0],
            range = item.key.split('_')[1];
        item.key = key;
        item.range = range ? parseInt(range) : 10;
      })
      let beaconData = {
        uuid : data.uuid,
        major : data.major,
        beanconList : data.beaconList
      }
      ibeacon.initSetting(beaconData.uuid, beaconData.major, beaconData.beanconList, 10,true)
      ibeacon.openBeaconDiscovery(that.handleBeaconCb)
    })
  }
})
