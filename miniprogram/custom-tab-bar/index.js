// components/customTabBar/customTabBar.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    selected: 0,
    color: "#fff",
    selectedColor: "#EFC100",
    list : [
      {
        "pagePath": "/pages/index/index",
        "iconPath": "https://game.gtimg.cn/images/game/act/a20201001vstation/tabbar/default/icon0.png",
        "selectedIconPath" : "https://game.gtimg.cn/images/game/act/a20201001vstation/tabbar/selected/icon0.png",
        "text": "首页"
      },
      {
        "pagePath": "/pages/map/map",
        "iconPath": "https://game.gtimg.cn/images/game/act/a20201001vstation/tabbar/default/icon5.png",
        "selectedIconPath" : "https://game.gtimg.cn/images/game/act/a20201001vstation/tabbar/selected/icon5.png",
        "text": "地图"
      },
      {
        "pagePath": "/pages/site/site",
        "iconPath": "https://game.gtimg.cn/images/game/act/a20201001vstation/tabbar/default/icon6.png",
        "selectedIconPath" : "https://game.gtimg.cn/images/game/act/a20201001vstation/tabbar/selected/icon6.png",
        "text": "现场"
      },
      {
        "pagePath": "/pages/act/act",
        "iconPath": "https://game.gtimg.cn/images/game/act/a20201001vstation/tabbar/default/icon7.png",
        "selectedIconPath" : "https://game.gtimg.cn/images/game/act/a20201001vstation/tabbar/selected/icon7.png",
        "text": "活动"
      },
      {
        "pagePath": "/pages/myQrcode/myQrcode",
        "iconPath": "https://game.gtimg.cn/images/game/act/a20201001vstation/tabbar/default/icon8.png",
        "selectedIconPath" : "https://game.gtimg.cn/images/game/act/a20201001vstation/tabbar/selected/icon8.png",
        "text": "我的"
      }
    ]

  },

  /**
   * 组件的方法列表
   */
  methods: {
    switchTab(e){
      const data = e.currentTarget.dataset
      const url = data.path
      wx.switchTab({url})
      this.setData({
        selected: data.index
      })
      wx.PTTSendClick("tab","tab"+data.index,"导航"+data.index);
    }
  }
})
