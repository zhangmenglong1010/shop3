module.exports = {
  isShowNotOpenBluetoothTip: true, // 是否要提醒用户打开蓝牙
  bluetoothAvailable: 0, // 蓝牙是否可用
  beaconData: [], // beacon及其相关的数据数组，每条数据是一个对象，对象必须包含minors字段，minors是iBeacon设备的次id数组
  uuids: ['B9407F30-F5F8-466E-AFF9-25556B57FE6D'], // iBeacon设备广播的uuid
  major: 700, // iBeacon设备的主id
  curRange: 3, // 检测距离，多少米内的beacon才属于有效beacon
  isCustomRange: false, // 是否使用每个位置的自定义检测距离，开启的话 每个位置的数据对象都需要设置range字段，否则依旧读总的检测距离
  initSetting: function(uuids, major, beacons, range, isCustomRange, closeOpenBluetoothTip) {
    /*
     * 初始化信息
     * uuids: iBeacon设备广播的uuid数组
     * major: iBeacon设备的主id
     * beacons: beacon及其相关的数据数组，每条数据是一个对象，对象必须包含minors字段，minors是iBeacon设备的次id数组
     * range: 检测距离
     * isCustomRange: false // 是否使用每个位置的自定义检测距离，开启的话 每个位置的数据对象都需要设置range字段，否则依旧读总的检测距离
     * closeOpenBluetoothTip: 是否关掉提醒用户打开蓝牙的提示
     */
    this.uuids = uuids
    this.major = major
    this.curRange = range || 3
    this.isCustomRange = !!isCustomRange
    this.beaconData = beacons
    this.isShowNotOpenBluetoothTip = closeOpenBluetoothTip ? false : true 
  },
  openBeaconDiscovery: function (cb) {
    // 开启ibeacon
    let _this = this;

    wx.openBluetoothAdapter({
      // 初始化蓝牙模块，成功了才能调用ibeacon的API
      success: (res) => {
        console.log('open Bluetooth Adapter success.')
        _this.onBeaconUpdate(cb);
        _this.bluetoothAvailable = 1
      },
      fail: () => {
        console.log('open Bluetooth Adapter fail.')
        _this.showNotOpenBluetoothTip();
        _this.bluetoothAvailable = 0
      }
    });
    wx.onBluetoothAdapterStateChange(res => {
      //监听蓝牙状态，当用户重新打开蓝牙时，会执行
      console.log('Bluetooth Adapter State Change:', res)
      if (res.available) {
        _this.onBeaconUpdate(cb);
        _this.bluetoothAvailable = 1
      } else {
        _this.showNotOpenBluetoothTip();
        _this.bluetoothAvailable = 0
      }
    });
  },
  stopBeaconDiscovery: function(){
    // 停止ibeacon
    wx.stopBeaconDiscovery();
  },
  onBeaconUpdate: function (cb) {
    // 开始监听beacon
    console.log('start search beacon...')
    let _this = this;
    let startTime = new Date();
    wx.startBeaconDiscovery({
      uuids: _this.uuids,
      ignoreBluetoothAvailable: true,
      success: () => {
        wx.onBeaconUpdate(function (res) {
          let endTime = new Date();
          startTime = endTime;
          let beaconResult = res.beacons.sort((a,b) => {a.accuracy - b.accuracy});
          // console.log('get beacons:', beaconResult)
          let bLength = beaconResult.length
          let isExist = false // 是否存在有效的目标beacon
          for (let i = 0; i < bLength; i++) {
            let curBeacon = beaconResult[i]
            if ( curBeacon.major == _this.major && curBeacon.accuracy >= 0 ) {
              let result = _this.filterBeacon(curBeacon, cb)
              if(result) {
                isExist = true;
                break;
              }
            }
          }

          !isExist && typeof cb === 'function' && cb(null) // 如果不存在有效beacon，则触发一次回调
        });
      }
    });
  },
  filterBeacon: function (beacon, cb) {
    // 判断搜索到的beacon是否属于名单寻找的beacon，且在有效距离内
    let _this = this
    let result = false
    let minor = String(beacon.minor);
    for (let k in _this.beaconData) {
      let curBeacon = _this.beaconData[k]
      if(curBeacon.minors.indexOf(minor)!==-1) {
        let range = _this.isCustomRange ? curBeacon.range || _this.curRange : _this.curRange;
        if(beacon.accuracy <= range) {
          // 在有效距离内的beacon
          typeof cb === 'function' && cb(Object.assign({}, curBeacon, {detail: beacon}))
          result = true
        }
      }
    }
    return result
  },
  showNotOpenBluetoothTip: function () {
    // 当用户没打开蓝牙时，调该方法提醒用户打开蓝牙
    this.isShowNotOpenBluetoothTip && wx.showModal({
      title: '温馨提醒',
      showCancel: false,
      confirmText: '知道了',
      content: '请打开手机蓝牙，以便小程序为您提供体验更佳的服务。'
    });
  }
};