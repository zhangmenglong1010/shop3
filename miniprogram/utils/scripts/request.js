const app = getApp()
// 请求云服务, header 遵循wx.request 的规范，实际请求通常是headers
export function requestCloud({ url, data = {}, method = 'post', header={}, weRunData }) {
  // data.token = app.globalData.token
  // data.expire = app.globalData.expire
  // console.log(app.globalData);
  // header.token = app.globalData.token
  // header.expire = app.globalData.expire
  return new Promise((resolve, reject) => {
    wx.cloud.callFunction({
      name: 'mnp',
      data: { url, data, method, headers: header, weRunData },
      success: result => {
        const data = result.result
        if (data.code !== 0) {
          // 未登录
          if (data.code === 10001) {
            // 重新登录
            reject(new Error('你的登录掉啦，请先登录！'))
            return
          }
          // 其他异常码的处理
          reject(new Error(data.message))
          return
        }
        resolve(data)
      },
      fail: err => {
        // new Error(res.data.message)
        console.error(err)
        reject(new Error('调用异常'))
      }
    })
  })
}
