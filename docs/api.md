[TOC]

# 电竞博物馆接口文档

## 公共说明

通用返回格式

```json
{
  "code": 0,
  "msg": "状态信息",
  "data": null // or {} or []
}
```

接口状态码

code 格式如下:

* 0为成功，其他为失败
* 错误码统一格式：A-BB-CC
* A: 错误级别，1代表系统级错误，前端接到后须弹窗提示；2代表服务级错误，只在接口中打印；
* B: 模块名称，一般不会超过99个模块；
* C: 具体错误编号，自增即可，一般不超过99个。
* --
* 1****、接口错误，前端可处理的错误
* 00 是通用模块
* 10001：未知用户，用户未登录
* 10002：未知路由或请求方法有误（GET || POST）
* 10003：接口参数错误，通常是必选字段缺失或字段格式有误
* 10004：无权限
* 10005：数据库中未找到对应的数据
* 10006：重复的多余的操作
* --
* 2****、系统错误/后台错误：
* [BB] 00：公共文件；01：Controller；02：Middleware；
* [CC] 01：函数参数错误；02：函数运行错误；03：数据库错误；09：其他异常错误

## 小程序版

### 说明

* 云函数名: `mnp`
* 接口前缀: `/api`
  
请求示例:

```js
wx.cloud.callFunction({
  name: 'mnp',
  data: {
    method: 'get',
    url: "/api/user/detail",
    data: {
      test: "123"
    },
  },
  success: res => {
    const data = res.result
    resolve(data)
  },
  fail: err => {
    reject(err)
  }
})
```

### 公共模块

#### 获取cos临时密钥

_GET_ `file/cos/token`

* 描述: 获取cos临时密钥。
* 请求参数：
  
  | 字段 | 类型   | 必选  | 默认值 | 说明                |
  | :--- | :----- | :---: | :----: | :------------------ |
  | ext  | string |   √   |   -    | 文件后缀名，如'jpg' |

* 响应
  
  ```js
  {
    "expired_time": 1599463400000, // 过期时间
    "object_path": "test-eiozq/common/5221c106-6d7c-4ef6-ab95-e5166ea897ca.png", // 已被分配好的cos路径，该文件只允许上传到此路径
    "session_token": "aB9gu1E267SFof72tbV0yLYHbLLLYG4a882c48", // token
    "start_time": 1599463100000,
    "tmp_secret_id": "AKIDNUlz8TrekBkaE0I2YwbxMPDD20MHAty78bEHsfh_c9FxGmVMoVFnic-BKGA4U67q", // 临时
    "tmp_secret_key": "NhefdGXHpeFLbrD2EWEBfOryim1yyRuTaVds11MuIU8=" // 临时
  }
  ```

### 用户模块

#### 获取用户详细信息

_GET_ `user/detail`

* 描述: 获取用户详细信息。
* 请求参数：无
* 响应

```javascript
{
  code : Number, // 返回状态码
  msg : String,  // "成功"
  data: {
    increment: 13, // 自增id，系统第N个用户
    avatar: "https://wx.qlogo.cn/mmopen/vi_32/pvVXOphTg89CIhCAt8eficAgrZkP2WJiaG1UAiaMKo8roTWvGZWF2F9YwEABTiarEjrr6BmX2HtoghDjTyV5iayBDRg/132",
    city: "Shenzhen",
    country: "China",
    gender: 1,
    language: "zh_CN",
    name: "Randle",
    openid: "oK05P5X6Ctv7I6OEOhENhQ-sxbzo",
    province: "Guangdong",
    currentRank: 10024, // 当前排名
    totalScoreDetail: {
      "focus": 36, // 专注力
      "achieve": 50, // 成就动机
      "multithread": 60, // 多线加工
      "dispatch": 42, // 调度力
      "instant": 39, // 即战力
      "coordinate": 30, // 手眼协调
      "compressive": 70 // 抗压力
    },
    score: 30, // 6个测试的总分数，-1代表分数未生成
    scoreTimestamp: 1595234988986, // 生成分数的时间戳
    testScoreList: {1: {…}, 2: {…}, 3: {…}, 4: {…}, 5: {…}, 6: {…}}, // 6个测试的得分
    timestamp: 1594690809714, // 用户生成openid的时间戳
    reportVideoCosKey: '', // 合成视频下载地址
    tutorId: 1, // 导师id，-1代表未选择导师
    reportVideoUrl: '', // 视频报告
    _id: "4c54b50d5f0c004a001eace00620ad05" // 用户id
  }
}
```

#### 更新用户信息

_POST_ `user/update`

* 描述: 更新用户详细信息。
* 请求参数：
  
  | 字段     | 类型   | 必选  | 默认值 | 说明                                               |
  | :------- | :----- | :---: | :----: | :------------------------------------------------- |
  | name     | string |   √   |   -    | 用户昵称                                           |
  | avatar   | string |   √   |   -    | 用户头像url，来自wx.getUserInfo官方接口的avatarUrl |
  | gender   | int    |   √   |   -    | 性别                                               |
  | city     | string |   -   |   -    | 城市                                               |
  | country  | string |   -   |   -    | 国家                                               |
  | province | string |   -   |   -    | 省份                                               |
  | language | string |   -   |   -    | 语言                                               |

* 响应

```javascript
{
  code : Number, // 返回状态码
  msg : String,  // "成功"
  data: {
    avatar: "https://wx.qlogo.cn/mmopen/vi_32/pvVXOphTg89CIhCAt8eficAgrZkP2WJiaG1UAiaMKo8roTWvGZWF2F9YwEABTiarEjrr6BmX2HtoghDjTyV5iayBDRg/132",
    city: "Shenzhen",
    country: "China",
    gender: 1,
    language: "zh_CN",
    name: "Randle",
    openid: "oK05P5X6Ctv7I6OEOhENhQ-sxbzo",
    province: "Guangdong",
    score: 30,
    scoreTimestamp: 1595234988986,
    testScoreList: {1: {…}, 2: {…}, 3: {…}, 4: {…}, 5: {…}, 6: {…}},
    timestamp: 1594690809714,
    tutorId: "1",
    _id: "4c54b50d5f0c004a001eace00620ad05"
  }
}
```

#### 获取用户应援列表（morphy中台）

* 描述: 获取用户应援列表。
* 请求示例：
  
  ```js
  // 小程序端调用
  wx.cloud.callFunction({
    // 需调用的云函数名
    name: 'morphy_customForm',
    // 传给云函数的参数
    data: {
      '$url':'client/getMyFormData',
      'tableName':'support'，
      'page': 1, // 页数，可不填，默认为1
      'size': 10, // 每页数量，可不填，默认为10，每次最多返回10条，看10条是否满足前端需求
      'order': 'desc' // 排序 desc 或者 asc，可不填，默认为desc
    },
    // 成功回调
    complete: console.log
  })
  ```

* 响应

```javascript
{
  code : Number, // 返回状态码
  msg : String,  // "成功"
  data: {
    list: [
      {
        createAt: 1598950191199,
        formData: {text: "阿水加油", isCheck: 1, isCheckSys: {…}},
        formSettingId: "65825b355f4e06d100a41fe66b3afe54",
        hidden: false,
        openid: "oK05P5X6Ctv7I6OEOhENhQ-sxbzo",
        source: "client",
        updateSys: {morphy: {…}},
        _id: "65825b355f4e0b2f00a445ab77994799",
      }
    ],
    total: 4, // 总条数，Math.ceil(total/size) 将得到总页数
    page: 1, // 当前页
    pageTotal: 2 // 总页数
  }
}
```

#### 获取用户的详细预约信息列表

_GET_ `user/reserve/list`

* 描述: 获取用户的详细预约信息列表
* 请求参数：无
* 响应

```javascript
{
  code : Number, // 返回状态码
  msg : String,  // "成功"
  data: [ // 我的预约记录
    {
      _id: 1598745600000, // 预约记录id，reserveRecord_id 用户取消预约时可用
      created_at: 1598428060272, // 该记录的创建时间
      dateString: "2020-08-30", // 预约日期
      text: "14:30-16:30", // 预约时间段
      startTime: 1598841000000, // 预约时间段的开始时间戳
      endTime: 1598848200000, // 预约时间段的结束时间戳
      phoneNumber: "15914781220", // 该次预约使用的票号
      isLocked: false, // 该时间段是否锁定，锁定的时间段无法预约，用于活动时间段锁定和提前2小时清场
      isExpired: false, // 该预约是否已过期
      member: [ // 该预约的随同成员
        {
          idcard: "", // 身份证号码
          name: "" // 姓名
        }
      ] // 同行成员
    },
    ...
  ]
}
```

#### 用户预约某个时间段

_POST_ `user/reserve/add`

* 描述: 用户预约某个时间段。
* 请求参数：
  
  | 字段            | 类型      | 必选  | 默认值 | 说明                                                      |
  | :-------------- | :-------- | :---: | :----: | :-------------------------------------------------------- |
  | reserve_id      | string    |   √   |   -    | 预约时间段的_id，见 /api/reserve/detail                   |
  | ticketTypeId    | string    |   √   |   -    | 票种id，通票/工作日票/活动票/，见 /api/reserve/config/get |
  | phoneNumber     | string    |   √   |   -    | 手机号                                                    |
  | idcard          | string    |   √   |   -    | 用户身份证(15-18位，可能含有X)                            |
  | name            | string    |   √   |   -    | 用户姓名                                                  |
  | member          | Array[{}] |   -   |   []   | 同行成员，多人，数组                                      |
  | member.*.idcard | string    |   -   |   []   | 同行成员的身份证，仅校验合法性，不校验年龄                |
  | member.*.name   | string    |   -   |   []   | 同行成员的姓名                                            |

* 响应

```javascript
{
  code : Number, // 返回状态码
  msg : String,  // "成功"
  data: {
    "day": "2020-05-30",
    "index": 3
  }
}
```

#### 用户取消预约某个时间段

_POST_ `user/reserve/remove`

* 描述: 用户取消预约某个时间段。
* 请求参数：
  
  | 字段             | 类型   | 必选  | 默认值 | 说明                                     |
  | :--------------- | :----- | :---: | :----: | :--------------------------------------- |
  | reserveRecord_id | string |   √   |   -    | 预约记录的_id，见 /api/user/reserve/list |

* 响应

```javascript
{
  code : Number, // 返回状态码
  msg : String,  // "成功"
  data: {
    "place_id": 2,
    "day": "2020-05-30",
    "index": 3
  }
}
```

#### 获取用户合影信息

_POST_ `user/photo/list`

* 描述: 获取用户合影信息
* 请求参数：
  
  | 字段     | 类型 | 必选  | 默认值 | 说明         |
  | :------- | :--- | :---: | :----: | :----------- |
  | page     | int  |   √   |   -    | 页数，1开始  |
  | pageSize | int  |   √   |   -    | 页码，默认10 |

* 响应

```javascript
{
  code : Number, // 返回状态码
  msg : String,  // "成功"
  data: {
    "page": 1,
    "pageSize": 10,
    "pageTotal": 1,
    "total": 4,
    "list": [
      {
        created_at: 1599633368312,
        photos: [
          {
            path: "cloud/2020-09-09/1599633400408/679765/企业微信截图_db222e5b-63b2-47cb-b8c8-ef6e456f746a.png",
            size: 54180
          },
          {
            path: "cloud/2020-09-09/1599633401905/53548/aaa.png",
            size: 76302
          },
        ],
        teamId: 1,
        uid: "b5416b755f41ecd3002ca04279f91217",
        video: {
          path: "cloud/2020-09-09/1599633403235/359781/transition.mp4",
          size: 172578
        },
        _id: "b5416b755f5877d8016cf0703eea1c4c"
      }
    ]
  }
}
```

#### 用户抽奖

_POST_ `user/lottery`

* 描述: 用户抽奖
* 请求参数：无

* 响应
  
  ```js
  {
    code : Number, // 返回状态码
    msg : String,  // "成功"
    data: { // 奖品名称
      _id: "524ebe8c5f606ed52700361b500125487",
      lotteryType: { // 奖品类型
        isNeedVirtual: true, // 是否需要虚拟key
        lotStatus: 1,
        text: "QQ飞车皮肤", // 奖品名称
        _id: "55aebe8c5f606ed9000361b500ffb65d"  // 奖品类型id
      },
      virtualInfo: { // 可能为空，取决于该奖品是否需要虚拟key
        content: "test888-cd", // 虚拟key
        _id: "c54dac465f608a550004ead805ee3074" // 虚拟卡id
      }
    }
  }
  ```

#### 获取用户的中奖记录列表

_GET_ `user/lottery/list`

* 描述: 获取用户中奖记录列表
* 请求参数：无
  
* 响应

  ```js
  {
    code : Number, // 返回状态码
    msg : String,  // "成功"
    data: [ // 中奖记录列表
      {
        created_at: 1600240596396, // 中奖时间
        exchanged_at: 0, // 兑换时间
        isExchange: false, // 是否已兑换
        uid: "b5416b755f41ecd3002ca04279f91217", // 用户id
        _id: "f1168d5b5f61bbcb0017ebd260ef26f0", // 中奖记录id
        lotteryType: { // 奖品类型
          isNeedVirtual: true, // 是否需要虚拟key
          lotStatus: 1,
          text: "QQ飞车皮肤", // 奖品名称
          _id: "55aebe8c5f606ed9000361b500ffb65d",  // 奖品类型id
        },
        virtualInfo: { // 可能为空，取决于该奖品是否需要虚拟key
          content: "test888-cd", // 虚拟key
          _id: "c54dac465f608a550004ead805ee3074" // 虚拟卡id
        }
      }
    ]
  }
  ```

#### 用户奖品核销兑换接口

_POST_ `user/lottery/exchange`

* 描述: 用户奖品核销兑换接口
* 请求参数：

  | 字段      | 类型   | 必选  | 默认值 | 说明         |
  | :-------- | :----- | :---: | :----: | :----------- |
  | record_id | string |   √   |   -    | 中奖记录的id |

* 响应
  
  ```js
  {
    code : Number, // 返回状态码
    msg : String,  // "兑换成功"
    data: null
  }
  ```

#### 删除用户账号

_GET_ `user/delete`

* 描述: 删除用户账号，用于调试。
* 请求参数：无
* 响应

```javascript
{
  "code": 0,
  "data": null,
  "msg": "删除用户成功"
}
```

### 管理员模块

#### 获取运营数据

_GET_ `/api/user/operator/data`(miniprogram_admin表-白名单，准备废弃)
_GET_ `/api/whitelist/operator/data`(miniprogram_admin表-白名单)
* 描述: 获取运营数据
* 请求参数：无
* 响应

```javascript
{
  code: Number, // 返回状态码
  msg: String,  // "成功"
  data: {
    "activeCount": 690, // 互动总人数
    "dayActiveCount": 46, // 今天互动总人数
    "totalScoreCount": 212, // 6个测试总人数
    "dayTotalScoreCount": 40, // 今天6个测试总人数
    "lotteryCount": 191, // 抽奖总人数
    "dayLotteryCount": 18, // 今天抽奖人数
    "dayLotteryMatCount": 9, // 今天的实体奖数量
    "dayExchangeLotteryCount": 14, // 今天已兑换人数
    "reserveCount": 111, // 预约总人数
    "dayReserveActiveCount": 17,// 预约后到场人数
    "dayReserveUnactiveCount": 94,// 预约了未到场互动 - 放飞机人数
    "dayUnreserveActiveCount": 29 // 未预约但到场人数
  }
}
```

#### 修改预约上限人数

_GET_ `/api/whitelist/reserve/maxCount/update`(miniprogram_admin表-白名单)
* 描述: 修改预约上限人数
* 请求参数：
  
  | 字段       | 类型    | 必选  | 默认值 | 说明           |
  | :--------- | :------ | :---: | :----: | :------------- |
  | reserve_id | string  |   √   |   -    | 预约时间段的id |
  | maxCount   | integer |   √   |   -    | 上限人数       |

* 响应

```javascript
{
  code: Number, // 返回状态码
  msg: String,  // "成功"
  data: null
}
```

#### 运营人员激活抽奖资格

_POST_ `/operator/lottery/sign`(运营人权限)

* 描述: 运营人员激活抽奖资格，用户需要和线下设备互动过，用户每天最多被激活一次
* 请求参数：

  | 字段   | 类型   | 必选  | 默认值 | 说明             |
  | :----- | :----- | :---: | :----: | :--------------- |
  | openid | string |   √   |   -    | 玩家用户的openid |

* 响应

```javascript
{
  code: Number, // 返回状态码
  msg: String,  // "成功"
  data: null // 数据
}
```

#### 获取用户信息的审批列表（废弃）

_GET_ `/admin/checkup/list`

* 描述: 获取用户昵称，头像的审核列表。
* 请求参数：
  
  | 字段          | 类型 | 必选  | 默认值 | 说明                                         |
  | :------------ | :--- | :---: | :----: | :------------------------------------------- |
  | page          | int  |   √   |   -    | 页数，1开始                                  |
  | pageSize      | int  |   √   |   -    | 页码，默认10                                 |
  | checkupStatus | int  |   -   |   -    | 审批状态：0-未审核，1-审核通过，2-审核不通过 |

* 响应

```javascript
{
  code : Number, // 返回状态码
  msg : String,  // "成功"
  data: {
    page: 2,
    total: 2,
    pageTotal: 3,
    list: [
      {
        name: "默认用户昵称",
        avatar: "默认头像",
        tmpName: "Randlehuang", // 待审核的昵称
        tmpAvatar: "https://***.jpg", // 待审核待头像
        text: "打call测试",
        timestamp: 1595557498550,
        uid: "4c54b50d5f0c004a001eace00620ad05",
        _id: "94f505805f1a462b0067af8e6dae538a",
      }
    ]
  }
}
```

#### 审核头像为通过(废弃)

_POST_ `/admin/checkup/name`

* 描述: 审核通过用户的昵称，管理员对tmpName字段审核通过，后台将tmpName赋值给name字段。
* 请求参数：
  
  | 字段 | 类型   | 必选  | 默认值 | 说明   |
  | :--- | :----- | :---: | :----: | :----- |
  | uid  | string |   √   |   -    | 用户id |

* 响应

```javascript
{
  code : Number, // 返回状态码
  msg : String,  // "用户昵称审核成功"
  data: {
        name: "默认用户昵称",
        avatar: "默认头像",
        tmpName: "Randlehuang", // 待审核的昵称
        tmpAvatar: "https://***.jpg", // 待审核待头像
        text: "打call测试",
        timestamp: 1595557498550,
        uid: "4c54b50d5f0c004a001eace00620ad05",
        _id: "94f505805f1a462b0067af8e6dae538a",
  }
}
```

#### 审核昵称为通过（废弃）

_POST_ `/admin/checkup/avatar`

* 描述: 审核通过用户的昵称，管理员对tmpAvatar字段审核通过，后台将tmpAvatar赋值给avatar字段。
* 请求参数：
  
  | 字段 | 类型   | 必选  | 默认值 | 说明   |
  | :--- | :----- | :---: | :----: | :----- |
  | uid  | string |   √   |   -    | 用户id |

* 响应

```javascript
{
  code : Number, // 返回状态码
  msg : String,  // "用户昵称审核成功"
  data: {
    name: "默认用户昵称",
    avatar: "默认头像",
    tmpName: "Randlehuang", // 待审核的昵称
    tmpAvatar: "https://***.jpg", // 待审核待头像
    text: "打call测试",
    timestamp: 1595557498550,
    uid: "4c54b50d5f0c004a001eace00620ad05",
    _id: "94f505805f1a462b0067af8e6dae538a",
  }
}
```

#### 添加用于预约的活动（待整合到中台）

_POST_ `/admin/act/add`

* 描述: 将一段时间设置为活动，添加到数据表中，将影响到预约及票种，actType。
* 请求参数：
  
  | 字段          | 类型       | 必选  | 默认值 | 说明                                                                     |
  | :------------ | :--------- | :---: | :----: | :----------------------------------------------------------------------- |
  | startTime     | string     |   √   |   -    | 开始时间(2018-05-20)                                                     |
  | endTime       | string     |   √   |   -    | 结束时间                                                                 |
  | actType       | Array[int] |   -   |   -    | 活动类型id，默认为[0]，日常活动，详见`/reserve/config/get`               |
  | supportTicket | Array[int] |   -   |   -    | 支持的票种，通票/工作日票无需填写，将自动分配，详见`/reserve/config/get` |

* 响应

```javascript
{
  code : Number, // 返回状态码
  msg : String,  // "成功"
  data: null
}
```

### 导师模块

#### 获取导师列表

* 描述: 获取导师列表。
* 请求参数：无
* 响应

```javascript
{
  code : Number, // 返回状态码
  msg : String,  // "成功"
  data: [
    {
      name: "若风"
      _id: "1503f3385f1949e70079725f13f88fe2"
    }
  ]
}
```

#### 获取导师信息

* 描述: 获取导师详情。
* 请求参数：
  
  | 字段     | 类型 | 必选  | 默认值 | 说明   |
  | :------- | :--- | :---: | :----: | :----- |
  | tutor_id | int  |   √   |   -    | 导师id |

* 响应

```javascript
{
  code : Number, // 返回状态码
  msg : String,  // "成功"
  data: {
    name: "若风"
    _id: "1503f3385f1949e70079725f13f88fe2"
  }
}
```

### 应援模块

#### 获取应援列表（定制化接口，兼容头像昵称后审，不使用中台的默认接口）

_GET_ `/support/list`

* 描述: 获取应援列表。
* 请求参数：
  
  | 字段     | 类型   | 必选  |      默认值      | 说明                                                 |
  | :------- | :----- | :---: | :--------------: | :--------------------------------------------------- |
  | page     | int    |   -   |        1         | 页数                                                 |
  | size     | int    |   -   |        10        | 每页数目                                             |
  | order    | string |   -   |       desc       | 排序规则 ['desc', 'asc']                             |
  | timstamp | int    |   -   | 当前时间减24小时 | 指定时间戳，将获取该时间戳～当前时间戳区间的应援信息 |
  
* 响应

```javascript
{
  code : Number, // 返回状态码
  msg : String,  // "成功"
  data: {
    list: [
      {
        avatar: "https://thirdwx.qlogo.cn/mmopen/vi_32/pvVXOphTg89CIhCAt8eficAgrZkP2WJiaG1UAiaMKo8roTWvGZWF2F9YwEABTiarEjrr6BmX2HtoghDjTyV5iayBDRg/132"
        createAt: 1599547969889
        name: "randle"
        text: "test123123123"
        _id: "65825b355f4e0b2f00a445ab77994799",
      }
    ],
    total: 4, // 总条数，Math.ceil(total/size) 将得到总页数
  }
}
```

#### 新增应援（Morphy中台）

* 描述: 新增应援。
* 云函数: morphy_customForm
* 请求示例：
  
  ```js
  // 小程序端调用
  wx.cloud.callFunction({
    // 需调用的云函数名
    name: 'morphy_customForm',
    // 传给云函数的参数
    data: {
      '$url': 'client/addFormData',
      'tableName': 'support',
      'formData': {
        "text": "阿水S10加油",
      }
    },
    // 成功回调
    complete: console.log
  })
  ```

* 响应

```javascript
{
  code : Number, // 返回状态码
  msg : String,  // "成功"
  data: [
    {
      "createAt": 1599115318034,
      "formData": {text: "阿水s10加油"},
      "formSettingId": "65825b355f4e06d100a41fe66b3afe54",
      "hidden": false,
      "openid": "oK05P5X6Ctv7I6OEOhENhQ-sxbzo",
      "source": "client",
      "_id": "7498b5fe5f50902c00e6d0d908c650c2"
    }
  ]
}
```

### 天梯模块

#### 获取天梯列表

_GET_ `rank/list`

* 描述: 获取天梯列表
* 请求示例：
    | 字段 | 类型 | 必选  | 默认值 | 说明     |
    | :--- | :--- | :---: | :----: | :------- |
    | size | int  |   -   |   20   | 每页数目 |
  

### 现场照片模块

#### 获取现场照片列表（定制化接口，不使用中台方法，用户的openid会影响结果）

_GET_ `picture/list`

* 描述: 获取现场照片列表（定制化接口，不使用中台方法，用户的openid会影响结果），返回用户自己的照片或他人的已过审照片列表
* 请求参数：

  | 字段            | 类型 | 必选  |  默认值  | 说明                                                                                             |
  | :-------------- | :--- | :---: | :------: | :----------------------------------------------------------------------------------------------- |
  | size            | int  |   -   |    10    | 每页数目                                                                                         |
  | lastRequestTime | int  |   -   | 当前时间 | 指定时间戳，将获取该时间戳以前的列表信息，用于下拉刷新，每次使用最后一条数据的createTime进行请求 |

* 响应

```javascript
{
  code : Number, // 返回状态码
  msg : String,  // "成功"
  data: {
    total: 2,
    list: [{
      // "hasIThumbed": false, // 本人是否已点赞
      content: "照片墙照片",
      isLiked: false,
      likeCount: 0,
      mediaInfo: {
        fileUrl: "https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqlQssXviagYhBjiapYVUE3ibGiaia3AKkSDJA0okD5HvXkrHZpaia6uAF8RcTeRh9j2yUK3v0uGgY0VklA/132",
        height: 100,
        width: 100
      },
      createTime: 1599801635134,
      userInfo: {
        avatar: "https://thirdwx.qlogo.cn/mmopen/vi_32/pvVXOphTg89CIhCAt8eficAgrZkP2WJiaG1UAiaMKo8roTWvGZWF2F9YwEABTiarEjrr6BmX2HtoghDjTyV5iayBDRg/132",
        name: "Randle",
        openid: "oK05P5X6Ctv7I6OEOhENhQ-sxbzo"
      },
      _id: "b5416b755f57367c015c9c6427b6fae6"
    }]
  }
}
```

#### 新增照片上墙（morphy中台）

* 描述: 新增照片上墙。
* 请求示例：
  
  ```js
  wx.cloud.callFunction({
    // 需调用的云函数名
    name: 'morphy_userAlbum',
    // 传给云函数的参数
    data: {
      '$url':'client/add',
      'mediaInfo': {
          width: 100, // 照片宽度
          height: 100, // 照片高度
          fileUrl: 'xxxx' // 文件url
      },
      content: 'xxxx', // 玩家发表图册时填写的内容，照片墙文本
    }
  })
  ```

#### 现场照片点赞/取消点赞

* 描述: 给现场的某张照片点赞或取消点赞。
* 请求示例：
  
  ```js
  // 小程序端调用
  wx.cloud.callFunction({
    // 需调用的云函数名
    name: 'morphy_userAlbum',
    // 传给云函数的参数
    data: {
        '$url':'client/setLike',
        '_id': 'xxxx',   // 需要设置点赞状态的相册id
        'status': -1   // 设置的点赞状态，-1为取消点赞，1为点赞
    },
    // 成功回调
    complete: console.log
  })
  ```

### 活动/预约模块

#### 获取活动轮播图（morphy中台）

* 描述: 获取活动轮播图，轮播图内容可在morphy中台 -> 官方图库进行增删改
* 云函数: morphy_officialAlbum
* 请求参数：
  
  | 字段   | 类型   | 必选  | 默认值 | 说明                                                                                                        |
  | :----- | :----- | :---: | :----: | :---------------------------------------------------------------------------------------------------------- |
  | cateId | string |   √   |   -    | 分类id，请先调用[getCate](http://morphy.oa.com/#/appmanager/5f39f134a14f0/docs)获取轮播图id，然后填入cateId |
  
  请求示例

  ```js
  {
    "$url": "get", // get为获取图片列表
    "cateId": "test*********123" // 轮播图类别的id
  }
  ```

* 响应

```js
{
  "cateId": "b5416b755f509b5f010b3cd0243ef1cf",
  "content": "TES",
  "createTime": 1599118784283,
  "creator": "5f39dc7789640",
  "isShow": false,
  "isSticky": false,
  "like": [],
  "mediaInfo": {width: 375, height: 172, fileUrl: "morphy-5f39f13436ab1-1259793757.cos.ap-guangzhou.m…ialAlbum/40846de8-6267-4ba8-a598-7acc7f6a74e9.png"},
  "sortIndex": 1599118784283,
  "tags": [],
  "updateTime": 1599118784283,
  "_id": "8a6c3bf65f509dc000bc708d7260f2e0",
}
```

#### 获取预约的配置信息，票种/活动/人数等

_GET_ `/reserve/config/get`

* 描述: 获取预约的配置信息，票种/活动类型等。
* 请求参数：无

* 响应

```javascript
{
  code : Number, // 返回状态码
  msg : String, // "成功"
  data: {
    "actTypeList": [ // 活动类型
      {id: 0, name: "日常活动"}
    ],
    "ticketTypeList": [ // 票种类型
      {id: 0, name: "通票"}
    ]
  }
}
```

#### 获取所有活动列表（morphy中台接口）

* 描述: 获取所有活动列表（morphy中台接口）
* 请求示例：
  
  ```js
  wx.cloud.callFunction({
    // 需调用的云函数名
    name: 'morphy_customForm',
    // 传给云函数的参数
    data: {
      "$url": "client/getAllFormData",
      "tableName": "activity",
      "page": 1, // 页数
      "size": 10 // 每页数量
    }
  })
  ```

#### 获取某两个日期之间的预约情况列表

_GET_ `/reserve/list`

* 描述: 获取某时间段、某地点的预约情况列表，最多查120天的数据。当某天为过去的日期，或该天的所有时间段人数均已满时，显示不可预约。
* 请求参数：
  
  | 字段            | 类型 | 必选  | 默认值 | 说明                 |
  | :-------------- | :--- | :---: | :----: | :------------------- |
  | startDateString | int  |   √   |   -    | 开始日期(2018-05-20) |
  | endDateString   | int  |   √   |   -    | 结束日期             |

* 响应

```javascript
{
  code : Number, // 返回状态码
  msg : String, // "成功"
  data: {
    "2020-09-01": {
      actType: [{}], // 当天有哪几个活动
      canReserve: true, // 当天是否可预约
      supportTicket: [{}], // 当天支持的票种
    },
    "2020-09-02": {
      actType: [{id, name}], // 当天有哪几个活动
      canReserve: true, // 当天是否可预约
      supportTicket: [{id, name}], // 当天支持的票种
    },
  }
}
```

#### 获取某个日期的详细预约情况列表

_GET_ `/reserve/detail`

* 描述: 获取某个日期的详细预约情况列表。
* 请求参数：
  
  | 字段       | 类型 | 必选  | 默认值 | 说明                   |
  | :--------- | :--- | :---: | :----: | :--------------------- |
  | dateString | int  |   √   |   -    | 日期字符串(2018-05-20) |

* 响应

```javascript
{
  code : Number, // 返回状态码
  msg : String, // "成功"
  data: {
    "list": [
      {
        actType: [{},{}], // 当天有哪几个活动
        canReserve: true, // 当天是否可预约
        supportTicket: [{}, {}], // 当天支持的票种
        isLocked: false, // 该时间段是否锁定，锁定则无法预约，用于活动的清场
        _id: 1598486400000, // 当天8点的时间戳
        text: "2018-08-27", // 当天的日期字符串
        timeline: [ // 当天的N个时间段预约情况
          {
            canReserve: true, // 该时间段是否可预约
            index: 0, // 该时间段的下标
            text: "10:30-12:30" // 该时间段的对应时间文本
          },
          {
            canReserve: false,
            index: 1,
            text: "12:30-14:30"
          }
        ]
      }
    ]
  }
}
```

#### 获取预约配置信息（活动，票种等）

_GET_ `reserve/detail`

* 描述: 获取预约配置信息（活动，票种等）。
* 请求参数：无

* 响应

```javascript
{
  code : Number, // 返回状态码
  msg : String, // "成功"
  data: [
    {
      _id: "ac5f38825f4dc03200c03a970653f4cd",
      text: "10:30-12:30",
      canReserve: false,
      created_at: 1598930994634,
      currentCount: 0,
      endTime: 1598934600000,
      isLocked: false,
      startTime: 159892740000,
      actTypeList: [ // 所有活动类型
        {
          id: 0,
          name: "日常活动"
        },
        {
          id: 1
          name: "PDD现身场馆"
        }
      ], //
      ticketTypeList: [ // 所有票种
        {
          id: 0,
          name: "通票"
        },
        {
          id: 1,
          name: "工作日票"
        },
        {
          id: 2,
          name: "活动票"
        },
      ]
    }
  ]
}
```

## http网页版

### 为用户添加分数

```bash
https://test-eiozq.service.tcloudbase.com/user?action=signGameTest&openid=***&gameTestId=1&score=22&key=kangtest
```
